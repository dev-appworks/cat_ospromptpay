/**
 * 
 */
/**
 * bill4000 –
 */

const bill4000 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	bill4000.initTable = function () {
		var table = $('#bill4000Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/bill4000/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
			},
			"columns": [{
				
				"data": "ba"
			}, {
				"data": "invoiceNo"
			}, {
				"data": "amtOriginal",
				"className": "text-right"
			}, {
				"data": "amtProduct",
				"className": "text-right"
			}, {
				"data": "diff",
				"className": "text-right"
			}

			]
		});
	};

	
	// load on start
	bill4000.initTable();
	
});