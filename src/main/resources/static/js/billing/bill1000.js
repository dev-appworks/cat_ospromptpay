/**
 * 
 */
/**
 * bill1000 –
 */

const bill1000 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	bill1000.initTable = function () {
		var table = $('#bill1000Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/bill1000/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
			},
			"columns": [{
				
				"data": "accountNo",
				"className":"text_center"
			}, {
				"data": "externalId",
				"className": "text-center"
			}, {
				"data": "lastBalance",
				"className": "text-right"
			}, {
				"data": "thisBalance",
				"className": "text-right"
			}, {
				"data": "billAmt",
				"className": "text-right"
			}, {
				"data": "migAmt",
				"className": "text-right"
			}, {
				"data": "payBill",
				"className": "text-right"
			}, {
				"data": "adjAmt",
				"className": "text-right"
			}, {
				"data": "revAdjAmt",
				"className": "text-right"
			}, {
				"data": "revAmt",
				"className": "text-right"
			}, {
				"data": "wrtAmt",
				"className": "text-right"
			}, {
				"data": "calc",
				"className": "text-right"
			}, {
				"data": "diff",
				"className": "text-right"
			}, {
				"data": "postError",
				"className": "text-right"
			}, {
				"data": "lastRegion",
				"className": "text-right"
			}, {
				"data": "thisRegion",
				"className": "text-right"
			}, {
				"data": "hold2unhold",
				"className": "text-right"
			}, {
				"data": "reason",
				"className": "text-right"
			}, {
				"data": "action",
				"className": "text-right"
			}

			]
		});
	};

	
	// load on start
	bill1000.initTable();
	
});