/**
 * 
 */
/**
 * bill5000 –
 */

const bill5000 = {};
$(function () {

	var contextPath = CommonUtils.getContextPath();

	bill5000.initTable = function () {
		var table = $('#bill5000Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/bill5000/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "name"
			}, {
				"data": "billingAccount"
			}, {
				"data": "invoiceNo"
			}, {
				"data": "issueDate"
			}, {
				"data": "dueDate"
			}, {
				"data": "balance"
			}

			]
		});
	};



	// load on start
	bill5000.initTable();
});