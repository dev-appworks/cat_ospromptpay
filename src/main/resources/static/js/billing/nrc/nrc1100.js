/**
 * 
 */
/**
 * nrc1100 –
 */

const nrc1100 = {};
$(function () {

	var contextPath = CommonUtils.getContextPath();

	nrc1100.initTable = function () {
		var table = $('#nrc1100Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/nrc/nrc1100/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
		
			},
			"columns": [{
				"data": "no"
			}, {
				"data": "ba"
			}, {
				"data": "externalId"
			}, {
				"data": "domponentId"
			}, {
				"data": "domponentName"
			}, {
				"data": "domponentActiveDt"
			}, {
				"data": "domponentInactiveDt"
			}, {
				"data": "packagetId"
			}, {
				"data": "packagetName"
			}, {
				"data": "packagetActiveDt"
			}, {
				"data": "packagetInactiveDt"
			}, {
				"data": "packagetPkgInstDt"
			}, {
				"data": "trueActiveDt"
			}, {
				"data": "trueInactiveDt"
			}, {
				"data":"del",
				"className": "text-center",
				"render": function (data, type, row) { return "<input type=\"checkbox\" >"; }
			}, {
				"data": "comment"
			}

			]
		});
	};



	// load on start
	nrc1100.initTable();
});