/**
 * 
 */
/**
 * nrc1300 –
 */

const nrc1300 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	nrc1300.initTable = function () {
		var table = $('#nrc1300Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/nrc/nrc1300/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
		
			},
			"columns": [{
				"data": "ba"
			},{
				"data": "mdn"
			}, {
				"data": "packageId"
			}, {
				"data": "packageName",
			}, {
				"data": "packageActiveDt",
			}, {
				"data": "packageInactiveDt",
			}, {
				"data": "packagePkgInstId"
			}, {
				"data": "componentId"
			}, {
				"data": "componentName"
			}, {
				"data": "componentActiceDt"
			},{
				"data": "componentInactiveDt"
			},{
				"data": "componentCompInstId"
			}

			]
		});
		
	};

	
	// load on start
	nrc1300.initTable();
	
});