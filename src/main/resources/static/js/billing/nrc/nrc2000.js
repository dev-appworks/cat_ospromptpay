/**
 * 
 */
/**
 * nrc2000 –
 */

const nrc2000 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	nrc2000.initTable = function () {
		var table = $('#nrc2000Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/nrc/nrc2000/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
		
			},
			"columns": [{
				"data": "ba"
			},{
				"data": "externalId"
			}, {
				"data": "externalIdType"
			}, {
				"data": "componentId",
			}, {
				"data": "componentActiveDt",
			}, {
				"data": "componentInactiveDt",
			}, {
				"data": "packageId"
			}, {
				"data": "packageInstId"
			}, {
				"data": "overrideName"
			}, {
				"data": "overrideRate"
			},{
				"data": "overrideUnit"
			}

			]
		});
		
	};

	
	// load on start
	nrc2000.initTable();
	
});