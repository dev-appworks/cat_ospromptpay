/**
 * 
 */
/**
 * nrc2100 –
 */

const nrc2100 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	nrc2100.initTable = function () {
		var table = $('#nrc2100Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/nrc/nrc2100/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
		
			},
			"columns": [{
				"data": "no"
			},{
				"data": "ba"
			}, {
				"data": "externalId"
			}, {
				"data": "componentId",
			}, {
				"data": "componentName",
			}, {
				"data": "componentActiveDt",
			}, {
				"data": "componentInactiveDt"
			}, {
				"data": "componentOverrideName"
			}, {
				"data": "componentOverrideRate"
			}, {
				"data": "componentOverrideUnit"
			},{
				"data": "packageId"
			},{
				"data": "packageName"
			},{
				"data": "packageActiveDt"
			},{
				"data": "packageInactiveDt"
			},{
				"data": "packagePkgInstDt"
			},{
				"data": "trueEffectDateActiceDt"
			},{
				"data": "trueEffectDateInactiveDt"
			}, {
				"data":"del",
				"className": "text-center",
				"render": function (data, type, row) { return "<input type=\"checkbox\" >"; }
			},{
				"data": "comment"
			}

			]
		});
		
	};

	
	// load on start
	nrc2100.initTable();
	
});