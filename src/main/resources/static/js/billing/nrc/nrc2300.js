/**
 * 
 */
/**
 * nrc2300 –
 */

const nrc2300 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	nrc2300.initTable = function () {
		var table = $('#nrc2300Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/nrc/nrc2300/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
		
			},
			"columns": [{
				"data": "ba"
			},{
				"data": "mdn"
			}, {
				"data": "packageId"
			}, {
				"data": "packageName",
			}, {
				"data": "packageActiveDt",
			}, {
				"data": "packageInactiveDt",
			}, {
				"data": "packagePkgInstId"
			}, {
				"data": "componentId"
			}, {
				"data": "componentName"
			}, {
				"data": "componentActiveDt"
			},{
				"data": "componentInactiveDt"
			},{
				"data": "componentOverrideName"
			},{
				"data": "componentOverrideRate"
			},{
				"data": "componentOverrideUnit"
			},{
				"data": "componentCompInstId"
			},

			]
		});
		
	};

	
	// load on start
	nrc2300.initTable();
	
});