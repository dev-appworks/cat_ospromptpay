/**
 * 
 */
/**
 * nrc2200 –
 */

const nrc2200 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	nrc2200.initTable = function () {
		var table = $('#nrc2200Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/nrc/nrc2200/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
	
			},
			"columns": [{
				"data": "no"
			},{
				"data": "ba"
			}, {
				"data": "externalId"
			}, {
				"data": "componentId",
			}, {
				"data": "componentName",
			}, {
				"data": "componentOverrideName"
			}, {
				"data": "componentOverrideRate"
			}, {
				"data": "componentOverrideUnit"
			},{
				"data": "packageId"
			},{
				"data": "packageName"
			},{
				"data": "packagePkgInstDt"
			},{
				"data": "effectiveDateActiceDt"
			},{
				"data": "effectiveDateInactiveDt"
			},{
				"data": "runDate"
			},{
				"data": "status",
				"className": "text-center",
				"render": function (data, type, row) {
					return "<span class=\"label label-success\">Success</span>";
				}
			},{
				"data": "rerunNo"
			},{
				"data": "compinstid"
			}

			]
		});
		
	};

	
	// load on start
	nrc2200.initTable();
	
});