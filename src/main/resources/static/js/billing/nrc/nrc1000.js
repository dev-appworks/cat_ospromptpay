/**
 * 
 */
/**
 * nrc1000 –
 */

const nrc1000 = {};
$(function () {

	var contextPath = CommonUtils.getContextPath();

	nrc1000.initTable = function () {
		var table = $('#nrc1000Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/nrc/nrc1000/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
	
		 
			},
			"columns": [{
				"data": "ba"
			}, {
				"data": "externalId"
			}, {
				"data": "externalIdType"
			}, {
				"data": "componentId"
			}, {
				"data": "componentActiveDt"
			}, {
				"data": "componentInactiveDt"
			}, {
				"data": "packageId"
			}, {
				"data": "packageInstId"
			}

			]
		});
	};



	// load on start
	nrc1000.initTable();
});