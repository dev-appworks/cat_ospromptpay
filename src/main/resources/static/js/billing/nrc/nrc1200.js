/**
 * 
 */
/**
 * nrc1200 –
 */

const nrc1200 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	nrc1200.initTable = function () {
		var table = $('#nrc1200Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/nrc/nrc1200/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
		
			},
			"columns": [{
				"data": "no"
			},{
				
				"data": "ba"
			}, {
				"data": "externalId"
			}, {
				"data": "componentId",
				"className": "text-right"
			}, {
				"data": "componentName",
				"className": "text-right"
			}, {
				"data": "packageId",
				"className": "text-right"
			}, {
				"data": "packageName",
				"className": "text-right"
			}, {
				"data": "packagePkgInstId"
			}, {
				"data": "trueEffectiveDateActiceDt"
			}, {
				"data": "trueEffectiveDateInactiveDt"
			}, {
				"data": "runDate"
			},{
				"data": "status",
				"className": "text-center",
				"render": function (data, type, row) {
					return "<span class=\"label label-success\">Success</span>";
				}
			},{
				"data": "rerunNo"
			},{
				"data": "compInstId"
			},

			]
		});
		
	};

	
	// load on start
	nrc1200.initTable();
	
});