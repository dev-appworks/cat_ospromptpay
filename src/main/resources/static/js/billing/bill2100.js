/**
 * 
 */
/**
 * bill2100 –
 */

const bill2100 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	bill2100.initTable = function () {
		var table = $('#bill2100TableOTBOSS_WEBBASE').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/bill2100/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
		
	
		
			},
			"columns": [{
				
				"data": "ba"
			}, {
				"data": "invoiceNo"
			}, {
				"data": "period",
				"className": "text-right"
			}, {
				"data": "amtOriginal",
				"className": "text-right"
			}, {
				"data": "amtProduct",
				"className": "text-right"
			}, {
				"data": "diff",
				"className": "text-right"
			}

			]
		});
		var table = $('#bill2100TableOTBOSS_BATCH').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/bill2100/list",
				"type": "POST",
				"contentType":"application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
						
					}));
				}
		
	
		
			},
			"columns": [{
				
				"data": "ba"
			}, {
				"data": "invoiceNo"
			}, {
				"data": "period",
				"className": "text-right"
			}, {
				"data": "amtOriginal",
				"className": "text-right"
			}, {
				"data": "amtProduct",
				"className": "text-right"
			}, {
				"data": "diff",
				"className": "text-right"
			}

			]
		});
	};

	
	// load on start
	bill2100.initTable();
	
});