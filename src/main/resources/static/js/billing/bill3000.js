/**
 * 
 */
/**
 * bill3000 –
 */

const bill3000 = {};
$(function () {

	var contextPath = CommonUtils.getContextPath();

	bill3000.initTable = function () {
		var table = $('#bill3000Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/bill3000/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "createdDate"
			}, {
				"data": "createdBy"
			}, {
				"data": "result",
			}, {
				"data": "action",
			}

			]
		});
	};



	// load on start
	bill3000.initTable();
});