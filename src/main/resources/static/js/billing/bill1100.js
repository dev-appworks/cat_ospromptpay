/**
 * 
 */
/**
 * bill1100 –
 */

const bill1100 = {};
$(function () {

	var contextPath = CommonUtils.getContextPath();

	bill1100.initTable = function () {
		var table = $('#bill1100Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/bill1100/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "ba"
			}, {
				"data": "invoiceNo"
			}, {
				"data": "amtOriginal",
				"className": "text-right"
			}, {
				"data": "amtProduct",
				"className": "text-right"
			}, {
				"data": "diff",
				"className": "text-right"
			}

			]
		});
	};



	// load on start
	bill1100.initTable();
});