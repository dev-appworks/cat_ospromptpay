/**
 * 
 */
/**
 * bill3100 –
 */

const bill3100 = {};
$(function () {

	var contextPath = CommonUtils.getContextPath();

	bill3100.initTable = function () {
		var table = $('#bill3100Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bill/bill3100/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data":"contractNo",
				"render": function (data, type, row) {
					return "<input type=\"checkbox\">";
				}
			},{
				"data": "contractNo",
				 
			}, {
				"data": "arRef"
			}, {
				"data": "invoiceAmt"
			}, {
				"data": "balanceAmt"
			}, {
				"data": "productCode"
				
			}, {
				"data": "subProductCode"
			}, {
				"data": "revenueTypeCode"
			}, {
				"data": "advanceAmt"
			}, {
				"data": "revenueAmt"
			}, {
				"data": "lgAmount"
			}, {
				"data": "netAdvAmt"
			}, {
				"data": "netAdvAmt",
				"render": function (data, type, row) {
					return "<input type=\"submit\" value=\"แก้ไข\" class=\"btn btn-success btn-sm\">";
				}
			}

			]
		});
	};



	// load on start
	bill3100.initTable();
});
