/**
 * promptpayRefCodeNew –
 */

const PP1003 = {};
$(function () {

	PP1003.initDatepicker = function () {

		var ctxPath = CommonUtils.getContextPath();

		$("#inputDateFrom").datetimepicker({
			format: 'DD/MM/YYYY HH:mm:ss',
//			stepping: 5
		});

		$("#inputDateTo").datetimepicker({
			format: 'DD/MM/YYYY HH:mm:ss',
//			stepping: 5
		});
	};
	
	PP1003.onChangeDateFrom = function () {
		$('#inputDateTo').data("DateTimePicker").minDate($('#inputDateFrom').val());
		
	}

	PP1003.onChangeDateTo = function () {
		$('#inputDateFrom').data("DateTimePicker").maxDate($('#inputDateTo').val());
	}

	PP1003.initDatepicker();
	PP1003.onChangeDateFrom();
	PP1003.onChangeDateTo();
	
	PP1003.convertDate = function (ms) {
		if (!ms)
			return "-";

		var date = new Date(ms), year = date.getFullYear(), month = (date
			.getMonth() + 1).toString(), formatedMonth = (month.length === 1) ? ("0" + month)
				: month, day = date.getDate().toString(), formatedDay = (day.length === 1) ? ("0" + day)
					: day, hour = date.getHours().toString(), formatedHour = (hour.length === 1) ? ("0" + hour)
						: hour, minute = date.getMinutes().toString(), formatedMinute = (minute.length === 1) ? ("0" + minute)
							: minute, second = date.getSeconds().toString(), formatedSecond = (second.length === 1) ? ("0" + second)
								: second;
		return formatedDay + "/" + formatedMonth + "/" + year + "  "
			+ formatedHour + ':' + formatedMinute;
	};
	
	var contextPath = CommonUtils.getContextPath();
	
	PP1003.onClickSave = function () {
			
		if ($.trim($("#sharedKey").val()) == "") {
			bootbox.alert("กรุณาระบุ Shared key");
			return false;
		}

		if ($.trim($("#inputDateFrom").val()) == "" && $.trim($("#inputDateTo").val()) == "") {
			bootbox.alert("กรุณาระบุ วันที่เริ่มต้น และ วันที่สิ้นสุด");
			return false;
		}
		
		if ($.trim($("#status").val()) == "") {
			bootbox.alert("กรุณาระบุ สถานะ");
			return false;
		}

//		var edit = $("input[type='hidden'][name='id']").val();
//		if ($.trim( edit ) == "" && $.trim($("#currentStatus").val()) == "") {
//			bootbox.alert("กรุณาระบุ สถานะเปิด/ปิด");
//			return false;
//		}

		CommonUtils.confirm(function (result) {
			if (result) {
				$("#refcodeForm").submit();
			}
		});

	};

});