const Omudb1001 = {};
$(function () {

    var contextPath = CommonUtils.getContextPath();
    var CAN_EDIT = $("#statusMappingKeys").val();

    Omudb1001.initTable = function () {
        Omudb1001.orderTableTable = $('#orderTable').DataTable({
            "searching": false,
            "processing": true,
            "order": [[1, 'asc']],
            "serverSide": true,
            "ajax": {
                "url": contextPath + "omudb/omudb1001/orderbyTask",
                "type": "POST", 
                "contentType": "application/json",
                "data": function (d) {
                    return JSON.stringify($.extend({}, d, {
                        "orderId": $('#orderId').val(),
                    }));
                }
            },
            "columns": [
                {
                    "data": "orderId",
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "orderable": false
                },
                { "data": "orderId" },
                { "data": "serviceItemId" },
                {
                    "data": "statusDisplay",
                    "className": "text-center",
                },
                {
                    "data": "statusCode",
                    "render": function (data, type, row, meta) {
                        if (CAN_EDIT.indexOf(data) == -1) {
                            return '';
                        }

                        var btn = "";
                        btn = `<button class="btn btn-inline btn-primary btn-sm ladda-button order-task-action" data-style="expand-right" data-size="s">
                             <span class="ladda-label">ข้ามสถานะ</span>
                        </button>`;

                        return btn;
                    },
                    "orderable": false
                },

            ]
        });

        Omudb1001.orderTableTable.on('click', 'tbody tr button.order-task-action', function () {
            var closestRow = $(this).closest('tr');
            var data = Omudb1001.orderTableTable.row(closestRow).data();
            var url = CommonUtils.getContextPath() + "omudb/omudb1002/edit?orderId=" + data.orderId
                + "&statusCode=" + data.statusCode
                + "&serviceItemId=" + data.serviceItemId;

            window.location.href = url;
        });

    }

    Omudb1001.onClickSearch = function () {
        Omudb1001.orderTableTable.ajax.reload();
    };

    Omudb1001.onClickClearData = function () {
        $('#orderId').val('');
        Omudb1001.orderTableTable.ajax.reload();
    };
    
    Omudb1001.onsearchSubmit = function () {
    	Omudb1001.onClickSearch();
    	return false;
    };
   
    //on init
    Omudb1001.initTable();

});