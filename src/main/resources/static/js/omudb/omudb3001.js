const OMUDB3001 = {};
$(function () {
    var contextPath = CommonUtils.getContextPath();



    OMUDB3001.initTable = function () {

        OMUDB3001.table = $('#userTable').DataTable({
            "searching": false,
            "order": [[1, 'asc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": contextPath + "omudb/usermgr/omudb3001/userList",
                "type": "POST",
                "contentType": "application/json",
                "data": function (d) {
                    return JSON.stringify($.extend({}, d, {
                        "firstName": $("#firstName").val(),
                        "lastName": $("#lastName").val(),
                        "empId": $("#empId").val(),
                        "userName": $("#userName").val(),
                    }));
                }
            },
            "columns": [
                {
                    "data": "agentId",
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "orderable": false
                },
                { "data": "firstName" },
                { "data": "lastName" },
                { "data": "empId" },
                { "data": "agentName" },
                {
                    "data": "agentId",
                    "render": function (data, type, row, meta) {
                        var btn = `<button class="btn btn-inline btn-primary btn-sm ladda-button user-edit-action" data-style="expand-right" data-size="s">
                                        <span class="ladda-label">แก้ไข</span>
                                    </button>`;
                        return btn;
                    },
                    "orderable": false
                },
            ]
        });


        OMUDB3001.table.on('click', 'tbody tr button.user-edit-action', function () {
            var closestRow = $(this).closest('tr');
            var data = OMUDB3001.table.row(closestRow).data();
            var url = CommonUtils.getContextPath() + "omudb/usermgr/omudb3002/edit?agentId=" + data.agentId;
            window.location.href = url;
        });

    };

    OMUDB3001.onClickSearch = function () {
        OMUDB3001.table.ajax.reload();
    };

    OMUDB3001.onClickClearData = function () {
        $("#userForm")[0].reset()
        OMUDB3001.table.ajax.reload();
    };

    OMUDB3001.onexportcsv = function () {
        var url = CommonUtils.getContextPath() + "omudb/usermgr/omudb3001/userList/export";
        window.open(url);
    }
   
    // construc
    OMUDB3001.initTable();


});
