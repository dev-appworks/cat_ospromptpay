const omudb3002 = {};
$(function () {

    var contextPath = CommonUtils.getContextPath();
    var prefix = "กรุณา ระบุ ";

    omudb3002.onClickSubmit = function () {

        if (!omudb3002.validFrom()) {
            return false;
        }

        CommonUtils.confirm(function (result) {

            if (result) {
                CommonUtils.blockUi();
                $("#actionType").val('SUBMIT');
                $("#mainForm").submit();
            }
        });

    };

    omudb3002.onClickAddGroup = function () {



        if (CommonUtils.isBlank("currentAddGroup")) {
            CommonUtils.alert(prefix + " สิทธิ์ ");
            return false;
        }

        //check dup
        var addGroups = $("input[type='hidden'][name^='groupAdd']");
        var cag = $("#currentAddGroup").val();
        for (var i = 0; i < addGroups.length; i++) {
            if (cag == $(addGroups[i]).val()) {
                CommonUtils.alert(cag + " : มีแล้วในระบบ ");
                return false;
            }
        }

        // CommonUtils.confirm(function (result) {
        //     if (result) {
        CommonUtils.blockUi();
        $("#actionType").val('ADD_GROUP');
        $("#mainForm").submit();
        //     }
        // });

    };

    omudb3002.onClickDelGroup = function (object) {
        var hiddenTag = $(object).parent().find("input[type='hidden']").val();

        CommonUtils.confirm(function (result) {

            if (result) {
                CommonUtils.blockUi();
                $("#currentDelGroup").val(hiddenTag);
                $("#actionType").val('DEL_GROUP');
                $("#mainForm").submit();
            }
        });

    };

    omudb3002.onClickBack = function () {
        var url = CommonUtils.getContextPath() + "omudb/usermgr/omudb3001/init";
        window.location.href = url;
    }

    omudb3002.validFrom = function () {
        var isNew = $("#isNew").val();
        var isValid = true;


        if (CommonUtils.isBlank("firstName")) {
            CommonUtils.alert(prefix + " ชื่อ ");
            isValid = false;
        } else
            if (CommonUtils.isBlank("lastName")) {
                CommonUtils.alert(prefix + " สกุล ");
                isValid = false;
            } else
                if (CommonUtils.isBlank("empId")) {
                    CommonUtils.alert(prefix + " รหัสพนักงาน / รหัสบัตรประชาชน ");
                    isValid = false;
                } else
                    if (CommonUtils.isBlank("userName")) {
                        CommonUtils.alert(prefix + " Username ");
                        isValid = false;
                    } else
                        if (CommonUtils.isBlank("territoryAgent")) {
                            CommonUtils.alert(prefix + " ส่วน ");
                            isValid = false;
                        }

        if (isNew == 'true') {
            if (CommonUtils.isBlank("password")) {
                CommonUtils.alert(prefix + " Password ");
                isValid = false;
            }
        }

        return isValid;
    };

});