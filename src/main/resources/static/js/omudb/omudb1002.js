const Omudb1002 = {};
$(function() {

	var contextPath = CommonUtils.getContextPath();

	Omudb1002.onClickSubmit = function() {

		if ($("#changeStatusCode").val() == "") {
			CommonUtils.alert("กรุณาระบุสถานะ");
		} else {

			CommonUtils.confirm(function(result) {

				if (result) {
					CommonUtils.blockUi();
					$("#mainForm").submit();
				}
			});

		};
	}
	Omudb1002.onClickBack = function() {
		var url = CommonUtils.getContextPath() + "omudb/omudb1001/init";
		window.location.href = url;
	}

});