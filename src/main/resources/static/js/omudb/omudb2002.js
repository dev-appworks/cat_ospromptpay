const Omudb2002 = {};
$(function () {

    var contextPath = CommonUtils.getContextPath();

    Omudb2002.onClickSubmit = function () {

        var cuurentCode = $.trim($("#changeStatusCode").val());
        if("" == cuurentCode){
            CommonUtils.alert("กรุณาเลือก สถานะ .");
            return false;
        }

        CommonUtils.confirm(function (result) {

            if (result) {
                CommonUtils.blockUi();
                $("#mainForm").submit();
            }
        });

    };

    Omudb2002.onClicBack = function () {
        window.location.href = contextPath + "omudb/omudb2001/init";
    };

});