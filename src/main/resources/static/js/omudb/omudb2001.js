const Omudb2001 = {};
$(function () {

    var contextPath = CommonUtils.getContextPath();
    var CAN_EDIT = $("#statusMappingKeys").val();
    

    Omudb2001.initTable = function () {
        Omudb2001.orderTableTable = $('#orderTable').DataTable({
            "searching": false,
            "processing": true,
            "order": [[1, 'asc']],
            "serverSide": true,
            "ajax": {
                "url": contextPath + "omudb/omudb1001/orderbyServiceItem",
                "type": "POST",
                "contentType": "application/json",
                "data": function (d) {
                    return JSON.stringify($.extend({}, d, {
                        "serviceItemId": $('#serviceItemId').val(),
                    }));
                }
            },
            "columns": [
                {
                    "data": "orderId",
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "orderable": false
                },
                { "data": "orderId" },
                { "data": "serviceItemId" },
                {
                    "data": "statusDisplay",
                    "className": "text-center",
                },
                {
                    "data": "statusCode",
                    "render": function (data, type, row, meta) {
                        if (CAN_EDIT.indexOf(data) == -1) {
                            return '';
                        }

                        var btn = "";
                        btn = `<button class="btn btn-inline btn-primary btn-sm ladda-button order-task-action" data-style="expand-right" data-size="s">
                             <span class="ladda-label">ข้ามสถานะ</span>
                        </button>`;

                        return btn;
                    },
                    "orderable": false
                },

            ]
        });

        Omudb2001.orderTableTable.on('click', 'tbody tr button.order-task-action', function () {
            var closestRow = $(this).closest('tr');
            var data = Omudb2001.orderTableTable.row(closestRow).data();
            var url = CommonUtils.getContextPath() + "omudb/omudb2002/edit?orderId=" + data.orderId
                + "&statusCode=" + data.statusCode
                + "&serviceItemId=" + data.serviceItemId;

            window.location.href = url;
        });

    };

    Omudb2001.onClickSearch = function () {
        Omudb2001.orderTableTable.ajax.reload();
    };

    Omudb2001.onClickClearData = function () {
        $('#serviceItemId').val('');
        Omudb2001.orderTableTable.ajax.reload();
    };


    Omudb2001.searchFormSubmit = function () {
        Omudb2001.onClickSearch();
        return false;
    };

    //on init
    Omudb2001.initTable();

});