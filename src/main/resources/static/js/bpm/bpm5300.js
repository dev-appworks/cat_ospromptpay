/**
 * 
 */
/**
 * bpm5300 
 */

const bpm5300 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	bpm5300.initTable = function () {
		var table = $('#bpm5300Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm5300/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
	
			},
			"columns": [{
				"data": "transactionId"
			}, {
				"data": "processId"
			}, {
				"data": "processType"
			}, {
				"data": "processName"
			}, {
				"data": "createdBy"
			}, {
				"data": "status"
			}, {
				"data": "createdDate"
			}, {
				"data": "logReport"
			}

			]
		});
		var table = $('#bpm5300MarkCDRsTable').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm5300/MarkCDRs",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
	
			},
			"columns": [
			{
				"data": "externalId"
			}, {
				"data": "type"
			}, {
				"data": "miuErrorCode"
			}, {
				"data": "count"
			}, {
				"data": "sum"
			}, {
				"data": "minTransDate"
			}, {
				"data": "maxTransDate"
			}, {
				"data": "extTrackingId"
			}, {
				"data": "remark"
			}, {
				"data": "attachedFile"
			}

			]
		});
	};


	
	// load on start
	bpm5300.initTable();
	
});