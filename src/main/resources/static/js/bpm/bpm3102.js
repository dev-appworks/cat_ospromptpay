/**
 * 
 */
/**
 * bpm3102 –
 */


const bpm3102 = {};
$(function () {

	
	var contextPath = CommonUtils.getContextPath();

	bpm3102.initTable = function () {
		var table = $('#bpm3102Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm3102/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "billingAccount"
			}, {
				"data": "guiding"
			}, {
				"data": "type"
			}, {
				"data": "accountNo"
			},{
				"data": "numberOfBill"
			},{
				"data": "pointOrigin"
			},{
				"data": "monthly"
			},{
				"data": "records"
			},{
				"data": "charge"
			},{
				"data": "secs"
			}, {
				"data": "minTransDate"
			},{
				"data": "maxTransDate"
			}

			]
		});
	};

	
	// load on start
	bpm3102.initTable();
});