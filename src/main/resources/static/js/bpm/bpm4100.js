/**
 * 
 */
/**
 * bpm4100 –
 */


const bpm4100 = {};
$(function () {
		
	var contextPath = CommonUtils.getContextPath();

	bpm4100.initTable = function () {
		var table = $('#bpm4100Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm4100/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{"data":"type",
				"render": function (data, type, row) {
					return "<input type=\"checkbox\">";
				}
			},{				
				"data": "parentAcountNo"
			}, {
				"data": "chargeDate"
			}, {
				"data": "subscriberNo"
			}, {
				"data": "serviceActiveDate"
			},{
				"data": "serviceInactiveDate"
			},{
				"data": "chargeStatus"
			}

			]
		});
		
		
	};

	
	// load on start
	bpm4100.initTable();
});