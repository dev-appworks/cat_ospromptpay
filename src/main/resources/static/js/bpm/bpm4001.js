/*
 bpm4001
 
 */

const bpm4001 = {};
$(function () {

	var contextPath = CommonUtils.getContextPath();

	bpm4001.initTable = function () {
		var table = $('#bpm4001Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm4001/ServiceType",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
		
		
			},
			"columns": [{"data":"type",
				"render": function (data, type, row) {
					return "<input type=\"checkbox\">";
				}
			},{
				"data": "ba"
					
			}, {
				"data": "externalIdType"
					
			}, {
				"data": "subscriberNo"
			}
			, {
				"data": "serviceDate"
			}
			, {
				"data": "serviceEndDate"
			}
			, {
				"data": "checkServiceType"
			}
			, {
				"data": "checkServiceDate"
			}
			, {
				"data": "checkNocus"
			}
			, {
				"data": "checkSession"
			}
		
			]
		});
	};


	
	// load on start
	bpm4001.initTable();

});

