/**
 * 
 */
/**
 * bpm2001 –
 */


const bpm2001 = {};
$(function () {
		
	var contextPath = CommonUtils.getContextPath();

	bpm2001.initTable = function () {
		var table = $('#bpm2001Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2001/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{				
				"data": "phoneNumber"
			}, {
				"data": "defaultBa"
			}, {
				"data": "group"
			}, {
				"data": "bill"
			},{
				"data": "invoiceNo"
			},{
				"data": "number"
			},{
				"data": "priceVat"
			},{
				"data": "newBa"
			},{
				"data": "newGroup"
			},{
				"data": "createdDate"
			},{
				"data":"totalBill",
				"className": "text-center",
				"render": function (data, type, row) {
					return "<input type=\"checkbox\">";
				},
				
			},{
				"data":"totalBill",
				"className": "text-center",
				"render": function (data, type, row) {
					return "<button class=\"btn btn-success\">แก้ไข</button> " +
							"<button class=\"btn btn-success\">ลบ</button>";
				},
				
			},

			]
		});
		
		var table = $('#bpm2001Table2').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2001/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{				
				"data": "phoneNumber"
			}, {
				"data": "defaultBa"
			}, {
				"data": "group"
			}, {
				"data": "bill"
			},{
				"data": "invoiceNo"
			},{
				"data": "number"
			},{
				"data": "priceVat"
			},{
				"data": "newBa"
			},{
				"data": "newGroup"
			},{
				"data": "createdDate"
			},{
				"data":"totalBill",
				"className": "text-center",
				"render": function (data, type, row) {
					return "<input type=\"checkbox\">";
				},
				
			},{
				"data":"totalBill",
				"className": "text-center",
				"render": function (data, type, row) {
					return "<button class=\"btn btn-success\">แก้ไข</button> " +
							"<button class=\"btn btn-success\">ลบ</button>";
				},
				
			},

			]
		});
		
		
	};

	
	// load on start
	bpm2001.initTable();
});