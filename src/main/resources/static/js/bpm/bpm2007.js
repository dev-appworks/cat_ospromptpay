/**
 * 
 */
/**
 * bpm2007 –
 */


const bpm2007 = {};
$(function () {
		
	var contextPath = CommonUtils.getContextPath();

	bpm2007.initTable = function () {
		var table = $('#bpm2007Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2007/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "ba"
			}, {
				"data": "idType"
			}, {
				"data": "accountNo"
			}, {
				"data": "noBill"
			},{
				"data": "pointOrigin"
			},{
				"data": "record"
			},{
				"data": "charge"
			},{
				"data": "secs"
			},{
				"data": "minTransDate"
			},{
				"data": "maxTransDate"
			},{
				"data": "bperiod"
			},{
				"data": "remark"
			}, {
				"data": "billType"
			}, {
				"data": "logReport",
				"className":"text-center",
				"render": function (data, type, row) {
					return "<input type=\"submit\" value=\"ปรับวันที่ย้อนหลัง\" class=\"btn btn-success btn-sm text-center\"> "				
				}
			}

			]
		});
		
	};

	
	// load on start
	bpm2007.initTable();
});