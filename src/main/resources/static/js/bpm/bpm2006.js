/**
 * 
 */
/**
 * bpm2006 –
 */


const bpm2006 = {};
$(function () {
		
	var contextPath = CommonUtils.getContextPath();

	bpm2006.initTable = function () {
		var table = $('#bpm2006Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2006/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "externalId"
			}, {
				"data": "externalIdType"
			}, {
				"data": "accountNo"
			}, {
				"data": "noBill"
			},{
				"data": "pointOrigin"
			},{
				"data": "monthly"
			},{
				"data": "record"
			},{
				"data": "charge"
			},{
				"data": "secs"
			},{
				"data": "minTransDate"
			},{
				"data": "maxTransDate"
			},  {
				"data": "logReport",
				"className":"text-center",
				"render": function (data, type, row) {
					return "<input type=\"submit\" value=\"ตรวจสอบ Suspend\" class=\"btn btn-success btn-sm text-center\"> " +
							"<input type=\"submit\" value=\"Mark CDRs\" class=\"btn btn-success btn-sm text-center\">";
				}
			}

			]
		});
		
	};

	
	// load on start
	bpm2006.initTable();
});