/**
 * 
 */
/**
 * bpm4200 –
 */


const bpm4200 = {};
$(function () {
		
	var contextPath = CommonUtils.getContextPath();

	bpm4200.initTable = function () {
		var table = $('#bpm4200Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm4200/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "transactionId"
			}, {
				"data": "processId"
			}, {
				"data": "processType"
			}, {
				"data": "processName"
			},{
				"data": "createBy"
			},{
				"data": "status"
			},{
				"data": "createdDate"
			},{
				"data": "logReport"
			}

			]
		});
		
		var table = $('#bpm4200MarkTable').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm4200/listMark",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "externalId"
			}, {
				"data": "type"
			}, {
				"data": "miuErrorCode"
			}, {
				"data": "count"
			},{
				"data": "sum"
			},{
				"data": "minTransDate"
			},{
				"data": "maxTransDate"
			},

			]
		});
		
		
	};
	
	// load on start
	bpm4200.initTable();
});