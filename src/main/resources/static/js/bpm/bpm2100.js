/**
 * 
 */
/**
 * bpm2100 
 */

const bpm2100 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	bpm2100.initTable = function () {
		var table = $('#bpm2100Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2100/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
	
			},
			"columns": [{
				"data": "transactionId"
			}, {
				"data": "processId"
			}, {
				"data": "processType"
			}, {
				"data": "processName"
			}, {
				"data": "createdBy"
			}, {
				"data": "status"
			}, {
				"data": "createdDate"
			}, {
				"data": "logReport"
			}

			]
		});
		var table = $('#bpm2100T123Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2100/T123",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}

		
			},
			"columns": [
			{
				"data": "phoneNo"
			}, {
				"data": "defaultBa"
			}, {
				"data": "group"
			}, {
				"data": "bill"
			}, {
				"data": "invoiceNo"
			}, {
				"data": "number"
			}, {
				"data": "vatAmountFirst"
			}, {
				"data": "newBa"
			}, {
				"data": "newGroup"
			}, {
				"data": "createdDate"
			}

			]
		});
	};


	
	// load on start
	bpm2100.initTable();
	
});