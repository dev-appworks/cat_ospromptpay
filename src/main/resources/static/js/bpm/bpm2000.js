/**
 * 
 */
/**
 * bpm2000 –
 */

const bpm2000 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();
	
	bpm2000.initTable = function () {
		var table = $('#bpm2000Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2000/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "description"
			}, {
				"data": "createdBy"
			}, {
				"data": "createDate"
			}, {
				"data": "state"
			}, {
				"data": "state",
				"render": function (data, type, row) {
					return "<input type=\"submit\" value=\"รายการโอนหนี้\" class=\"btn btn-success btn-sm\">";
				}
			}

			]
		});
	};

		
	
	// load on start
	bpm2000.initTable();
	
});