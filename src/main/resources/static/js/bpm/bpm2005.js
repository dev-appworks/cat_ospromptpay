/**
 * 
 */
/**
 * bpm2005 –
 */


const bpm2005 = {};
$(function () {

	
	var contextPath = CommonUtils.getContextPath();

	bpm2005.initTable = function () {
		var table = $('#bpm2005Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2005/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "transactionId"
			}, {
				"data": "processId"
			}, {
				"data": "processType"
			}, {
				"data": "processName"
			},{
				"data": "createdBy"
			},{
				"data": "status"
			},{
				"data": "createdDate"
			},{
				"data": "logReport"
			}

			]
		});
		
		var tableT = $('#bpm2005TTable').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2005/listT",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			
			"columns": [{
				"data": "phoneNo"
			}, {
				"data": "defaultBa"
			}, {
				"data": "group"
			}, {
				"data": "bill"
			},{
				"data": "invoiceNo"
			},{
				"data": "number"
			},{
				"data": "priceVat"
			},{
				"data": "newBa"
			},{
				"data": "newgroup"
			},{
				"data": "createdDate"
			}

			]
		});
	};

	
	// load on start
	bpm2005.initTable();
});