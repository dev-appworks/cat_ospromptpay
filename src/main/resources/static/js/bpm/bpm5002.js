/**
 * 
 */
/**
 * bpm5002 –
 */


const bpm5002 = {};
$(function () {
		
	var contextPath = CommonUtils.getContextPath();

	bpm5002.initTable = function () {
		var table = $('#bpm5002Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm5002/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
		
			"columns": [{
				"data": "transactionId"
			}, {
				"data": "processId"
			}, {
				"data": "processType"
			}, {
				"data": "processName"
			},{
				"data": "createdBy"
			},{
				"data": "status"
			},{
				"data": "createdDate"
			},{
				"data": "logReport"
			}

			]
		});
		
		var tableMark = $('#bpm5002MarkTable').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm5002/listMark",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			
			"columns": [{
				"data": "externalId"
			}, {
				"data": "type"
			}, {
				"data": "miuErrorCode"
			}, {
				"data": "count"
			},{
				"data": "sum"
			},{
				"data": "minTransDate"
			},{
				"data": "maxTransDate"
			},{
				"data": "extTrackingId"
			},{
				"data": "remark"
			},{
				"data": "attachedFile"
			},

			]
		});
		
		
	};
	
	// load on start
	bpm5002.initTable();
});