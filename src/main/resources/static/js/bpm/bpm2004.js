/**
 * 
 */
/**
 * bpm2004 –
 */


const bpm2004 = {};
$(function () {
		
	var contextPath = CommonUtils.getContextPath();

	bpm2004.initTable = function () {
		var table = $('#bpm2004Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm2004/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "billRefNo"
			}, {
				"data": "accountNo"
			}, {
				"data": "ba"
			}, {
				"data": "noBill"
			},{
				"data": "pointOrigin"
			},{
				"data": "monthly"
			},{
				"data": "record"
			},{
				"data": "charge"
			},{
				"data": "secs"
			},{
				"data": "minTransDate"
			},{
				"data": "maxTransDate"
			}

			]
		});
		
	};

	
	// load on start
	bpm2004.initTable();
});