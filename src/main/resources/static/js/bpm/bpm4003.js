/**
 * 
 */
/**
 * bpm4003 –
 */

const bpm4003 = {};
$(function () {


	var contextPath = CommonUtils.getContextPath();

	bpm4003.initTable = function () {
		var table = $('#bpm4003Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm4003/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}


			},
			"columns": [{
				"data": "transactionId"
			}, {
				"data": "processId"
			}, {
				"data": "processType"
			}, {
				"data": "processName"
			}, {
				"data": "createdBy"
			}, {
				"data": "status"
			}, {
				"data": "createdDate"
			}, {
				"data": "logReport"
			}

			]
		});
		var table = $('#bpm4003FixCaseTable').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm4003/FixCase",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
		
			},
			"columns": [{
				"data": "externalId"
			}, {
				"data": "type"
			}, {
				"data": "miuErrorCode"
			}, {
				"data": "count"
			}, {
				"data": "sum"
			}, {
				"data": "minTransDate"
			}, {
				"data": "maxTransDate"
			}

			]
		});
	};


	
	// load on start
	bpm4003.initTable();
	
});