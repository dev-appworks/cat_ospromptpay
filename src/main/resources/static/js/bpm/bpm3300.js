/**
 * 
 */
/**
 * bpm3300 –
 */


const bpm3300 = {};
$(function () {
		
	var contextPath = CommonUtils.getContextPath();

	bpm3300.initTable = function () {
		var table = $('#bpm3300Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm3300/list",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "transactionId"
			}, {
				"data": "processId"
			}, {
				"data": "processType"
			}, {
				"data": "processName"
			},{
				"data": "createdBy"
			},{
				"data": "state"
			},{
				"data": "createdDate"
			},{
				"data": "logReport"
			}

			]
		});
		
		var tableRab = $('#bpm3300RapTable').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm3300/listRap",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}
			},
			"columns": [{
				"data": "billingAccount"
			}, {
				"data": "guilding"
			}, {
				"data": "type"
			}, {
				"data": "accountNo"
			},{
				"data": "numberOfBill"
			},{
				"data": "pointOrigin"
			},{
				"data": "monthly"
			},{
				"data": "records"
			},{
				"data": "charge"
			},{
				"data": "secs"
			}, {
				"data": "minTransDate"
			},{
				"data": "maxTransDate"
			}
			]
		});		
	};

	
	// load on start
	bpm3300.initTable();
});