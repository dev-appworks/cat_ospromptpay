/*
 bpm4002
 
 */

const bpm4002 = {};
$(function () {

	var contextPath = CommonUtils.getContextPath();

	bpm4002.initTable = function () {
		var table = $('#bpm4002Table').DataTable({
			"searching": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": contextPath + "bpm/bpm4002/ServiceSession",
				"type": "POST",
				"contentType": "application/json",
				"data": function (d) {
					return JSON.stringify($.extend({}, d, {
					}));
				}

		
			},
			"columns": [
			{
				"data": "externalId"
			}, {
				"data": "miuError"
			}, {
				"data": "nRow"
			}, {
				"data": "sMin"
			}, {
				"data": "sMax"
			}, {
				"data": "displayValue"
			}, {
				"data": "activeDate"
			}, {
				"data": "inactiveDate"
			}, {
				"data": "changeDate"
			}, {
				"data": "usageStatus"
			}
		
			]
		});
	};


	
	// load on start
	bpm4002.initTable();

});

