CREATE TABLE `ibacss_payment_data` (
  `payment_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `src_filename` varchar(50) NOT NULL,
  `tx_date` date NOT NULL,
  `filename` varchar(30) NOT NULL,
  `num_of_records` varchar(11) DEFAULT NULL,
  `e_auction_1000` varchar(11) DEFAULT NULL,
  `e_auction_1200` varchar(11) DEFAULT NULL,
  `e_auction_1400` varchar(11) DEFAULT NULL,
  `e_auction_1600` varchar(11) DEFAULT NULL,
  `e_auction_1800` varchar(11) DEFAULT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `created_by` varchar(30) NOT NULL DEFAULT 'BATCH',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ibacss_payment_data_account_pay` (
  `account_pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_date` date NOT NULL,
  `pay_mode` varchar(30) DEFAULT NULL,
  `desc` varchar(64) DEFAULT NULL,
  `num_of_records` varchar(11) DEFAULT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `created_by` varchar(30) NOT NULL DEFAULT 'BATCH',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ibacss_payment_data_account_new_inv` (
  `account_new_inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_date` date NOT NULL,
  `bill_order_no` varchar(30) DEFAULT NULL,
  `amount` varchar(15) DEFAULT NULL,
  `tax` varchar(14) DEFAULT NULL,
  `num_of_records` varchar(11) DEFAULT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `created_by` varchar(30) NOT NULL DEFAULT 'BATCH',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_new_inv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ibacss_payment_data_tax_inv_summary_wt` (
  `tax_inv_summary_wt_id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_date` date NOT NULL,
  `bill_order_number` varchar(30) DEFAULT NULL,
  `rental` varchar(15) DEFAULT NULL,
  `usage` varchar(15) DEFAULT NULL,
  `total_amount_ex_vat` varchar(15) DEFAULT NULL,
  `vat_amount` varchar(15) DEFAULT NULL,
  `rental_wt` varchar(15) DEFAULT NULL,
  `usage_wt` varchar(15) DEFAULT NULL,
  `num_of_records` varchar(11) DEFAULT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `created_by` varchar(30) NOT NULL DEFAULT 'BATCH',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tax_inv_summary_wt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
