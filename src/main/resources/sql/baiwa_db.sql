CREATE TABLE `adm_operation` (
  `operation_id` int(11) NOT NULL AUTO_INCREMENT,
  `operation_code` varchar(20) NOT NULL,
  `operation_desc` varchar(200) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `version` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`operation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `adm_operation` VALUES (1,'OP_P1','',NULL,'N',1,'SYSTEM','2016-06-06 21:02:05',NULL,NULL),(2,'OP_P2','',NULL,'N',1,'SYSTEM','2016-06-06 21:02:05',NULL,NULL);

CREATE TABLE `adm_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_code` varchar(20) NOT NULL,
  `role_desc` varchar(200) NOT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `version` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `adm_role` VALUES (1,'ROLE_USER','Normal User','N',1,'SYSTEM','2016-06-06 21:02:05',NULL,NULL),(2,'ROLE_ADMIN','Administrator','N',1,'SYSTEM','2016-06-06 21:02:05',NULL,NULL);

CREATE TABLE `adm_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `enabled` char(1) NOT NULL,
  `account_non_expired` char(1) NOT NULL,
  `credentials_non_expired` char(1) NOT NULL,
  `account_non_locked` char(1) NOT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `version` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

INSERT INTO `adm_user` VALUES (1,'admin','$2a$10$f39laYr01qsXek4AmzvWy.zahGK2AsrOS1EGFnYcGMFDMrsRY4QSW','Y','Y','Y','Y','N',2,'SYSTEM','2016-04-28 00:26:44','SYSTEM','2016-06-04 23:16:37');

CREATE TABLE `adm_user_attempt` (
  `user_attempt_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `attempts` int(1) NOT NULL DEFAULT '0',
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`user_attempt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO `adm_user_attempt` VALUES (1,'admin',0,'2017-12-13 12:59:40'),(2,'user',2,'2017-10-18 10:37:56');

CREATE TABLE `adm_user_operation` (
  `user_operation_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `operation_id` int(11) NOT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `version` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_operation_id`),
  KEY `adm_user_operation_fk01` (`user_id`),
  KEY `adm_user_operation_fk02` (`operation_id`),
  CONSTRAINT `adm_user_operation_fk01` FOREIGN KEY (`user_id`) REFERENCES `adm_user` (`user_id`),
  CONSTRAINT `adm_user_operation_fk02` FOREIGN KEY (`operation_id`) REFERENCES `adm_operation` (`operation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `adm_user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `version` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_role_id`),
  KEY `usr_user_role_fk01` (`user_id`),
  KEY `usr_user_role_fk02` (`role_id`),
  CONSTRAINT `usr_user_role_fk01` FOREIGN KEY (`user_id`) REFERENCES `adm_user` (`user_id`),
  CONSTRAINT `usr_user_role_fk02` FOREIGN KEY (`role_id`) REFERENCES `adm_role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `adm_user_role` VALUES (1,1,1,'N',1,'SYSTEM','2016-06-06 21:02:54','admin','2017-10-24 11:01:16'),(2,1,2,'N',1,'SYSTEM','2016-06-06 21:02:54','admin','2017-10-24 11:01:16');

CREATE TABLE `sys_message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_code` varchar(50) NOT NULL,
  `message_en` varchar(200) NOT NULL,
  `message_th` varchar(200) DEFAULT NULL,
  `message_type` char(1) NOT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `version` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `sys_parameter_group` (
  `param_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `param_group_code` varchar(50) NOT NULL,
  `param_group_desc` varchar(200) NOT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `version` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`param_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO `sys_parameter_group` VALUES (6,'USSD_ERROR','USSD_ERROR','N',1,'SYSTEM','2017-11-28 00:00:00',NULL,NULL);

CREATE TABLE `sys_parameter_info` (
  `param_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `param_group_id` int(11) NOT NULL,
  `param_code` varchar(50) NOT NULL,
  `value_1` varchar(200) DEFAULT NULL,
  `value_2` varchar(200) DEFAULT NULL,
  `value_3` varchar(200) DEFAULT NULL,
  `value_4` varchar(200) DEFAULT NULL,
  `value_5` varchar(200) DEFAULT NULL,
  `value_6` varchar(200) DEFAULT NULL,
  `sorting_order` int(11) DEFAULT '0',
  `is_default` char(1) DEFAULT 'N',
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `version` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`param_info_id`),
  KEY `sys_parameter_info_fk01` (`param_group_id`),
  CONSTRAINT `sys_parameter_info_fk01` FOREIGN KEY (`param_group_id`) REFERENCES `sys_parameter_group` (`param_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

INSERT INTO `sys_parameter_info` VALUES (1,6,'E001','ไม่พบบริการนี้ในระบบ USSD',NULL,NULL,NULL,NULL,NULL,0,'N','N',1,'SYSTEM','2017-10-28 00:00:00',NULL,NULL),(2,6,'E002','ปัจจุบันบริการนี้สถานะบนระบบปิดอยู่',NULL,NULL,NULL,NULL,NULL,0,'N','N',1,'SYSTEM','2017-10-28 00:00:00',NULL,NULL),(3,6,'E003','ปัจจุบันบริการนี้สถานะบนระบบเปิดอยู่',NULL,NULL,NULL,NULL,NULL,0,'N','N',1,'SYSTEM','2017-10-28 00:00:00',NULL,NULL),(4,6,'E004','ผู้ใช้งานไม่มีสิทธิ์เข้าใช้งานบริการนี้',NULL,NULL,NULL,NULL,NULL,0,'N','N',1,'SYSTEM','2017-10-28 00:00:00',NULL,NULL),(18,6,'E999','System Exception',NULL,NULL,NULL,NULL,NULL,0,'N','N',1,'SYSTEM','2017-10-28 00:00:00',NULL,NULL);

CREATE TABLE `usr_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `is_deleted` char(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `usr_profile_fk01` (`user_id`),
  CONSTRAINT `usr_profile_fk01` FOREIGN KEY (`user_id`) REFERENCES `adm_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


