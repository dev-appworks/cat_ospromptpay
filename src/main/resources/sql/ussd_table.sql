CREATE TABLE `service_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Running Number',
  `SERVICE_ID` varchar(25) NOT NULL COMMENT 'รหัสบริการ',
  `SERVICE_NAME` varchar(255) DEFAULT NULL COMMENT 'ชื่อบริการ',
  `STATUS` int(1) DEFAULT NULL COMMENT '0 – ปิดบริการ 1 – เปิดบริการ',
  `TIMER` datetime DEFAULT NULL COMMENT 'ข้อมูลตั้งค่าวันเวลาเปิด/ปิด บริการ',
  `EMAIL` varchar(1000) DEFAULT NULL COMMENT 'อีเมลล์',
  `IS_DELETED` int(1) DEFAULT 0 COMMENT '0 – active 1 – inactive',
  `UPDATED_BY` varchar(255) DEFAULT NULL COMMENT 'ผู้ทำการแก้ไขล่าสุด',
  `UPDATED_DATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'วันที่ทำการแก้ไขล่าสุด',
  PRIMARY KEY (`ID`,`SERVICE_ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

INSERT INTO `service_status` VALUES 
(1,'USSD001','Check Officer',0,NULL,NULL,0,'ผู้ดำเนินการ','2017-12-08 08:58:38'),
(2,'USSD002','Add Officer',1,NULL,NULL,0,'ผู้ดำเนินการ','2017-12-13 05:49:29'),
(3,'USSD003','Delete Officer',0,NULL,NULL,0,'ผู้ดำเนินการ','2017-12-08 09:05:20'),
(4,'USSD004','Change Officer',0,NULL,NULL,0,'ผู้ดำเนินการ','2017-12-13 05:49:29'),
(5,'USSD005','Activation',1,NULL,NULL,0,'ผู้ดำเนินการ','2017-12-06 04:05:48'),
(6,'USSD006','Redeem Expiry Date',0,NULL,NULL,0,'ผู้ดำเนินการ','2017-12-06 04:05:48');

CREATE TABLE `auditlog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Running Number',
  `TYPE` varchar(10) NOT NULL COMMENT 'ประเภทรายการ 1 - ระบบ Order Management (OM UDB) 2 - จัดทำใบแจ้งค่าใช้บริการ (Billing)3 - ส่งข้อมูลเข้าระบบ ERP SAP4 - ส่งข้อมูลเข้าระบบ Data Warehouse 5 - การบริหารจัดการเลขหมายโทรศัพท์เคลื่อนที่ (Numbering Inventory)6 - ส่วนงานบริหารจัดการข้อมูล USSD7 - ส่วนงานสนับสนุนระบบ TwoShot',
  `LOG_DATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'วันเวลาที่สร้างข้อมูล',
  `LOG_BY` varchar(255) DEFAULT NULL COMMENT 'ผู้ที่ทำการสร้างข้อมูล',
  `SERVICE_ID` varchar(25) DEFAULT NULL COMMENT 'รหัสบริการ',
  `SERVICE_NAME` varchar(25) DEFAULT NULL COMMENT 'ชื่อบริการ',
  `ACTION` int(1) DEFAULT NULL COMMENT '0 – ปิดบริการ 1 – เปิดบริการ',
  `TIMER` datetime DEFAULT NULL COMMENT 'ข้อมูลตั้งค่าวันเวลาเปิด/ปิด บริการ',
  `STATUS` varchar(255) DEFAULT NULL COMMENT 'สถานะการทำงาน  – สำเร็จ   – ไม่สำเร็จ',
  `EMAIL` varchar(1000) DEFAULT NULL COMMENT 'อีเมลล์',
  `UPDATED_BY` varchar(255) DEFAULT NULL COMMENT 'ผู้ทำการแก้ไขล่าสุด',
  `UPDATED_DATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'วันที่ทำการแก้ไขล่าสุด',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `oswserrorlog` (
  `error_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Running Number',
  `error_code` VARCHAR(30) NOT NULL COMMENT 'รหัสบริการ',
  `error_desc` VARCHAR(500) NOT NULL COMMENT 'ชื่อบริการ',
  PRIMARY KEY (`error_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
  
 CREATE TABLE `oswslog` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Runing Number',
  `type` VARCHAR(10) NOT NULL COMMENT 'Action เว็บเซอร์วิสบนระบบ\nREQ – Request จากผู้เรียกใช้งานเว็บเซอร์วิส\nRES – Response จากผู้ถูกเรียกใช้งานเว็บเซอร์วิส',
  `logdatetime` TIMESTAMP NOT NULL COMMENT 'วันเวลาที่สร้างข้อมูล',
  `function_name` VARCHAR(30) NOT NULL COMMENT 'ข้อมูลฟังก์ชันการทำงานของเว็บเซอร์วิส',
  `request_id` VARCHAR(47) NULL COMMENT 'Request ID จากผู้เรียกใช้งาน',
  `response_id` VARCHAR(47) NULL COMMENT 'Response ID จากผู้ถูกใช้งาน',
  `request_datetime` DATETIME NULL COMMENT 'วันเวลาที่ทำการเรียกใช้งานเว็บเซอร์วิส',
  `response_datetime` DATETIME NULL COMMENT 'วันเวลาที่ส่งสถานะการทำงานกลับไปยังผู้เรียกใช้งาน',
  `request_appid` VARCHAR(255) NULL COMMENT 'Request Application ID ของระบบที่เรียกใช้งาน',
  `response_appid` VARCHAR(255) NULL COMMENT 'Response Application ID ของระบบที่ถูกเรียกใช้งาน',
  `user_id` VARCHAR(32) NULL COMMENT 'ข้อมูลรหัสพนักงาน',
  `ipaddress` VARCHAR(15) NULL COMMENT 'ข้อมูล IP Address ของเครื่อง Client',
  `auth_user_id` VARCHAR(15) NULL COMMENT 'รหัสพนักงานที่ทำการ Authorization',
  `status_code` VARCHAR(2) NULL COMMENT 'ข้อมูลสถานะการทำงาน',
  `error_id` INT NULL COMMENT 'Error ข้อมูลความผิดพลาด ในกรณีสถานะการทำงานไม่สำเร็จ',
  `service_id` VARCHAR(25) NULL COMMENT 'ข้อมูลรหัสบริการ USSSD',
  `status` INT NULL COMMENT 'สถานะ เปิด/ปิด บริการ USSD',
  PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;