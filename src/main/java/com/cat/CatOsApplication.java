package com.cat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ComponentScan("com.cat.os")
@EnableAsync
@EnableAutoConfiguration 
@PropertySources({
	 @PropertySource(value = "classpath:application.properties", ignoreResourceNotFound = true),//for dev only
//	 @PropertySource(value = "file:${root.app.config.path}/${spring.profiles.active}/application.properties", ignoreResourceNotFound = true),// for deploy in sever
	})
public class CatOsApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CatOsApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CatOsApplication.class);
	}
}
