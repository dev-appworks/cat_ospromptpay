
package com.cat.ussd.performonoffservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cat.ussd.performonoffservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Process_QNAME = new QName("http://ussd.cat.com/PerformOnOffService", "process");
    private final static QName _ProcessResponse_QNAME = new QName("http://ussd.cat.com/PerformOnOffService", "processResponse");
    private final static QName _RqHeaderTerminalId_QNAME = new QName("", "terminalId");
    private final static QName _RqHeaderAuthUserId_QNAME = new QName("", "authUserId");
    private final static QName _RqHeaderAuthPassword_QNAME = new QName("", "authPassword");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cat.ussd.performonoffservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link Rq }
     * 
     */
    public Rq createRq() {
        return new Rq();
    }

    /**
     * Create an instance of {@link RqHeader }
     * 
     */
    public RqHeader createRqHeader() {
        return new RqHeader();
    }

    /**
     * Create an instance of {@link RqBody }
     * 
     */
    public RqBody createRqBody() {
        return new RqBody();
    }

    /**
     * Create an instance of {@link Rs }
     * 
     */
    public Rs createRs() {
        return new Rs();
    }

    /**
     * Create an instance of {@link RsHeader }
     * 
     */
    public RsHeader createRsHeader() {
        return new RsHeader();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link RsBody }
     * 
     */
    public RsBody createRsBody() {
        return new RsBody();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Process }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ussd.cat.com/PerformOnOffService", name = "process")
    public JAXBElement<Process> createProcess(Process value) {
        return new JAXBElement<Process>(_Process_QNAME, Process.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ussd.cat.com/PerformOnOffService", name = "processResponse")
    public JAXBElement<ProcessResponse> createProcessResponse(ProcessResponse value) {
        return new JAXBElement<ProcessResponse>(_ProcessResponse_QNAME, ProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "terminalId", scope = RqHeader.class)
    public JAXBElement<String> createRqHeaderTerminalId(String value) {
        return new JAXBElement<String>(_RqHeaderTerminalId_QNAME, String.class, RqHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "authUserId", scope = RqHeader.class)
    public JAXBElement<String> createRqHeaderAuthUserId(String value) {
        return new JAXBElement<String>(_RqHeaderAuthUserId_QNAME, String.class, RqHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "authPassword", scope = RqHeader.class)
    public JAXBElement<String> createRqHeaderAuthPassword(String value) {
        return new JAXBElement<String>(_RqHeaderAuthPassword_QNAME, String.class, RqHeader.class, value);
    }

}
