package com.cat.os.service;

import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

@Service
public class SftpService {
	
	private static final Logger log = LoggerFactory.getLogger(SftpService.class);
	
	@Value("${ibacss.sftp.host}")
	private String host;
	
	@Value("${ibacss.sftp.port}")
	private int port;
	
	@Value("${ibacss.sftp.username}")
	private String username;
	
	@Value("${ibacss.sftp.password}")
	private String password;
	
	@Value("${ibacss.sftp.path}")
	private String rootPath;
	
	
	public byte[] downloadFile(String path, String filename) {
		log.info("downloadFile file={}", rootPath + path + "/" + filename);
		
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		byte[] downloadFile = null;
		
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(username, host, port);
			session.setPassword(password);
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			log.info("Host Session connected");
			
			channel = session.openChannel("sftp");
			channel.connect();
			log.info("Channel connected");
			
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(rootPath + path);
			
			downloadFile = IOUtils.toByteArray(channelSftp.get(filename));
			log.info("downloading file={} success", rootPath + path + "/" + filename);
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			if (channelSftp != null) {
				channelSftp.exit();
				log.debug("sftp Channel exited");
				channel.disconnect();
				log.info("Channel disconnected");
				session.disconnect();
				log.info("Host Session disconnected");
			}
		}
		
		return downloadFile;
	}
	
	public void test() {
		log.info("host > " + host);
		log.info("port > " + port);
		log.info("username > " + username);
		log.info("password > " + password);
		log.info("rootPath > " + rootPath);
	}
	
}
