package com.cat.os.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cat.os.common.constant.OmUDBConstant;
import com.cat.os.common.constant.UssdConstant;
import com.cat.os.dao.entity.SysParameterGroup;
import com.cat.os.dao.entity.SysParameterInfo;
import com.cat.os.dao.mysql.SysParameterGroupRepository;

@Service
public class ApplicationCacheService {

	private static final Logger log = LoggerFactory.getLogger(ApplicationCacheService.class);

	@Autowired
	private SysParameterGroupRepository sysParameterGroupRepository;
	
	
	private static Map<String , SysParameterInfo> USSD_ERROR = new HashMap<>();
	private static Map<String , SysParameterInfo> OM_ERROR = new HashMap<>();
	private static Map<String , SysParameterInfo> OM_STATUS_FLOW = new HashMap<>();
	
	@PostConstruct
	@Transactional(value="transactionManager")
	public void init() {
		this.loadUssdErrorCode();
		this.loadOMErrorCode();
		this.loadOMStatusFlowMapping();
	}
	
	
	public synchronized String getUssdErrorCode(String errorCode) {
		if(USSD_ERROR.get(errorCode) == null) {
			log.debug("getUssdErrorCode not Found " + errorCode);
			return null;
		}
		return USSD_ERROR.get(errorCode).getValue1();
	}
	
	public synchronized void loadUssdErrorCode() {
		USSD_ERROR.clear();
		log.info("##### USSD CONFIG LOAD...");
		SysParameterGroup ussdError = sysParameterGroupRepository.findSysParameterGroupByParamGroupCode(UssdConstant.USSD_ERROR_GROUP_CODE);
		if(ussdError != null) {
			for (SysParameterInfo info : ussdError.getParameterInfos()) {
				USSD_ERROR.put(info.getParamCode(), info);
				log.info(" >> LOAD {} : {} " , info.getParamCode() , info.getValue1() );
			}
		}
		log.info("##### USSD CONFIG END ...");
	}

	public synchronized void loadOMErrorCode() {
		OM_ERROR.clear();
		log.info("##### OM CONFIG LOAD...");
		SysParameterGroup ussdError = sysParameterGroupRepository.findSysParameterGroupByParamGroupCode(OmUDBConstant.OM_ERROR);
		if(ussdError != null) {
			for (SysParameterInfo info : ussdError.getParameterInfos()) {
				OM_ERROR.put(info.getParamCode(), info);
				log.info(" >> LOAD {} : {} " , info.getParamCode() , info.getValue1() );
			}
		}
		log.info("##### OM_ERROR CONFIG END ...");
	}
	
	public synchronized String getOMErrorCode(String errorCode) {
		if(OM_ERROR.get(errorCode) == null) {
			log.debug("getOMErrorCode not Found " + errorCode);
			return null;
		}
		return OM_ERROR.get(errorCode).getValue1();
	}
	
	
	public synchronized void loadOMStatusFlowMapping() {
		OM_STATUS_FLOW.clear();
		log.info("##### OM CONFIG MAPPING LOAD...");
		SysParameterGroup ussdError = sysParameterGroupRepository.findSysParameterGroupByParamGroupCode(OmUDBConstant.OM_STATUS_FLOW);
		if(ussdError != null) {
			for (SysParameterInfo info : ussdError.getParameterInfos()) {
				OM_STATUS_FLOW.put(info.getParamCode(), info);
				log.info(" >> LOAD {} : {} " , info.getParamCode() , info.getValue1() );
			}
		}
		log.info("##### OM_ERROR CONFIG MAPPING END ...");
	}
	
	public Map<String, SysParameterInfo> getOMStatusFlowMapping() {
		return OM_STATUS_FLOW;
	}
	
}
