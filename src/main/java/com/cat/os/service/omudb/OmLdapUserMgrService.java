package com.cat.os.service.omudb;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.utils.OsUtils;
import com.cat.os.dao.vo.OmUserFormVo;

@Service
@Profile(Profiles.OMUDB)
public class OmLdapUserMgrService {

	private static final Logger log = LoggerFactory.getLogger(OmLdapUserMgrService.class);

	@Value("${omdb.usermanager.ldap.host}")
	private String host;

	@Value("${omdb.usermanager.ldap.port}")
	private Integer port;

	@Value("${omdb.usermanager.ldap.base}")
	private String base;

	@Value("${omdb.usermanager.ldap.user}")
	private String user;

	@Value("${omdb.usermanager.ldap.pass}")
	private String pass;

	@Value("${omdb.usermanager.ldap.create.group}")
	private String createGroup;

	@Value("${omdb.usermanager.ldap.create.user}")
	private String createUser;

	final Hashtable<String, String> env = new Hashtable<String, String>();

	@PostConstruct
	public void init() {
		String url = String.format("ldap://%s:%d/%s", host, port, base);
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, url);
		env.put(Context.SECURITY_PRINCIPAL, user);
		env.put(Context.SECURITY_CREDENTIALS, pass);

		log.info("##### LDAP Manager URL : " + url);

	}

	public List<String> findAllGroup() {
		List<String> groups = new ArrayList<String>();

		NamingEnumeration<SearchResult> answer = null;
		DirContext ctx = null;

		try {
			ctx = new InitialDirContext(env);
			Attributes matchAttrs = new BasicAttributes(true);
			matchAttrs.put(new BasicAttribute("objectClass", "groupOfNames"));

			answer = ctx.search(createGroup, matchAttrs);
			while (answer.hasMore()) {
				SearchResult sr = answer.next();
				Attributes arrt = sr.getAttributes();
				Attribute cn = arrt.get("cn");
				String cnValue = cn.get().toString();
				log.info("cn = " + cnValue);
				groups.add(cnValue);

			}
		} catch (NameNotFoundException e) {
			log.error("findAllGroup", e);
		} catch (NamingException e) {
			log.error("findAllGroup", e);
		} finally {
			this.closeLdapConnection(ctx, answer);
		}

		return groups;
	}

	private void closeLdapConnection(DirContext ctx, NamingEnumeration<SearchResult> answer) {
		if (answer != null) {
			try {
				answer.close();
			} catch (Exception e) {
			}
		}
		if (ctx != null) {
			try {
				ctx.close();
			} catch (Exception e) {
			}
		}
	}

	public boolean checkDuplicatUser(String cnUsername) {

		Assert.hasLength(cnUsername, "cnUsername is null");

		log.info("checkDuplicatUser = {} ", cnUsername);

		boolean isDup = false;

		NamingEnumeration<SearchResult> answer = null;
		DirContext ctx = null;

		try {
			ctx = new InitialDirContext(env);

			SearchControls cons = new SearchControls();
			cons.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String filter = String.format("(uid=%s)", cnUsername);
			answer = ctx.search("ou=om_udb", filter, cons);
			while (answer.hasMore()) {
				SearchResult sr = answer.next();
				Attributes arrt = sr.getAttributes();
				Attribute cn = arrt.get("cn");
				String cnValue = cn.get().toString();
				log.info("cn = " + cnValue);
				isDup = true;
				break;

			}
		} catch (NameNotFoundException e) {
			log.error("checkDuplicatUser", e);
		} catch (NamingException e) {
			log.error("checkDuplicatUser", e);
		} finally {
			this.closeLdapConnection(ctx, answer);
		}

		return isDup;

	}

	public void createUserInLdap(OmUserFormVo omUserFormVo) throws Exception {

		DirContext ctx = null;

		try {
			ctx = new InitialDirContext(env);

			// Create attributes to be associated with the object
			Attributes attrs = new BasicAttributes(true); // case-ignore
			Attribute objclass = new BasicAttribute("objectclass");
			objclass.add("inetOrgPerson");
			objclass.add("organizationalPerson");
			objclass.add("person");
			objclass.add("posixAccount");
			objclass.add("top");
			attrs.put(objclass);

			Attribute attr = null;

			Attribute cn = new BasicAttribute("cn");
			cn.add(String.format("%s %s", omUserFormVo.getFirstName(),omUserFormVo.getLastName()));
			attrs.put(cn);

			attr = new BasicAttribute("gidNumber");
			attr.add("501");
			attrs.put(attr);

			attr = new BasicAttribute("homeDirectory");
			attr.add("/home/users/" + omUserFormVo.getUserName());
			attrs.put(attr);

			attr = new BasicAttribute("loginShell");
			attr.add("/bin/sh");
			attrs.put(attr);

			attr = new BasicAttribute("sn");
			attr.add(omUserFormVo.getLastName());
			attrs.put(attr);

			attr = new BasicAttribute("givenName");
			attr.add(omUserFormVo.getFirstName());
			attrs.put(attr);

			attr = new BasicAttribute("uid");
			attr.add(omUserFormVo.getUserName());
			attrs.put(attr);

			Assert.hasLength(omUserFormVo.getUidNumber(),"uidNumber must not null.");
			attr = new BasicAttribute("uidNumber");
			attr.add(omUserFormVo.getUidNumber());
			attrs.put(attr);

			attr = new BasicAttribute("userPassword");
			String pwdStr = OsUtils.encryptMD5Password(omUserFormVo.getPassword());
			System.out.println("PWD : " + pwdStr);
			attr.add(pwdStr);
			attrs.put(attr);

			LdapContext users = (LdapContext) ctx.lookup(createUser);

			log.info("Create in : {} " , createUser);
			log.info("Attribut  : {} "  , attrs.toString());
			// Perform bind
			users.bind("cn=" + cn.get().toString() , null, attrs);

			log.info("success...");

		} catch (NameNotFoundException e) {
			log.error("checkDuplicatUser", e);
			throw e;
		} catch (NamingException e) {
			log.error("checkDuplicatUser", e);
			throw e;
		} finally {
			this.closeLdapConnection(ctx, null);
		}

	}
	
	public void addGroup(String cnUser , List<String> group) {

		NamingEnumeration<SearchResult> answer = null;
		DirContext ctx = null;

		try {

			ctx = new InitialDirContext(env);
			
			SearchResult existUser = this.findUserInLdap(cnUser, ctx);
			if(existUser == null) {
				log.info("not exsit User");
				return ;
			}
			
			for (String g : group) {
				
				Attributes matchAttrs = new BasicAttributes(true);
				matchAttrs.put(new BasicAttribute("objectClass", "groupOfNames"));
				matchAttrs.put(new BasicAttribute("cn", g));
	
				answer = ctx.search(createGroup, matchAttrs);
				
				log.info("Serach : {} " , g);
				while (answer.hasMore()) {
		    	    SearchResult sr = answer.next();
		    	    String nameOfGroup = sr.getName() + "," + createGroup;
		    	    String baseUserCn =  existUser.getNameInNamespace();
		    	    
		    	    ModificationItem[] mods = new ModificationItem[1];
		    	    mods[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("member", baseUserCn));
					log.info("add member : {} " , baseUserCn);
		    	    ctx.modifyAttributes(nameOfGroup, mods);
		    	    break;
				}
			}
		} catch (NameNotFoundException e) {
			log.error("addGroup", e);
		} catch (NamingException e) {
			log.error("addGroup", e);
		} finally {
			this.closeLdapConnection(ctx, answer);
		}

	
	}
	
	public void delGroup(String cnUser , List<String> delGroup) {

		NamingEnumeration<SearchResult> answer = null;
		DirContext ctx = null;

		try {

			ctx = new InitialDirContext(env);
			
			SearchResult existUser = this.findUserInLdap(cnUser, ctx);
			if(existUser == null) {
				log.info("not exsit User");
				return ;
			}
			
			for (String g : delGroup) {
				
				Attributes matchAttrs = new BasicAttributes(true);
				matchAttrs.put(new BasicAttribute("objectClass", "groupOfNames"));
				matchAttrs.put(new BasicAttribute("cn", g));
	
				answer = ctx.search(createGroup, matchAttrs);
				
				log.info("delGroup Serach : {} " , g);
				while (answer.hasMore()) {
		    	    SearchResult sr = answer.next();
		    	    String nameOfGroup = sr.getName() + "," + createGroup;
		    	    String baseUserCn =  existUser.getNameInNamespace();
		    	    
		    	    ModificationItem[] mods = new ModificationItem[1];
		    	    mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("member", baseUserCn));
					log.info("del member : {} " , baseUserCn);
		    	    ctx.modifyAttributes(nameOfGroup, mods);
		    	    break;
				}
			}
		} catch (NameNotFoundException e) {
			log.error("delGroup", e);
		} catch (NamingException e) {
			log.error("delGroup", e);
		} finally {
			this.closeLdapConnection(ctx, answer);
		}

	
	}
	
	private SearchResult findUserInLdap(String cnUsername, DirContext rootBaseCtx) {

			Assert.hasLength(cnUsername, "cnUsername is null");

			log.info("findUserInLdap = {} ", cnUsername);

			NamingEnumeration<SearchResult> answer = null;

			try {
				SearchControls cons = new SearchControls();
				cons.setSearchScope(SearchControls.SUBTREE_SCOPE);
				String filter = String.format("(uid=%s)", cnUsername);
				answer = rootBaseCtx.search("ou=om_udb", filter, cons);
				while (answer.hasMore()) {
					SearchResult sr = answer.next();
					Attributes arrt = sr.getAttributes();
					Attribute cn = arrt.get("cn");
					String cnValue = cn.get().toString();
					log.info("cn = " + cnValue);
					return sr;
				}
			} catch (NameNotFoundException e) {
				log.error("findUserInLdap", e);
			} catch (NamingException e) {
				log.error("findUserInLdap", e);
			}

			return null;
	}

	public List<String> findUserGroup(String uiUsername) {

		List<String> groups = new ArrayList<String>();

		NamingEnumeration<SearchResult> answer = null;
		DirContext ctx = null;

		try {
			ctx = new InitialDirContext(env);
			
			SearchResult userSearch = this.findUserInLdap(uiUsername, ctx);
			Assert.notNull(userSearch, uiUsername + " ไม่พบ ผู้ใช้งานในระบบ LDAP ");
			
			Attributes matchAttrs = new BasicAttributes(true);
			matchAttrs.put(new BasicAttribute("objectClass", "groupOfNames"));
			matchAttrs.put(new BasicAttribute("member", userSearch.getNameInNamespace()));

			answer = ctx.search(createGroup, matchAttrs);
			while (answer.hasMore()) {
				SearchResult sr = answer.next();
				Attributes arrt = sr.getAttributes();
				Attribute cn = arrt.get("cn");
				String cnValue = cn.get().toString();
				log.info("cn = " + cnValue);
				groups.add(cnValue);

			}
		} catch (NameNotFoundException e) {
			log.error("findUserGroup", e);
		} catch (NamingException e) {
			log.error("findUserGroup", e);
		} finally {
			this.closeLdapConnection(ctx, answer);
		}

		return groups;
	
	}

	public void updatePassword(String uidUsername, String password) {

		Assert.hasLength(uidUsername, "uidUsername is not null.");
		Assert.hasLength(password, "password is not null.");
		
		NamingEnumeration<SearchResult> answer = null;
		DirContext ctx = null;

		try {
			ctx = new InitialDirContext(env);
			
			SearchResult findUser = this.findUserInLdap(uidUsername, ctx);
			ModificationItem[] mods = new ModificationItem[1];
			String pwd = OsUtils.encryptMD5Password(password);
    	    mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userpassword", pwd));
    	    String nameOfGroup = findUser.getNameInNamespace().replace("," + base, "");
    	    log.info("change pwd dir : {}  :  {}" , nameOfGroup, pwd);

    	    ctx.modifyAttributes(nameOfGroup , mods);

    	    log.info("change pwd success... :");
			
		} catch (NameNotFoundException e) {
			log.error("findAllGroup", e);
		} catch (NamingException e) {
			log.error("findAllGroup", e);
		} finally {
			this.closeLdapConnection(ctx, answer);
		}

	
	}
}
