package com.cat.os.service.omudb;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.cat.os.common.constant.OmUDBConstant;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.omudb.OmOrdersJdbcRepository;
import com.cat.os.dao.vo.OmFormVo;
import com.cat.os.dao.vo.OmOrderVo;
import com.cat.os.dao.vo.ResponseBpmInstance;
import com.cat.os.dao.vo.ResponseBpmTaskDetail;

@Service
@Profile(Profiles.OMUDB)
public class OmCancelTaskService {

	private static final Logger log = LoggerFactory.getLogger(OmCancelTaskService.class);
	
	@Value("${bpm.task.detail.url:#{null}}")
	private String bpmTaskDetailUrl; 
	
	@Value("${bpm.terminal.instance.url:#{null}}")
	private String bpmTerminalInstanceUrl;
	
	@Value("${bpm.delete.instance.url:#{null}}")
	private String bpmDeleteInstanceUrl;
	
	@Value("${bpm.authen.user:#{null}}")
	private String userName;
	
	@Value("${bpm.authen.password:#{null}}")
	private String password;
	
	@Autowired
	private OmOrdersJdbcRepository omOrdersJdbcRepository;
	
	public void callRestApiOmCancelTask(OmFormVo omFormVo) throws Exception {
		if (omFormVo != null && StringUtils.isNotBlank(omFormVo.getOrderId())) {
			log.info("callRestApiOmCancelTask Order Id : " + omFormVo.getOrderId());
			
			List<OmOrderVo> omOrderVoList = omOrdersJdbcRepository.getBpmTaskIdById(omFormVo.getOrderId());
			if (omOrderVoList != null && omOrderVoList.size() > 0) {
				String taskId = omOrderVoList.get(0).getBpmTaskId();
				if (StringUtils.isNotBlank(taskId)) {
					String bpmTaskId = taskId.trim().replace(".00", "");
					String piId = callBpmRestApiTaskDetail(bpmTaskId);
					
					if (StringUtils.isNotBlank(piId)) {
						String state = callBpmRestApiTernimalInstance(piId);
						
						if (OmUDBConstant.BPM_REST_API_TERMINAL_INSTANCE.STATE_TERMINATED.equals(state)) {
							String deleteState = callBpmRestApiDeleteInstance(piId);
							log.info("callRestApiOmCancelTask Completed piid : " + piId + " , state : " + state + " , Delete State : " + deleteState);
						}
					}
				}
			}
		}
	}
	
	public String callBpmRestApiTaskDetail(String bpmTaskId) throws Exception {
		String piId = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName, password));
			log.info("bpmTaskDetailUrl : {} ", bpmTaskDetailUrl);
			ResponseEntity<ResponseBpmTaskDetail> response = restTemplate.exchange(bpmTaskDetailUrl, HttpMethod.GET, null, ResponseBpmTaskDetail.class, bpmTaskId);
			
			if (HttpStatus.OK.equals(response.getStatusCode())) {
				ResponseBpmTaskDetail body = response.getBody();
				if (body != null && body.getData() != null) {
					piId = body.getData().getPiid();

					log.info("callBpmRestApiTaskDetail Completed piid : " + piId + " , tkiid : " + body.getData().getTkiid());
				}
			}
			log.info("callBpmRestApiTaskDetail Completed Status  : " + response.getStatusCode() + " , bpmTaskId : " + bpmTaskId);
		}catch(HttpClientErrorException ehttp) {
			log.info("is ok ehttp : " + ehttp.getStatusCode());
			log.info("is ok ehttp : " + ehttp.getResponseBodyAsString());
			if(ehttp.getStatusCode() != HttpStatus.NOT_FOUND) {
				throw ehttp;
			}
		}catch (Exception e) {
			log.error("Error in CallBpmRestApiTaskDetail ", e);
			throw e;
		}
		return piId;
	}
	
	public String callBpmRestApiTernimalInstance(String piId) throws Exception {
		String state = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName, password));
			log.info("callBpmRestApiTernimalInstance : {} ", bpmTerminalInstanceUrl);
			ResponseEntity<ResponseBpmInstance> response = restTemplate.exchange(bpmTerminalInstanceUrl, HttpMethod.POST, 
					null, ResponseBpmInstance.class, piId);
			
			if (HttpStatus.OK.equals(response.getStatusCode())) {
				ResponseBpmInstance body = response.getBody();
				if (body != null && body.getData() != null) {
					state = body.getData().getState();

					log.info("callBpmRestApiTernimalInstance Completed State : " + state + " , executionState : " + body.getData().getExecutionState());
				}
			}
			log.info("callBpmRestApiTernimalInstance Completed Status  : " + response.getStatusCode() + " , piId : " + piId);
		}catch(HttpClientErrorException ehttp) {
			log.info("is ok ehttp : " + ehttp.getStatusCode());
			log.info("is ok ehttp : " + ehttp.getResponseBodyAsString());
			if(ehttp.getStatusCode() != HttpStatus.NOT_FOUND) {
				throw ehttp;
			}
		} catch (Exception e) {
			log.error("Error in callBpmRestApiTernimalInstance ", e);
			throw e;
		}
		return state;
	}
	
	public String callBpmRestApiDeleteInstance(String piId) throws Exception {
		String responseState = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName, password));
			
			log.info("callBpmRestApiDeleteInstance : {} ", bpmDeleteInstanceUrl);
			ResponseEntity<ResponseBpmInstance> response = restTemplate.exchange(bpmDeleteInstanceUrl, HttpMethod.DELETE, 
					null, ResponseBpmInstance.class, piId);
			
			if (HttpStatus.OK.equals(response.getStatusCode())) {
				ResponseBpmInstance body = response.getBody();
				if (body != null && body.getData() != null) {
					responseState = body.getData().getState();

					log.info("callBpmRestApiTernimalInstance Completed State : " + responseState + " , executionState : " + body.getData().getExecutionState());
				}
			}
			log.info("callBpmRestApiDeleteInstance Completed Status  : " + response.getStatusCode() + " , piId : " + piId);
		} catch(HttpClientErrorException ehttp) {
			log.info("is ok ehttp : " + ehttp.getStatusCode());
			log.info("is ok ehttp : " + ehttp.getResponseBodyAsString());
			if(ehttp.getStatusCode() != HttpStatus.NOT_FOUND) {
				throw ehttp;
			}
		} catch (Exception e) {
			log.error("Error in callBpmRestApiDeleteInstance ", e);
			throw e;
		}
		return responseState;
	}
	
}
