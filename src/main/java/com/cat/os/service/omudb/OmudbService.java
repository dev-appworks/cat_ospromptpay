package com.cat.os.service.omudb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.OmUDBConstant;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.entity.SysParameterInfo;
import com.cat.os.dao.entity.ZbowCrossflowOrders;
import com.cat.os.dao.omudb.OmOrdersJdbcRepository;
import com.cat.os.dao.vo.OmFormVo;
import com.cat.os.dao.vo.OmOrderVo;
import com.cat.os.dao.vo.OmudbHistoryFormVo;
import com.cat.os.dao.vo.OmudbHistoryVo;
import com.cat.os.dao.vo.OrderStatusVo;
import com.cat.os.service.ApplicationCacheService;

@Service
@Profile(Profiles.OMUDB)
public class OmudbService {

	private static final Logger log = LoggerFactory.getLogger(OmudbService.class);

	@Autowired
	private OmOrdersJdbcRepository omOrdersJdbcRepository;
	
	@Autowired
	private OmCancelTaskService omCancelTaskService;

	@Autowired
	private ApplicationCacheService applicationCacheService;
	
	
	public DataTableAjax<OmOrderVo> orderbyTask(OmFormVo omFormVo) {
		DataTableAjax<OmOrderVo> dataTableAjax = new DataTableAjax<OmOrderVo>();
		dataTableAjax.setDraw(omFormVo.getDraw()+1);
		
		List<OmOrderVo> res = omOrdersJdbcRepository.getOrderByTask (omFormVo);
		Long count = omOrdersJdbcRepository.getCountOrderByTask(omFormVo);
		dataTableAjax.setRecordsFiltered(count);
		dataTableAjax.setRecordsTotal(count);
		dataTableAjax.setData(res);
		
		return dataTableAjax;
	}

	public DataTableAjax<OmOrderVo> orderbyServiceItem(OmFormVo omFormVo) {
		DataTableAjax<OmOrderVo> dataTableAjax = new DataTableAjax<OmOrderVo>();
		dataTableAjax.setDraw(omFormVo.getDraw()+1);
		
		List<OmOrderVo> res = omOrdersJdbcRepository.getOrderByServiceItemId (omFormVo);
		Long count = omOrdersJdbcRepository.getCountOrderByServiceItemId(omFormVo);
		dataTableAjax.setRecordsFiltered(count);
		dataTableAjax.setRecordsTotal(count);
		dataTableAjax.setData(res);
		
		return dataTableAjax;
	}
	
	
	public List<OrderStatusVo> getFilterDropdownByStatusCode(String statusCode) {
		
		String[] listCode = this.getStatusMaping(statusCode);
		if(listCode == null) {
			log.error("Not found status Code in Config : " + statusCode);
			return null;
		}
		
		List<OrderStatusVo> listCodeConfig = omOrdersJdbcRepository.getFilterDropdownByStatusCode(listCode);
		
		return listCodeConfig;
		
	}

	@Transactional("omudbTransactionManager")
	public void changeOrderTaskStatus(OmFormVo omFormVo, String userlogin) throws Exception {
		
		ZbowCrossflowOrders enity = new ZbowCrossflowOrders();
		enity.setOrderId(omFormVo.getOrderId());
		enity.setOrderSvcItem(omFormVo.getServiceItemId());
		enity.setSvcStatusId(omFormVo.getChangeStatusCode());
		enity.setRemark(omFormVo.getRemark());
		enity.setUsername(userlogin);
		enity.setUpdateDate(new Date());
		
		omOrdersJdbcRepository.insertZbowCrossflowOrders(enity);
		
		omOrdersJdbcRepository.updateSvcStatusId(omFormVo.getOrderId(),omFormVo.getChangeStatusCode());
		
		if(OmUDBConstant.OM_ORDER_CHANGE_STATUS.CANCELLED.equals(omFormVo.getChangeStatusCode())) {
			omCancelTaskService.callRestApiOmCancelTask(omFormVo);
		}
		
	}

	@Transactional("omudbTransactionManager")
	public void changeOrderStatusByServiceItem(OmFormVo omFormVo, String userlogin) throws Exception {
		
		ZbowCrossflowOrders enity = new ZbowCrossflowOrders();
		enity.setOrderId(omFormVo.getOrderId());
		enity.setOrderSvcItem(omFormVo.getServiceItemId());
		enity.setSvcStatusId(omFormVo.getChangeStatusCode());
		enity.setRemark(omFormVo.getRemark());
		enity.setUsername(userlogin);
		enity.setUpdateDate(new Date());
		
		omOrdersJdbcRepository.insertZbowCrossflowOrders(enity);
		omOrdersJdbcRepository.updateSvcStatusIdCaseServiceItem(omFormVo.getServiceItemId(),omFormVo.getChangeStatusCode());

		if(OmUDBConstant.OM_ORDER_CHANGE_STATUS.CANCELLED.equals(omFormVo.getChangeStatusCode())) {
			omCancelTaskService.callRestApiOmCancelTask(omFormVo);
		}
	}
	
	public DataTableAjax<OmudbHistoryVo> getOmudbHistoryCriteria(OmudbHistoryFormVo omudbHistoryFormVo) {

		DataTableAjax<OmudbHistoryVo> dataTableAjax = new DataTableAjax<OmudbHistoryVo>();
		dataTableAjax.setDraw(omudbHistoryFormVo.getDraw()+1);
		
		List<OmudbHistoryVo> res = omOrdersJdbcRepository.getOmudbHistoryCriteria(omudbHistoryFormVo);
		Long count = omOrdersJdbcRepository.getCountOmudbHistoryCriteria(omudbHistoryFormVo);
		dataTableAjax.setRecordsFiltered(count);
		dataTableAjax.setRecordsTotal(count);
		dataTableAjax.setData(res);
		
		return dataTableAjax;
	
	}
	
	private String[] getStatusMaping(String key) {
		Map<String, SysParameterInfo> maps = applicationCacheService.getOMStatusFlowMapping();
		SysParameterInfo sys = maps.get(key);
		if(sys == null) return null;
		return sys.getValue1().split(",");
	}
	
	public String getStatusMapingKey() {
		Map<String, SysParameterInfo> maps = applicationCacheService.getOMStatusFlowMapping();
	    List<String> keys = new ArrayList<>(maps.keySet());
	    return StringUtils.join(keys,",");
	}
	
}
