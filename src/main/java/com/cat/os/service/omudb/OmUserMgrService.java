package com.cat.os.service.omudb;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.cat.os.common.bean.BusinessException;
import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.OmUDBConstant;
import com.cat.os.common.constant.OmUDBConstant.OM_ERROR_STATUS;
import com.cat.os.common.constant.ProjectConstant.PARAMETER_CONFIG;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.utils.OsUtils;
import com.cat.os.dao.entity.SysParameterGroup;
import com.cat.os.dao.entity.SysParameterInfo;
import com.cat.os.dao.entity.UAgent;
import com.cat.os.dao.entity.UETerritoryAgent;
import com.cat.os.dao.mysql.SysParameterGroupRepository;
import com.cat.os.dao.omudb.UAgentJdbcRepository;
import com.cat.os.dao.omudb.UAgentRepository;
import com.cat.os.dao.omudb.UETerritoryAgentRepository;
import com.cat.os.dao.vo.OmUserFormVo;
import com.cat.os.dao.vo.UeTerritoryAgentVo;
import com.cat.os.dao.vo.UeTerritoryDropdown;

@Service
@Profile(Profiles.OMUDB)
public class OmUserMgrService {
	private static final Logger log = LoggerFactory.getLogger(OmUserMgrService.class);

	@Autowired
	private UAgentRepository uAgentRepository;

	@Autowired
	private UAgentJdbcRepository uAgentJdbcRepository;
	
	@Autowired
	private UETerritoryAgentRepository ueTerritoryAgentRepository;
	
	@Autowired
	private OmLdapUserMgrService omLdapUserMgrService;
	
	@Autowired
	private SysParameterGroupRepository sysParameterGroupRepository;
	
	private static final List<String> ALL_LDAP_GROUP = new ArrayList<>();

	public DataTableAjax<UAgent> findOmUserByCriteria(OmUserFormVo omUserFormVo) {
		
		DataTableAjax<UAgent> dataTableAjax = new DataTableAjax<UAgent>();
		dataTableAjax.setDraw(omUserFormVo.getDraw()+1);
		
		List<UAgent> res = uAgentJdbcRepository.getUserByCriteria (omUserFormVo);
		Long count = uAgentJdbcRepository.geCounttUserByCriteria(omUserFormVo);
		dataTableAjax.setRecordsFiltered(count);
		dataTableAjax.setRecordsTotal(count);
		dataTableAjax.setData(res);
		return dataTableAjax;
	}

	public OmUserFormVo findUserByAgenId(OmUserFormVo omUserFormVo) {


		UAgent user = uAgentRepository.findOne(omUserFormVo.getAgentId());
		
		OmUserFormVo vo = new OmUserFormVo();
		vo.setAgentId(user.getAgentId());
		vo.setFirstName(user.getFirstName());
		vo.setLastName(user.getLastName());
		vo.setUserName(user.getAgentName());
		vo.setEmpId(user.getEmpId());
		vo.setIsNew(false);
		
		List<UeTerritoryAgentVo> territory = uAgentJdbcRepository.findTerritoryAgent(user.getAgentId());
		if(!territory.isEmpty()) {
			UeTerritoryAgentVo t = territory.get(0);
			vo.setTerritoryAgent(t.geteTerritoryId());
			log.info("geteTerritoryId  : {} " , t.geteTerritoryId());
		}
		
		Assert.hasLength(vo.getUserName(), "getUserName must not be empty");
		//find ldap roles
		List<String> groups = omLdapUserMgrService.findUserGroup(vo.getUserName());
		//exclude default role
		groups.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_BACK_DATE_NEW);
		groups.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_BACK_DATE_OTHERS);
		groups.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_CA_BA);
		groups.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_ORDER);
		groups.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_REPORT);
		vo.setGroupAdd(groups);
		vo.setGroupTempInLdap(groups);
		return vo;
	}
	
	@Transactional("omudbTransactionManager")
	private UAgent updateOrInsertUserAgent(String agentId, String firstName, String lastName, String empId, String userName) throws Exception {
		//**SET agentId null is Insert *//
		UAgent oldUserAgent = null;
		String jobTitle = firstName.concat("   ").concat(lastName);
		UAgent newUserAgent = new UAgent();

		if(StringUtils.isNotBlank(agentId)) {
			oldUserAgent = uAgentRepository.findByAgentIdAndObsoleteFlag(agentId, OmUDBConstant.TERRITORY_FLAG.ACTIVE);
		}
		
		if (oldUserAgent != null) {
			BeanUtils.copyProperties(oldUserAgent, newUserAgent);
			int revision = oldUserAgent.getRevisionNumber() == null ? 0 : oldUserAgent.getRevisionNumber() + 1;
			
			oldUserAgent.setObsoleteFlag(1);
			oldUserAgent.setRevisionNumber(revision);
			uAgentRepository.save(oldUserAgent);

		} else {
			newUserAgent.setUserDataId(OsUtils.generateUUID()); //User data id
			newUserAgent.setEmailAddress("");
			newUserAgent.setEmailAddressUp("");
			newUserAgent.setSelfRegisteredFlag(0);
			newUserAgent.setDownlineSize(0);
			newUserAgent.setObsoleteFlag(0);
		}
		
		newUserAgent.setAgentId(OsUtils.generateUUID());
		newUserAgent.setAgentName(userName);
		newUserAgent.setFirstName(firstName);
		newUserAgent.setLastName(lastName);
		newUserAgent.setEmpId(empId);
		
		newUserAgent.setRevisionNumber(1);
		newUserAgent.setJobTitle(jobTitle);
		newUserAgent.setJobTitleUp(jobTitle);
		
		uAgentRepository.save(newUserAgent);

		log.info("UpdateUserAgent Completed agentId : " + agentId + " , Name : " + jobTitle);
		
		return newUserAgent;
	}
	
	
	
	@Transactional("omudbTransactionManager")
	public void updateUser(OmUserFormVo omUserFormVo) throws Exception {
		UAgent user = uAgentRepository.findOne(omUserFormVo.getAgentId());

		//update User
		boolean difUser = false;
		if(!omUserFormVo.getFirstName().equalsIgnoreCase(user.getFirstName())) {
			difUser = true;
		}else
		if(!omUserFormVo.getLastName().equalsIgnoreCase(user.getLastName())) {
			difUser = true;
		}else
		if(!omUserFormVo.getEmpId().equalsIgnoreCase(user.getEmpId())) {
			difUser = true;
		}
		if(difUser) {
			user = this.updateOrInsertUserAgent(omUserFormVo.getAgentId(), omUserFormVo.getFirstName(),
					omUserFormVo.getLastName(), omUserFormVo.getEmpId(), user.getAgentName());
		}
		
		List<UeTerritoryAgentVo> territory = uAgentJdbcRepository.findTerritoryAgent(omUserFormVo.getAgentId());
		if(!territory.isEmpty()) {
			UeTerritoryAgentVo ueTerritoryAgentVo = territory.get(0);
			log.info("territory : agentId  = " + omUserFormVo.getAgentId());
			log.info("territory : old      = " + ueTerritoryAgentVo.geteTerritoryId());
			log.info("territory : new      = " + omUserFormVo.getTerritoryAgent());
			
			//change when select territory or update user
			if(!omUserFormVo.getTerritoryAgent().equalsIgnoreCase(ueTerritoryAgentVo.geteTerritoryId()) || difUser) {
				log.info("change territory");
				UETerritoryAgent oldTerritory = ueTerritoryAgentRepository.findByAgentIdAndObsoleteFlag(omUserFormVo.getAgentId(), OmUDBConstant.TERRITORY_FLAG.ACTIVE);
				String guid = OsUtils.generateUUID();
				UETerritoryAgent newTerritory = new UETerritoryAgent();
				newTerritory.seteTerritoryAgentId(guid);
				newTerritory.seteTerritoryId(omUserFormVo.getTerritoryAgent());
				newTerritory.setAgentId(user.getAgentId());
				newTerritory.setObsoleteFlag(OmUDBConstant.TERRITORY_FLAG.ACTIVE);
				newTerritory.setRevisionNumber(1);
				newTerritory.setAgentRoleLkp(oldTerritory.getAgentRoleLkp());
				
				//set Inactive
				oldTerritory.setObsoleteFlag(OmUDBConstant.TERRITORY_FLAG.INACTIVE);
				
				ueTerritoryAgentRepository.save(newTerritory);
				ueTerritoryAgentRepository.save(oldTerritory);
				
				log.info("change territory success");
			}
		}
		
		//change group in ldap
		Assert.hasLength(omUserFormVo.getUserName(), "getUserName must not be empty");
		if(!omUserFormVo.getGroupDel().isEmpty()) {
			omLdapUserMgrService.delGroup(omUserFormVo.getUserName(), omUserFormVo.getGroupDel());
		}
		if(!omUserFormVo.getGroupAdd().isEmpty()) {
			List<String> addNew = new ArrayList<>();
			for (String add : omUserFormVo.getGroupAdd()) {
				if(omUserFormVo.getGroupTempInLdap().indexOf(add) == -1) {
					addNew.add(add);
				}
			}
			
			if(!addNew.isEmpty())
				omLdapUserMgrService.addGroup(omUserFormVo.getUserName(), addNew);
		}
		
		//update password if input
		if(StringUtils.isNotBlank(omUserFormVo.getPassword())) {
			log.info("update password ....");
			omLdapUserMgrService.updatePassword(omUserFormVo.getUserName(), omUserFormVo.getPassword());
		}
		
		log.info("update success.");
		
	}
	
	public List<UeTerritoryDropdown> getTerritoryDropdown() {
		return uAgentJdbcRepository.getTerritoryDropdown();
	}

	@Transactional("omudbTransactionManager")
	public void createNewUser(OmUserFormVo omUserFormVo) throws Exception {
		
		Assert.hasLength(omUserFormVo.getUserName(), "getUserName must not be empty");
		Assert.hasLength(omUserFormVo.getFirstName(), "getFirstName must not be empty");
		Assert.hasLength(omUserFormVo.getLastName(), "getLastName must not be empty");
		Assert.hasLength(omUserFormVo.getEmpId(),    "getEmpId must not be empty");
		Assert.hasLength(omUserFormVo.getUserName(), "getUserName must not be empty");
		Assert.hasLength(omUserFormVo.getTerritoryAgent(), "getTerritoryAgent must not be empty");
		
		if(uAgentJdbcRepository.checkDupicateUser(omUserFormVo.getUserName())) {
			throw new BusinessException(OM_ERROR_STATUS.USER_EXIST, omUserFormVo.getUserName() + " มีผู้ใช้งานแล้วในระบบ");
		}
		if(omLdapUserMgrService.checkDuplicatUser(omUserFormVo.getUserName())) {
			log.info("{} : มีผู้ใช้งานแล้วในระบบ LDAP." , omUserFormVo.getUserName());
			throw new BusinessException(OM_ERROR_STATUS.USER_EXIST, omUserFormVo.getUserName() + " มีผู้ใช้งานแล้วในระบบ");
		}
		
		String firstName = omUserFormVo.getFirstName();
		String lastName  = omUserFormVo.getLastName();
		String empId     = omUserFormVo.getEmpId();
		String userName  = omUserFormVo.getUserName();
		UAgent user = this.updateOrInsertUserAgent(null, firstName, lastName, empId, userName);
		
		UETerritoryAgent newTerritory = new UETerritoryAgent();
		newTerritory.seteTerritoryAgentId(user.getAgentId());
		newTerritory.seteTerritoryId(omUserFormVo.getTerritoryAgent());
		newTerritory.setAgentId(user.getAgentId());
		newTerritory.setObsoleteFlag(OmUDBConstant.TERRITORY_FLAG.ACTIVE);
		newTerritory.setRevisionNumber(1);
		newTerritory.setAgentRoleLkp(OmUDBConstant.AGENT_ROLE_LKP);
		ueTerritoryAgentRepository.save(newTerritory);
		
		//Ldap
		omLdapUserMgrService.createUserInLdap(omUserFormVo);
		
		List<String> groups = new ArrayList<>();
		groups.add(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_BACK_DATE_NEW);
		groups.add(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_BACK_DATE_OTHERS);
		groups.add(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_CA_BA);
		groups.add(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_ORDER);
		groups.add(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_REPORT);
		//add from ui
		if(omUserFormVo.getGroupAdd() != null && ! omUserFormVo.getGroupAdd().isEmpty()) {
			groups.addAll(omUserFormVo.getGroupAdd());
		}
		omLdapUserMgrService.addGroup(userName, groups);
		
		log.info("create success");
	}
	
	
	public List<String> getGroupLdap() {
		if(ALL_LDAP_GROUP.isEmpty()) {
			List<String> res = omLdapUserMgrService.findAllGroup();
			ALL_LDAP_GROUP.addAll(res);
			ALL_LDAP_GROUP.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_BACK_DATE_NEW);
			ALL_LDAP_GROUP.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_BACK_DATE_OTHERS);
			ALL_LDAP_GROUP.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_CA_BA);
			ALL_LDAP_GROUP.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_ORDER);
			ALL_LDAP_GROUP.remove(OmUDBConstant.OM_BASIC_LDAP_GROUPS.L_SALES_REPORT);
		}
		
		return ALL_LDAP_GROUP;
	}
	
	@Transactional("transactionManager")
	public String getRunningNumber() {
		SysParameterGroup sysp = sysParameterGroupRepository.findSysParameterGroupByParamGroupCode(PARAMETER_CONFIG.OM_LDAP_UID_NUMBER);
		SysParameterInfo uid = sysp.getParameterInfos().get(0);
		String currentUid = uid.getValue1();
		log.info("getRunningNumber : {} ", currentUid);
		long next = Long.parseLong(currentUid) + 1;
		uid.setValue1(String.valueOf(next));
		
		return currentUid;
	}
}
