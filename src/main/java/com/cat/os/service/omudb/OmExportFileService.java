package com.cat.os.service.omudb;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.entity.UAgent;
import com.cat.os.dao.omudb.UAgentJdbcRepository;
import com.cat.os.dao.vo.OmUserFormVo;
import com.opencsv.CSVWriter;

@Service
@Profile(Profiles.OMUDB)
public class OmExportFileService {
	
	private static final Logger log = LoggerFactory.getLogger(OmExportFileService.class);

	@Autowired
	private UAgentJdbcRepository uAgentJdbcRepository;

	public FileSystemResource exportFile(OmUserFormVo omUserFormVo) throws Exception {
		List<UAgent> uAgentList = uAgentJdbcRepository.getUserByCriteriaExistLimit(omUserFormVo);
		
		if (uAgentList != null && !uAgentList.isEmpty()) {
			File file = File.createTempFile("generate", ".csv");
			log.info(file.getAbsolutePath());
			
			FileWriter fileWriter = new FileWriter(file);

			CSVWriter csvWriter = new CSVWriter(fileWriter,
			        CSVWriter.DEFAULT_SEPARATOR,
			        CSVWriter.NO_QUOTE_CHARACTER,
			        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
			        CSVWriter.DEFAULT_LINE_END);
	            
            String[] headerRecord = {"ชื่อ", "สกุล", "รหัสพนักงาน /รหัสบัตรประชาชน", "Username"};
            csvWriter.writeNext(headerRecord);

            for (UAgent uAgent : uAgentList) {
                csvWriter.writeNext(new String[] {uAgent.getAgentName(), uAgent.getFirstName(), uAgent.getLastName(), uAgent.getEmpId()});
			}
            
            csvWriter.close();
                    
            return new FileSystemResource(file);
		}
		
		return null;
	}
}
