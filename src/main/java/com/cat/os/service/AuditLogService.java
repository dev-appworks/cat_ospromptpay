package com.cat.os.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cat.os.common.constant.UssdConstant;
import com.cat.os.common.constant.UssdConstant.USSD_ACTION;
import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.common.utils.OsUtils;
import com.cat.os.dao.entity.AuditLog;
import com.cat.os.dao.entity.OsWsErrorLog;
import com.cat.os.dao.entity.OsWsLog;
import com.cat.os.dao.entity.ServiceStatus;
import com.cat.os.dao.mysql.AuditLogRepository;
import com.cat.os.dao.mysql.OsWsErrorLogRepository;
import com.cat.os.dao.mysql.OsWsLogRepository;
import com.cat.ussd.performonoffservice.Rq;
import com.cat.ussd.performonoffservice.RqBody;
import com.cat.ussd.performonoffservice.RqHeader;
import com.cat.ussd.performonoffservice.Rs;
import com.cat.ussd.performonoffservice.RsHeader;

@Service
public class AuditLogService {

	private static final Logger log = LoggerFactory.getLogger(AuditLogService.class);

	@Autowired
	private AuditLogRepository auditLogRepository;

	@Autowired
	private OsWsErrorLogRepository osWsErrorLogRepository;

	@Autowired
	private OsWsLogRepository osWsLogRepository;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public AuditLog saveAuditLog(String  userName, ServiceStatus serviceStatus,String status) throws Exception {
		AuditLog auditLog = new AuditLog();
		auditLog.setLogBy(userName);
		auditLog.setLogDatetime(new Date());
		auditLog.setServiceId(serviceStatus.getServiceId());
		auditLog.setServiceName(serviceStatus.getServiceName());
		auditLog.setStatus(status);
		auditLog.setTimer(serviceStatus.getTimer());
		auditLog.setType(UssdConstant.USSD_TYPE);
		auditLog.setUpdatedBy(userName);
		auditLog.setUpdatedDatetime(new Date());
		auditLog.setAction(serviceStatus.getStatus() == UssdConstant.USSD_STATUS.ON ? USSD_ACTION.ACTION_ACTIVE : USSD_ACTION.ACTION_INACTIVE);
		auditLog = auditLogRepository.save(auditLog);
		log.info("Insert AuditLog : LogBy : " + auditLog.getLogBy() + " :: Serivce Id : " + auditLog.getServiceId()
				+ " :: Status : " + auditLog.getStatus() + " :: Timer : " + auditLog.getTimer());
		return auditLog;
	}

	@Transactional
	public AuditLog saveAuditLogUSSD(AuditLog auditLog) throws Exception {
		auditLog.setType(UssdConstant.USSD_TYPE);
		auditLog.setLogDatetime(new Date());
		auditLog = auditLogRepository.save(auditLog);
		log.info("Insert AuditLog : ");
		return auditLog;
	}
	
	public OsWsErrorLog saveOsWsErrorLog(String errorCode, String errorDesc) throws Exception {
		OsWsErrorLog osWsErrorLog = new OsWsErrorLog();
		osWsErrorLog.setErrorCode(errorCode);
		osWsErrorLog.setErrorDesc(errorDesc);
		osWsErrorLog = osWsErrorLogRepository.save(osWsErrorLog);

		return osWsErrorLog;
	}

	public OsWsLog saveOsWsLog(Rq rq, Rs rs, String errorCode, String errorDesc, String type) {
		OsWsLog osWsLog = new OsWsLog();
		RqHeader rqHeader = rq.getRqHeader();
		RqBody rqBody = rq.getRqBody();
		RsHeader rsHeader = rs.getRsHeader();

		try {
			if (!StringUtils.isAllBlank(errorCode, errorDesc)) {
				OsWsErrorLog osWsErrorLog = saveOsWsErrorLog(errorCode, errorDesc);
				log.info("Insert OsWsErrorLog : RequestId : " + rqHeader.getRequestId() + " :: Serivce Id : "
						+ rqBody.getServiceId() + " :: Error Code : " + errorCode + " :: Error Desc : " + errorDesc
						+ " :: Type : " + type);
				if (osWsErrorLog == null || osWsErrorLog.getErrorId() == null) {
					throw new Exception(UssdConstant.E999_CODE);
				}

				osWsLog.setErrorId(osWsErrorLog.getErrorId());
			}

			osWsLog.setLogDateTime(new Date());
			osWsLog.setAuthUserId(OsUtils.getValue(rqHeader.getAuthUserId()));
			osWsLog.setFunctionName(rqHeader.getFunctionName());
			osWsLog.setRequestAppid(rqHeader.getRequestAppId());
			osWsLog.setIpaddress(OsUtils.getValue(rqHeader.getTerminalId()));
			osWsLog.setRequestDatetime(OsDateUtils.convertXMLGregorianCalendarToDate(rqHeader.getRequestDatetime()));
			osWsLog.setRequestId(rqHeader.getRequestId());
			osWsLog.setUserId(rqHeader.getUserId());

			osWsLog.setServiceId(rqBody.getServiceId());
			osWsLog.setStatus(rqBody.getStatus());

			if (rsHeader != null) {
				osWsLog.setResponseId(rqHeader.getRequestId());
				osWsLog.setResponseAppid(rsHeader.getResponseAppId());
				osWsLog.setResponseDatetime(
						OsDateUtils.convertXMLGregorianCalendarToDate(rsHeader.getResponseDatetime()));
				osWsLog.setStatusCode(rsHeader.getStatusCode());
			}

			osWsLog.setType(type);

			osWsLog = osWsLogRepository.save(osWsLog);
			log.info("Insert OsWsLog : RequestId : " + rqHeader.getRequestId() + " :: Serivce Id : "
					+ rqBody.getServiceId() + " :: Type : " + type);
		} catch (Exception e) {
			log.error("Error saveOsWsLog : ", e);
		}

		return osWsLog;
	}
	
	public Page<AuditLog> findByCriteria(String type, String dateFrom, String dateTo, String logBy, 
			String serviceId, String status, Pageable pageable) {
		
		Page<AuditLog> pageRst = null;
		
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<AuditLog> criteriaQuery = criteriaBuilder.createQuery(AuditLog.class);
			Root<AuditLog> rootFrom = criteriaQuery.from(AuditLog.class);
			List<Predicate> predicates = new ArrayList<Predicate>();

			if (StringUtils.isNotBlank(type)) {
				Predicate predicate = criteriaBuilder.equal(rootFrom.get("type").as(String.class), type);
				predicates.add(predicate);
			}
			
			if(StringUtils.isAllBlank(dateFrom, dateTo, logBy, serviceId, status)) {
				String newDate = OsDateUtils.getCalendarDateEnDDMMYYYYFormat();
				Predicate predicate = criteriaBuilder.between(rootFrom.get("logDatetime").as(Date.class), 
						DateUtils.parseDate(newDate, "dd/MM/yyyy"), 
						DateUtils.parseDate(newDate.concat(" 23:59"), "dd/MM/yyyy HH:mm"));
				predicates.add(predicate);
			}
			
			if (StringUtils.isNotBlank(dateFrom)) {
				Predicate predicate = criteriaBuilder.greaterThanOrEqualTo(rootFrom.get("logDatetime").as(Date.class), DateUtils.parseDate(dateFrom, "dd/MM/yyyy"));
				predicates.add(predicate);
			}
			
			if (StringUtils.isNotBlank(dateTo)) {
				Predicate predicate = criteriaBuilder.lessThanOrEqualTo(rootFrom.get("logDatetime").as(Date.class), DateUtils.parseDate(dateTo.concat(" 23:59"), "dd/MM/yyyy HH:mm"));
				predicates.add(predicate);
			}
			
			if (StringUtils.isNotBlank(status)) {
				Predicate predicate = criteriaBuilder.equal(rootFrom.get("status").as(String.class), status);
				predicates.add(predicate);
			}
			
			if (StringUtils.isNotBlank(logBy)) {
				Predicate predicate = criteriaBuilder.like(rootFrom.get("logBy").as(String.class), "%".concat(logBy).concat("%"));
				predicates.add(predicate);
			}

			if (StringUtils.isNotBlank(serviceId)) {
				Predicate predicate = criteriaBuilder.equal(rootFrom.get("serviceId").as(String.class), serviceId);
				predicates.add(predicate);
			}
			
			if (StringUtils.isNotBlank(status)) {
				Predicate predicate = criteriaBuilder.equal(rootFrom.get("status").as(String.class), status);
				predicates.add(predicate);
			}

			criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			
			if(StringUtils.isAllBlank(dateFrom, dateTo, logBy, serviceId, status)) {
				criteriaQuery.orderBy(criteriaBuilder.desc(rootFrom.get("logDatetime")));
			}

			
			// SQL Query object
			TypedQuery<AuditLog> createQuery = entityManager.createQuery(criteriaQuery);

			// Paging parameter
			Integer pageSize = pageable.getPageSize();
			Integer pageNo = pageable.getPageNumber();

			// Count the number of query results
			TypedQuery<AuditLog> createCountQuery = entityManager.createQuery(criteriaQuery);

			// The actual query returns paging objects
			int startIndex = pageSize * pageNo;
			createQuery.setFirstResult(startIndex);
			createQuery.setMaxResults(pageable.getPageSize());
			pageRst = new PageImpl<AuditLog>(createQuery.getResultList(), pageable,
					createCountQuery.getResultList().size());
			log.info("============================ Audit Log ============================");
		} catch (Exception e) {
			log.error("Error findByCriteria : ", e);
		}
		
		return pageRst;
	}
	
	public List<String> getStatusLog(){
		List<String> status = new ArrayList<>();
		status.add(UssdConstant.STATUS_SUCCESS);
		status.add(UssdConstant.STATUS_FAIL);
		return status;
	}
}
