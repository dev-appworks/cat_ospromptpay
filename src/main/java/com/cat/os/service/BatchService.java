package com.cat.os.service;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class BatchService {

	private static final Logger log = LoggerFactory.getLogger(BatchService.class);

	
	public void run() {
		Process p;
		try {
			p = Runtime.getRuntime().exec("ipconfig /all");
			p.waitFor();
			InputStream inps = p.getInputStream();
			String theString = IOUtils.toString(inps); 
			log.info(theString);
		}catch (Exception e) {
			log.error(e.getMessage());
		}
	}
}
