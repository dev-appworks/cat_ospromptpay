package com.cat.os.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cat.os.dao.entity.AdmRoleEntity;
import com.cat.os.dao.mysql.AdmRoleEntityRepository;
import com.cat.os.dao.vo.AdmRoleVo;

@Service
public class AdmRoleEntityService {

	@Autowired
	private AdmRoleEntityRepository admRoleEntityRepository;

	public Page<AdmRoleEntity> findAll(Pageable pageable) {
		return admRoleEntityRepository.findAll(pageable);
	}

	public AdmRoleVo queryRoleById(AdmRoleVo admRoleVo) {
	 AdmRoleEntity role = admRoleEntityRepository.findOne(admRoleVo.getId());
	 
	 admRoleVo.setRoleCode(role.getRoleCode());
	 admRoleVo.setRoleDesc(role.getRoleDesc());
	 
	 return admRoleVo;
	}

	public void save(AdmRoleVo admRoleVo) {
		
		 AdmRoleEntity role = admRoleEntityRepository.findOne(admRoleVo.getId());
		 role.setRoleCode(admRoleVo.getRoleCode());
		 role.setRoleDesc(admRoleVo.getRoleDesc());

		 admRoleEntityRepository.save(role);
	}

	public void insert(AdmRoleVo admRoleVo) {
		 AdmRoleEntity role = new AdmRoleEntity();
		 role.setRoleCode(admRoleVo.getRoleCode());
		 role.setRoleDesc(admRoleVo.getRoleDesc());
		 role.setCreatedBy("MOCK");
		 role.setCreatedDate(new Date());
		 admRoleEntityRepository.save(role);
	}
}
