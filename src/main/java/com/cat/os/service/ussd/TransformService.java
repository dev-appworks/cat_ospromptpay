package com.cat.os.service.ussd;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cat.os.dao.entity.PromptpayReferenceCode;
import com.cat.os.dao.vo.PromptPayReferenceCodeVo;

public class TransformService {

	public PromptpayReferenceCode transformModelToEntity(PromptPayReferenceCodeVo model, PromptpayReferenceCode entity) throws Exception {
		if (model != null) {
            if (entity == null) {
            	entity = new PromptpayReferenceCode();
            } 
            BeanUtils.copyProperties(entity, model); 
           /* if(!CommonService.isNull(model.getUserDataTemplateModel()) && !CommonService.isNull(model.getUserDataTemplateModel().getId())) {
            	UserDataTemplate userDataTemplate = new UserDataTemplate();
                BeanUtils.copyProperties(userDataTemplate, model.getUserDataTemplateModel());
                entity.setUserDataTemplate(userDataTemplate);
            } */
        } 
		
        return entity;
    }

    public List<PromptPayReferenceCodeVo> transformEntityToModel(List<PromptpayReferenceCode> entityList) throws Exception {
        List<PromptPayReferenceCodeVo> modelList = null;
        if (entityList != null || entityList.size() != 0) {
        	modelList = new ArrayList<PromptPayReferenceCodeVo>(); 
            for (PromptpayReferenceCode entity : entityList) {
            	modelList.add(transformEntityToModel(entity, null));
            }
        } 
        
        return modelList;
    }

    public PromptPayReferenceCodeVo transformEntityToModel(PromptpayReferenceCode entity, PromptPayReferenceCodeVo  model) throws Exception { 
        if (entity != null) { 
            if (model == null){
            	model = new PromptPayReferenceCodeVo();
            } 
            BeanUtils.copyProperties(entity, model); 
        }
        
        return model;
    }
    
    public <T> Class<T> transformModelToEntityss(Class<T> model, Class<T> entity) throws Exception {
		if (model != null) {
            if (entity == null) {
            	entity = entity.getClass().newInstance();
            } 
            BeanUtils.copyProperties(entity, model);
        } 
		
        return entity;
    }
}
