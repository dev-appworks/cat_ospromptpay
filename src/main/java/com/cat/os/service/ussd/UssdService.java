package com.cat.os.service.ussd;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cat.os.common.bean.BusinessException;
import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.bean.DataTableOrder;
import com.cat.os.common.constant.DataTableConstant;
import com.cat.os.common.constant.ProjectConstant.ApplicationID;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.UssdConstant;
import com.cat.os.common.constant.UssdConstant.USSD_ACTION;
import com.cat.os.common.constant.UssdConstant.USSD_STATUS;
import com.cat.os.dao.entity.AuditLog;
import com.cat.os.dao.entity.ServiceStatus;
import com.cat.os.dao.entity.ServicesTimer;
import com.cat.os.dao.ussd.ServiceStatusRepository;
import com.cat.os.dao.ussd.ServicesTimerJdbcRepository;
import com.cat.os.dao.ussd.ServicesTimerRepository;
import com.cat.os.dao.vo.PerformOnOffServiceRequest;
import com.cat.os.dao.vo.PerformOnOffServiceResponse;
import com.cat.os.dao.vo.USSDServicesTimerVo;
import com.cat.os.dao.vo.USSDVo;
import com.cat.os.service.ApplicationCacheService;
import com.cat.os.service.AuditLogService;

@Service
@Profile(Profiles.USSD)
public class UssdService {

	private static final Logger log = LoggerFactory.getLogger(UssdService.class);

	@Autowired
	private ServiceStatusRepository serviceStatusRepository;

	@Autowired
	private ApplicationCacheService applicationCacheService;
	
	
	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired(required=false)
	private ServicesTimerRepository servicesTimerRepository;
	
	@PersistenceContext(unitName="system")
	private EntityManager entityManager;
	
	@Autowired(required=false)
	private ServicesTimerJdbcRepository servicesTimerJdbcRepository;
	
	
	@Transactional
	public Page<ServiceStatus> findAll(Pageable pageable) {
		return serviceStatusRepository.findAllByIsDeleted(pageable, UssdConstant.STATUS.ACTIVE);
	}
	
	public List<ServiceStatus> findAll() {
		return serviceStatusRepository.findAllByIsDeleted(UssdConstant.STATUS.ACTIVE);
	}

	@Transactional
	public ServiceStatus save(ServiceStatus serviceStatusVo) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			if (serviceStatusVo != null) {
				if (serviceStatusVo.getId() != null) {
					serviceStatus = serviceStatusRepository.findOne(serviceStatusVo.getId());
					serviceStatus.setServiceId(serviceStatusVo.getServiceId());
					serviceStatus.setStatus(serviceStatusVo.getStatus());
					serviceStatus.setTimer(serviceStatusVo.getTimer());
				} else {
					serviceStatus.setServiceId(serviceStatusVo.getServiceId());
					serviceStatus.setStatus(serviceStatusVo.getStatus());
					serviceStatus.setTimer(serviceStatusVo.getTimer());
				}

				serviceStatus = serviceStatusRepository.save(serviceStatus);
				log.info("Save UssdService : ServiceId : " + serviceStatusVo.getServiceId() + " :: Status : "
						+ serviceStatusVo.getStatus() + " :: Timer : " + serviceStatusVo.getTimer());
			}
		} catch (Exception e) {
			log.error("Error Save UssdSerivce : ", e);
		}

		return serviceStatus;
	}

	public USSDVo findOneById(Integer id) {
		ServiceStatus ussd = serviceStatusRepository.findOne(id);
		USSDVo u = null;
		if (ussd != null) {
			u = new USSDVo();
			u.setId(id);
			u.setCurrentStatus(ussd.getStatus());
			u.setServiceId(ussd.getServiceId());
			u.setServiceName(ussd.getServiceName());
			u.setModBatchConfig("OFF");
			
//			if (ussd.getTimer() != null) {
//				String displayDate = DateFormatUtils.format(ussd.getTimer(), "dd/MM/yyyy");
//				u.setDisplayDate(displayDate);
//				String hh = DateFormatUtils.format(ussd.getTimer(), "HH");
//				String mm = DateFormatUtils.format(ussd.getTimer(), "mm");
//				u.setDisplayHH(hh);
//				u.setDisplayMM(mm);
//				u.setModBatchConfig("ON");
//			} else {
//			}
		}

		return u;
	}

	public PerformOnOffServiceResponse process(PerformOnOffServiceRequest request) throws Exception {
		PerformOnOffServiceResponse response = new PerformOnOffServiceResponse();
		String serviceId = request.getServiceId();
		int status = request.getStatus();

		if (!validatePerformOnOffServiceRequest(request)) {
			throw new Exception(UssdConstant.E999_CODE);
		}
		
		if (!validateStatus(serviceId, status)) {
			if (status == 0) {
				throw new BusinessException(UssdConstant.E002_CODE, applicationCacheService.getUssdErrorCode(UssdConstant.E002_CODE));
			} else {
				throw new BusinessException(UssdConstant.E003_CODE, applicationCacheService.getUssdErrorCode(UssdConstant.E003_CODE));
			}
		}

		updateServiceStatusByServiceId(request);
		
		log.info("UssdService Process Complete: RequestId : " + request.getRequestId() + " :: ServiceId : " + request.getServiceId());
		
		response.setFunctionName(request.getFunctionName());
		response.setRequestId(request.getRequestId());
		response.setResponseAppId(request.getRequestAppId());
		response.setResponseDatetime(new Date());
		response.setResponseId(request.getRequestId());
		response.setServiceId(request.getServiceId());
		response.setStatus(request.getStatus());
		response.setStatusCode(UssdConstant.STATUS_CODE_00);
		
		return response;
	}

	public boolean validateStatus(String serviceId, int status) {
		boolean validFlag = true;

		log.info("Validate Status : ServiceId : " + serviceId + " :: status : " + status);
		if (serviceId != null) {
			ServiceStatus serviceStatus = serviceStatusRepository.findOneByServiceIdAndIsDeleted(serviceId, UssdConstant.STATUS.ACTIVE);
			if (serviceStatus != null && status == serviceStatus.getStatus() && serviceStatus.getTimer() == null) {
				validFlag = false;
			}
		}

		return validFlag;
	}

	@Transactional
	public ServiceStatus updateServiceStatusByServiceId(PerformOnOffServiceRequest request) throws Exception {
		ServiceStatus serviceStatus = new ServiceStatus();

		if (StringUtils.isBlank(request.getServiceId())) {
			throw new BusinessException(UssdConstant.E001_CODE, applicationCacheService.getUssdErrorCode(UssdConstant.E001_CODE));
		}

		serviceStatus = serviceStatusRepository.findOneByServiceIdAndIsDeleted(request.getServiceId(), UssdConstant.STATUS.ACTIVE);
		if (serviceStatus == null) {
			throw new BusinessException(UssdConstant.E001_CODE, applicationCacheService.getUssdErrorCode(UssdConstant.E001_CODE));
		}

		serviceStatus = serviceStatusRepository.findOneByServiceIdAndIsDeleted(request.getServiceId(), UssdConstant.STATUS.ACTIVE);
		serviceStatus.setStatus(request.getStatus());
		
		if(request.getTimer() != null) {
			serviceStatus.setTimer(request.getTimer());
		}else {
			serviceStatus.setTimer(null);
		}
		
		serviceStatus.setUpdatedBy(request.getUserId());
		serviceStatus.setUpdatedDatetime(new Date());
		
		serviceStatus = serviceStatusRepository.save(serviceStatus);
		
		log.info("Update UssdService updateServiceStatusByServiceId : RequestId : " + request.getRequestId() + " ServiceId : " + request.getServiceId());

		return serviceStatus;
	}
	
	public boolean validatePerformOnOffServiceRequest(PerformOnOffServiceRequest request) {
		if(StringUtils.isBlank(request.getFunctionName())) {
			log.info("validatePerformOnOffServiceRequest : FunctionName is Invalid");
			return false;
		}
		
		if(StringUtils.isBlank(request.getRequestId())) {
			log.info("validatePerformOnOffServiceRequest : RequestId is Invalid");
			return false;
		}
		
		if(StringUtils.isBlank(request.getRequestAppId())) {
			log.info("validatePerformOnOffServiceRequest : RequestAppId is Invalid");
			return false;
		}
		
		if(request.getRequestDatetime() == null) {
			log.info("validatePerformOnOffServiceRequest : RequestDateTime is Invalid");
			return false;
		}
		
		if(StringUtils.isBlank(request.getUserId())) {
			log.info("validatePerformOnOffServiceRequest : UserId is Invalid");
			return false;
		}
		
		if(StringUtils.isBlank(request.getServiceId())) {
			log.info("validatePerformOnOffServiceRequest : ServiceId is Invalid");
			return false;
		}
		
		if(!(request.getStatus() == 0 || request.getStatus() == 1)) {
			log.info("validatePerformOnOffServiceRequest : Status is Invalid");
			return false;
		}
		
		return true;
	}
	
	public List<String> getHH(){
		List<String> hh = new ArrayList<>();
		for(int i=0;i<24;i++) {
			hh.add(String.format("%02d", i));
		}
		return hh;
	}
	
	public List<String> getMM(){
		List<String> hh = new ArrayList<>();
		for(int i=0;i<60;i+=5) {
			hh.add(String.format("%02d", i));
		}
		return hh;
	}

	@Transactional
	public String updateUssdStatus(USSDVo ussd, String userName) throws Exception {

		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			
		serviceStatus = serviceStatusRepository.findOneByServiceIdAndIsDeleted(ussd.getServiceId(),UssdConstant.STATUS.ACTIVE);
		
		if("ON".equalsIgnoreCase(ussd.getModBatchConfig())) {
			
			String strDate = ussd.getDisplayDate() + " " + ussd.getDisplayHH() + ":" +  ussd.getDisplayMM() ;
			log.info("Timer : " + strDate);
			Date timer = DateUtils.parseDate(strDate, "dd/MM/yyyy HH:mm");
			log.info("parseDate : " + timer);
//			serviceStatus.setTimer(timer);
//			serviceStatus.setEmail(ussd.getEmail());
			serviceStatus.setUpdatedBy(userName);
			
			
			ServicesTimer st = new ServicesTimer();
			st.setServiceStatusId(serviceStatus.getId());
			st.setTimer(timer);
//			st.setEmail(ussd.getEmail());
			st.setUpdateBy(userName);
			st.setStatus(serviceStatus.getStatus());
			servicesTimerRepository.save(st);
			
			auditLogService.saveAuditLog(userName, serviceStatus,UssdConstant.STATUS_SUCCESS );
			
			return UssdConstant.STATUS_CODE_00;
		}else {
			// no timer
			PerformOnOffServiceRequest request = new PerformOnOffServiceRequest();
			request.setFunctionName("performOnOffService");
			request.setRequestId("WEB" + new Date().getTime());
			request.setRequestAppId(ApplicationID.APPLICATION_OS);
			request.setServiceId(ussd.getServiceId());
			request.setStatus(ussd.getCurrentStatus());
			request.setRequestDatetime(new Date());
			request.setUserId(userName);
			
			PerformOnOffServiceResponse resp = this.process(request);

			if(UssdConstant.STATUS_CODE_00.equalsIgnoreCase(resp.getStatusCode())) {
				auditLogService.saveAuditLog(userName, serviceStatus,UssdConstant.STATUS_SUCCESS );
			}else {
				auditLogService.saveAuditLog(userName, serviceStatus,UssdConstant.STATUS_FAIL);
			}
			
			return UssdConstant.STATUS_CODE_00;
		}
		
		}catch (Exception e) {
			log.error("updateUssdStatus ",e);
			auditLogService.saveAuditLog(userName, serviceStatus,UssdConstant.STATUS_FAIL);
			
			return UssdConstant.E999_CODE;
		}
		
	}
	
	@Transactional
	public void cancelTime(USSDVo ussdVo, String userName) throws Exception {
		ServiceStatus serviceStatus = serviceStatusRepository.findOne(ussdVo.getId());
		serviceStatus.setTimer(null);
		serviceStatus.setUpdatedBy(userName);
		auditLogService.saveAuditLog(userName, serviceStatus,UssdConstant.STATUS_SUCCESS );
	}
	
	public ServiceStatus checkExistServiceStatus(USSDVo ussdVo) {
		return serviceStatusRepository.findOneByServiceIdAndIsDeleted(ussdVo.getServiceId(), UssdConstant.STATUS.ACTIVE);
	}

	@Transactional
	public void createNewService(USSDVo ussdVo, String userName) throws Exception {
		ServiceStatus entity = new ServiceStatus();
		entity.setServiceId(ussdVo.getServiceId());
		entity.setServiceName(ussdVo.getServiceName());
		entity.setStatus(ussdVo.getCurrentStatus());
		entity.setIsDeleted(USSD_STATUS.OFF);
		entity.setUpdatedBy(userName);
		entity.setUpdatedDatetime(new Date());
		serviceStatusRepository.save(entity);
		
		AuditLog audit = new AuditLog();
		audit.setServiceId(ussdVo.getServiceId());
		audit.setServiceName(ussdVo.getServiceName());
		audit.setLogBy(userName);
		audit.setStatus(UssdConstant.STATUS_SUCCESS);
		audit.setAction(USSD_ACTION.ACTION_NEW);
		
		auditLogService.saveAuditLogUSSD( audit );
		
	}

	public void editService(USSDVo ussdVo, String userName) throws Exception {
		ServiceStatus entity = serviceStatusRepository.findOne(ussdVo.getId());
		entity.setServiceName(ussdVo.getServiceName());
		entity.setUpdatedBy(userName);
		entity.setUpdatedDatetime(new Date());
		
		AuditLog audit = new AuditLog();
		audit.setServiceId(entity.getServiceId());
		audit.setServiceName(ussdVo.getServiceName());
		audit.setLogBy(userName);
		audit.setStatus(UssdConstant.STATUS_SUCCESS);
		audit.setAction(USSD_ACTION.ACTION_EDIT);
		
		auditLogService.saveAuditLogUSSD( audit );
		serviceStatusRepository.save(entity);
		
	}

	@Transactional
	public void deleteService(USSDVo ussdVo, String userName) throws Exception {
		ServiceStatus entity = serviceStatusRepository.findOne(ussdVo.getId());
		entity.setUpdatedBy(userName);
		entity.setUpdatedDatetime(new Date());
		entity.setIsDeleted(UssdConstant.STATUS.INACTIVE);
		
		AuditLog audit = new AuditLog();
		audit.setServiceId(entity.getServiceId());
		audit.setServiceName(entity.getServiceName());
		audit.setLogBy(userName);
		audit.setStatus(UssdConstant.STATUS_SUCCESS);
		audit.setAction(USSD_ACTION.ACTION_DELETE);
		
		//delete timer
		List<ServicesTimer> timers = entity.getServicesTimers();
		for (ServicesTimer servicesTimer : timers) {
			//delete Time
			servicesTimerJdbcRepository.deleteServicesTimerById(servicesTimer.getId());

		}
		auditLogService.saveAuditLogUSSD( audit );
		serviceStatusRepository.save(entity);
		
	}
	
	public Page<ServiceStatus> findByCriteria(USSDVo ussd, Pageable pageable) {
		
		Page<ServiceStatus> pageRst = null;
		
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<ServiceStatus> criteriaQuery = criteriaBuilder.createQuery(ServiceStatus.class);
			Root<ServiceStatus> rootFrom = criteriaQuery.from(ServiceStatus.class);
			List<Predicate> predicates = new ArrayList<Predicate>();
			
			if (StringUtils.isNotBlank(ussd.getServiceId())) {
				Predicate predicate = criteriaBuilder.like(rootFrom.get("serviceId").as(String.class), "%".concat(ussd.getServiceId()).concat("%"));
				predicates.add(predicate);
			}
			
			if (StringUtils.isNotBlank(ussd.getServiceName())) {
				Predicate predicate = criteriaBuilder.like(rootFrom.get("serviceName").as(String.class), "%".concat(ussd.getServiceName()).concat("%"));
				predicates.add(predicate);
			}
			
			predicates.add(criteriaBuilder.equal(rootFrom.get("isDeleted").as(Integer.class), UssdConstant.STATUS.ACTIVE));

			criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			
			for (DataTableOrder order : ussd.getOrder()) {
				String columnData = ussd.getColumns().get(order.getColumn()).getData();
				if (DataTableConstant.ORDER.ASC.equals(order.getDir())) {
					criteriaQuery.orderBy(criteriaBuilder.asc(rootFrom.get(columnData)));
				}else {
					criteriaQuery.orderBy(criteriaBuilder.desc(rootFrom.get(columnData)));
				}
			}

			// SQL Query object
			TypedQuery<ServiceStatus> createQuery = entityManager.createQuery(criteriaQuery);
			
			Integer pageSize = pageable.getPageSize();
			Integer pageNo = pageable.getPageNumber();

			// Count the number of query results
			TypedQuery<ServiceStatus> createCountQuery = entityManager.createQuery(criteriaQuery);

			int startIndex = pageSize * pageNo;
			createQuery.setFirstResult(startIndex);
			createQuery.setMaxResults(pageable.getPageSize());
			pageRst = new PageImpl<ServiceStatus>(createQuery.getResultList(), pageable,
					createCountQuery.getResultList().size());
		} catch (Exception e) {
			log.error("Error UssdService findByCriteria : ", e);
		}
		
		return pageRst;
	}
	
	
	
	public DataTableAjax<USSDServicesTimerVo> findServicesTimeByServiceStatusId(USSDVo ussdVo) {
		DataTableAjax<USSDServicesTimerVo> dataTableAjax = new DataTableAjax<USSDServicesTimerVo>();
		dataTableAjax.setDraw(ussdVo.getDraw()+1);
		
		List<USSDServicesTimerVo> res = servicesTimerJdbcRepository.getservicesTimeByServiceStatusId(ussdVo);
		Long count = servicesTimerJdbcRepository.getCountServicesTimeByTask(ussdVo);
		dataTableAjax.setRecordsFiltered(count);
		dataTableAjax.setRecordsTotal(count);
		dataTableAjax.setData(res);
		
		return dataTableAjax;
	}
	
	@Transactional
	public void deleteServiceTimerById(Integer servicesTimerId, Integer serviceStatusId, String userName) throws Exception {
		AuditLog audit = new AuditLog();
		try {
			ServicesTimer ussdTimer = servicesTimerRepository.findOne(servicesTimerId);
			ServiceStatus ussd = serviceStatusRepository.findOne(serviceStatusId);
			
			if (ussd == null) {
				ussd = new ServiceStatus();
				
			}
			
			if (ussdTimer == null) {
				ussdTimer = new ServicesTimer();
			}
			
			audit.setServiceId(ussd.getServiceId());
			audit.setServiceName(ussd.getServiceName());
			audit.setLogBy(userName);
			audit.setAction(UssdConstant.USSD_ACTION.ACTION_CANCEL);
			audit.setStatus(UssdConstant.STATUS_SUCCESS);
			audit.setTimer(ussdTimer.getTimer());
			audit.setEmail(ussdTimer.getEmail());
			auditLogService.saveAuditLogUSSD(audit);

			servicesTimerJdbcRepository.deleteServicesTimerById(servicesTimerId);
		} catch (Exception e) {
			audit.setStatus(UssdConstant.STATUS_FAIL);
			auditLogService.saveAuditLogUSSD(audit);
			throw e;
		}
		
	}
	
	public USSDServicesTimerVo findServicesTimerById(Integer id) {
		ServicesTimer ussdTimer = servicesTimerRepository.findOne(id);
		USSDServicesTimerVo u = null;
		if (ussdTimer != null) {
			u = new USSDServicesTimerVo();
			u.setId(id);
			u.setStatus(ussdTimer.getStatus());
			ServiceStatus ussd = serviceStatusRepository.findOne(ussdTimer.getServiceStatusId());
			if (ussd != null) {
				u.setServiceStatusId(ussd.getId());
				u.setServiceStatusCode(ussd.getServiceId());
				u.setServiceName(ussd.getServiceName());
			}
			
			u.setModBatchConfig("OFF");
			
			if (ussdTimer.getTimer() != null) {
				String displayDate = DateFormatUtils.format(ussdTimer.getTimer(), "dd/MM/yyyy");
				u.setDisplayDate(displayDate);
				String hh = DateFormatUtils.format(ussdTimer.getTimer(), "HH");
				String mm = DateFormatUtils.format(ussdTimer.getTimer(), "mm");
				u.setDisplayHH(hh);
				u.setDisplayMM(mm);
				u.setModBatchConfig("ON");
			} 
			u.setEmail(ussdTimer.getEmail());
			u.setServicesTimerId(ussdTimer.getId());
		}

		return u;
	}
	
	@Transactional
	public void updateUssdTimer(USSDServicesTimerVo ussdTimer, String userName) throws Exception {
		ServicesTimer servicesTimer = new ServicesTimer();
		ServiceStatus serviceStatus = new ServiceStatus();
		AuditLog audit = new AuditLog();
		try {
			serviceStatus = serviceStatusRepository.findOneByServiceIdAndIsDeleted(ussdTimer.getServiceStatusCode(), UssdConstant.STATUS.ACTIVE);
			servicesTimer = servicesTimerRepository.findOne(ussdTimer.getServicesTimerId());
			
			if("ON".equalsIgnoreCase(ussdTimer.getModBatchConfig())) {
				
				String strDate = ussdTimer.getDisplayDate() + " " + ussdTimer.getDisplayHH() + ":" +  ussdTimer.getDisplayMM() ;
				log.info("Timer : " + strDate);
				Date timer = DateUtils.parseDate(strDate, "dd/MM/yyyy HH:mm");
				log.info("parseDate : " + timer);
				
				servicesTimer.setTimer(timer);
			}else {
				servicesTimer.setTimer(null);
			}
			servicesTimer.setEmail(ussdTimer.getEmail());
			servicesTimer.setUpdateBy(userName);
			servicesTimer.setStatus(ussdTimer.getStatus());
			servicesTimer.setUpdateDatetime(new Date());
			
			audit.setServiceId(serviceStatus.getServiceId());
			audit.setServiceName(serviceStatus.getServiceName());
			audit.setLogBy(userName);
			audit.setAction(UssdConstant.USSD_ACTION.ACTION_EDIT);
			audit.setStatus(UssdConstant.STATUS_SUCCESS);
			audit.setTimer(ussdTimer.getTimer());
			audit.setEmail(ussdTimer.getEmail());
			servicesTimerRepository.save(servicesTimer);

			auditLogService.saveAuditLogUSSD(audit);
		} catch (Exception e) {
			audit.setStatus(UssdConstant.STATUS_FAIL);
			auditLogService.saveAuditLogUSSD(audit);
			throw e;
		}
	}
	
}
