package com.cat.os.service.ussd;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

import java.security.MessageDigest;
import java.security.PrivateKey;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.PromptPayConstant;
import com.cat.os.config.security.CustomUserPrincipal;
import com.cat.os.dao.entity.CatCrmMobileInfo;
import com.cat.os.dao.entity.PromptpayConsent;
import com.cat.os.dao.entity.PromptpayMapping;
import com.cat.os.dao.entity.PromptpayReferenceCode;
import com.cat.os.dao.promptpay.CatCrmMobileInfoRepository;
import com.cat.os.dao.promptpay.PromptPayConsentJdbcRepository;
import com.cat.os.dao.promptpay.PromptPayConsentRepository;
import com.cat.os.dao.promptpay.PromptPayMappingRepository;
import com.cat.os.dao.promptpay.PromptPayReferenceCodeJdbcRepository;
import com.cat.os.dao.promptpay.PromptPayReferenceCodeRepository;
import com.cat.os.dao.vo.PromptPayConsentVo;
import com.cat.os.dao.vo.PromptPayReferenceCodeVo;
import com.cat.os.service.CommonService;
import com.cat.os.service.SmsService;
import com.cat.ussd.promptpay.ProcessUMBReqInput;
import com.cat.ussd.promptpay.ProcessUMBReqOutput;

import th.co.itmx.otp.KeyManager;
import th.co.itmx.otp.OTPManager;
import th.co.itmx.otp.TelcoIdentifier;

import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.text.SimpleDateFormat;
import java.util.Base64;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

@Service
public class PromptPayService {

	private static final Logger log = LoggerFactory.getLogger(PromptPayService.class);

	@Autowired
	private PromptPayConsentRepository promptPayConsentRepository;

	@Autowired
	private PromptPayReferenceCodeRepository promptPayReferenceCodeRepository;

	@Autowired
	private PromptPayMappingRepository promptPayMappingRepository;

	@Autowired
	private CatCrmMobileInfoRepository catCrmMobileInfoRepository;

	@PersistenceContext(unitName = "crmoracle")
	private EntityManager entityManager;

	@Autowired(required = false)
	private PromptPayConsentJdbcRepository promptPayConsentJdbcRepository;

	@Autowired(required = false)
	private PromptPayReferenceCodeJdbcRepository promptPayRefCodeJdbcRepository;

	@Autowired
	private SmsService smsService;

	private byte[] seed;

	@Value("${ftp.config.host}")
	private String host;

	@Value("${ftp.config.port}")
	private int ports;

	@Value("${ftp.config.username}")
	private String username;

	@Value("${ftp.config.password}")
	private String password;

	@Value("${ftp.config.path}")
	private String path;

	public boolean checkMobileInfo(String msisdn, String idNumber) {

		List<CatCrmMobileInfo> mobileInfo = catCrmMobileInfoRepository.findByMsisdnAndIdNumberAndStatus(msisdn,
				idNumber, PromptPayConstant.MOBILE_INFO_STATUS.ACTIVE);

		return mobileInfo.size() > 0 ? true : false;
	}

	@Transactional
	public String generateRefernceCode(String msisdn, String cid, String smsDate) throws Exception {
		PromptpayReferenceCode referenceCode = getRefCodeActive();

//		log.info("SharedKey = " + referenceCode.getSharedKey());
		String RefText = msisdn + cid + smsDate + referenceCode.getSharedKey();
//		log.info("RefText = " + RefText);

		MessageDigest m = MessageDigest.getInstance("SHA-256");
		byte[] RefDigest = m.digest(RefText.getBytes(StandardCharsets.UTF_8));
		BigInteger RefBigInt = new BigInteger(1, RefDigest); // to big integer
		String RefHex = RefBigInt.toString(16); // to hex string
//		log.info("RefHex  = " + RefHex);
		while (RefHex.length() < 32)
			RefHex = "0" + RefHex; // pad leading zero
		String RefDigit = RefHex.replaceAll("\\D+", ""); // delete non-digits
//		log.info("RefDigit= " + RefDigit);

		if (RefDigit.length() < 6) { // if not 6 digits
			String RefAlpha = RefHex.replaceAll("[0-9]+", ""); // delete digits
			for (int i = 0; i < RefAlpha.length(); i++) // all alphabets
				RefDigit = RefDigit + (RefAlpha.charAt(i) - 49);// convert to digits
		}
		String RefCode = RefDigit.substring(0, 6); // got RefCode
//		log.info("RefCode = " + RefCode);

		return RefCode;
	}

	public static PrivateKey getPrivateKey(String privateKeyPEM) throws Exception {

		String privateKeyPEMR = privateKeyPEM.replace("-----BEGIN PRIVATE KEY-----", " ")
				.replace("-----END PRIVATE KEY-----", " ").replaceAll("\\s", "");

		log.info("String private key after replace >>>>>>>>>"+privateKeyPEMR);

		byte[] privateKeyDER = Base64.getDecoder().decode(privateKeyPEMR);

		/* Add PKCS#8 formatting */
//		    ASN1EncodableVector v = new ASN1EncodableVector();
//		    v.add(new ASN1Integer(0));
//		    ASN1EncodableVector v2 = new ASN1EncodableVector();
//		    v2.add(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.rsaEncryption.getId()));
//		    v2.add(DERNull.INSTANCE);
//		    v.add(new DERSequence(v2));
//		    v.add(new DEROctetString(privateKeyDER));
//		    ASN1Sequence seq = new DERSequence(v);
//		    byte[] privKey = seq.getEncoded("DER");

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(privateKeyDER);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = keyFactory.generatePrivate(spec);

		log.info("private key after generate >>>>>>>>>>>>>>" + privateKey);

		return privateKey;

	}

	@Transactional
	public String genRefCode(String mobile, String nationalID) throws Exception {

		String server = host;
		int port = ports;
		String user = username;
		String pass = password;
		String paths = path;

		FTPClient ftpClient = new FTPClient();
		String otp = null;
		try {

			ftpClient.connect(server, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();

			ftpClient.changeWorkingDirectory(paths);
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date date = new Date();
			String localDateString = dateFormat.format(date);
			Date dateLocal = new SimpleDateFormat("yyyyMMdd").parse(localDateString);

			log.info("localDate>>>>>>>>" + dateLocal);
			
			File fileEn = null;
			File filekey = null;
			String filePri = null;
			byte[] encryptedSeed = null;
			String[] filesName = ftpClient.listNames();/*<---list file name ใน patch FTP---->*/
			if (filesName != null && filesName.length > 0) {  /*<---condition ไฟลต้องไม่เท่ากับ null---->*/
				for (String aFile : filesName) {  /*<---loop แสดง list ของไฟล---->*/
					if (aFile.contains("-") && aFile.contains(".")) {
						String index[] = aFile.split("\\."); /*<---แยก String ของชื่อไฟลให้เป็นแต่ละ index โดยจับจากเครื่องหมาย . ที่กันอย่---->*/
						String dindex[] = aFile.split("\\-"); /*<---แยก String ของชื่อไฟลให้เป็นแต่ละ index โดยจับจากเครื่องหมาย - ที่กันอย่---->*/
						
						String getFileName = index[0];
						log.info("getFileName out of condition>>>>>>>"+getFileName);
						if (aFile.contains("-") && aFile.contains(".enc")) {	/*<---อ่านชื่อไฟล ENC---->*/					
							String startDateEncString = dindex[3]; /*<---อ่านชื่อไฟลและเอาเฉพาะ index ตัวที่ 1---->*/
							Date startDateEnc = new SimpleDateFormat("yyyyMMdd").parse(startDateEncString);
							String EndDateEncString = dindex[4]; /*<---อ่านชื่อไฟลและเอาเฉพาะ index ตัวที่ 2---->*/
							Date EndDateEnc = new SimpleDateFormat("yyyyMMdd").parse(EndDateEncString);

							if (((startDateEnc.equals(dateLocal))||(startDateEnc.before(dateLocal))) && ((EndDateEnc.after(dateLocal)||(EndDateEnc.equals(dateLocal))))) {	/*<---ช่วงของเวลา Start date ถึง End date เที่ยบกับ Current time---->*/							
								log.info("ENC File Name>>>>>>>"+getFileName);
								
								/*<---อ่านไฟล ENC จาก FTP server---->*/
								fileEn = File.createTempFile(getFileName, ".enc");  /*<---สร้าง tempt file---->*/
								FileOutputStream outputEn = new FileOutputStream(fileEn); /*<---กำหนด output stream---->*/
								ftpClient.retrieveFile(getFileName+".enc", outputEn); /*<---อ่านไฟลจาก patch ftp ลงไฟล tempt---->*/
								encryptedSeed = FileUtils.readFileToByteArray(fileEn);
								fileEn.delete();								
								/*<---อ่านไฟล ENC จาก FTP server---->*/
								
							} else {
								log.info("ENC File Somethig Fail!");
							}

						} else if (aFile.contains("-") && aFile.contains(".pem")) {	/*<---อ่านชื่อไฟล PEM---->*/	
							String startDatePemString = dindex[3];
							Date startDatePem = new SimpleDateFormat("yyyyMMdd").parse(startDatePemString);
							String EndDatePemString = dindex[4];
							Date EndDatePem = new SimpleDateFormat("yyyyMMdd").parse(EndDatePemString);
							if ((((startDatePem.equals(dateLocal))||(startDatePem.before(dateLocal))) && ((EndDatePem.after(dateLocal))||(EndDatePem.equals(dateLocal))))) {
								log.info("PEM File Name>>>>>>>"+getFileName);
								
								/*<---อ่านไฟล PEM จาก FTP server---->*/
								filekey = File.createTempFile(getFileName, ".pem");
								FileOutputStream output = new FileOutputStream(filekey);
								ftpClient.retrieveFile(getFileName+".pem", output);
								filePri = FileUtils.readFileToString(filekey);
								filekey.delete();
								/*<---อ่านไฟล PEM จาก FTP server---->*/
								
							} else {
								log.info("PEM File Somethig Fail!");
							}
						}
					} else {
						log.info("Fail formate!");
						return null;
					}
				}
			} else {
				log.info("File not found!");
				return null;
			}

//			File filekey = File.createTempFile("privatekey", ".pem");
//			FileOutputStream output = new FileOutputStream(filekey);
//			ftpClient.retrieveFile(privatekey, output);

//			File fileEn = File.createTempFile("telco_key", ".enc");
//			FileOutputStream outputEn = new FileOutputStream(fileEn);
//			ftpClient.retrieveFile(encrypted, outputEn);

//			byte[] encryptedSeed = FileUtils.readFileToByteArray(fileEn);

//			String filePri = FileUtils.readFileToString(filekey);

			KeyManager km = getRefCodeActiveLib(encryptedSeed);

			PrivateKey pri = getPrivateKey(filePri);

			byte[] seed = km.decrypt(encryptedSeed, pri);

			OTPManager manager = OTPManager.newInstance();

			long currentEpoch = Instant.now().toEpochMilli();
			otp = manager.generateRefCodeByDay(seed, currentEpoch, nationalID, mobile, TelcoIdentifier.CAT);

			log.info("OTP Password final >>>>>>>"+otp);
			
			ftpClient.disconnect();
		} catch (Exception ex) {
			log.info("Oops! Something wrong happened");
			ex.printStackTrace();
			ftpClient.disconnect();
		}
		return otp;
	}

	@Transactional
	public KeyManager getRefCodeActiveLib(byte[] encryptedSeed) {

		KeyManager km = KeyManager.newInstance();

		km.validateExpiry(encryptedSeed);

		log.info("Encrypted Start Date: " + km.getStartDate(encryptedSeed) + ", Encrypted End Date: " + km.getEndDate(encryptedSeed));

		return km;

	}

	public PromptpayConsent findOne(int id) {
		return promptPayConsentRepository.findOne(id);
	}

	@Transactional
	public PromptpayConsent save(PromptpayConsent consent) {
		PromptpayConsent promptpayConsent = new PromptpayConsent();
		try {
			if (consent != null) {
				if (consent.getId() != null) {
					promptpayConsent = promptPayConsentRepository.findOne(consent.getId());
				} else {
					promptpayConsent.setId(promptPayConsentRepository.getNextId());
				}
				promptpayConsent.setSystemName(consent.getSystemName());
				promptpayConsent.setMsisdn(consent.getMsisdn());
				promptpayConsent.setCid(consent.getCid());
				promptpayConsent.setInpf01(consent.getInpf01());
				promptpayConsent.setReferenceCode(consent.getReferenceCode());
				promptpayConsent.setStatus(consent.getStatus());
				promptpayConsent.setDescription(consent.getDescription());
				promptpayConsent.setUpdatedBy(consent.getUpdatedBy());
				promptpayConsent.setUpdatedByName(consent.getUpdatedByName());
				promptpayConsent.setUpdatedDate(consent.getUpdatedDate());

				promptpayConsent = promptPayConsentRepository.save(promptpayConsent);
				log.info("Save PromptPay Consent : Status : " + consent.getStatus());
			}
		} catch (Exception e) {
			log.error("Error Save PromptPay Consent : ", e);
		}

		return promptpayConsent;
	}

	@Transactional
	public PromptpayReferenceCode save(PromptPayReferenceCodeVo refCodeVo, CustomUserPrincipal user) throws Exception {
		PromptpayReferenceCode refCode = new PromptpayReferenceCode();
		try {
			if (refCodeVo != null) {

				if (refCodeVo.getId() != null) {
					refCode = promptPayReferenceCodeRepository.findOne(refCodeVo.getId());
					refCode.setUpdatedBy(user.getUsername());
					refCode.setUpdatedByName(user.getFullName());
					refCode.setUpdatedDate(new Date());

				} else {
					refCode.setId(promptPayReferenceCodeRepository.getNextId());
//					refCode.setIsActive(PromptPayConstant.IS_ACTIVE.INACTIVE);
					refCode.setIsDeleted(PromptPayConstant.IS_DELETED.ACTIVE);
					refCode.setCreatedBy(user.getUsername());
					refCode.setCreatedByName(user.getFullName());
					refCode.setCreatedDate(new Date());

				}

				refCode.setSharedKey(refCodeVo.getSharedKey().trim());
				refCode.setStatus(refCodeVo.getStatus());

				if (StringUtils.isAllBlank(refCodeVo.getDateFrom(), refCodeVo.getDateTo())) {
					refCode.setStartDate(refCodeVo.getStartDate());
					refCode.setEndDate(refCodeVo.getEndDate());
				} else {
					refCode.setStartDate(DateUtils.parseDate(refCodeVo.getDateFrom(), "dd/MM/yyyy HH:mm:ss"));
					refCode.setEndDate(DateUtils.parseDate(refCodeVo.getDateTo(), "dd/MM/yyyy HH:mm:ss"));
//					refCode.setStartDate(DateUtils.parseDate(refCodeVo.getDateFrom().concat(" 00:00:01"), "dd/MM/yyyy HH:mm:ss"));
//					refCode.setEndDate(DateUtils.parseDate(refCodeVo.getDateTo().concat(" 23:59:59"), "dd/MM/yyyy HH:mm:ss"));
				}

				refCode = promptPayReferenceCodeRepository.save(refCode);

				log.info("Save PromptPayService : Shared Key : " + refCodeVo.getSharedKey());
			}
		} catch (Exception e) {
			log.error("Error Save PromptPayService RefCode : ", e);
			throw new Exception(e.getMessage());
		}

		return refCode;
	}

	@Transactional
	public PromptpayReferenceCode delete(Integer id, CustomUserPrincipal user) throws Exception {
		PromptpayReferenceCode refCode = new PromptpayReferenceCode();

		refCode = promptPayReferenceCodeRepository.findOne(id);
		refCode.setIsDeleted(PromptPayConstant.IS_DELETED.INACTIVE);
		refCode.setUpdatedBy(user.getUsername());
		refCode.setUpdatedByName(user.getFullName());
		refCode.setUpdatedDate(new Date());

		refCode = promptPayReferenceCodeRepository.save(refCode);

		log.debug("Delete PromptPayService ");

		return refCode;
	}

	public DataTableAjax<PromptPayConsentVo> findByCriteria(PromptPayConsentVo consentVo) {

		DataTableAjax<PromptPayConsentVo> dataTableAjax = new DataTableAjax<PromptPayConsentVo>();
		dataTableAjax.setDraw(consentVo.getDraw() + 1);

		List<PromptPayConsentVo> res = promptPayConsentJdbcRepository.getPromptpayConsent(consentVo);
		Long count = promptPayConsentJdbcRepository.getPromptpayConsentCount(consentVo);
		dataTableAjax.setRecordsFiltered(count);
		dataTableAjax.setRecordsTotal(count);

		String barFormat = promptPayMappingRepository.findValue(PromptPayConstant.MAPPING.MASK_ID_CARD,
				PromptPayConstant.STATUS.ACTIVE, PromptPayConstant.IS_DELETED.ACTIVE);

		try {
			for (PromptPayConsentVo vo : res) {
				vo.setCid(CommonService.maskString(vo.getCid(), barFormat));
			}
		} catch (Exception e) {
			log.error("Consent findByCriteria > ", e);
		}

		dataTableAjax.setData(res);

		return dataTableAjax;
	}

	public DataTableAjax<PromptPayReferenceCodeVo> findByCriteria(PromptPayReferenceCodeVo refCodeVo) {

		DataTableAjax<PromptPayReferenceCodeVo> dataTableAjax = new DataTableAjax<PromptPayReferenceCodeVo>();
		dataTableAjax.setDraw(refCodeVo.getDraw() + 1);

		List<PromptPayReferenceCodeVo> res = promptPayRefCodeJdbcRepository.getPromptpayRefCodePage(refCodeVo);
		Long count = promptPayRefCodeJdbcRepository.getPromptpayRefCodeCount(refCodeVo);
		dataTableAjax.setRecordsFiltered(count);
		dataTableAjax.setRecordsTotal(count);
		dataTableAjax.setData(res);

		return dataTableAjax;
	}

	public PromptPayReferenceCodeVo findOneById(Integer id) {
		PromptpayReferenceCode refCode = promptPayReferenceCodeRepository.findOne(id);
		PromptPayReferenceCodeVo refCodeVo = null;
		if (refCode != null) {
			refCodeVo = new PromptPayReferenceCodeVo();
			BeanUtils.copyProperties(refCode, refCodeVo);
		}

		return refCodeVo;
	}

	public List<PromptPayReferenceCodeVo> findCriteria(PromptPayReferenceCodeVo refCodeVo) throws Exception {
		return promptPayRefCodeJdbcRepository.getPromptpayRefCode(refCodeVo);
	}

	public List<PromptpayReferenceCode> findDateOverlap(String startDate, Integer isDeleted) throws Exception {
		return promptPayReferenceCodeRepository.findDateOverlap(startDate, isDeleted);
	}

	@Transactional
	public PromptpayReferenceCode getRefCodeActive() throws Exception {

		autoUpdate();
		List<PromptpayReferenceCode> referenceCodeList = promptPayReferenceCodeRepository
				.findByStatusAndIsDeleted(PromptPayConstant.STATUS.ACTIVE, PromptPayConstant.IS_DELETED.ACTIVE);
		PromptpayReferenceCode referenceCode = null;
		if (referenceCodeList.size() > 0) {
			referenceCode = referenceCodeList.get(0);

			if (!(referenceCode.getStartDate().before(Calendar.getInstance().getTime())
					&& referenceCode.getEndDate().after(Calendar.getInstance().getTime()))) {
				throw new Exception("ไม่มี Shared Key ที่อยู่ในช่วงเวลาและสถานะเปิดใช้งานอยู่ในระบบ");
			}
		} else {
			throw new Exception("ไม่มี Shared Key เปิดใช้งานอยู่ในระบบ");
		}

		return referenceCode;
	}

	@Transactional
	public PromptpayReferenceCode autoUpdate() throws Exception {
		PromptpayReferenceCode referenceCode = null;

		List<String> status = new ArrayList<String>();
		status.add(PromptPayConstant.STATUS.ACTIVE);
		status.add(PromptPayConstant.STATUS.PENDING);
		List<PromptpayReferenceCode> refCodeList = promptPayReferenceCodeRepository.findByStatus(status,
				PromptPayConstant.IS_DELETED.ACTIVE);
		if (refCodeList.size() > 0) {
			referenceCode = new PromptpayReferenceCode();
			for (PromptpayReferenceCode promptpayReferenceCode : refCodeList) {

				referenceCode = promptpayReferenceCode;
				referenceCode.setUpdatedBy("SYSTEM");
				referenceCode.setUpdatedByName("SYSTEM");
				referenceCode.setUpdatedDate(new Date());

				if (referenceCode.getStartDate().before(Calendar.getInstance().getTime())
						&& referenceCode.getEndDate().after(Calendar.getInstance().getTime())) {
					referenceCode.setStatus(PromptPayConstant.STATUS.ACTIVE);
					referenceCode = promptPayReferenceCodeRepository.save(referenceCode);
				}

				if (referenceCode.getEndDate().before(Calendar.getInstance().getTime())) {
					referenceCode.setStatus(PromptPayConstant.STATUS.INACTIVE);
					referenceCode = promptPayReferenceCodeRepository.save(referenceCode);
				}
			}
		}

		return referenceCode;
	}

	public Long checkRefCodeActive() throws Exception {
		return Long.valueOf(promptPayReferenceCodeRepository
				.findByStatusAndIsDeleted(PromptPayConstant.STATUS.ACTIVE, PromptPayConstant.IS_DELETED.ACTIVE).size());
	}

	public PromptPayReferenceCodeVo getLastEnd() throws Exception {
		List<PromptPayReferenceCodeVo> referenceCodeVo = promptPayRefCodeJdbcRepository.getLastEnd();
		return referenceCodeVo.size() > 0 ? referenceCodeVo.get(0) : null;
	}

//	@Async
	public ProcessUMBReqOutput provider(ProcessUMBReqInput processUMBReqInput) {

		ProcessUMBReqOutput processUMBReqOutput = new ProcessUMBReqOutput();

		Map<String, String> mapping = new HashMap<String, String>();
		List<PromptpayMapping> mappingList = promptPayMappingRepository.findValue(PromptPayConstant.STATUS.ACTIVE,
				PromptPayConstant.IS_DELETED.ACTIVE);
		for (PromptpayMapping promptpayMapping : mappingList) {
			mapping.put(promptpayMapping.getName(), promptpayMapping.getValue());
		}

		PromptpayConsent consent = null;
		String refCode = "";
		String smsMessage = "";
		String sourceAddr = mapping.get(PromptPayConstant.MAPPING.SOURCE_ADDR);
		String destAddr = "";
		String date = DateFormatUtils.format(Calendar.getInstance().getTime(), "dd/MM/yy", new Locale("th", "TH"));
		String time = DateFormatUtils.format(Calendar.getInstance().getTime(), "HH:mm");
		String msisdn = "";
		String idCard = "";
		String idCardMask = "";
		String idCardLibFormat = "";
		String msisdnLib = "";

		String smsSuccessMatch = mapping.get(PromptPayConstant.MAPPING.SMS_SUCCESS_MATCH);
		String smsSuccessNotMatch = mapping.get(PromptPayConstant.MAPPING.SMS_SUCCESS_NOT_MATCH);
		String smsFail = mapping.get(PromptPayConstant.MAPPING.SMS_FAIL);
		String markMsisdnFormat = mapping.get(PromptPayConstant.MAPPING.MASK_MOBILE_NO);
		String markIdnoFormat = mapping.get(PromptPayConstant.MAPPING.MASK_ID_CARD);
		String markIdnoLibFormat = mapping.get(PromptPayConstant.MAPPING.ID_CARD_LIB);
		String mobileLib = mapping.get(PromptPayConstant.MAPPING.MOBILE_LIB);
		String smsIdCardLess = mapping.get(PromptPayConstant.MAPPING.SMS_IDCARD_LESS_13);
		String smsIdCardMore = mapping.get(PromptPayConstant.MAPPING.SMS_IDCARD_MORE_13);

		try {
			processUMBReqOutput.setMsgCode(PromptPayConstant.WS.SUCCESS);
			String[] splitUSSD = processUMBReqInput.getInpF01().split("\\*");
			idCard = splitUSSD[splitUSSD.length - 1].replace("#", "");
			destAddr = processUMBReqInput.getMsisdn();

			idCardMask = CommonService.maskString(idCard, markIdnoFormat);
			idCardLibFormat = CommonService.maskString(idCard, markIdnoLibFormat);

			msisdnLib = CommonService.maskString(destAddr, mobileLib);

//			System.out.println("ID Card>>>>>>>>>>>>" + idCard);
//			System.out.println("idCardLibFormat>>>>>>>>>>>>" + idCardLibFormat);
//			System.out.println("idCardMask>>>>>>>>>>>>" + idCardMask);
//			System.out.println("Mobile>>>>>>>>>>>>" + destAddr);
//			System.out.println("msisdnLib>>>>>>>>>>>>" + msisdnLib);
//			System.out.println("time---------------->" + time);

			try {

				if (idCard.length() < 13) {
//					processUMBReqOutput.setMsgDesc("คุณกดหมายเลขบัตรประชาชนไม่ครบ 13 หลัก กรุณาทำรายการใหม่");
					processUMBReqOutput.setMsgDesc(smsIdCardLess);
				}

				else if (idCard.length() > 13) {
//					processUMBReqOutput.setMsgDesc("คุณกดหมายเลขบัตรประชาชนเกิน 13 หลัก กรุณาทำรายการใหม่");
					processUMBReqOutput.setMsgDesc(smsIdCardMore);
				}

				else if (!CommonService.isNumbericDigit(destAddr, 11)) {
					processUMBReqOutput.setMsgDesc("หมายเลยโทรศัพท์ ไม่มีข้อมูลอยู่ในระบบ กรุณาติดต่อเจ้าหน้าที่");
				} else {

					msisdn = CommonService.maskString(destAddr.replaceFirst("66", "0"), markMsisdnFormat);
					idCardMask = CommonService.maskString(idCard, markIdnoFormat);
					idCardLibFormat = CommonService.maskString(idCard, markIdnoLibFormat);

					smsSuccessNotMatch = smsSuccessNotMatch.replace("{date}", date);
					smsSuccessNotMatch = smsSuccessNotMatch.replace("{id_card}", idCardMask);
					smsSuccessNotMatch = smsSuccessNotMatch.replace("{mobile_no}", msisdn);
					smsSuccessNotMatch = smsSuccessNotMatch.replace("{time}", time);

					if (!checkMobileInfo(destAddr.replaceFirst("66", ""), idCard)) {

						smsMessage = smsSuccessNotMatch;
						processUMBReqOutput.setMsgDesc(smsSuccessNotMatch);

					} else {
						refCode = genRefCode(msisdnLib, idCardLibFormat);

						if(refCode == null){						
							processUMBReqOutput.setMsgDesc(smsFail); /*ในกรณีที่ไม่พบไฟลใน FTP Server*/
							processUMBReqOutput.setOutF01("'Secret key' or 'Private key' not found!");
						}else if (StringUtils.isBlank(refCode)) {

							smsMessage = smsSuccessNotMatch;
							processUMBReqOutput.setMsgDesc(smsMessage);

						} else {

							smsMessage = smsSuccessMatch;
							smsMessage = smsMessage.replace("{reference_code}", refCode);
							smsMessage = smsMessage.replace("{date}", date);
							smsMessage = smsMessage.replace("{time}", time);
							smsMessage = smsMessage.replace("{id_card}", idCardMask);
							smsMessage = smsMessage.replace("{mobile_no}", msisdn);

							processUMBReqOutput.setMsgDesc(smsMessage);
						}
					}
				}

			} catch (Exception e) {
				processUMBReqOutput.setMsgCode(PromptPayConstant.WS.ERROR);
				smsMessage = smsFail;
//				processUMBReqOutput.setMsgDesc(smsMessage + " : " + e.getMessage());
				processUMBReqOutput.setMsgDesc(smsMessage);
				processUMBReqOutput.setOutF01(e.getMessage());
				e.printStackTrace();
				log.error("PromptPayServiceProvider >> " + e.getMessage());
			}

//			try {
//				smsService.sendSMS(sourceAddr, destAddr, smsMessage);
//			} catch (Exception e) {
//				processUMBReqOutput.setMsgCode(PromptPayConstant.WS.ERROR);
//				processUMBReqOutput.setMsgDesc(e.getMessage());
//				e.printStackTrace();
//				log.error("PromptPayServiceProvider SMS >> " + e.getMessage());
//			}

			consent = new PromptpayConsent();
			consent.setSystemName(processUMBReqInput.getCpurl());
			consent.setMsisdn(processUMBReqInput.getMsisdn());
			consent.setCid(idCard);
			consent.setInpf01(processUMBReqInput.getInpF01());
			consent.setReferenceCode(refCode);
			consent.setStatus(processUMBReqOutput.getMsgCode());

			if (StringUtils.isBlank(processUMBReqOutput.getOutF01()))
				consent.setDescription(processUMBReqOutput.getMsgDesc());
			else
				consent.setDescription(processUMBReqOutput.getMsgDesc() + " >>> " + processUMBReqOutput.getOutF01());

			consent.setUpdatedBy("SYSTEM");
			consent.setUpdatedByName("SYSTEM");
			consent.setUpdatedDate(new Date());
			consent = save(consent);

		} catch (Exception e) {
			processUMBReqOutput.setMsgCode(PromptPayConstant.WS.ERROR);
			processUMBReqOutput.setMsgDesc(e.getMessage());
			e.printStackTrace();
			log.error("PromptPayServiceProvider >> " + e.getMessage());
		}

		return processUMBReqOutput;
	}

}