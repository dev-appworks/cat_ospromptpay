package com.cat.os.service.ussd;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.constant.ProjectConstant.ApplicationID;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.UssdConstant;
import com.cat.os.dao.entity.ServiceStatus;
import com.cat.os.dao.entity.ServicesTimer;
import com.cat.os.dao.ussd.ServiceStatusRepository;
import com.cat.os.dao.ussd.ServicesTimerJdbcRepository;
import com.cat.os.dao.ussd.ServicesTimerRepository;
import com.cat.os.dao.vo.PerformOnOffServiceRequest;
import com.cat.os.dao.vo.PerformOnOffServiceResponse;
import com.cat.os.service.AuditLogService;

@Service
@Profile(Profiles.USSD)
public class UssdAutoOnOffJobService {

	private static final Logger log = LoggerFactory.getLogger(UssdAutoOnOffJobService.class);

	@Autowired
	private ServicesTimerRepository servicesTimerRepository;

	@Autowired
	private ServicesTimerJdbcRepository servicesTimerJdbcRepository;

	@Autowired
	private ServiceStatusRepository serviceStatusRepository;

	@Autowired
	private UssdService ussdService;
	
	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private UssdEmailSendMailService ussdEmailSendMailService;

	public void runUssdAuto() {

		Calendar cc = Calendar.getInstance();
		cc.add(Calendar.MINUTE, -1);
		Date currentDate = cc.getTime();
		Calendar shiftTime = Calendar.getInstance();
		shiftTime.add(Calendar.MINUTE, 1);
		Date currentDateshiftTime = shiftTime.getTime();

		log.info("Now : => " + currentDate);
		log.info("ShifTime : => " + currentDateshiftTime);
		List<ServicesTimer> res = servicesTimerRepository.findByTimerGreaterThanEqualAndTimerLessThanEqual(currentDate,currentDateshiftTime);
		log.info("find => " + res.size());
		for (ServicesTimer servicesTimer : res) {
			
			ServiceStatus serviceStatus = serviceStatusRepository.findOne(servicesTimer.getServiceStatusId());
			log.info("Begin chage status id : {} , to status {} ",servicesTimer.getId() , servicesTimer.getStatus());
			
			PerformOnOffServiceRequest request = new PerformOnOffServiceRequest();
			request.setFunctionName("performOnOffService");
			request.setRequestId(UssdConstant.USSD_BATCH_PROCESS + new Date().getTime());
			request.setRequestAppId(ApplicationID.APPLICATION_OS);
			request.setStatus(servicesTimer.getStatus());
			request.setServiceId(serviceStatus.getServiceId());
			request.setRequestDatetime(new Date());
			request.setUserId(UssdConstant.USSD_BATCH_PROCESS);
			
			try {
				PerformOnOffServiceResponse resp = ussdService.process(request);
				
				log.info("Auto complete =>  " + resp.getStatusCode());
				
				if(UssdConstant.STATUS_CODE_00.equalsIgnoreCase(resp.getStatusCode())) {
					auditLogService.saveAuditLog(UssdConstant.USSD_BATCH_PROCESS, serviceStatus, UssdConstant.STATUS_SUCCESS );
				}else {
					auditLogService.saveAuditLog(UssdConstant.USSD_BATCH_PROCESS, serviceStatus, UssdConstant.STATUS_FAIL);
				}
				//send Mail success
				ussdEmailSendMailService.sendErrorMail(serviceStatus, servicesTimer , UssdConstant.STATUS_SUCCESS);
			} catch (Exception e) {
				try {
					auditLogService.saveAuditLog(UssdConstant.USSD_BATCH_PROCESS, serviceStatus, UssdConstant.STATUS_FAIL);
				} catch (Exception e1) {
					log.error("ussdService.process" , e1);
				}
				log.error("ussdService.process" , e);
				
				//send Mail error
				ussdEmailSendMailService.sendErrorMail(serviceStatus, servicesTimer , e.getMessage());
			}
			
			//remove when run job
			log.info("deleteServicesTimerById : {}" , servicesTimer.getId());
			servicesTimerJdbcRepository.deleteServicesTimerById(servicesTimer.getId());
			
		}
	}

}
