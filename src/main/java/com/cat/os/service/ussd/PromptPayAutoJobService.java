package com.cat.os.service.ussd;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.constant.ProjectConstant.Profiles;

@Service
@Profile(Profiles.PROMPTPAY)
public class PromptPayAutoJobService {

	private static final Logger log = LoggerFactory.getLogger(PromptPayAutoJobService.class);

	@Autowired
	private PromptPayService promptPayService;
	
	public void runAuto() {
		try {
			Calendar cc = Calendar.getInstance();
//			cc.add(Calendar.MINUTE, -1);
			Date currentDate = cc.getTime();
			Calendar shiftTime = Calendar.getInstance();
//			shiftTime.add(Calendar.MINUTE, 1);
			Date currentDateshiftTime = shiftTime.getTime();
	
			log.info("Now : => " + currentDate);
			log.info("ShifTime : => " + currentDateshiftTime);
			
			//=============================================================================================================
			
			promptPayService.autoUpdate();

		} catch (Exception e) {
			log.error("PromptPayAutoJobService Error", e);
		}

	}

}
