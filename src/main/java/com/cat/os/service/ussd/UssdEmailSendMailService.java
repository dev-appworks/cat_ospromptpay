package com.cat.os.service.ussd;

import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.UssdConstant;
import com.cat.os.common.constant.UssdConstant.USSD_ACTION;
import com.cat.os.dao.entity.EmailTemplate;
import com.cat.os.dao.entity.ServiceStatus;
import com.cat.os.dao.entity.ServicesTimer;
import com.cat.os.dao.mysql.EmailTemplateRepository;

@Service
@Profile(Profiles.USSD)
public class UssdEmailSendMailService {

	private static final Logger log = LoggerFactory.getLogger(UssdEmailSendMailService.class);

	
	@Autowired
	private JavaMailSenderImpl ussdEmailServer;
	
	@Autowired
	private SpringTemplateEngine emailTemplateEngine;

	@Autowired
	private EmailTemplateRepository emailTemplateRepository;
	
	@Async
	public void sendErrorMail(ServiceStatus serviceStatus, ServicesTimer servicesTimer , String errorMsg) {
		MimeMessage message = ussdEmailServer.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
		try {
			EmailTemplate email = emailTemplateRepository.findOne(UssdConstant.USSD_EMAIL_FAIL_TEMPLATE);
			
			if(StringUtils.isBlank(servicesTimer.getEmail())) {
				log.info("Not Found Email not send mail...");
				return;
			}
			
			final Context ctx = new Context(Locale.US);
		    ctx.setVariable("servicesTimer", servicesTimer);
		    ctx.setVariable("serviceStatus", serviceStatus);
		    ctx.setVariable("status", servicesTimer.getStatus() == UssdConstant.USSD_STATUS.ON ?
		    		USSD_ACTION.ACTION_ACTIVE: USSD_ACTION.ACTION_INACTIVE);
		    ctx.setVariable("error", errorMsg);
		    
			String finalText = emailTemplateEngine.process(email.getBody(), ctx);
			log.info("Mail : {}" , finalText);
			
			helper.setFrom(email.getFrom());
			helper.setSubject(email.getSubject());
			helper.setTo(servicesTimer.getEmail());
			helper.setText(finalText, true);
			
			//sender
			ussdEmailServer.send(message);
			
		} catch (MessagingException e) {
			log.error("sendErrorMail", e);
		} 
	}
}
