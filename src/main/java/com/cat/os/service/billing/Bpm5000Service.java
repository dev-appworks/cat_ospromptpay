package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm5000Service {

	public DataTableAjax<Bpm5000Vo> findAll(Bpm5000Vo bpm5000Vo) {
		List<Bpm5000Vo> bpm5000VoList = new ArrayList<Bpm5000Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm5000Vo bpm = new Bpm5000Vo();
			bpm.setExternalId("setExternalId" + (i + 1));
			bpm.setType("setType" + (i + 1));
			bpm.setMiuErrorCode("setMiuErrorCode" + (i + 1));
			bpm.setCount("setCount" + (i + 1));
			bpm.setSum("setSum" + (i + 1));
			bpm.setMinTransDate("setMinTransDate" + (i + 1));
			bpm.setMaxTransDate("setMaxTransDate" + (i + 1));

			bpm5000VoList.add(bpm);
		}

		DataTableAjax<Bpm5000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm5000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm5000VoList);

		return dataTableAjax;

	}
}
