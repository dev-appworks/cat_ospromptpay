package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3300RapVo;
import com.cat.os.dao.vo.Bpm3300Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm3300Service {
	public DataTableAjax<Bpm3300Vo> findALL(Bpm3300Vo bpm3300Vo) {

		List<Bpm3300Vo> bpm3300VoList = new ArrayList<Bpm3300Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm3300Vo bpm = new Bpm3300Vo();
			// bill.setUpdateDate(new Date());
			bpm.setTransactionId("transactionId" + (i + 1));
			bpm.setProcessId("processId");
			bpm.setProcessType("processType");
			bpm.setProcessName("processName");
			bpm.setCreatedBy("createdBy");
			bpm.setState("state");
			bpm.setCreatedDate("createdDate");
			bpm.setLogReport("logReport");

			bpm3300VoList.add(bpm);
		}

		DataTableAjax<Bpm3300Vo> dataTableAjax = new DataTableAjax<Bpm3300Vo>();
		dataTableAjax.setDraw(bpm3300Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm3300VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm3300RapVo> findALL(Bpm3300RapVo bpm3300RapVo) {

		List<Bpm3300RapVo> bpm3300RapVoList = new ArrayList<Bpm3300RapVo>();

		for (int i = 0; i < 5; i++) {
			Bpm3300RapVo bpm = new Bpm3300RapVo();
			// bill.setUpdateDate(new Date());
			bpm.setBillingAccount("billingAccount" + (i + 1));
			bpm.setGuilding("guilding");
			bpm.setType("type");
			bpm.setAccountNo("accountNo");
			bpm.setNumberOfBill("numberOfBill");
			bpm.setPointOrigin("pointOrigin");
			bpm.setMonthly("monthly");
			bpm.setRecords("records");
			bpm.setCharge("charge");
			bpm.setSecs("secs");
			bpm.setMinTransDate("minTransDate");
			bpm.setMaxTransDate("maxTransDate");
			bpm3300RapVoList.add(bpm);
		}

		DataTableAjax<Bpm3300RapVo> dataTableAjax = new DataTableAjax<Bpm3300RapVo>();
		dataTableAjax.setDraw(bpm3300RapVo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm3300RapVoList);

		return dataTableAjax;

	}
}
