package com.cat.os.service.billing.nrc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc1000Vo;

@Service
@Profile(Profiles.BILLING)
public class Nrc1000Service {
	
	public DataTableAjax<Nrc1000Vo> findAll(Nrc1000Vo nrc1000Vo){
		List<Nrc1000Vo> nrc1000VoVoList = new ArrayList<Nrc1000Vo>();

		
		for (int i = 0; i < 3 ; i++) {
			Nrc1000Vo nrc = new Nrc1000Vo();
			nrc.setBa("700012765");
			nrc.setExternalId("882901858");
			nrc.setExternalIdType("700012765");
			nrc.setComponentId("882901858");
			nrc.setComponentActiveDt(""+(17+i));
			nrc.setComponentInactiveDt(""+(1+i));
			nrc.setPackageId(""+(1+i));
			nrc.setPackageInstId(""+(1+i));
		
			
			nrc1000VoVoList.add(nrc);
		}
		
		DataTableAjax<Nrc1000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(nrc1000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(nrc1000VoVoList);
		
		return dataTableAjax ;
		
	
	}
	

}
