package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3101RapVo;
import com.cat.os.dao.vo.Bpm3101Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm3101Service {

	public DataTableAjax<Bpm3101Vo> findALL(Bpm3101Vo bpm3101Vo) {

		List<Bpm3101Vo> bpm3101VoList = new ArrayList<Bpm3101Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm3101Vo bpm = new Bpm3101Vo();
			bpm.setTransactionId("transactionId" + (i + 1));
			bpm.setProcessId("processId");
			bpm.setProcessType("processType");
			bpm.setProcessName("processName");
			bpm.setCreatedBy("createdBy");
			bpm.setState("state");
			bpm.setCreatedDate("createdDate");
			bpm.setLogReport("logReport");

			bpm3101VoList.add(bpm);
		}

		DataTableAjax<Bpm3101Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm3101Vo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm3101VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm3101RapVo> findALL(Bpm3101RapVo bpm3101RapVo) {

		List<Bpm3101RapVo> bpm3101RapVoList = new ArrayList<Bpm3101RapVo>();

		for (int i = 0; i < 5; i++) {
			Bpm3101RapVo bpm = new Bpm3101RapVo();
			bpm.setBillingAccount("billingAccount" + (i + 1));
			bpm.setGuilding("guilding");
			bpm.setType("type");
			bpm.setAccountNo("accountNo");
			bpm.setNumberOfBill("numberOfBill");
			bpm.setPointOrigin("pointOrigin");
			bpm.setMonthly("monthly");
			bpm.setRecords("records");
			bpm.setCharge("charge");
			bpm.setSecs("secs");
			bpm.setMinTransDate("minTransDate");
			bpm.setMaxTransDate("maxTransDate");
			bpm3101RapVoList.add(bpm);
		}

		DataTableAjax<Bpm3101RapVo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm3101RapVo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm3101RapVoList);

		return dataTableAjax;

	}
}
