package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3200Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm3200Service {

	public DataTableAjax<Bpm3200Vo> findAll(Bpm3200Vo bpm3200Vo) {
		List<Bpm3200Vo> bpm3200VoList = new ArrayList<Bpm3200Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm3200Vo bpm = new Bpm3200Vo();
			bpm.setBillingAccount("billingAccount" + (i + 1));
			bpm.setGuilding("guilding");
			bpm.setType("type");
			bpm.setAccountNo("accountNo");
			bpm.setNumberOfBill("numberOfBill");
			bpm.setPointOrigin("pointOrigin");
			bpm.setMonthly("monthly");
			bpm.setRecords("records");
			bpm.setCharge("charge");
			bpm.setSecs("secs");
			bpm.setMinTransDate("minTransDate");
			bpm.setMaxTransDate("maxTransDate");

			bpm3200VoList.add(bpm);
		}

		DataTableAjax<Bpm3200Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm3200Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm3200VoList);

		return dataTableAjax;

	}
}
