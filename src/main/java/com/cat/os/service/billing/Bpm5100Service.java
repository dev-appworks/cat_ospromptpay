package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5100Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm5100Service {

	public DataTableAjax<Bpm5100Vo> findAll(Bpm5100Vo bpm5100Vo) {
		List<Bpm5100Vo> bpm5100VoList = new ArrayList<Bpm5100Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm5100Vo bpm = new Bpm5100Vo();
			bpm.setExternalId("" + (i + 1));
			bpm.setType("" + (i + 1));
			bpm.setMiuErrorCode("" + (i + 1));
			bpm.setCount("" + (i + 1));
			bpm.setSum("" + (i + 1));
			bpm.setMinTransDate("" + (i + 1));
			bpm.setMaxTransDate("" + (i + 1));

			bpm5100VoList.add(bpm);
		}

		DataTableAjax<Bpm5100Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm5100Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm5100VoList);

		return dataTableAjax;

	}
}
