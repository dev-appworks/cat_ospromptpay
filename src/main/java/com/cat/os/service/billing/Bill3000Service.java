package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill3000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bill3000Service {
	public DataTableAjax<Bill3000Vo> findALL(Bill3000Vo bill3000Vo) {

		List<Bill3000Vo> bill3000VoList = new ArrayList<Bill3000Vo>();

		for (int i = 0; i < 5; i++) {
			Bill3000Vo bill = new Bill3000Vo();
			bill.setCreatedDate("01-01-2018");
			bill.setCreatedBy("1001-test");
			bill.setResult("Result" + (i + 1));
			bill.setAction("action");
			bill3000VoList.add(bill);
		}

		DataTableAjax<Bill3000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bill3000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bill3000VoList);

		return dataTableAjax;

	}
}
