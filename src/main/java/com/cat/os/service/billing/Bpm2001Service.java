package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2001Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm2001Service {
	
	public DataTableAjax<Bpm2001Vo> findAll(Bpm2001Vo bpm2001Vo){
		List<Bpm2001Vo> bpm2001VoList = new ArrayList<Bpm2001Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Bpm2001Vo bpm = new Bpm2001Vo();
			bpm.setPhoneNumber("phoneNumber"+(i+1));
			bpm.setDefaultBa("defaultBa");
			bpm.setGroup("group");
			bpm.setBill("bill");
			bpm.setInvoiceNo("invoiceNo");
			bpm.setNumber("number");
			bpm.setPriceVat("priceVat");
			bpm.setNewBa("newBa");
			bpm.setNewGroup("newGroup");
			bpm.setCreatedDate("createdDate");
			bpm.setTotalBill("totalBill");
			
			bpm2001VoList.add(bpm);
			
		}
		
		DataTableAjax<Bpm2001Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2001Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm2001VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
