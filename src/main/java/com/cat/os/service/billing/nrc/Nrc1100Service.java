package com.cat.os.service.billing.nrc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc1100Vo;

@Service
@Profile(Profiles.BILLING)
public class Nrc1100Service {
	
	public DataTableAjax<Nrc1100Vo> findAll(Nrc1100Vo nrc1100Vo){
		List<Nrc1100Vo> nrc1100VoVoList = new ArrayList<Nrc1100Vo>();

		
		for (int i = 0; i < 3 ; i++) {
			Nrc1100Vo nrc = new Nrc1100Vo();
			nrc.setNo(""+(i+1));
			nrc.setBa("700012765​");
			nrc.setExternalId("882901861​");
			nrc.setDomponentId("911070014​");
			nrc.setDomponentName("ส่วนลดค่าบริการเดือน 100%");
			nrc.setDomponentActiveDt("1/1/2017");
			nrc.setDomponentInactiveDt("9/10/2017");
			nrc.setPackagetId("110602");
			nrc.setPackagetName("my SME Extra Speed 599 ");
			nrc.setPackagetActiveDt("10/1/2016");
			nrc.setPackagetInactiveDt("9/10/2017");
			nrc.setPackagetPkgInstDt("3477333");
			nrc.setTrueActiveDt("2/10/2017");
			nrc.setTrueInactiveDt("9/10/2017");
			nrc.setDel("");
			nrc.setComment("");
	
			
			nrc1100VoVoList.add(nrc);
		}
		
		DataTableAjax<Nrc1100Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(nrc1100Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(nrc1100VoVoList);
		
		return dataTableAjax ;
		
	
	}
	

}
