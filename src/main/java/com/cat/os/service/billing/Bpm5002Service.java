package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5002MarkVo;
import com.cat.os.dao.vo.Bpm5002Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm5002Service {

	public DataTableAjax<Bpm5002Vo> findAll(Bpm5002Vo bpm5002Vo) {
		List<Bpm5002Vo> bpm5002VoList = new ArrayList<Bpm5002Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm5002Vo bpm = new Bpm5002Vo();
			bpm.setTransactionId("transactionId" + (i + 1));
			bpm.setProcessId("processId");
			bpm.setProcessType("processType");
			bpm.setProcessName("processName");
			bpm.setCreatedBy("createdBy");
			bpm.setStatus("status");
			bpm.setCreatedDate("createdDate");
			bpm.setLogReport("logReport");
			bpm5002VoList.add(bpm);
		}

		DataTableAjax<Bpm5002Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm5002Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm5002VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm5002MarkVo> findAll(Bpm5002MarkVo bpm5002MarkVo) {
		List<Bpm5002MarkVo> bpm5002MarkVoList = new ArrayList<Bpm5002MarkVo>();

		for (int i = 0; i < 5; i++) {
			Bpm5002MarkVo bpm = new Bpm5002MarkVo();
			bpm.setExternalId("externalId" + (i + 1));
			bpm.setType("type");
			bpm.setMiuErrorCode("miuErrorCode");
			bpm.setCount("count");
			bpm.setSum("sum");
			bpm.setMinTransDate("minTransDate");
			bpm.setMaxTransDate("maxTransDate");
			bpm.setExtTrackingId("extTrackingId");
			bpm.setRemark("remark");
			bpm.setAttachedFile("attachedFile");
			bpm5002MarkVoList.add(bpm);
		}

		DataTableAjax<Bpm5002MarkVo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm5002MarkVo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm5002MarkVoList);

		return dataTableAjax;

	}
}
