package com.cat.os.service.billing.nrc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc2100Vo;
@Service
@Profile(Profiles.BILLING)
public class Nrc2100Service {
	
	public DataTableAjax<Nrc2100Vo> findAll(Nrc2100Vo nrc2100Vo){
		List<Nrc2100Vo> nrc2100VoList = new ArrayList<Nrc2100Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Nrc2100Vo nrc = new Nrc2100Vo();
			
			nrc.setNo(""+(i+1));
			nrc.setBa("9033478");
			nrc.setExternalId("89303848");
			nrc.setComponentId("10000000");
			nrc.setComponentName("componentName");
			nrc.setComponentActiveDt("12/02/2017");
			nrc.setComponentInactiveDt("componentInactiveDt");
			nrc.setComponentOverrideName("componentOverrideName");
			nrc.setComponentOverrideRate("100.00");
			nrc.setComponentOverrideUnit("");
			nrc.setPackageId("100223");
			nrc.setPackageName("packageName");
			nrc.setPackageActiveDt("18/02/2017");
			nrc.setPackageInactiveDt("");
			nrc.setPackagePkgInstDt("381223");
			nrc.setTrueEffectDateActiceDt("12/04/2017");
			nrc.setTrueEffectDateInactiveDt("");
			nrc.setDel("");
			nrc.setComment("comment");
			nrc2100VoList.add(nrc);
		}
		
		DataTableAjax<Nrc2100Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(nrc2100Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(nrc2100VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
