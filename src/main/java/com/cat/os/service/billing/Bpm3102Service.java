package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3102Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm3102Service {

	public DataTableAjax<Bpm3102Vo> findALL(Bpm3102Vo bpm3102Vo) {

		List<Bpm3102Vo> bpm3102VoList = new ArrayList<Bpm3102Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm3102Vo bpm = new Bpm3102Vo();
			bpm.setBillingAccount("50000000" + (i + 1));
			bpm.setGuiding("guiding");
			bpm.setType("type");
			bpm.setAccountNo("accountNo");
			bpm.setNumberOfBill("numberOfBill");
			bpm.setPointOrigin("pointOrigin");
			bpm.setMonthly("monthly");
			bpm.setRecords("records");
			bpm.setCharge("charge");
			bpm.setSecs("secs");
			bpm.setMinTransDate("minTransDate");
			bpm.setMaxTransDate("maxTransDate");

			bpm3102VoList.add(bpm);
		}

		DataTableAjax<Bpm3102Vo> dataTableAjax = new DataTableAjax<Bpm3102Vo>();
		dataTableAjax.setDraw(bpm3102Vo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm3102VoList);

		return dataTableAjax;

	}
}
