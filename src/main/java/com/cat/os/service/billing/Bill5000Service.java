package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill5000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bill5000Service {

	public DataTableAjax<Bill5000Vo> findALL(Bill5000Vo bill5000Vo) {

		List<Bill5000Vo> bill5000VoList = new ArrayList<Bill5000Vo>();

		for (int i = 0; i < 5; i++) {
			Bill5000Vo bill = new Bill5000Vo();
			bill.setName("Name" + (i + 1));
			bill.setBillingAccount("Billing Account" + (i + 1));
			bill.setInvoiceNo("Invoice No" + (i + 1));
			bill.setIssueDate("issueDate" + (i + 1));
			bill.setDueDate("dueDate" + (i + 1));
			bill.setBalance("Balance" + (i + 1));
			bill5000VoList.add(bill);
		}

		DataTableAjax<Bill5000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bill5000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bill5000VoList);

		return dataTableAjax;

	}
}
