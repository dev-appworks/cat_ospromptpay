package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill3100Vo;

@Service
@Profile(Profiles.BILLING)
public class Bill3100Service {
	@Profile(Profiles.BILLING)
	public DataTableAjax<Bill3100Vo> findALL(Bill3100Vo bill3100Vo) {

		List<Bill3100Vo> bill3100VoList = new ArrayList<Bill3100Vo>();

		for (int i = 0; i < 5; i++) {
			Bill3100Vo bill = new Bill3100Vo();
			bill.setContractNo("contractNo");
			bill.setArRef("arRef");
			bill.setInvoiceAmt("invoiceAmt");
			bill.setBalanceAmt("balanceAmt");
			bill.setProductCode("productCode");
			bill.setSubProductCode("subProductCode");
			bill.setRevenueTypeCode("revenueTypeCode");
			bill.setAdvanceAmt("advanceAmt");
			bill.setRevenueAmt("revenueAmt");
			bill.setLgAmount("lgAmount");
			bill.setNetAdvAmt("netAdvAmt");
			bill3100VoList.add(bill);
		}

		DataTableAjax<Bill3100Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bill3100Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bill3100VoList);

		return dataTableAjax;

	}
}
