package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5300MarkCDRsVo;
import com.cat.os.dao.vo.Bpm5300Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm5300Service {

	public DataTableAjax<Bpm5300Vo> findAll(Bpm5300Vo bpm5300Vo) {
		List<Bpm5300Vo> bpm5300VoList = new ArrayList<Bpm5300Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm5300Vo bpm = new Bpm5300Vo();
			bpm.setTransactionId("" + (i + 1));
			bpm.setProcessId("" + (i + 1));
			bpm.setProcessType("" + (i + 1));
			bpm.setProcessName("" + (i + 1));
			bpm.setCreatedBy("" + (i + 1));
			bpm.setStatus("" + (i + 1));
			bpm.setCreatedDate("" + (i + 1));
			bpm.setLogReport("" + (i + 1));

			bpm5300VoList.add(bpm);
		}

		DataTableAjax<Bpm5300Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm5300Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm5300VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm5300MarkCDRsVo> findAll(Bpm5300MarkCDRsVo bpm5300MarkCDRsVo) {
		List<Bpm5300MarkCDRsVo> bpm5300MarkCDRsVoList = new ArrayList<Bpm5300MarkCDRsVo>();

		for (int i = 0; i < 5; i++) {
			Bpm5300MarkCDRsVo bill = new Bpm5300MarkCDRsVo();
			bill.setExternalId("" + (i + 1));
			bill.setType("" + (i + 1));
			bill.setMiuErrorCode("" + (i + 1));
			bill.setCount("" + (i + 1));
			bill.setSum("" + (i + 1));
			bill.setMinTransDate("" + (i + 1));
			bill.setMaxTransDate("" + (i + 1));
			bill.setExtTrackingId("" + (i + 1));
			bill.setRemark("" + (i + 1));
			bill.setAttachedFile("" + (i + 1));

			bpm5300MarkCDRsVoList.add(bill);
		}

		DataTableAjax<Bpm5300MarkCDRsVo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm5300MarkCDRsVo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm5300MarkCDRsVoList);

		return dataTableAjax;

	}

}
