package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2005TVo;
import com.cat.os.dao.vo.Bpm2005Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm2005Service {

	public DataTableAjax<Bpm2005Vo> findALL(Bpm2005Vo bpm2005Vo) {

		List<Bpm2005Vo> bpm2005VoList = new ArrayList<Bpm2005Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm2005Vo bpm = new Bpm2005Vo();
			bpm.setTransactionId("transactionId" + (i + 1));
			bpm.setProcessId("processId");
			bpm.setProcessType("processType");
			bpm.setProcessName("processName");
			bpm.setCreatedBy("createdBy");
			bpm.setStatus("status");
			bpm.setCreatedDate("createdDate");
			bpm.setLogReport("logReport");

			bpm2005VoList.add(bpm);
		}

		DataTableAjax<Bpm2005Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2005Vo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm2005VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm2005TVo> findALL(Bpm2005TVo bpm2005tVo) {

		List<Bpm2005TVo> bpm2005tVoList = new ArrayList<Bpm2005TVo>();

		for (int i = 0; i < 5; i++) {
			Bpm2005TVo bpm = new Bpm2005TVo();

			bpm.setPhoneNo("phoneNo" + (i + 1));
			bpm.setDefaultBa("defaultBa");
			bpm.setGroup("group");
			bpm.setBill("bill");
			bpm.setInvoiceNo("invoiceNo");
			bpm.setNumber("number");
			bpm.setPriceVat("priceVat");
			bpm.setNewBa("newBa");
			bpm.setNewgroup("newgroup");
			bpm.setCreatedDate("createdDate");

			bpm2005tVoList.add(bpm);
		}

		DataTableAjax<Bpm2005TVo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2005tVo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm2005tVoList);

		return dataTableAjax;

	}
}
