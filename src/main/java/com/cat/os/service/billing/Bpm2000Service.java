package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.dao.vo.Bpm2000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm2000Service {

	public DataTableAjax<Bpm2000Vo> findALL(Bpm2000Vo bpm2000Vo) {

		List<Bpm2000Vo> bpm2000VoList = new ArrayList<Bpm2000Vo>();
		for (int i = 0; i < 5; i++) {
			Bpm2000Vo bpm = new Bpm2000Vo();
			bpm.setDescription("description" + (i + 1));
			bpm.setCreatedBy("createdBy");
			bpm.setCreateDate(OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
			bpm.setState("state");
			bpm2000VoList.add(bpm);
		}

		DataTableAjax<Bpm2000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2000Vo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm2000VoList);

		return dataTableAjax;

	}
}
