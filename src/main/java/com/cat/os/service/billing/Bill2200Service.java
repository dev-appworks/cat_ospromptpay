package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill2200Vo;

@Service
@Profile(Profiles.BILLING)
public class Bill2200Service {
	public DataTableAjax<Bill2200Vo> findALL(Bill2200Vo bill2200Vo) {

		List<Bill2200Vo> bill2200VoList = new ArrayList<Bill2200Vo>();

		for (int i = 0; i < 5; i++) {
			Bill2200Vo bill = new Bill2200Vo();
			bill.setBa("BA" + (i + 1));
			bill.setInvoiceNo("Invoice No" + (i + 1));
			bill.setPeriod("Period" + (i + 1));
			bill.setAmtOriginal("Amt Original" + (i + 1));
			bill.setAmtProduct("Amt Product" + (i + 1));
			bill.setDiff("Diff" + (i + 1));
			bill2200VoList.add(bill);
		}

		DataTableAjax<Bill2200Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bill2200Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bill2200VoList);

		return dataTableAjax;

	}
}
