package com.cat.os.service.billing.nrc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc2300Vo;
@Service
@Profile(Profiles.BILLING)
public class Nrc2300Service {
	
	public DataTableAjax<Nrc2300Vo> findAll(Nrc2300Vo nrc2300Vo){
		List<Nrc2300Vo> nrc2300VoList = new ArrayList<Nrc2300Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Nrc2300Vo nrc = new Nrc2300Vo();
			
			nrc.setBa("9000030"+(i+1));
			nrc.setMdn("894945");
			nrc.setPackageId("110567");
			nrc.setPackageName("packageName");
			nrc.setPackageActiveDt("03/12/2017");
			nrc.setPackageInactiveDt("");
			nrc.setPackagePkgInstId("3229934");
			nrc.setComponentId("11002304");
			nrc.setComponentName("componentName");
			nrc.setComponentActiveDt("23/12/2017");
			nrc.setComponentInactiveDt("");
			nrc.setComponentOverrideName("componentOverrideName");
			nrc.setComponentOverrideRate("100.00");
			nrc.setComponentOverrideUnit("componentOverrideUnit");
			nrc.setComponentCompInstId("68485868");
			
			nrc2300VoList.add(nrc);
		}
		
		DataTableAjax<Nrc2300Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(nrc2300Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(nrc2300VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
