package com.cat.os.service.billing.nrc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc1300Vo;
@Service
@Profile(Profiles.BILLING)
public class Nrc1300Service {
	
	public DataTableAjax<Nrc1300Vo> findAll(Nrc1300Vo nrc1300Vo){
		List<Nrc1300Vo> nrc1300VoList = new ArrayList<Nrc1300Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Nrc1300Vo nrc = new Nrc1300Vo();
			
			nrc.setBa("ba"+(i+1));
			nrc.setMdn("mdn");
			nrc.setPackageId("packageId");
			nrc.setPackageName("packageName");
			nrc.setPackageActiveDt("packageActiveDt");
			nrc.setPackageInactiveDt("packageInactiveDt");
			nrc.setPackagePkgInstId("packagePkgInstId");
			nrc.setComponentId("componentId");
			nrc.setComponentName("componentName");
			nrc.setComponentActiceDt("componentActiceDt");
			nrc.setComponentInactiveDt("componentInactiveDt");
			nrc.setComponentCompInstId("componentCompInstId");
			nrc1300VoList.add(nrc);
		}
		
		DataTableAjax<Nrc1300Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(nrc1300Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(nrc1300VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
