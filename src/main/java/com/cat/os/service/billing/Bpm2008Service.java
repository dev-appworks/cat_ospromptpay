package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2008T2Vo;
import com.cat.os.dao.vo.Bpm2008T3Vo;
import com.cat.os.dao.vo.Bpm2008Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm2008Service {

	public DataTableAjax<Bpm2008Vo> findAll(Bpm2008Vo bpm2008Vo) {
		List<Bpm2008Vo> bpm2008VoList = new ArrayList<Bpm2008Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm2008Vo bpm = new Bpm2008Vo();
			bpm.setCutOffDate("" + (i + 1));
			bpm.setPpddDate("" + (i + 1));
			bpm.setNextPpddDate("" + (i + 1));

			bpm2008VoList.add(bpm);
		}

		DataTableAjax<Bpm2008Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2008Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm2008VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm2008T2Vo> findAll(Bpm2008T2Vo bpm2008T2Vo) {
		List<Bpm2008T2Vo> bpm2008T2VoList = new ArrayList<Bpm2008T2Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm2008T2Vo bpm = new Bpm2008T2Vo();
			bpm.setAccountNo("" + (i + 1));
			bpm.setPrevCutOffDate("" + (i + 1));
			bpm.setPrevBillDate("" + (i + 1));
			bpm.setNextBillDate("" + (i + 1));

			bpm2008T2VoList.add(bpm);
		}

		DataTableAjax<Bpm2008T2Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2008T2Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm2008T2VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm2008T3Vo> findAll(Bpm2008T3Vo bpm2008T3Vo) {
		List<Bpm2008T3Vo> bpm2008T3VoList = new ArrayList<Bpm2008T3Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm2008T3Vo bpm = new Bpm2008T3Vo();
			bpm.setAccountNo("" + (i + 1));
			bpm.setPrevCutOffDate("" + (i + 1));
			bpm.setPrevBillDate("" + (i + 1));
			bpm.setNextBillDate("" + (i + 1));

			bpm2008T3VoList.add(bpm);
		}

		DataTableAjax<Bpm2008T3Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2008T3Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm2008T3VoList);

		return dataTableAjax;

	}
}
