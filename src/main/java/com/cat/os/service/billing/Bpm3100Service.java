package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3100Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm3100Service {

	public DataTableAjax<Bpm3100Vo> findALL(Bpm3100Vo bpm3100Vo) {

		List<Bpm3100Vo> bpm3100VoList = new ArrayList<Bpm3100Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm3100Vo bpm = new Bpm3100Vo();
			bpm.setBillingAccount("50000000" + (i + 1));
			bpm.setGuiding("guiding");
			bpm.setType("type");
			bpm.setAccountNo("accountNo");
			bpm.setNumberOfBill("numberOfBill");
			bpm.setPointOrigin("pointOrigin");
			bpm.setMonthly("monthly");
			bpm.setRecords("records");
			bpm.setCharge("charge");
			bpm.setSecs("secs");
			bpm.setMinTransDate("minTransDate");
			bpm.setMaxTransDate("maxTransDate");

			bpm3100VoList.add(bpm);
		}

		DataTableAjax<Bpm3100Vo> dataTableAjax = new DataTableAjax<Bpm3100Vo>();
		dataTableAjax.setDraw(bpm3100Vo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm3100VoList);

		return dataTableAjax;

	}
}
