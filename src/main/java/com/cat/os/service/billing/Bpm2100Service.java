package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2100T123Vo;
import com.cat.os.dao.vo.Bpm2100Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm2100Service {

	public DataTableAjax<Bpm2100Vo> findAll(Bpm2100Vo bpm2100Vo) {
		List<Bpm2100Vo> bpm2100VoList = new ArrayList<Bpm2100Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm2100Vo bill = new Bpm2100Vo();
			bill.setTransactionId("" + (i + 1));
			bill.setProcessId("" + (i + 1));
			bill.setProcessType("" + (i + 1));
			bill.setProcessName("" + (i + 1));
			bill.setCreatedBy("" + (i + 1));
			bill.setStatus("" + (i + 1));
			bill.setCreatedDate("" + (i + 1));
			bill.setLogReport("" + (i + 1));

			bpm2100VoList.add(bill);
		}

		DataTableAjax<Bpm2100Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2100Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm2100VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm2100T123Vo> findAll(Bpm2100T123Vo bpm2100T123Vo) {
		List<Bpm2100T123Vo> bpm2100T123VoList = new ArrayList<Bpm2100T123Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm2100T123Vo bpm = new Bpm2100T123Vo();
			bpm.setPhoneNo("" + (i + 1));
			bpm.setDefaultBa("" + (i + 1));
			bpm.setGroup("" + (i + 1));
			bpm.setBill("" + (i + 1));
			bpm.setInvoiceNo("" + (i + 1));
			bpm.setNumber("" + (i + 1));
			bpm.setVatAmountFirst("" + (i + 1));
			bpm.setNewBa("" + (i + 1));
			bpm.setNewGroup("" + (i + 1));
			bpm.setCreatedDate("" + (i + 1));

			bpm2100T123VoList.add(bpm);
		}

		DataTableAjax<Bpm2100T123Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2100T123Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm2100T123VoList);

		return dataTableAjax;

	}

}
