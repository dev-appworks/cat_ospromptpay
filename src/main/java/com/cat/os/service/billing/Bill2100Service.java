package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill2100Vo;


@Service
@Profile(Profiles.BILLING)
public class Bill2100Service {
	
	public DataTableAjax<Bill2100Vo> findAll(Bill2100Vo bill2100Vo){
		List<Bill2100Vo> bill2100VoList = new ArrayList<Bill2100Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Bill2100Vo bill = new Bill2100Vo();
			bill.setBa("BA"+(i+1));
			bill.setInvoiceNo("No."+(i+1));
			bill.setPeriod("01");
			bill.setAmtOriginal("2100");
			bill.setAmtProduct("P-2100");	
			bill.setDiff("0");
			bill2100VoList.add(bill);
		}
		
		DataTableAjax<Bill2100Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bill2100Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bill2100VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
