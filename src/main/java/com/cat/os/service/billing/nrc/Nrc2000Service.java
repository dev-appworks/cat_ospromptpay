package com.cat.os.service.billing.nrc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc2000Vo;
@Service
@Profile(Profiles.BILLING)
public class Nrc2000Service {
	
	public DataTableAjax<Nrc2000Vo> findAll(Nrc2000Vo nrc2000Vo){
		List<Nrc2000Vo> nrc2000VoList = new ArrayList<Nrc2000Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Nrc2000Vo nrc = new Nrc2000Vo();
			
			nrc.setBa("9000000"+(i+1));
			nrc.setExternalId("89997979");
			nrc.setExternalIdType("17");
			nrc.setComponentId("1110010");
			nrc.setComponentActiveDt("componentActiveDt");
			nrc.setComponentInactiveDt("componentInactiveDt");
			nrc.setPackageId("1020002");
			nrc.setPackageInstId("packageInstId");
			nrc.setOverrideName("overrideName");
			nrc.setOverrideRate("100000");
			nrc.setOverrideUnit("overrideUnit");
			nrc2000VoList.add(nrc);
		}
		
		DataTableAjax<Nrc2000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(nrc2000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(nrc2000VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
