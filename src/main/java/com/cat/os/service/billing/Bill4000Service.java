package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill4000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bill4000Service {

	public DataTableAjax<Bill4000Vo> findAll(Bill4000Vo bill4000Vo) {
		List<Bill4000Vo> bill4000VoList = new ArrayList<Bill4000Vo>();

		for (int i = 0; i < 5; i++) {
			Bill4000Vo bill = new Bill4000Vo();
			bill.setBa("BA" + (i + 1));
			bill.setInvoiceNo("01");
			bill.setAmtOriginal("4000");
			bill.setAmtProduct("P-4000");
			bill.setDiff("0");
			bill4000VoList.add(bill);
		}

		DataTableAjax<Bill4000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bill4000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bill4000VoList);

		return dataTableAjax;

	}
}
