package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm4100Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm4100Service {

	public DataTableAjax<Bpm4100Vo> findAll(Bpm4100Vo bpm4100Vo) {
		List<Bpm4100Vo> bpm4100VoList = new ArrayList<Bpm4100Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm4100Vo bpm = new Bpm4100Vo();
			bpm.setParentAcountNo("parentAcountNo" + (i + 1));
			bpm.setChargeDate("chargeDate");
			bpm.setSubscriberNo("subscriberNo");
			bpm.setServiceActiveDate("serviceActiveDate");
			bpm.setServiceInactiveDate("serviceInactiveDate");
			bpm.setChargeStatus("chargeStatus");
			bpm4100VoList.add(bpm);
		}

		DataTableAjax<Bpm4100Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm4100Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm4100VoList);

		return dataTableAjax;

	}

}
