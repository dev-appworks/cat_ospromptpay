package com.cat.os.service.billing.nrc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc1200Vo;
@Service
@Profile(Profiles.BILLING)
public class Nrc1200Service {
	
	public DataTableAjax<Nrc1200Vo> findAll(Nrc1200Vo nrc1200Vo){
		List<Nrc1200Vo> nrc1200VoList = new ArrayList<Nrc1200Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Nrc1200Vo nrc = new Nrc1200Vo();
			
			nrc.setNo(""+(i+1));
			nrc.setBa("ba"+(i+1));
			nrc.setExternalId("externalId");
			nrc.setComponentId("componentId");
			nrc.setComponentName("componentName");
			nrc.setPackageId("packageId");
			nrc.setPackageName("packageName");
			nrc.setPackagePkgInstId("packagePkgInstId");
			nrc.setTrueEffectiveDateActiceDt("trueEffectiveDateActiceDt");
			nrc.setTrueEffectiveDateInactiveDt("trueEffectiveDateInactiveDt");
			nrc.setRunDate("runDate");
			nrc.setStatus("status");
			nrc.setRerunNo("rerunNo");
			nrc.setCompInstId("compInstId");
			nrc1200VoList.add(nrc);
		}
		
		DataTableAjax<Nrc1200Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(nrc1200Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(nrc1200VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
