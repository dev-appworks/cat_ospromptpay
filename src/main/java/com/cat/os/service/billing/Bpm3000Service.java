package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm3000Service {

	public DataTableAjax<Bpm3000Vo> findAll(Bpm3000Vo bpm3000Vo) {
		List<Bpm3000Vo> bpm3000VoList = new ArrayList<Bpm3000Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm3000Vo bpm = new Bpm3000Vo();
			bpm.setExternalId("" + (i + 1));
			bpm.setExternalType("" + (i + 1));
			bpm.setActiveDate("" + (i + 1));
			bpm.setInactiveDate("" + (i + 1));
			bpm.setSubscriberNo("" + (i + 1));
			bpm.setAccountNumber("" + (i + 1));
			bpm.setAccountStartDate("" + (i + 1));
			bpm.setAccountEndDate("" + (i + 1));
			bpm.setBillingStartDate("" + (i + 1));
			bpm.setBillingEndDate("" + (i + 1));
			bpm.setBillingAccount("" + (i + 1));

			bpm3000VoList.add(bpm);
		}

		DataTableAjax<Bpm3000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm3000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm3000VoList);

		return dataTableAjax;

	}
}
