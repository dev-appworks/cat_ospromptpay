package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.dao.vo.Bpm1000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm1000Service {

	public DataTableAjax<Bpm1000Vo> findALL(Bpm1000Vo bpm1000Vo) {

		List<Bpm1000Vo> bpm1000VoList = new ArrayList<Bpm1000Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm1000Vo bpm = new Bpm1000Vo();
			bpm.setTransectoinId("transectoinId" + (i + 1));
			bpm.setProcressId("procressId");
			bpm.setProcessType("processType");
			bpm.setCretedBy("cretedBy");
			bpm.setState("state");
			bpm.setCreatedDate(OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
			bpm.setLogReport("logReport");

			bpm1000VoList.add(bpm);
		}

		DataTableAjax<Bpm1000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm1000Vo.getDraw());
		dataTableAjax.setRecordsTotal(7l);
		dataTableAjax.setRecordsFiltered(7l);
		dataTableAjax.setData(bpm1000VoList);

		return dataTableAjax;

	}
}
