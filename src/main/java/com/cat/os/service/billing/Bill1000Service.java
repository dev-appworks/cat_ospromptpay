package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill1000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bill1000Service {
	
	public DataTableAjax<Bill1000Vo> findAll(Bill1000Vo bill1000Vo){
		List<Bill1000Vo> bill1000VoList = new ArrayList<Bill1000Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Bill1000Vo bill = new Bill1000Vo();
			bill.setAccountNo("No"+(i+1));
			bill.setExternalId("ID"+(i+1));
			bill.setLastBalance(""+(i+1));
			bill.setThisBalance(""+(i+1));
			bill.setBillAmt(""+(i+1));
			bill.setMigAmt(""+(i+1));
			bill.setPayBill(""+(i+1));
			bill.setAdjAmt(""+(i+1));
			bill.setRevAdjAmt(""+(i+1));
			bill.setRevAmt(""+(i+1));
			bill.setWrtAmt(""+(i+1));
			bill.setCalc(""+(i+1));
			bill.setDiff(""+(i+1));
			bill.setPostError(""+(i+1));
			bill.setLastRegion(""+(i+1));
			bill.setThisRegion(""+(i+1));
			bill.setHold2unhold(""+(i+1));
			bill.setReason(""+(i+1));
			bill.setAction(""+(i+1));
			bill1000VoList.add(bill);
		}
		
		DataTableAjax<Bill1000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bill1000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bill1000VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
