package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2006Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm2006Service {

	public DataTableAjax<Bpm2006Vo> findALL(Bpm2006Vo bpm2006Vo) {

		List<Bpm2006Vo> bpm2006VoList = new ArrayList<Bpm2006Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm2006Vo bpm = new Bpm2006Vo();
			bpm.setExternalId("800000" + (i + 1));
			bpm.setExternalIdType("12");
			bpm.setAccountNo("177778");
			bpm.setNoBill("0");
			bpm.setPointOrigin("219991");
			bpm.setMonthly("2014/08");
			bpm.setRecord("1");
			bpm.setCharge("123");
			bpm.setSecs("2067");
			bpm.setMinTransDate("20/1/2018");
			bpm.setMaxTransDate("22/1/2018");

			bpm2006VoList.add(bpm);
		}

		DataTableAjax<Bpm2006Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2006Vo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm2006VoList);

		return dataTableAjax;

	}

}
