package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2007Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm2007Service {

	public DataTableAjax<Bpm2007Vo> findALL(Bpm2007Vo bpm2007Vo) {

		List<Bpm2007Vo> bpm2007VoList = new ArrayList<Bpm2007Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm2007Vo bpm = new Bpm2007Vo();
			bpm.setBa("700000" + (i + 1));
			bpm.setIdType("12");
			bpm.setAccountNo("177778");
			bpm.setNoBill("0");
			bpm.setPointOrigin("219991");
			bpm.setRecord("1");
			bpm.setCharge("123");
			bpm.setSecs("2067");
			bpm.setMinTransDate("20/1/2018");
			bpm.setMaxTransDate("22/1/2018");
			bpm.setBperiod("M07_2017");
			bpm.setRemark("โอนหนี้");
			bpm.setBillType("แยก");

			bpm2007VoList.add(bpm);
		}

		DataTableAjax<Bpm2007Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2007Vo.getDraw());
		dataTableAjax.setRecordsTotal(20l);
		dataTableAjax.setRecordsFiltered(20l);
		dataTableAjax.setData(bpm2007VoList);

		return dataTableAjax;

	}

}
