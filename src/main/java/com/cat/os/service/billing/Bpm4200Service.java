package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm4200MarkVo;
import com.cat.os.dao.vo.Bpm4200Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm4200Service {

	public DataTableAjax<Bpm4200Vo> findAll(Bpm4200Vo bpm4200Vo) {
		List<Bpm4200Vo> bpm4200VoList = new ArrayList<Bpm4200Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm4200Vo bpm = new Bpm4200Vo();
			bpm.setTransactionId("transactionId" + (i + 1));
			bpm.setProcessId("processId");
			bpm.setProcessType("processType");
			bpm.setProcessName("processName");
			bpm.setCreateBy("createBy");
			bpm.setStatus("status");
			bpm.setCreatedDate("createdDate");
			bpm.setLogReport("logReport");
			bpm4200VoList.add(bpm);
		}

		DataTableAjax<Bpm4200Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm4200Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm4200VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm4200MarkVo> findAll(Bpm4200MarkVo bpm4200MarkVo) {
		List<Bpm4200MarkVo> bpm4200MarkVoList = new ArrayList<Bpm4200MarkVo>();

		for (int i = 0; i < 5; i++) {
			Bpm4200MarkVo bpm = new Bpm4200MarkVo();
			bpm.setExternalId("externalId" + (i + 1));
			bpm.setType("type");
			bpm.setMiuErrorCode("miuErrorCode");
			bpm.setCount("count");
			bpm.setSum("sum");
			bpm.setMinTransDate("minTransDate");
			bpm.setMaxTransDate("maxTransDate");
			bpm4200MarkVoList.add(bpm);
		}

		DataTableAjax<Bpm4200MarkVo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm4200MarkVo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm4200MarkVoList);

		return dataTableAjax;

	}

}
