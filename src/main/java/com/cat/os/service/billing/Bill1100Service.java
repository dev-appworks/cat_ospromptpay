package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill1100Vo;

@Service
@Profile(Profiles.BILLING)
public class Bill1100Service {

	public DataTableAjax<Bill1100Vo> findALL(Bill1100Vo bill1100Vo) {

		List<Bill1100Vo> bill1100VoList = new ArrayList<Bill1100Vo>();

		for (int i = 0; i < 5; i++) {
			Bill1100Vo bill = new Bill1100Vo();
			bill.setBa("BA" + (i + 1));
			bill.setInvoiceNo("01");
			bill.setAmtOriginal("1100");
			bill.setAmtProduct("P-1100");
			bill.setDiff("0");
			bill1100VoList.add(bill);
		}

		DataTableAjax<Bill1100Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bill1100Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bill1100VoList);

		return dataTableAjax;

	}
}
