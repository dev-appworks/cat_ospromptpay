package com.cat.os.service.billing.inw;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cat.os.common.constant.ProjectConstant.TransactionManagerRef;
import com.cat.os.dao.entity.IbacssPaymentData;
import com.cat.os.dao.entity.IbacssPaymentDataAccountNewInv;
import com.cat.os.dao.entity.IbacssPaymentDataAccountPay;
import com.cat.os.dao.entity.IbacssPaymentDataTaxInvSummaryWt;
import com.cat.os.dao.mysql.IbacssAccountNewInvJdbcRepository;
import com.cat.os.dao.mysql.IbacssAccountPayJdbcRepository;
import com.cat.os.dao.mysql.IbacssPaymentDataJdbcRepository;
import com.cat.os.dao.mysql.IbacssTaxSummaryWtJdbcRepository;
import com.cat.os.service.SftpService;

@Service
public class ImportIbacssPaymentDataJobService {
	
	private static final Logger log = LoggerFactory.getLogger(ImportIbacssPaymentDataJobService.class);
	
	private static final String DIR_NAME = "/GenFile_%s";
	private static final String CSV_FILENAME = "Kenan_Payment_Summary_%s.csv";
	
	@Autowired
	private SftpService sftpService;
	
	@Autowired
	@Qualifier("ibacssPaymentDataJdbcRepository")
	private IbacssPaymentDataJdbcRepository paymentDataJdbcRepository;
	
	@Autowired
	@Qualifier("ibacssAccountPayJdbcRepository")
	private IbacssAccountPayJdbcRepository accountPayJdbcRepository;
	
	@Autowired
	@Qualifier("ibacssAccountNewInvJdbcRepository")
	private IbacssAccountNewInvJdbcRepository accountNewInvJdbcRepository;
	
	@Autowired
	@Qualifier("ibacssTaxSummaryWtJdbcRepository")
	private IbacssTaxSummaryWtJdbcRepository taxSummaryWtJdbcRepository;
	
	public enum DataType {
		PAYMENT_DATA("Transaction Date", "File Name", 1),
		ACCOUNT_PAY("ACCOUNT_PAY", "Pay Mode", 2),
		ACCOUNT_NEW_INV("ACCOUNT_NEW_INV", "Bill Order Number", 3),
		TAX_INV_SUMMARY_WT("TAX_INV_SUMMARY_WT", "Bill Order Number", 4);
		
		private String title;
		private String header;
		private int flag;
		private DataType(String title, String header, int flag) {
			this.title = title;
			this.header = header;
			this.flag = flag;
		}
	}
	
	public void runJob(String txDate) {
		log.info("Job Process Start");
		long start = System.currentTimeMillis();
		
		String filename = null;
		List<String> lineList = null;
		InputStream is = null;
		try {
			String path = String.format(DIR_NAME, txDate);
			filename = String.format(CSV_FILENAME, txDate);
			
			byte[] csvFlie = sftpService.downloadFile(path, filename);
			is = new ByteArrayInputStream(csvFlie);
			lineList = IOUtils.readLines(is, "TIS-620");
		} catch(IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(is);
		}
		
		if (lineList != null) {
			try {
				Map<DataType, Object> dataMap = processCsvData(filename, lineList);
				insertData(dataMap);
				
				long end = System.currentTimeMillis();
				log.info("Job Process Success, using {} seconds", (float) (end - start) / 1000F);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}
	
	public Map<DataType, Object> processCsvData(String filename, List<String> lineList) {
		log.info("processCsvData");
		
		// Prepare Data
		Date txDate = null;
		List<IbacssPaymentData> paymentDataList = new ArrayList<IbacssPaymentData>();
		List<IbacssPaymentDataAccountPay> accountPayList = new ArrayList<IbacssPaymentDataAccountPay>();
		List<IbacssPaymentDataAccountNewInv> accountNewInvList = new ArrayList<IbacssPaymentDataAccountNewInv>();
		List<IbacssPaymentDataTaxInvSummaryWt> taxInvSummaryWtList = new ArrayList<IbacssPaymentDataTaxInvSummaryWt>();
		
		String[] datas = null;
		String temp = null;
		int titleFlag = 0;
		int headerFlag = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		for (String line : lineList) {
			// Splitting a comma-separated string but ignoring commas in quotes
			// https://stackoverflow.com/questions/18893390/splitting-on-comma-outside-quotes
			datas = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
			
			// Remove double quote and space
			temp = StringUtils.trimToEmpty(StringUtils.removeAll(datas[0], "\""));
			// Check for skip line for empty and "=" is first character
			if ((StringUtils.isEmpty(temp) && datas.length == 1) || temp.startsWith("=", 0)) {
				continue;
			}
			
			// Decision titleFlag
			if (DataType.PAYMENT_DATA.title.equals(temp)) {
				titleFlag = DataType.PAYMENT_DATA.flag;
				// get Transaction Date
				try {
					txDate = dateFormat.parse(StringUtils.trimToEmpty(StringUtils.removeAll(datas[1], "\"")));
				} catch (ParseException e) {
					log.warn(e.getMessage(), e);
				}
				log.debug("Transaction Date: " + txDate);
				continue;
			} else if (DataType.ACCOUNT_PAY.title.equals(temp)) {
				titleFlag = DataType.ACCOUNT_PAY.flag;
				continue;
			} else if (DataType.ACCOUNT_NEW_INV.title.equals(temp)) {
				titleFlag = DataType.ACCOUNT_NEW_INV.flag;
				continue;
			} else if (DataType.TAX_INV_SUMMARY_WT.title.equals(temp)) {
				titleFlag = DataType.TAX_INV_SUMMARY_WT.flag;
				continue;
			}
			
			// Decision headerFlag
			if (DataType.PAYMENT_DATA.flag == titleFlag && DataType.PAYMENT_DATA.header.equals(temp)) {
				headerFlag = DataType.PAYMENT_DATA.flag;
				continue;
			} else if (DataType.ACCOUNT_PAY.flag == titleFlag && DataType.ACCOUNT_PAY.header.equals(temp)) {
				headerFlag = DataType.ACCOUNT_PAY.flag;
				continue;
			} else if (DataType.ACCOUNT_NEW_INV.flag == titleFlag && DataType.ACCOUNT_NEW_INV.header.equals(temp)) {
				headerFlag = DataType.ACCOUNT_NEW_INV.flag;
				continue;
			} else if (DataType.TAX_INV_SUMMARY_WT.flag == titleFlag && DataType.TAX_INV_SUMMARY_WT.header.equals(temp)) {
				headerFlag = DataType.TAX_INV_SUMMARY_WT.flag;
				continue;
			}
			
			// Put data from line into Object following by Type with titleFlag == headerFlag
			if (DataType.PAYMENT_DATA.flag == titleFlag && DataType.PAYMENT_DATA.flag == headerFlag) {
				paymentDataList.add(processPaymentDate(datas, filename, txDate));
			} else if (DataType.ACCOUNT_PAY.flag == titleFlag && DataType.ACCOUNT_PAY.flag == headerFlag) {
				accountPayList.add(processAccountPay(datas, txDate));
			} else if (DataType.ACCOUNT_NEW_INV.flag == titleFlag && DataType.ACCOUNT_NEW_INV.flag == headerFlag) {
				accountNewInvList.add(processAccountNewInv(datas, txDate));
			} else if (DataType.TAX_INV_SUMMARY_WT.flag == titleFlag && DataType.TAX_INV_SUMMARY_WT.flag == headerFlag) {
				taxInvSummaryWtList.add(processTaxInvSummaryWt(datas, txDate));
			}
		}
		
		Map<DataType, Object> dataMap = new HashMap<DataType, Object>();
		dataMap.put(DataType.PAYMENT_DATA, paymentDataList);
		dataMap.put(DataType.ACCOUNT_PAY, accountPayList);
		dataMap.put(DataType.ACCOUNT_NEW_INV, accountNewInvList);
		dataMap.put(DataType.TAX_INV_SUMMARY_WT, taxInvSummaryWtList);
		
		return dataMap;
	}
	
	private IbacssPaymentData processPaymentDate(String[] datas, String filename, Date txDate) {
		IbacssPaymentData paymentData = new IbacssPaymentData();
		paymentData.setSrcFilename(filename);
		paymentData.setTxDate(txDate);
		paymentData.setFilename(prepareData(datas[0]));
		paymentData.setNumOfRecords(prepareData(datas[1]));
		paymentData.setEauction1000(prepareData(datas[3]));
		paymentData.setEauction1200(prepareData(datas[4]));
		paymentData.setEauction1400(prepareData(datas[5]));
		paymentData.setEauction1600(prepareData(datas[6]));
		paymentData.setEauction1800(prepareData(datas[7]));
		return paymentData;
	}
	
	private IbacssPaymentDataAccountPay processAccountPay(String[] datas, Date txDate) {
		IbacssPaymentDataAccountPay accountPay = new IbacssPaymentDataAccountPay();
		accountPay.setTxDate(txDate);
		accountPay.setPayMode(prepareData(datas[0]));
		accountPay.setDesc(prepareData(datas[1]));
		accountPay.setNumOfRecords(prepareData(datas[2]));
		return accountPay;
	}
	
	private IbacssPaymentDataAccountNewInv processAccountNewInv(String[] datas, Date txDate) {
		IbacssPaymentDataAccountNewInv accountNewInv = new IbacssPaymentDataAccountNewInv();
		accountNewInv.setTxDate(txDate);
		accountNewInv.setBillOrderNo(prepareData(datas[0]));
		accountNewInv.setAmount(prepareData(datas[1]));
		accountNewInv.setTax(prepareData(datas[2]));
		accountNewInv.setNumOfRecords(prepareData(datas[3]));
		return accountNewInv;
	}
	
	private IbacssPaymentDataTaxInvSummaryWt processTaxInvSummaryWt(String[] datas, Date txDate) {
		IbacssPaymentDataTaxInvSummaryWt taxInvSummaryWt = new IbacssPaymentDataTaxInvSummaryWt();
		taxInvSummaryWt.setTxDate(txDate);
		taxInvSummaryWt.setBillOrderNumber(prepareData(datas[0]));
		taxInvSummaryWt.setRental(prepareData(datas[1]));
		taxInvSummaryWt.setUsage(prepareData(datas[2]));
		taxInvSummaryWt.setTotalAmountExVat(prepareData(datas[3]));
		taxInvSummaryWt.setVatAmount(prepareData(datas[4]));
		taxInvSummaryWt.setRentalWt(prepareData(datas[5]));
		taxInvSummaryWt.setUsageWt(prepareData(datas[6]));
		taxInvSummaryWt.setNumOfRecords(prepareData(datas[7]));
		return taxInvSummaryWt;
	}
	
	private String prepareData(String data) {
		return StringUtils.trimToEmpty(StringUtils.removeAll(data, "\""));
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(transactionManager = TransactionManagerRef.DEFAULT_MYSQL, rollbackFor = { SQLException.class })
	public void insertData(Map<DataType, Object> dataMap) throws SQLException {
		log.info("insertData");
		
		// Process PaymentData
		List<IbacssPaymentData> paymentDataList = (List<IbacssPaymentData>) dataMap.get(DataType.PAYMENT_DATA);
		int[][] resultPaymentData = paymentDataJdbcRepository.batchInsert(paymentDataList);
		if (log.isDebugEnabled()) {
			for (int i = 0; i < resultPaymentData.length; i++) {
				for (int j = 0; j < resultPaymentData[i].length; j++) {
					log.debug("resultPaymentData[" + i + "][" + j + "]: " + resultPaymentData[i][j]);
				}
			}
		}
		
		// Process AccountPay
		List<IbacssPaymentDataAccountPay> accountPayList = (List<IbacssPaymentDataAccountPay>) dataMap.get(DataType.ACCOUNT_PAY);
		int[][] resultAccountPay = accountPayJdbcRepository.batchInsert(accountPayList);
		if (log.isDebugEnabled()) {
			for (int i = 0; i < resultAccountPay.length; i++) {
				for (int j = 0; j < resultAccountPay[i].length; j++) {
					log.debug("resultAccountPay[" + i + "][" + j + "]: " + resultAccountPay[i][j]);
				}
			}
		}
		
		// Process AccountNewInv
		List<IbacssPaymentDataAccountNewInv> accountNewInvList = (List<IbacssPaymentDataAccountNewInv>) dataMap.get(DataType.ACCOUNT_NEW_INV);
		int[][] resultAccountNewInv = accountNewInvJdbcRepository.batchInsert(accountNewInvList);
		if (log.isDebugEnabled()) {
			for (int i = 0; i < resultAccountNewInv.length; i++) {
				for (int j = 0; j < resultAccountNewInv[i].length; j++) {
					log.debug("resultAccountNewInv[" + i + "][" + j + "]: " + resultAccountNewInv[i][j]);
				}
			}
		}
		
		// Process TaxInvSummaryWt
		List<IbacssPaymentDataTaxInvSummaryWt> taxInvSummaryWtList = (List<IbacssPaymentDataTaxInvSummaryWt>) dataMap.get(DataType.TAX_INV_SUMMARY_WT);
		int[][] resultTaxSummaryWt = taxSummaryWtJdbcRepository.batchInsert(taxInvSummaryWtList);
		if (log.isDebugEnabled()) {
			for (int i = 0; i < resultTaxSummaryWt.length; i++) {
				for (int j = 0; j < resultTaxSummaryWt[i].length; j++) {
					log.debug("resultTaxSummaryWt[" + i + "][" + j + "]: " + resultTaxSummaryWt[i][j]);
				}
			}
		}
	}
	
}
