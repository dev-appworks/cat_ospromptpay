package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill2000Vo;

@Service
@Profile(Profiles.BILLING)
public class Bill2000Service {
	
	public DataTableAjax<Bill2000Vo> findAllBacth() {
		DataTableAjax<Bill2000Vo> dataTableAjaxBacth = new DataTableAjax<Bill2000Vo>();
		List<Bill2000Vo> bill2000VoListBacth = new ArrayList<Bill2000Vo>();
	
		for (int i = 0; i < 5; i++) {
			Bill2000Vo bill2000VoBatch = new Bill2000Vo();
			bill2000VoBatch.setBa(121313);
			bill2000VoBatch.setInvoiceNo(34234234);
			bill2000VoBatch.setPostDate(new Date());
			bill2000VoBatch.setPayDate(new Date());
			bill2000VoBatch.setAmtOriginal(1000);
			bill2000VoBatch.setAmtProduct(1000);
			bill2000VoBatch.setDiff(0);
			bill2000VoListBacth.add(bill2000VoBatch);
			
		}
		
		
		dataTableAjaxBacth.setData(bill2000VoListBacth);
		dataTableAjaxBacth.setRecordsTotal(new Long(bill2000VoListBacth.size()));
		dataTableAjaxBacth.setRecordsFiltered(new Long(bill2000VoListBacth.size()));
		return dataTableAjaxBacth;
		
	}
	
	
	public DataTableAjax<Bill2000Vo> findAll() {
		DataTableAjax<Bill2000Vo> dataTableAjax = new DataTableAjax<Bill2000Vo>();
		List<Bill2000Vo> bill2000VoList = new ArrayList<Bill2000Vo>();
	
		for (int i = 0; i < 5; i++) {
			Bill2000Vo bill2000Vo = new Bill2000Vo();
			bill2000Vo.setBa(121313);
			bill2000Vo.setInvoiceNo(34234234);
			bill2000Vo.setPostDate(new Date());
			bill2000Vo.setPayDate(new Date());
			bill2000Vo.setAmtOriginal(1000);
			bill2000Vo.setAmtProduct(1000);
			bill2000Vo.setDiff(0);
			bill2000VoList.add(bill2000Vo);
			
		}
		
		
		dataTableAjax.setData(bill2000VoList);
		dataTableAjax.setRecordsTotal(new Long(bill2000VoList.size()));
		dataTableAjax.setRecordsFiltered(new Long(bill2000VoList.size()));
		return dataTableAjax;
		
	}
	
	
	

}
