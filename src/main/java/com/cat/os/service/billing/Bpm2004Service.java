package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2004Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm2004Service {
	
	public DataTableAjax<Bpm2004Vo> findAll(Bpm2004Vo bpm2004Vo){
		List<Bpm2004Vo> bpm2004VoList = new ArrayList<Bpm2004Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Bpm2004Vo bpm = new Bpm2004Vo();
			bpm.setBillRefNo("2000300"+(i+1));
			bpm.setAccountNo("accountNo");
			bpm.setBa("ba");
			bpm.setNoBill("0");
			bpm.setPointOrigin("pointOrigin");
			bpm.setMonthly("monthly");
			bpm.setRecord("1");
			bpm.setCharge("512");
			bpm.setSecs("2067");
			bpm.setMinTransDate("minTransDate");
			bpm.setMaxTransDate("maxTransDate");
			
			bpm2004VoList.add(bpm);
			
		}
		
		DataTableAjax<Bpm2004Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm2004Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm2004VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
