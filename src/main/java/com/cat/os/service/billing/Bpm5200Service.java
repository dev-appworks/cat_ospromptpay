package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5200Vo;

@Service
@Profile(Profiles.BILLING)
public class Bpm5200Service {

	public DataTableAjax<Bpm5200Vo> findAll(Bpm5200Vo bpm5200Vo) {
		List<Bpm5200Vo> bpm5200VoList = new ArrayList<Bpm5200Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm5200Vo bpm = new Bpm5200Vo();
			bpm.setCreateDate("" + (i + 1));
			bpm.setExtTrackingId("" + (i + 1));
			bpm.setSourceTb("" + (i + 1));
			bpm.setFileId("" + (i + 1));
			bpm.setNumRecords("" + (i + 1));
			bpm.setRemark("" + (i + 1));
			bpm.setAttachedFile("" + (i + 1));

			bpm5200VoList.add(bpm);
		}

		DataTableAjax<Bpm5200Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm5200Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm5200VoList);

		return dataTableAjax;

	}
}
