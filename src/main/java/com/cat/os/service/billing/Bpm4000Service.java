package com.cat.os.service.billing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm4000Vo;
import com.cat.os.dao.vo.Bpm4001Vo;
import com.cat.os.dao.vo.Bpm4002Vo;
import com.cat.os.dao.vo.Bpm4003Vo;
import com.cat.os.dao.vo.Bpm4003FixCaseVo;

@Service
@Profile(Profiles.BILLING)
public class Bpm4000Service {

	public DataTableAjax<Bpm4000Vo> findAll(Bpm4000Vo bpm4000Vo) {
		List<Bpm4000Vo> bpm4000VoList = new ArrayList<Bpm4000Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm4000Vo bill = new Bpm4000Vo();
			bill.setExternalId("" + (i + 1));
			bill.setType("" + (i + 1));
			bill.setMiuErrorCode("" + (i + 1));
			bill.setCount("" + (i + 1));
			bill.setSum("" + (i + 1));
			bill.setMinTransDate("" + (i + 1));
			bill.setMaxTransDate("" + (i + 1));

			bpm4000VoList.add(bill);
		}

		DataTableAjax<Bpm4000Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm4000Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm4000VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm4001Vo> findAll(Bpm4001Vo bpm4001Vo) {
		List<Bpm4001Vo> bpm4001VoList = new ArrayList<Bpm4001Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm4001Vo bill = new Bpm4001Vo();
			bill.setBa("" + (i + 1));
			bill.setExternalIdType("" + (i + 1));
			bill.setSubscriberNo("" + (i + 1));
			bill.setServiceDate("" + (i + 1));
			bill.setServiceEndDate("" + (i + 1));
			bill.setCheckServiceType("" + (i + 1));
			bill.setCheckNocus("" + (i + 1));
			bill.setCheckNocus("" + (i + 1));
			bill.setCheckSession("" + (i + 1));

			bpm4001VoList.add(bill);
		}

		DataTableAjax<Bpm4001Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm4001Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm4001VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm4002Vo> findAll(Bpm4002Vo bpm4002Vo) {
		List<Bpm4002Vo> bpm4002VoList = new ArrayList<Bpm4002Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm4002Vo bill = new Bpm4002Vo();
			bill.setExternalId("" + (i + 1));
			bill.setMiuError("" + (i + 1));
			bill.setnRow("" + (i + 1));
			bill.setsMin("" + (i + 1));
			bill.setsMax("" + (i + 1));
			bill.setDisplayValue("" + (i + 1));
			bill.setActiveDate("" + (i + 1));
			bill.setInactiveDate("" + (i + 1));
			bill.setChangeDate("" + (i + 1));
			bill.setUsageStatus("" + (i + 1));

			bpm4002VoList.add(bill);
		}

		DataTableAjax<Bpm4002Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm4002Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm4002VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm4003Vo> findAll(Bpm4003Vo bpm4003Vo) {
		List<Bpm4003Vo> bpm4003VoList = new ArrayList<Bpm4003Vo>();

		for (int i = 0; i < 5; i++) {
			Bpm4003Vo bill = new Bpm4003Vo();
			bill.setTransactionId("" + (i + 1));
			bill.setProcessId("" + (i + 1));
			bill.setProcessType("" + (i + 1));
			bill.setProcessName("" + (i + 1));
			bill.setCreatedBy("" + (i + 1));
			bill.setStatus("" + (i + 1));
			bill.setCreatedDate("" + (i + 1));
			bill.setLogReport("" + (i + 1));

			bpm4003VoList.add(bill);
		}

		DataTableAjax<Bpm4003Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(bpm4003Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(bpm4003VoList);

		return dataTableAjax;

	}

	public DataTableAjax<Bpm4003FixCaseVo> findAll(Bpm4003FixCaseVo bpm4003FixCaseVo) {
		DataTableAjax<Bpm4003FixCaseVo> dataTableAjax = new DataTableAjax<>();
		try {
			List<Bpm4003FixCaseVo> bpm4003FixCaseVoList = new ArrayList<Bpm4003FixCaseVo>();

			for (int i = 0; i < 5; i++) {
				Bpm4003FixCaseVo bill = new Bpm4003FixCaseVo();
				bill.setExternalId("" + (i + 1));
				bill.setType("" + (i + 1));
				bill.setMiuErrorCode("" + (i + 1));
				bill.setCount("" + (i + 1));
				bill.setSum("" + (i + 1));
				bill.setMinTransDate("" + (i + 1));
				bill.setMaxTransDate("" + (i + 1));

				bpm4003FixCaseVoList.add(bill);
			}

			dataTableAjax.setDraw(bpm4003FixCaseVo.getDraw());
			dataTableAjax.setRecordsTotal(5l);
			dataTableAjax.setRecordsFiltered(5l);
			dataTableAjax.setData(bpm4003FixCaseVoList);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataTableAjax;

	}
}
