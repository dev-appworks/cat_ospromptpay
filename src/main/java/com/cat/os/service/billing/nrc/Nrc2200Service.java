package com.cat.os.service.billing.nrc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc2200Vo;

@Service
@Profile(Profiles.BILLING)
public class Nrc2200Service {
	
	public DataTableAjax<Nrc2200Vo> findAll(Nrc2200Vo nrc2200Vo){
		List<Nrc2200Vo> nrc2200VoList = new ArrayList<Nrc2200Vo>();
	
		
		
		for (int i = 0; i < 5 ; i++) {
			Nrc2200Vo nrc = new Nrc2200Vo();
			
			nrc.setNo(""+(i+1));
			nrc.setBa("9033478");
			nrc.setExternalId("89303848");
			
			nrc.setComponentId("10000000");
			nrc.setComponentName("componentName");
			
			nrc.setComponentOverrideName("componentOverrideName");
			nrc.setComponentOverrideRate("100.00");
			nrc.setComponentOverrideUnit("");
			
			nrc.setPackageId("100223");
			nrc.setPackageName("packageName");
			nrc.setPackagePkgInstDt("381223");
			
			nrc.setEffectiveDateActiceDt("12/04/2017");
			nrc.setEffectiveDateInactiveDt("");
			
			nrc.setRunDate("18/8/2017");
			nrc.setStatus("");
			nrc.setRerunNo("7299976");
			nrc.setCompinstid("");
			
			nrc2200VoList.add(nrc);

		}
		
		DataTableAjax<Nrc2200Vo> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(nrc2200Vo.getDraw());
		dataTableAjax.setRecordsTotal(5l);
		dataTableAjax.setRecordsFiltered(5l);
		dataTableAjax.setData(nrc2200VoList);
		
		return dataTableAjax ;
		
	
	}
	

}
