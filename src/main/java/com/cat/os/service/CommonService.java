package com.cat.os.service;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class CommonService {
	
	public static String maskFormat(String value, String format) throws ParseException {
		MaskFormatter mf = new MaskFormatter(format);
	    mf.setValueContainsLiteralCharacters(false);
		return mf.valueToString(value);
	}
	
	public static String maskString(String text, String maskFormat) throws ParseException {
		
		Integer start = 0;
		Integer end = 0;
		maskFormat = maskFormat.replace("X", "?");
		
		MaskFormatter mf = new MaskFormatter(maskFormat);	    
	    mf.setValueContainsLiteralCharacters(false);
	    
	    start += maskFormat.replace("-", "").indexOf("?");
	    end += maskFormat.replace("-", "").lastIndexOf("?") + 1;
//	    System.out.println("Start > " + start);
//	    System.out.println("End > " + end);
		if(StringUtils.isBlank(text))
			return null;
		
		text = StringUtils.overlay(text, StringUtils.repeat("X", StringUtils.countMatches(maskFormat, "?")), start, end);
//		System.out.println("TEXT > " + text);
		text = mf.valueToString(text);
		
		return text;
	}	
	
	public static boolean isNumbericDigit(String value, Integer digit) {
		return value.matches("[0-9]{" + digit + "}");
	}
	
}
