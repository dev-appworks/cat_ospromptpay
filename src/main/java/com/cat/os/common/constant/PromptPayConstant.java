package com.cat.os.common.constant;

public class PromptPayConstant {
	
	public static class SYSTEM_NAME {
		public static String USSD = "USSD";
	}
	
	public static class WS {
		public static String WS_SUCCESS = "0";
		public static String SUCCESS = "SUCCESS";
		public static String FAIL = "FAIL";
		public static String ERROR = "ERROR";
	}
	
	public static class IS_DELETED {
		public static int ACTIVE = 0;
		public static int INACTIVE = 1;
	}
	
	public static class IS_ACTIVE {
		public static int ACTIVE = 0;
		public static int INACTIVE = 1;
	}
	
	public static class STATUS {
		public static String ACTIVE = "ACTIVE";
		public static String INACTIVE = "INACTIVE";
		public static String PENDING = "PENDING";
	}
	
	public static class MOBILE_INFO_STATUS {
		public static String ACTIVE = "Active";
		public static String SUSPENDED_FRAUD = "Suspended-Fraud";
		public static String SUSPENDED_CUST = "Suspended-Cust";
		public static String SUSPENDED_CAT = "Suspended-CAT";
		public static String SUSPENDED_M_DEPT = "Suspended-M-Debt";
		public static String SUSPENDED_DEPT = "Suspended-Debt";
		public static String DISCONNECTED_FRAUD = "Disconnected-Fraud";
		public static String DISCONNECTED_CUST = "Disconnected-Cust";
		public static String DISCONNECTED_CAT = "Disconnected-CAT";
		public static String DISCONNECTED_M_DEPT = "Disconnected-M-Debt";
		public static String DISCONNECTED_DEPT = "Disconnected-Debt";
	}	
	
	public static class FORMAT {
		public static String DATE = "yyyyMMdd";
		public static String ID_CARD = "#-###X-XXXXX-##-#";
		public static String MOBILE_NO = "XX-XX##-####";
		public static String ID_CARD_LIB = "X-XXXX-XXXXX-XX-X";
	}	
	
	public static class MAPPING {
		public static String SMS_RESPONSE = "SMS_RESPONSE";
		public static String SMS_SUCCESS_MATCH = "SMS_SUCCESS_MATCH";
		public static String SMS_SUCCESS_NOT_MATCH = "SMS_SUCCESS_NOT_MATCH";
		public static String SMS_FAIL = "SMS_FAIL";
		public static String SOURCE_ADDR = "SOURCE_ADDR";
		public static String MASK_MOBILE_NO = "MASK_MOBILE_NO";
		public static String MASK_ID_CARD = "MASK_ID_CARD";
		public static String ID_CARD_LIB = "ID_CARD_LIB";
		public static String MOBILE_LIB = "MOBILE_LIB"; 
		public static String SMS_IDCARD_LESS_13 = "SMS_IDCARD_LESS_13";
		public static String SMS_IDCARD_MORE_13 = "SMS_IDCARD_MORE_13";
	}	
}