package com.cat.os.common.bean;

public class AuthenticationException extends Exception {

	private static final long serialVersionUID = -268167222316142858L;
	
	private String errorCode;
	private String errorDesc;
	
	public AuthenticationException(String errorCode, String errorDesc) {
		super(errorCode + errorDesc);
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDesc() {
		return errorDesc;
	}
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	
}
