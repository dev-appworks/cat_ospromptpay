package com.cat.os.common.utils;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import javax.xml.bind.JAXBElement;

import org.apache.commons.codec.binary.Base64;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.DigestUtils;

import com.cat.os.config.security.CustomUserPrincipal;

public class OsUtils {

	public static <T> T getValue(JAXBElement<T> jb) {
		if(jb == null) {
			return null;
		}
		
		return jb.getValue();
	}
	
	
	public static CustomUserPrincipal getCurrentUser() {
		CustomUserPrincipal user = (CustomUserPrincipal)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user;
	}
	
	public static String generateUUID() {
		return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
	}
	
	public static String encryptMD5Password(String pwdstr) {
		  
		  String pwdStr  = null;
		try {
			byte[] pwd = DigestUtils.md5Digest(pwdstr.getBytes("UTF8"));
			pwdStr = "{MD5}"+ Base64.encodeBase64String(pwd);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		  return pwdStr;
	}
	
}
