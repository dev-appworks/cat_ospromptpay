package com.cat.os.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OsDateUtils {

	private static final Logger log = LoggerFactory.getLogger(OsDateUtils.class);
	
	public final static String DD_MM_YYYY = "dd/MM/yyyy";

	public static XMLGregorianCalendar convertDateToXMLGregorianCalendar(Date date) {
		XMLGregorianCalendar calendar = null;
		
		if(date == null) {
			return calendar;
		}
		
		try {
			GregorianCalendar gregory = new GregorianCalendar();
			gregory.setTime(date);
			calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
		} catch (DatatypeConfigurationException e) {
			log.error("Error convertDateToXMLGregorianCalendar : ", e);
		}
		
		return calendar;
	}
	
	public static Date convertXMLGregorianCalendarToDate(XMLGregorianCalendar calendar) {
		Date date = null;
		
		if(calendar == null) {
			return date;
		}
		
		try {
			date = calendar.toGregorianCalendar().getTime();
		} catch (Exception e) {
			log.error("Error convertXMLGregorianCalendarToDate : ", e);
		}
		
		return date;
	}
	
	public static String getCalendarDateEnDDMMYYYYFormat() {
		Calendar calendar = Calendar.getInstance();
	    SimpleDateFormat format = new SimpleDateFormat(DD_MM_YYYY, Locale.ENGLISH);
	    String date = format.format(calendar.getTime());
	    
		return date;
	}
}
