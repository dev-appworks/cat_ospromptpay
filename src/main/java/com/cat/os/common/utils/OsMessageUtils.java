package com.cat.os.common.utils;

import javax.servlet.http.HttpServletRequest;

import com.cat.os.common.constant.MsgConstant;

public class OsMessageUtils {

	public static void setSuccess(HttpServletRequest httpServletRequest) {
		 OsMessageUtils.setSuccess(httpServletRequest, MsgConstant.SUCCESS_DESC);
	}
	
	public static void setSuccess(HttpServletRequest httpServletRequest , String message) {
		httpServletRequest.getSession().setAttribute(MsgConstant.SUCCESS_STATUS,message);
	}
	
	public static void setFail(HttpServletRequest httpServletRequest) {
		OsMessageUtils.setFail(httpServletRequest, MsgConstant.ERROR_DESC);
	}
	
	public static void setFail(HttpServletRequest httpServletRequest , String message) {
		httpServletRequest.getSession().setAttribute(MsgConstant.ERROR_STATUS,message);
	}
}
