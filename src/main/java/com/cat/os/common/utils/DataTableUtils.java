package com.cat.os.common.utils;

import java.util.Iterator;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.bean.DataTableColumn;
import com.cat.os.common.bean.DataTableOrder;
import com.cat.os.common.constant.OmUDBConstant;

public class DataTableUtils {
	
	public static Pageable toPages(int start , int length) {
		int p = 0;
		if(start > 0) {
			p = (start / length);
		}
		Pageable pageable = new PageRequest(p, length);
		return pageable;
	}
	
	public static <T> void toList(Page<T> pages, DataTableAjax<T> dataTableAjax) {
		if(pages == null) {
			dataTableAjax.setRecordsTotal(0l);
			dataTableAjax.setRecordsFiltered(0l);
			return ;
		}
		
		dataTableAjax.setRecordsTotal(pages.getTotalElements());
		dataTableAjax.setRecordsFiltered(pages.getTotalElements());
		Iterator<T> p = pages.iterator();
		while (p.hasNext()) {
			T item = p.next();
			dataTableAjax.getData().add(item);
		}
	}
	
	public static String getFieldFromDataTableOmOrder(List<DataTableOrder> orders, List<DataTableColumn> columns) {
		String field = "";
		for (DataTableOrder order : orders) {
			String column = columns.get(order.getColumn()).getData();
			if (OmUDBConstant.FIELD.ORDER_ID_COLUMN.equals(column)) {
				field = OmUDBConstant.FIELD.ORDER_ID;
			}else if (OmUDBConstant.FIELD.SERVICE_ITEM_ID_COLUMN.equals(column)) {
				field = OmUDBConstant.FIELD.SERVICE_ITEM_ID;
			}else if (OmUDBConstant.FIELD.STATUS_DISPLAY_COLUMN.equals(column)) {
				field = OmUDBConstant.FIELD.STATUS_DISPLAY;
			}
		}
		return field;
	}
	
	public static String getFieldFromDataTableUserAgent(List<DataTableOrder> orders, List<DataTableColumn> columns) {
		String field = "";
		for (DataTableOrder order : orders) {
			String column = columns.get(order.getColumn()).getData();
			if (OmUDBConstant.FIELD.FIRST_NAME_COLUMN.equals(column)) {
				field = OmUDBConstant.FIELD.FIRST_NAME;
			}else if (OmUDBConstant.FIELD.LAST_NAME_COLUMN.equals(column)) {
				field = OmUDBConstant.FIELD.LAST_NAME;
			}else if (OmUDBConstant.FIELD.EMP_ID_COLUMN.equals(column)) {
				field = OmUDBConstant.FIELD.EMP_ID;
			}else if (OmUDBConstant.FIELD.AGENT_NAME_COLUMN.equals(column)) {
				field = OmUDBConstant.FIELD.AGENT_NAME;
			}
		}
		return field;
	}
	
	public static String getOrderByFromDataTable(List<DataTableOrder> orders) {
		String orderBy = "";
		for (DataTableOrder order : orders) {
			orderBy = order.getDir();
		}
		return orderBy;
	}
	
	public static StringBuilder setParamToSqlSortDB2(StringBuilder sql, String field, String orderBy) {
		if (sql != null) {
			sql = new StringBuilder(String.format(sql.toString(), field, orderBy, field, orderBy));
		}
		
		return sql;
	}
}
