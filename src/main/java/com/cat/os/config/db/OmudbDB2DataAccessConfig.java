package com.cat.os.config.db;

import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.ProjectConstant.TransactionManagerRef;
import com.cat.os.dao.entity.OmOrders;
import com.cat.os.dao.entity.OrdsvcItems;
import com.cat.os.dao.entity.UETerritoryAgent;
import com.ibm.db2.jcc.DB2SimpleDataSource;

@Configuration
@EnableTransactionManagement
@ConfigurationProperties(value = "omdb.db2")
@EnableJpaRepositories(basePackages = "com.cat.os.dao.omudb", entityManagerFactoryRef = "omudbManagerFactory", transactionManagerRef = TransactionManagerRef.OMUDB_DB2)
@Profile(Profiles.OMUDB)
public class OmudbDB2DataAccessConfig {

	private static final Logger log = LoggerFactory.getLogger(OmudbDB2DataAccessConfig.class);
	
	private String host;
	private Integer port;
	private String username;
	private String password;
	private String databaseName;

	@Bean
	public DataSource omudbDataSource() throws SQLException {
		log.info("@@ DB2SimpleDataSource DataAccessConfig...");
		DB2SimpleDataSource db2SimpleDataSource = new DB2SimpleDataSource();
		db2SimpleDataSource.setServerName(host);
		db2SimpleDataSource.setPortNumber(port);
		db2SimpleDataSource.setUser(username);
		db2SimpleDataSource.setPassword(password);
		db2SimpleDataSource.setDriverType(4);
		db2SimpleDataSource.setDatabaseName(databaseName);
		return db2SimpleDataSource;
	}

	@Bean(name="omudbJdbcTemplate")
	public JdbcTemplate omudbJdbcTemplate() throws SQLException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(omudbDataSource());
		return jdbcTemplate;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean omudbManagerFactory(EntityManagerFactoryBuilder builder) throws SQLException {
		return builder.dataSource(omudbDataSource()).packages(
				OmOrders.class,
				OrdsvcItems.class,
				UETerritoryAgent.class
				).persistenceUnit("omudb")
				.build();
	}

	@Bean(name=TransactionManagerRef.OMUDB_DB2)
	public PlatformTransactionManager omudbTransactionManager(@Qualifier("omudbManagerFactory")EntityManagerFactory omudbManagerFactory) throws SQLException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(omudbManagerFactory);
		transactionManager.setDataSource(omudbDataSource());
		return transactionManager;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

}
