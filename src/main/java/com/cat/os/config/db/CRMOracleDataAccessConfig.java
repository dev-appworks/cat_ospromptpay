package com.cat.os.config.db;

import java.sql.SQLException;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Map;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.entity.CatCrmMobileInfo;
import com.cat.os.dao.entity.PromptpayConsent;
import com.cat.os.dao.entity.PromptpayMapping;
import com.cat.os.dao.entity.PromptpayReferenceCode;

import oracle.jdbc.pool.OracleDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.cat.os.dao.promptpay", entityManagerFactoryRef = "crmOracleEntityManagerFactory", transactionManagerRef = "crmTransactionManager")
@Profile(Profiles.PROMPTPAY)
public class CRMOracleDataAccessConfig {

	private static final Logger log = LoggerFactory.getLogger(CRMOracleDataAccessConfig.class);

	@Value("${crm.oracle.url}")
	private String url;

	@Value("${crm.oracle.jpa.properties.hibernate.dialect}")
	private String dialect;

	@Value("${crm.oracle.datasource.driver-class-name}")
	private String driverClassName;

	@Value("${crm.oracle.user}")
	private String username;

	@Value("${crm.oracle.password}")
	private String password;

	@Value("${crm.oracle.jndi}")
	private String jndi;

	@Bean
	@ConfigurationProperties("crm.oracle")
	public DataSource crmOracleDataSource() throws SQLException, IllegalArgumentException, NamingException {
		log.info("@@ Cat CRM Oracle DataAccessConfig...");
		DataSource dsObj = null;
		if (StringUtils.isNotBlank(jndi)) {
			JndiObjectFactoryBean jndiObj = new JndiObjectFactoryBean();
			jndiObj.setJndiName(jndi);
			jndiObj.setProxyInterface(DataSource.class);
			jndiObj.setLookupOnStartup(false);
			jndiObj.afterPropertiesSet();

			dsObj = (DataSource) jndiObj.getObject();
			log.info("@@ Load JNDI Cat BILLING Oracle DataAccessConfig...");
		} else {
			DataSource ds = DataSourceBuilder.create().username(username).password(password)
					.driverClassName(driverClassName).url(url).build();
			dsObj = ds;

			log.info("@@ Load JDBC Cat BILLING Oracle DataAccessConfig...");
		}
		return dsObj;
//		OracleDataSource oracleDataSource = new OracleDataSource();
//		return oracleDataSource;
	}

	@Bean(name = "crmOracleEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean crmOracleEntityManagerFactory(EntityManagerFactoryBuilder builder)
			throws SQLException, IllegalArgumentException, NamingException {
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("hibernate.dialect", dialect);
		return builder
				.dataSource(crmOracleDataSource()).packages(CatCrmMobileInfo.class, PromptpayConsent.class,
						PromptpayReferenceCode.class, PromptpayMapping.class)
				.persistenceUnit("crmoracle").properties(properties).build();
	}

	@Bean(name = "crmTransactionManager")
	public PlatformTransactionManager ussdOraclTransactionManager(
			@Qualifier("crmOracleEntityManagerFactory") EntityManagerFactory crmOracleEntityManagerFactory)
			throws SQLException, IllegalArgumentException, NamingException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(crmOracleEntityManagerFactory);
		transactionManager.setDataSource(crmOracleDataSource());
		return transactionManager;
	}

	@Bean("crmOracleJdbcTemplate")
	public JdbcTemplate crmOracleJdbcTemplate() throws SQLException, IllegalArgumentException, NamingException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(crmOracleDataSource());
		return jdbcTemplate;
	}

}
