package com.cat.os.config.db;

import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.ProjectConstant.TransactionManagerRef;
import com.cat.os.dao.entity.ServiceStatus;
import com.cat.os.dao.entity.ServicesTimer;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.cat.os.dao.ussd", entityManagerFactoryRef = "ussdOracleEntityManagerFactory", transactionManagerRef = TransactionManagerRef.USSD_ORACLE)
@Profile(Profiles.USSD)
public class USSDOracleDataAccessConfig {
	
	private static final Logger log = LoggerFactory.getLogger(USSDOracleDataAccessConfig.class);

	@Bean
	@ConfigurationProperties("ussd.oracle")
	public DataSource ussdOracleDataSource() {
		log.info("@@ Cat USSD Oracle DataAccessConfig...");
//		OracleDataSource oracleDataSource = new OracleDataSource();
//		oracleDataSource.setURL(url);
//		oracleDataSource.setUser(username);
//		oracleDataSource.setPassword(password);
		return DataSourceBuilder.create().build();
	}

	
	@Bean
	public LocalContainerEntityManagerFactoryBean ussdOracleEntityManagerFactory(
	        EntityManagerFactoryBuilder builder) {
	    return builder
	            .dataSource(ussdOracleDataSource())
	            .packages(ServiceStatus.class,ServicesTimer.class)
	            .persistenceUnit("ussdoracle")
	            .build();
	}
	
    @Bean(name=TransactionManagerRef.USSD_ORACLE)
    public PlatformTransactionManager ussdOraclTransactionManager(@Qualifier("ussdOracleEntityManagerFactory") EntityManagerFactory  ussdOracleEntityManagerFactory) {
        JpaTransactionManager transactionManager  = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(ussdOracleEntityManagerFactory);
        transactionManager.setDataSource(ussdOracleDataSource());
        return transactionManager;
    }
	
    
	@Bean("ussdOracleJdbcTemplate")
	public JdbcTemplate ussdOracleJdbcTemplate() throws SQLException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ussdOracleDataSource());
		return jdbcTemplate;
	}


}
