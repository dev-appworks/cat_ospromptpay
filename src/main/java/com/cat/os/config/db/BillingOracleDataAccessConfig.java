package com.cat.os.config.db;

import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.ProjectConstant.TransactionManagerRef;
import com.cat.os.dao.entity.ServiceStatus;
import com.cat.os.dao.entity.ServicesTimer;

import oracle.jdbc.pool.OracleDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.cat.os.dao.billing", entityManagerFactoryRef = "billingOracleEntityManagerFactory", transactionManagerRef = TransactionManagerRef.BILLING_ORACLE)
@Profile(Profiles.BILLING)
public class BillingOracleDataAccessConfig {
	
	private static final Logger log = LoggerFactory.getLogger(BillingOracleDataAccessConfig.class);

	@Bean
	@ConfigurationProperties("oracle.billing")
	public DataSource billingOracleDataSource() throws SQLException {
		log.info("@@ Cat BILLING Oracle DataAccessConfig...");
		OracleDataSource oracleDataSource = new OracleDataSource();
//		oracleDataSource.setURL(url);
//		oracleDataSource.setUser(username);
//		oracleDataSource.setPassword(password);
		return oracleDataSource;
	}

	
	@Bean(name="billingOracleEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean billingOracleEntityManagerFactory(
	        EntityManagerFactoryBuilder builder) throws SQLException {
	    return builder
	            .dataSource(billingOracleDataSource())
	            .packages(ServiceStatus.class,ServicesTimer.class)
	            .persistenceUnit("billingOracle")
	            .build();
	}
	
    @Bean(name=TransactionManagerRef.BILLING_ORACLE)
    public PlatformTransactionManager ussdOraclTransactionManager(@Qualifier("billingOracleEntityManagerFactory") EntityManagerFactory  billingOracleEntityManagerFactory) throws SQLException {
        JpaTransactionManager transactionManager  = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(billingOracleEntityManagerFactory);
        transactionManager.setDataSource(billingOracleDataSource());
        return transactionManager;
    }
	
    
	@Bean("billingOracleJdbcTemplate")
	public JdbcTemplate billingOracleJdbcTemplate() throws SQLException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(billingOracleDataSource());
		return jdbcTemplate;
	}


}
