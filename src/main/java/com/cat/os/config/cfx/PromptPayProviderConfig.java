package com.cat.os.config.cfx;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.cat.os.cfxwebservice.provider.PromptPayServiceProvider;
import com.cat.os.common.constant.ProjectConstant.Profiles;

@Configuration
@Profile(Profiles.PROMPTPAY)
public class PromptPayProviderConfig {

	@Autowired
	private Bus bus;

	@Bean
	public Endpoint promptPayEndpoint() {
		EndpointImpl endpoint = new EndpointImpl(bus, promptPayServiceProvider());
		endpoint.publish("/promptpay");
		return endpoint;
	}

	@Bean
	public PromptPayServiceProvider promptPayServiceProvider() {
		return new PromptPayServiceProvider();
	}
}
