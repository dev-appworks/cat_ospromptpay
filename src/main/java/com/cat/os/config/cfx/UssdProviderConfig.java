package com.cat.os.config.cfx;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.cat.os.cfxwebservice.provider.UssdSeviceProvider;
import com.cat.os.common.constant.ProjectConstant.Profiles;

@Configuration
@Profile(Profiles.USSD)
public class UssdProviderConfig {

	@Autowired
	private Bus bus;

	@Bean
	public Endpoint ussdEndpoint() {
		EndpointImpl endpoint = new EndpointImpl(bus, ussdSeviceProvider());
		endpoint.publish("/ussd");
		return endpoint;
	}

	@Bean
	public UssdSeviceProvider ussdSeviceProvider() {
		return new UssdSeviceProvider();
	}
}
