package com.cat.os.config.jobconfig;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.job.UssdAutoOnOffJob;
import com.cat.os.service.ussd.UssdAutoOnOffJobService;

@Configuration
@ConditionalOnProperty("os.ussd.onoff.job.enable")
@Profile(Profiles.USSD)
public class UssdBatchJobSchedulerConfig {
	
	private static final Logger log = LoggerFactory.getLogger(UssdAutoOnOffJob.class);

	@Autowired
	private UssdAutoOnOffJobService ussdAutoOnOffJobService;
	
	@Value("${os.ussd.onoff.job.cronExpressions}")
	private String cronExpressions;
	
	@PostConstruct
	public void init(){
		log.info("### UssdBatchJobSchedulerConfig cronEx : " + cronExpressions);
	}
	
	@Bean
	public JobDetail ussdJobDetail() {
		JobDataMap newJobDataMap = new JobDataMap();
		newJobDataMap.put("ussdAutoOnOffJobService", ussdAutoOnOffJobService);
		JobDetail job = JobBuilder.newJob(UssdAutoOnOffJob.class)
			      .withIdentity("ussdJobDetail", "group1") // name "myJob", group "group1"
			      .usingJobData(newJobDataMap )
			      .build();
		return job;
	}
	
	@Bean
	public CronTrigger ussdCronTrigger() {
		CronTrigger trigger = TriggerBuilder.newTrigger()
			    .withIdentity("ussdCronTrigger", "group1")
			    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpressions))
			    .build();
		return trigger;
	}
	
	@Bean
	public SchedulerFactory  ussdSchedulerFactory() throws SchedulerException {
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();
		sched.scheduleJob(ussdJobDetail(), ussdCronTrigger());
		sched.start();
		return sf;
	}
	
	@PreDestroy
	public void destroy() throws SchedulerException {
		log.info("UssdBatchJobSchedulerConfig.. shutdown");
		ussdSchedulerFactory().getScheduler().shutdown();
	}
}
