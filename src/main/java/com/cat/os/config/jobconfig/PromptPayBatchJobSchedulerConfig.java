package com.cat.os.config.jobconfig;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.job.PromptPayAutoJob;
import com.cat.os.service.ussd.PromptPayAutoJobService;

@Configuration
@ConditionalOnProperty("os.promptpay.job.enable")
@Profile(Profiles.PROMPTPAY)
public class PromptPayBatchJobSchedulerConfig {
	
	private static final Logger log = LoggerFactory.getLogger(PromptPayAutoJob.class);

	@Autowired
	private PromptPayAutoJobService promptpayAutoJobService;
	
	@Value("${os.promptpay.job.cronExpressions}")
	private String cronExpressions;
	
	@PostConstruct
	public void init(){
		log.info("### PromptPayBatchJobSchedulerConfig cronEx : " + cronExpressions);
	}
	
	@Bean
	public JobDetail promptpayJobDetail() {
		JobDataMap newJobDataMap = new JobDataMap();
		newJobDataMap.put("promptpayAutoJobService", promptpayAutoJobService);
		JobDetail job = JobBuilder.newJob(PromptPayAutoJob.class)
			      .withIdentity("promptpayJobDetail", "group1") // name "myJob", group "group1"
			      .usingJobData(newJobDataMap )
			      .build();
		return job;
	}
	
	@Bean
	public CronTrigger promptpayCronTrigger() {
		log.info(cronExpressions);
		CronTrigger trigger = TriggerBuilder.newTrigger()
			    .withIdentity("promptpayCronTrigger", "group1")
			    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpressions))
			    .build();
		return trigger;
	}
	
	@Bean
	public SchedulerFactory  promptpaySchedulerFactory() throws SchedulerException {
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();
		sched.scheduleJob(promptpayJobDetail(), promptpayCronTrigger());
		sched.start();
		return sf;
	}
	
	@PreDestroy
	public void destroy() throws SchedulerException {
		log.info("PromptPayBatchJobSchedulerConfig.. shutdown");
		promptpaySchedulerFactory().getScheduler().shutdown();
	}
}
