package com.cat.os.config.security;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cat.os.config.security.CatLdapAuthenticationProvider.CatUserDetailsService;

@Service
public class CustomUserDetailsService implements UserDetailsService , CatUserDetailsService {

	private static final Logger log = LoggerFactory.getLogger(CustomUserDetailsService.class);

	@Autowired
	private PasswordEncoder encoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		CustomUserPrincipal customUserPrincipal = new CustomUserPrincipal();
		String pwd = encoder.encode("password");
		log.info("username=>" + username);
//		log.info("password=>" + pwd);
		customUserPrincipal.setUserName(username);
		customUserPrincipal.setPassword(pwd);
		
		if("user".equalsIgnoreCase(username)) {
			SimpleGrantedAuthority user = new SimpleGrantedAuthority("ROLE_USER");
			customUserPrincipal.addRole(user);
			customUserPrincipal.setFullName("user  user");
		}else
		if("admin".equalsIgnoreCase(username)) {
			SimpleGrantedAuthority user = new SimpleGrantedAuthority("ROLE_ADMIN");
			customUserPrincipal.addRole(user);
			user = new SimpleGrantedAuthority("ROLE_ACTUATOR");
			customUserPrincipal.addRole(user);
			customUserPrincipal.setFullName("admin  admin");
		}else {
			SimpleGrantedAuthority user = new SimpleGrantedAuthority("ROLE_OTHER");
			customUserPrincipal.addRole(user);
			customUserPrincipal.setFullName("other  kak");
		}

		log.info("getAuthorities => " + customUserPrincipal.getAuthorities());
		return customUserPrincipal;
	}

	@Override
	public UserDetails loadUserDetails(String user, String fullName, List<String> catRoles) throws UsernameNotFoundException {
		CustomUserPrincipal customUserPrincipal = new CustomUserPrincipal();
		SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_ADMIN");
		customUserPrincipal.addRole(simpleGrantedAuthority);
		customUserPrincipal.setFullName(fullName);
		customUserPrincipal.setUserName(user);
		return customUserPrincipal;
	}

}
