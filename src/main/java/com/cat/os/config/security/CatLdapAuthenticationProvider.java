package com.cat.os.config.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CatLdapAuthenticationProvider implements AuthenticationProvider {

	private static final Logger log = LoggerFactory.getLogger(CatLdapAuthenticationProvider.class);

	private Hashtable<String, String> env = new Hashtable<String, String>();
	private static String FULL_NAME = "FULL_NAME";
	private static String ROLES = "ROLES";
	private CatUserDetailsService defaultCatUserDetailsService = null;
	private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();

	@Value("${ldap.cat.host}")
	private String host;
	@Value("${ldap.cat.port}")
	private Integer port;
	@Value("${ldap.cat.base}")
	private String base;
	@Value("${ldap.cat.accont.username}")
	private String username;
	@Value("${ldap.cat.accont.password}")
	private String password;

	@PostConstruct
	public void init() {
		String url = String.format("ldap://%s:%d/%s",host,port,base) ;
		log.info("##### LDAP URL : " + url);
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,url);
		env.put(Context.SECURITY_PRINCIPAL, username);
		env.put(Context.SECURITY_CREDENTIALS, password);
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String userName = authentication.getName();
		String password = authentication.getCredentials().toString();

		try {
			Map<String, Object> res = this.doAuthenicat(userName, password);
			@SuppressWarnings("unchecked")
			UserDetails user = defaultCatUserDetailsService.loadUserDetails(userName, (String) res.get(FULL_NAME),
					(List<String>) res.get(ROLES));

			UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user,
					authentication.getCredentials(), authoritiesMapper.mapAuthorities(user.getAuthorities()));

			result.setDetails(authentication.getDetails());

			return result;
		} catch (Exception e) {
			log.error("authenticate : " + e.getMessage());
			throw new BadCredentialsException("username or password not match.");
		}

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	private Map<String, Object> doAuthenicat(String user, String password) throws Exception {
		DirContext ctx;
		List<String> ldapRoles = new ArrayList<String>();
		Map<String, Object> authen = new HashMap<>();

		try {
			ctx = new InitialDirContext(env);
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}

		NamingEnumeration<SearchResult> answer = null;

		try {

			// Create the default search controls
			SearchControls ctls = new SearchControls();
			ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			String filter = String.format("(&(uid=%s))", user);
			log.info("ldap filter : " + filter);
			// Search for objects using the filter
			answer = ctx.search("", filter, ctls);

			log.info("results.hasMore() >> " + answer.hasMore());
			if (!answer.hasMore()) {
				log.info("User not found in Ldap : " + user);
				throw new UsernameNotFoundException("User not found in Ldap : " + user);
			}
			while (answer.hasMore()) {
				SearchResult searchResult = (SearchResult) answer.next();

				Attributes attributes = searchResult.getAttributes();
				Attribute attr = attributes.get("cn");
				log.info("USER = " + attr);

				log.info("authen password ............ "  + searchResult.getNameInNamespace());
				Hashtable<String, String> authenpass = new Hashtable<String, String>();
				authenpass.put(Context.INITIAL_CONTEXT_FACTORY, env.get(Context.INITIAL_CONTEXT_FACTORY));
				authenpass.put(Context.PROVIDER_URL, env.get(Context.PROVIDER_URL));
				authenpass.put(Context.SECURITY_PRINCIPAL, searchResult.getNameInNamespace());
				authenpass.put(Context.SECURITY_CREDENTIALS, password);
				new InitialDirContext(authenpass);

				log.info("Get  Roles ............");
				String groupFilter = String.format("(member=%s)", searchResult.getNameInNamespace());
				System.out.println("groupFilter == " + groupFilter);
				SearchControls ctls2 = new SearchControls();
				ctls2.setSearchScope(SearchControls.SUBTREE_SCOPE);
				NamingEnumeration<SearchResult> groupAnswer = ctx.search("", groupFilter, ctls2);
				while (groupAnswer.hasMore()) {
					SearchResult groupSearchResult = groupAnswer.next();
					Attributes gattributes = groupSearchResult.getAttributes();
					Attribute gattr = gattributes.get("cn");
					log.debug("ROLE = " + gattr);
					ldapRoles.add(gattr.get().toString());
				}

				// finish
				authen.put(CatLdapAuthenticationProvider.FULL_NAME, attr.get().toString());
				authen.put(CatLdapAuthenticationProvider.ROLES, ldapRoles);

				break;

			}
		} catch (NameNotFoundException e) {
			// The base context was not found.
			// Just clean up and exit.
			throw new Exception(e.getMessage());
		} catch (NamingException e) {
			throw new Exception(e.getMessage());
		} finally {
			if (answer != null) {
				try {
					answer.close();
				} catch (Exception e) {
					// Never mind this.
				}
			}
			if (ctx != null) {
				try {
					ctx.close();
				} catch (Exception e) {
					// Never mind this.
				}
			}
		}

		// end

		return authen;
	}

	public interface CatUserDetailsService {
		UserDetails loadUserDetails(String user, String fullName, List<String> catRoles)
				throws UsernameNotFoundException;
	}

	public void setUserDetailsService(CatUserDetailsService defaultCatUserDetailsService) {
		this.defaultCatUserDetailsService = defaultCatUserDetailsService;
	}

}
