package com.cat.os.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cat.os.service.ussd.UssdAutoOnOffJobService;

public class UssdAutoOnOffJob implements Job {

	private static final Logger log = LoggerFactory.getLogger(UssdAutoOnOffJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("execute");
//		   	  JobKey key = context.getJobDetail().getKey();
		      JobDataMap dataMap = context.getJobDetail().getJobDataMap();
//		      String jobSays = dataMap.getString("jobSays");
			try {
				UssdAutoOnOffJobService ussdAutoOnOffJobService = (UssdAutoOnOffJobService) dataMap.get("ussdAutoOnOffJobService");
				ussdAutoOnOffJobService.runUssdAuto();
			}catch (Exception e) {
				log.error("UssdAutoOnOffJob" , e);
			}
		
	}


}
