package com.cat.os.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cat.os.service.ussd.PromptPayAutoJobService;

public class PromptPayAutoJob implements Job {

	private static final Logger log = LoggerFactory.getLogger(PromptPayAutoJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("execute");
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		
		try {
			PromptPayAutoJobService promptpayAutoJobService = (PromptPayAutoJobService) dataMap.get("promptpayAutoJobService");
			promptpayAutoJobService.runAuto();
		}catch (Exception e) {
			log.error("PromptPayAutoJob" , e);
		}
		
	}
}
