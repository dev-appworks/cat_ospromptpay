package com.cat.os.job;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cat.os.service.billing.inw.ImportIbacssPaymentDataJobService;

@Service
public class ImportIbacssPaymentDataJob implements Job {
	
	private static final Logger log = LoggerFactory.getLogger(ImportIbacssPaymentDataJob.class);
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("execute");
		
		Date currentDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.US);
		String txDate = dateFormat.format(currentDate);
		
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		try {
			ImportIbacssPaymentDataJobService service = (ImportIbacssPaymentDataJobService) dataMap.get("importIbacssPaymentDataJobService");
			service.runJob(txDate);
		} catch (Exception e) {
			log.error("importIbacssPaymentDataJob", e);
		}
	}
	
}
