package com.cat.os.controller.omudb;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.utils.OsMessageUtils;
import com.cat.os.dao.vo.OmFormVo;
import com.cat.os.dao.vo.OmOrderVo;
import com.cat.os.dao.vo.OmudbHistoryFormVo;
import com.cat.os.dao.vo.OmudbHistoryVo;
import com.cat.os.service.omudb.OmudbService;

@Controller
@RequestMapping("/omudb")
@Profile(Profiles.OMUDB)
public class OmOrderManagementController {

	private static final Logger log = LoggerFactory.getLogger(OmOrderManagementController.class);

	@Autowired
	private OmudbService omudbService;
	
	@GetMapping("omudb1001/init")
	public ModelAndView omudb1001() {
		ModelAndView mav = new ModelAndView("views/omudb/omudb1001.html");
		OmFormVo omFormVo = new OmFormVo();
		String statusMappingKeys = omudbService.getStatusMapingKey();
		omFormVo.setStatusMappingKeys(statusMappingKeys);
		mav.addObject("omFormVo", omFormVo);
		

		return mav;
	}
	
	
	@PostMapping("omudb1001/orderbyTask")
	@ResponseBody
	public DataTableAjax<OmOrderVo> orderbyTask(@RequestBody OmFormVo omFormVo) {
		return omudbService.orderbyTask(omFormVo);
	}
	
	
	@GetMapping("omudb2001/init")
	public ModelAndView omudb2001() {
		ModelAndView mav = new ModelAndView("views/omudb/omudb2001.html");
		OmFormVo omFormVo = new OmFormVo();
		String statusMappingKeys = omudbService.getStatusMapingKey();
		omFormVo.setStatusMappingKeys(statusMappingKeys);
		mav.addObject("omFormVo", omFormVo);
		
		return mav;
	}
	
	
	@PostMapping("omudb1001/orderbyServiceItem")
	@ResponseBody
	public DataTableAjax<OmOrderVo> orderbyServiceItem(@RequestBody OmFormVo omFormVo) {
		return omudbService.orderbyServiceItem(omFormVo);
	}
	

	@GetMapping("omudb1002/edit")
	public ModelAndView omudb1002Edit(@ModelAttribute OmFormVo omFormVo) {
		ModelAndView mav = new ModelAndView("views/omudb/omudb1002.html");
		mav.addObject("omFormVo", omFormVo);
		mav.addObject("dropdown", omudbService.getFilterDropdownByStatusCode(omFormVo.getStatusCode()));
		return mav;
	}
	
	@PostMapping("omudb1002/edit")
	public ModelAndView omudb1002PostEdit(@ModelAttribute OmFormVo omFormVo , Principal principal, HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/omudb/omudb1002.html");
		mav.addObject("omFormVo", omFormVo);
		mav.addObject("dropdown", omudbService.getFilterDropdownByStatusCode(omFormVo.getStatusCode()));
		
		String userlogin = principal.getName();
		try {
			omudbService.changeOrderTaskStatus(omFormVo,userlogin);
			mav.setViewName("redirect:/omudb/omudb1001/init");
			OsMessageUtils.setSuccess(httpServletRequest);
		} catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("omudb1002/edit", e);
		}
		
		return mav;
	}
	
	

	@GetMapping("omudb2002/edit")
	public ModelAndView omudb2002Edit(@ModelAttribute OmFormVo omFormVo) {
		ModelAndView mav = new ModelAndView("views/omudb/omudb2002.html");
		mav.addObject("omFormVo", omFormVo);
		mav.addObject("dropdown", omudbService.getFilterDropdownByStatusCode(omFormVo.getStatusCode()));
		return mav;
	}
	
	@PostMapping("omudb2002/edit")
	public ModelAndView omudb10022PostEdit(@ModelAttribute OmFormVo omFormVo , Principal principal, HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/omudb/omudb2002.html");
		mav.addObject("omFormVo", omFormVo);
		mav.addObject("dropdown", omudbService.getFilterDropdownByStatusCode(omFormVo.getStatusCode()));
		
		String userlogin = principal.getName();
		try {
			omudbService.changeOrderStatusByServiceItem(omFormVo,userlogin);
			mav.setViewName("redirect:/omudb/omudb2001/init");
			OsMessageUtils.setSuccess(httpServletRequest);
		} catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("omudb1002/edit", e);
		}
		
		return mav;
	}
	
	
	@GetMapping("omudb4000/init")
	public ModelAndView omudb4000() {
		ModelAndView mav = new ModelAndView("views/omudb/omudb4000.html");
		mav.addObject("historyVo", new OmudbHistoryFormVo());
		return mav;
	}
	
	@PostMapping("omudb4000/omudbHistoryList")
	@ResponseBody
	public DataTableAjax<OmudbHistoryVo> omudb4000HistoryList(@RequestBody OmudbHistoryFormVo historyVo) {
		return omudbService.getOmudbHistoryCriteria(historyVo);
	}
}
