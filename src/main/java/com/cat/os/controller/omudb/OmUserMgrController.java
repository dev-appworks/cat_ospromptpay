package com.cat.os.controller.omudb;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.BusinessException;
import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.OmUDBConstant;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.common.utils.OsMessageUtils;
import com.cat.os.common.utils.OsUtils;
import com.cat.os.config.security.CustomUserPrincipal;
import com.cat.os.dao.entity.UAgent;
import com.cat.os.dao.vo.OmUserFormVo;
import com.cat.os.dao.vo.UeTerritoryDropdown;
import com.cat.os.service.omudb.OmExportFileService;
import com.cat.os.service.omudb.OmUserMgrService;

@Controller
@RequestMapping("/omudb/usermgr")
@Profile(Profiles.OMUDB)
public class OmUserMgrController {
	private static final Logger log = LoggerFactory.getLogger(OmUserMgrController.class);

	@Autowired
	private OmUserMgrService omUserMgrService;
	
	@Autowired
	private OmExportFileService omExportFileService;
	
	@GetMapping("omudb3001/init")
	public ModelAndView omudb3001init() {
		ModelAndView mav = new ModelAndView("views/omudb/omudb3001.html");
		mav.addObject("omUserFormVo", new OmUserFormVo());
		return mav;
	}
	
	@PostMapping("omudb3001/userList")
	@ResponseBody
	public DataTableAjax<UAgent> query(@RequestBody OmUserFormVo omUserFormVo) {
		DataTableAjax<UAgent> dataTableAjax = omUserMgrService.findOmUserByCriteria(omUserFormVo);
		return dataTableAjax;
	}
	

	@GetMapping("omudb3002/init")
	public ModelAndView omudb3002init() {
		ModelAndView mav = new ModelAndView("views/omudb/omudb3002.html");
		OmUserFormVo form = new OmUserFormVo();
		form.setIsNew(true);
		form.setActionType(OmUDBConstant.ACTION_TYPE_SUBMIT);
		mav.addObject("omUserFormVo", form);
		
		List<UeTerritoryDropdown> dropdown = omUserMgrService.getTerritoryDropdown();
		mav.addObject("dropdown", dropdown);

		List<String> groups = omUserMgrService.getGroupLdap();
		mav.addObject("groupLdap", groups);
		return mav;
	}
	
	@GetMapping("omudb3002/edit")
	public ModelAndView omudb3002edit(@ModelAttribute OmUserFormVo omUserFormVo, HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/omudb/omudb3002.html");
		try {
			
			OmUserFormVo form = omUserMgrService.findUserByAgenId(omUserFormVo);
			List<UeTerritoryDropdown> dropdown = omUserMgrService.getTerritoryDropdown();
			form.setIsNew(false);
			mav.addObject("omUserFormVo", form);
			mav.addObject("dropdown", dropdown);
			
			List<String> groups = omUserMgrService.getGroupLdap();
			mav.addObject("groupLdap", groups);
		}catch (Exception e) {
			OmUserFormVo error = new OmUserFormVo();
			error.setIsNew(false);
			mav.addObject("omUserFormVo", error);
			OsMessageUtils.setFail(httpServletRequest, e.getMessage());
		}
		
		return mav;
	}
	
	@PostMapping("omudb3002/edit")
	public ModelAndView omudb3002editPost(@ModelAttribute OmUserFormVo omUserFormVo ,  HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/omudb/omudb3002.html");

		try {
			
			List<UeTerritoryDropdown> dropdown = omUserMgrService.getTerritoryDropdown();
			List<String> groups = omUserMgrService.getGroupLdap();
			mav.addObject("dropdown", dropdown);
			mav.addObject("groupLdap", groups);
			
			if(OmUDBConstant.ACTION_TYPE_ADD_GROUP.equalsIgnoreCase(omUserFormVo.getActionType())) {
				
				if(omUserFormVo.getGroupAdd() != null) {
					omUserFormVo.getGroupAdd().add(omUserFormVo.getCurrentAddGroup());
					omUserFormVo.setCurrentAddGroup(null);
				}
				
			}
			
			if(OmUDBConstant.ACTION_TYPE_DEL_GROUP.equalsIgnoreCase(omUserFormVo.getActionType())) {
				
				if(omUserFormVo.getGroupAdd() != null && StringUtils.isNotBlank(omUserFormVo.getCurrentDelGroup())) {
					if(omUserFormVo.getIsNew()) {
						omUserFormVo.getGroupAdd().remove(omUserFormVo.getCurrentDelGroup());
						omUserFormVo.setCurrentDelGroup(null);
					}else {
						if(omUserFormVo.getGroupTempInLdap() != null) {
							int res = omUserFormVo.getCurrentDelGroup().indexOf(omUserFormVo.getCurrentDelGroup());
							if(res != -1) {
								if(omUserFormVo.getGroupDel().indexOf(omUserFormVo.getCurrentDelGroup()) == -1) {
									omUserFormVo.getGroupDel().add(omUserFormVo.getCurrentDelGroup());
								}
							}
							omUserFormVo.getGroupAdd().remove(omUserFormVo.getCurrentDelGroup());
							omUserFormVo.setCurrentDelGroup(null);
						}
					}
				}
				
			}

			if(OmUDBConstant.ACTION_TYPE_SUBMIT.equalsIgnoreCase(omUserFormVo.getActionType())) {
				if( omUserFormVo.getIsNew()) {
					String uidnumber = omUserMgrService.getRunningNumber();
					omUserFormVo.setUidNumber(uidnumber);
					omUserMgrService.createNewUser(omUserFormVo);
				}else {
					omUserMgrService.updateUser(omUserFormVo);
				}
				OsMessageUtils.setSuccess(httpServletRequest);
				mav.addObject("dropdown", null);
				mav.addObject("groupLdap", null);
				mav.setViewName("redirect:/omudb/usermgr/omudb3001/init");
			}
			
			
		} catch (BusinessException e) {
			OsMessageUtils.setFail(httpServletRequest, e.getErrorDesc());
		} catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("omudb3002editPost", e);
		}
		mav.addObject("omUserFormVo", omUserFormVo);
		return mav;
	}
	
	
	@GetMapping("omudb3001/userList/export")
	public ResponseEntity<FileSystemResource> export(@ModelAttribute OmUserFormVo omUserFormVo, HttpServletRequest request) throws Exception {
		CustomUserPrincipal user = OsUtils.getCurrentUser();
		FileSystemResource ins = omExportFileService.exportFile(omUserFormVo);
        String fileName = String.format("Export_%s_%s_%s.csv", "User_OM", user.getUserName(), OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
        		
		HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(MediaType.TEXT_PLAIN);
        respHeaders.setContentLength(ins.getFile().length());
        respHeaders.setContentDispositionFormData("attachment", fileName);
        
		return new ResponseEntity<FileSystemResource>(ins, respHeaders, HttpStatus.OK);
	}
	
}
