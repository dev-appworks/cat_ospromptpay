package com.cat.os.controller;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.dao.vo.ArmVo;

@Controller
public class HomeController {

	private static final Logger log = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private Environment environment;
	
	@GetMapping("home2")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/home/homePage");
		ArmVo armVo = new ArmVo(10, "CODE");
		mav.addObject("armVo", armVo);
		return mav;
	}
	
	@GetMapping("home")
	public ModelAndView redirectHome() {
		ModelAndView mav = new ModelAndView();
		String[] ps = environment.getActiveProfiles();
		log.info("getActiveProfiles : "  + ps);
		if(ArrayUtils.contains(ps, "ussd")) {
			mav.setViewName("redirect:/ussd/os1001/init");
		}else if(ArrayUtils.contains(ps, "omudb")) {
			mav.setViewName("redirect:/omudb/omudb1001/init");
		}else if(ArrayUtils.contains(ps, "billing")) {
			mav.setViewName("redirect:bill/bill1000/");
		}else  if(ArrayUtils.contains(ps, "promptpay")) {
			mav.setViewName("redirect:/promptpay/report/init");
		}else{
			ArmVo armVo = new ArmVo(10, "CODE");
			mav.addObject("armVo", armVo);
			mav.setViewName("views/home/homePage");
		}
		return mav;
	}
	
	@GetMapping("/")
	public ModelAndView index() {
		return redirectHome();
	}
}
