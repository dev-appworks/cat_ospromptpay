package com.cat.os.controller.ussd;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.MsgConstant;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.UssdConstant;
import com.cat.os.common.utils.DataTableUtils;
import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.common.utils.OsMessageUtils;
import com.cat.os.common.utils.OsUtils;
import com.cat.os.config.security.CustomUserPrincipal;
import com.cat.os.dao.entity.AuditLog;
import com.cat.os.dao.entity.ServiceStatus;
import com.cat.os.dao.vo.USSDHistoryLogVo;
import com.cat.os.dao.vo.USSDServicesTimerVo;
import com.cat.os.dao.vo.USSDVo;
import com.cat.os.service.AuditLogService;
import com.cat.os.service.ussd.UssdService;

@Controller
@RequestMapping("/ussd")
@Profile(Profiles.USSD)
public class USSDController {

	private static final Logger log = LoggerFactory.getLogger(USSDController.class);

	@Autowired
	private UssdService ussdService;

	@Autowired
	private AuditLogService auditLogService;

	@GetMapping("os1001/init")
	public ModelAndView init(String okMsg) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1001.html");
		mav.addObject("ussd", new USSDVo());
		return mav;
	}

	@PostMapping("os1001/ussdList")
	@ResponseBody
	public DataTableAjax<ServiceStatus> query(@RequestBody USSDVo ussd) {
		DataTableAjax<ServiceStatus> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(ussd.getDraw());
		
		Pageable pageable = DataTableUtils.toPages(ussd.getStart(), ussd.getLength());
		Page<ServiceStatus> pages = ussdService.findByCriteria(ussd, pageable);
		
		DataTableUtils.toList(pages, dataTableAjax);

		return dataTableAjax;
	}

	@GetMapping("os1002/init")
	public ModelAndView os1002(@RequestParam Integer id) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1002.html");
		USSDVo ussd = ussdService.findOneById(id);
		if (ussd == null) {
			mav.addObject(MsgConstant.ERROR_STATUS, MsgConstant.ERROR_DESC);
			log.info("Ussd not found Id : " + id);
		}
		mav.addObject("ussd", ussd);
		mav.addObject("hhdropdown", ussdService.getHH());
		mav.addObject("mmdropdown", ussdService.getMM());
		return mav;
	}

	@PostMapping("os1002/edit")
	public ModelAndView os1002Edit(@ModelAttribute USSDVo ussd ,Principal principal, HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1002.html");
		try {
			UsernamePasswordAuthenticationToken u = (UsernamePasswordAuthenticationToken) principal;
			CustomUserPrincipal user = (CustomUserPrincipal) u.getPrincipal();
			String userName = principal.getName() + " - " + user.getFullName();
			String responeCode = ussdService.updateUssdStatus(ussd, userName);
			mav.addObject("ussd", ussd);
			mav.addObject("hhdropdown", ussdService.getHH());
			mav.addObject("mmdropdown", ussdService.getMM());
			
			if(UssdConstant.STATUS_CODE_00.equalsIgnoreCase(responeCode)){
				OsMessageUtils.setSuccess(httpServletRequest);
				mav.addObject("ussd", null);
				mav.addObject("hhdropdown", null);
				mav.addObject("mmdropdown", null);
				mav.setViewName("redirect:/ussd/os1001/init");
			}else {
				OsMessageUtils.setFail(httpServletRequest);
			}
		} catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("os1002Edit", e);
		}

		return mav;
	}
	
	@GetMapping("os1003/init")
	public ModelAndView os1003() {
		ModelAndView mav = new ModelAndView("views/ussd/OS1003.html");
		List<ServiceStatus> serviceStatus = ussdService.findAll();
		USSDHistoryLogVo ussd = new USSDHistoryLogVo();
		ussd.setDateFrom(OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
		ussd.setDateTo(OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
		mav.addObject("ussd", ussd);
		mav.addObject("serviceStatus", serviceStatus);
		mav.addObject("logStatus", auditLogService.getStatusLog());
		return mav;
	}
	
	@PostMapping("os1003/ussdAuditList")
	@ResponseBody
	public DataTableAjax<AuditLog> os1003AuditList(@ModelAttribute USSDHistoryLogVo ussdHistoryLogVo) {
		DataTableAjax<AuditLog> dataTableAjax = new DataTableAjax<>();
		dataTableAjax.setDraw(ussdHistoryLogVo.getDraw());
		Pageable pageable = DataTableUtils.toPages(ussdHistoryLogVo.getStart(), ussdHistoryLogVo.getLength());
		Page<AuditLog> pages = auditLogService.findByCriteria(UssdConstant.USSD_TYPE, 
				ussdHistoryLogVo.getDateFrom(), ussdHistoryLogVo.getDateTo(), ussdHistoryLogVo.getLogBy(), 
				ussdHistoryLogVo.getServiceId(), ussdHistoryLogVo.getStatus(), pageable);
		DataTableUtils.toList(pages, dataTableAjax);

		return dataTableAjax;
	}
	
	@PostMapping("os1001/cancletimer")
	public ModelAndView cancletimer(@ModelAttribute USSDVo ussdVo,Principal principal, HttpServletRequest httpServletRequest) throws Exception {
		
		ModelAndView mav = new ModelAndView("views/ussd/OS1001.html");

		try {
			ussdService.cancelTime(ussdVo,principal.getName());
			mav.setViewName("redirect:/ussd/os1001/init");
			OsMessageUtils.setSuccess(httpServletRequest);
		}catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			mav.addObject("ussd",ussdVo);
			log.error("cancletimer",e);
		}
		
		return mav;
		
	}
	
	
	@GetMapping("os1004/new")
	public ModelAndView os1004New() {
		ModelAndView mav = new ModelAndView("views/ussd/OS1004.html");
		mav.addObject("ussd", new USSDVo());
		return mav;
	}
	
	@PostMapping("os1004/edit")
	public ModelAndView os1004newSave(@ModelAttribute USSDVo ussdVo,Principal principal, HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1004.html");
		mav.addObject("ussd",ussdVo);
		
		try {
			
			CustomUserPrincipal user = OsUtils.getCurrentUser();
			String userName = user.getUsername() + " - " + user.getFullName();
			
			//new 
			if(ussdVo.getId() == null) {
				log.info("create new Service..");
				//check dup
				ServiceStatus res = ussdService.checkExistServiceStatus(ussdVo);
				if(res != null) {
					OsMessageUtils.setFail(httpServletRequest, "มี รหัสบริการ : " + ussdVo.getServiceId()  +" อยู่ในระบบแล้ว");
					return mav;
				}
				
				ussdService.createNewService(ussdVo,userName);
			}else {
				//edit
				log.info("edit service");
				ussdService.editService(ussdVo,userName);
			}
			
			OsMessageUtils.setSuccess(httpServletRequest);
			mav.setViewName("redirect:/ussd/os1001/init");

		} catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("os1004newSave", e);
		}
		return mav;
	}
	
	@GetMapping("os1004/edit")
	public ModelAndView os1004Edit(@RequestParam Integer id) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1004.html");
		
		USSDVo ussdVo = ussdService.findOneById(id);
		mav.addObject("ussd",ussdVo);
		
		return mav;
	}
	
	@PostMapping("os1001/deleteService")
	public ModelAndView deleteService(@ModelAttribute USSDVo ussdVo,Principal principal, HttpServletRequest httpServletRequest) throws Exception {
		ModelAndView mav = new ModelAndView("views/ussd/OS1001.html");
		try {
			
			CustomUserPrincipal user = OsUtils.getCurrentUser();
			String userLogin = user.getUserName() + " - " + user.getFullName();
			ussdService.deleteService(ussdVo, userLogin);
			OsMessageUtils.setSuccess(httpServletRequest);
		}catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			mav.addObject("ussd",ussdVo);
			log.error("deleteService",e);
		}
		
		mav.setViewName("redirect:/ussd/os1001/init");
		return mav;
	}
	
	@GetMapping("os1005/init")
	public ModelAndView os1005(@RequestParam Integer id) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1005.html");
		USSDVo ussd = ussdService.findOneById(id);
		mav.addObject("ussd", ussd);
		return mav;
	}
	
	@PostMapping("os1005/list")
	@ResponseBody
	public DataTableAjax<USSDServicesTimerVo> queryServicesTimerList(@RequestBody USSDVo ussdVo) {
		return ussdService.findServicesTimeByServiceStatusId(ussdVo);
	}
	
	@PostMapping("os1005/deleteServiceTimer")
	public ModelAndView deleteServiceTimer(@RequestParam Integer servicesTimerId, @RequestParam Integer serviceStatusId, 
			HttpServletRequest httpServletRequest, Principal principal) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1005.html");
		try {
			CustomUserPrincipal user = OsUtils.getCurrentUser();
			String userLogin = user.getUserName() + " - " + user.getFullName();
			ussdService.deleteServiceTimerById(servicesTimerId, serviceStatusId, userLogin);
			mav.setViewName("redirect:/ussd/os1005/init");
			mav.addObject("id", serviceStatusId);
			OsMessageUtils.setSuccess(httpServletRequest);
		}catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("deleteServiceTimer",e);
		}
		
		return mav;
	}
	
	@GetMapping("os1006/init")
	public ModelAndView os1006(@RequestParam Integer id) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1006.html");
		USSDServicesTimerVo ussdTimer = ussdService.findServicesTimerById(id);
		if (ussdTimer == null) {
			mav.addObject(MsgConstant.ERROR_STATUS, MsgConstant.ERROR_DESC);
			log.info("Ussd not found Id : " + id);
		}
		mav.addObject("ussd", ussdTimer);
		mav.addObject("hhdropdown", ussdService.getHH());
		mav.addObject("mmdropdown", ussdService.getMM());
		return mav;
	}
	
	@PostMapping("os1006/edit")
	public ModelAndView os1006Edit(@ModelAttribute USSDServicesTimerVo ussdTimer, Principal principal, HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/ussd/OS1006.html");
		try {
			UsernamePasswordAuthenticationToken u = (UsernamePasswordAuthenticationToken) principal;
			CustomUserPrincipal user = (CustomUserPrincipal) u.getPrincipal();
			String userName = principal.getName() + " - " + user.getFullName();
			ussdService.updateUssdTimer(ussdTimer, userName);
			mav.addObject("ussd", ussdTimer);
			mav.addObject("hhdropdown", ussdService.getHH());
			mav.addObject("mmdropdown", ussdService.getMM());
			
			OsMessageUtils.setSuccess(httpServletRequest);
			mav.addObject("ussd", null);
			mav.addObject("hhdropdown", null);
			mav.addObject("mmdropdown", null);
			mav.setViewName("redirect:/ussd/os1005/init");
			mav.addObject("id", ussdTimer.getServiceStatusId());
		} catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("os1006Edit", e);
		}

		return mav;
	}
	
}