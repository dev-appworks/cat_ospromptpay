package com.cat.os.controller.ussd;

import java.security.Principal;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.PromptPayConstant;
import com.cat.os.common.utils.OsUtils;
import com.cat.os.config.security.CustomUserPrincipal;
import com.cat.os.dao.vo.PromptPayReferenceCodeVo;
import com.cat.os.service.ussd.PromptPayService;

@RestController
@RequestMapping("/promptpay/rest")
@Profile(Profiles.PROMPTPAY)
public class PromptPayRestController {
	private static final Logger log = LoggerFactory.getLogger(PromptPayRestController.class);
	
	@Autowired
	private PromptPayService promptPayService;
	
	@ResponseBody
	@GetMapping("refcode/onoff/{id}/{status}")
	public MessageDto refCodeUpdateStatus(@PathVariable("id") Integer id, @PathVariable("status") String status, Principal principal) {
		MessageDto messageDto = new MessageDto();
		PromptPayReferenceCodeVo refCodeVo = null;
		
		messageDto.setMsgCode(MessageDto.STATUS.SUCCESS);
		
		if(id == null) {
			messageDto.setMsgCode(MessageDto.STATUS.ERROR);
			messageDto.setMsgDesc("ID is null!!!");
		}
		
		try {
			
			refCodeVo = promptPayService.findOneById(id);
			
			CustomUserPrincipal user = OsUtils.getCurrentUser();
			
			if(messageDto.getMsgCode().equals(MessageDto.STATUS.ERROR))
				return messageDto;
			
			refCodeVo.setStatus((status.equals(PromptPayConstant.STATUS.ACTIVE) ? PromptPayConstant.STATUS.ACTIVE : status.equals(PromptPayConstant.STATUS.PENDING) ? PromptPayConstant.STATUS.PENDING : PromptPayConstant.STATUS.INACTIVE));
			
			if(promptPayService.checkRefCodeActive() > 0 && refCodeVo.getStatus().equals(PromptPayConstant.STATUS.ACTIVE)) {
				messageDto.setMsgCode(MessageDto.STATUS.ERROR);
				messageDto.setMsgDesc("มีรายการที่ เปิดใช้งานอยู่");
				return messageDto;
			}
			
//			if(refCodeVo.getEndDate().before(Calendar.getInstance().getTime())) {
//				messageDto.setMsgCode(MessageDto.STATUS.ERROR);
//				messageDto.setMsgDesc("รายการดังกล่าวไม่อยู่ในช่วงเวลาที่เปิดใช้งานได้");
//				return messageDto;
//			}
			
			if(!(refCodeVo.getStartDate().before(Calendar.getInstance().getTime()) && refCodeVo.getEndDate().after(Calendar.getInstance().getTime())) && !refCodeVo.getStatus().equals(PromptPayConstant.STATUS.INACTIVE))
			{
				messageDto.setMsgCode(MessageDto.STATUS.ERROR);
				messageDto.setMsgDesc("รายการดังกล่าวไม่อยู่ในช่วงเวลาที่เปิดใช้งานได้");
				return messageDto;
			}
			
			promptPayService.save(refCodeVo, user);
			
//			OsMessageUtils.setSuccess(httpServletRequest);
			
		} catch (Exception e) {
			messageDto.setMsgCode(MessageDto.STATUS.ERROR);
			messageDto.setMsgDesc(e.getMessage());
//			OsMessageUtils.setFail(httpServletRequest);
			log.error("PromptpayRefCode Rest", e);
		}
		
		log.info("============================== Call Rest Controller ==============================");
		
		return messageDto;
	}
}
