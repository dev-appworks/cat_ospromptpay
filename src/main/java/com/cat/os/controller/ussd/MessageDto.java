package com.cat.os.controller.ussd;

public class MessageDto {

	
	private String status;
	private String msgCode;
	private String msgDesc;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	
	public static class STATUS {
		public static String SUCCESS = "SUCCESS";
		public static String ERROR = "ERROR";
	}
	
}
