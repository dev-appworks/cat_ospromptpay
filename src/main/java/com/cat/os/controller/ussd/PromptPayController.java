package com.cat.os.controller.ussd;

import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.common.constant.PromptPayConstant;
import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.common.utils.OsMessageUtils;
import com.cat.os.common.utils.OsUtils;
import com.cat.os.config.security.CustomUserPrincipal;
import com.cat.os.dao.entity.PromptpayReferenceCode;
import com.cat.os.dao.vo.PromptPayConsentVo;
import com.cat.os.dao.vo.PromptPayReferenceCodeVo;
import com.cat.os.service.ussd.PromptPayService;

@Controller
@RequestMapping("/promptpay")
@Profile(Profiles.PROMPTPAY)
public class PromptPayController {

	private static final Logger log = LoggerFactory.getLogger(PromptPayController.class);

	@Autowired
	private PromptPayService promptPayService;
	
	@GetMapping("report/init")
	public ModelAndView pp1001Init() {
		ModelAndView mav = new ModelAndView("views/ussd/PP1001.html");
		PromptPayConsentVo consent = new PromptPayConsentVo();
		consent.setDateFrom(OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
		consent.setDateTo(OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
		mav.addObject("consent", consent);
		
		return mav;
	}
	
	@ResponseBody
	@PostMapping("report/list")
	public DataTableAjax<PromptPayConsentVo> pp1001List(@RequestBody PromptPayConsentVo consentVo) {
		DataTableAjax<PromptPayConsentVo> consent = promptPayService.findByCriteria(consentVo);
		return consent;	
	}
	
	@GetMapping("refcode/init")
	public ModelAndView pp1002Init() {
		ModelAndView mav = new ModelAndView("views/ussd/PP1002.html");
		PromptPayReferenceCodeVo refCodeVo = new PromptPayReferenceCodeVo();
		refCodeVo.setDateFrom(OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
		refCodeVo.setDateTo(OsDateUtils.getCalendarDateEnDDMMYYYYFormat());
		mav.addObject("refCode", refCodeVo);
		
		return mav;
	}
	
	@ResponseBody
	@PostMapping("refcode/list")
	public DataTableAjax<PromptPayReferenceCodeVo> pp1002List(@RequestBody PromptPayReferenceCodeVo consentVo) {
		DataTableAjax<PromptPayReferenceCodeVo> consent = promptPayService.findByCriteria(consentVo);
		return consent;	
	}
	
	@ResponseBody
	@GetMapping("refcode/new")
	public ModelAndView pp1003New(HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/ussd/PP1003.html");
		PromptPayReferenceCodeVo refCodeVo = new PromptPayReferenceCodeVo();
		Calendar calendar = Calendar.getInstance();
		
		try {
			PromptPayReferenceCodeVo lastDateVo = promptPayService.getLastEnd();
			
//			calendar.set(Calendar.SECOND, 00);
//			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
			if(lastDateVo == null)
				calendar.setTime(new Date());
			else
				calendar.setTime(lastDateVo.getEndDate());
			calendar.set(Calendar.SECOND, calendar.get(Calendar.SECOND) + 1); 
			refCodeVo.setDateFrom(DateFormatUtils.format(calendar.getTime(), "dd/MM/yyyy HH:mm:ss"));
			log.info("Date From >> " + refCodeVo.getDateFrom());
			
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			refCodeVo.setDateTo(DateFormatUtils.format(calendar.getTime(), "dd/MM/yyyy HH:mm:ss"));
			log.info("Date To >> " + refCodeVo.getDateTo());
			refCodeVo.setStatus(PromptPayConstant.STATUS.PENDING);
			
			mav.addObject("refCodeVo", refCodeVo);
		} catch(Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("PromptpayRefcode New", e);
		}
		
		return mav;
	}
	
	@GetMapping("refcode/edit")
	public ModelAndView pp1003Edit(@RequestParam Integer id) {
		ModelAndView mav = new ModelAndView("views/ussd/PP1003.html");
		
		PromptPayReferenceCodeVo refCodeVo = promptPayService.findOneById(id);
		
		refCodeVo.setDateFrom(DateFormatUtils.format(refCodeVo.getStartDate(), "dd/MM/yyyy HH:mm:ss"));
		refCodeVo.setDateTo(DateFormatUtils.format(refCodeVo.getEndDate(), "dd/MM/yyyy HH:mm:ss"));
		
		mav.addObject("refCodeVo", refCodeVo);
		
		return mav;
	}
	
	@PostMapping("refcode/edit")
	public ModelAndView refCodeSave(@ModelAttribute PromptPayReferenceCodeVo refCodeVo, Principal principal, HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/ussd/PP1003.html");
		mav.addObject("refCodeVo", refCodeVo);
		
		List<PromptpayReferenceCode> refCodeList = null;
		PromptPayReferenceCodeVo refCodeVoBefore = null;
		Date dateFrom = null;
		Date dateTo = null;
		
		try {
			if(refCodeVo.getId() != null) {
				refCodeVoBefore = promptPayService.findOneById(refCodeVo.getId());
				dateFrom = DateUtils.parseDate(refCodeVo.getDateFrom(), "dd/MM/yyyy HH:mm:ss");
				dateTo = DateUtils.parseDate(refCodeVo.getDateTo(), "dd/MM/yyyy HH:mm:ss");
			}
			
			refCodeList = promptPayService.findDateOverlap(refCodeVo.getDateFrom(), PromptPayConstant.IS_DELETED.ACTIVE);
			if(refCodeVoBefore != null) {
				if(!refCodeVoBefore.getStartDate().equals(dateFrom) || !refCodeVoBefore.getEndDate().equals(dateTo))
					if(refCodeList.size() > 0) {
						OsMessageUtils.setFail(httpServletRequest, "วันเวลา เริ่มต้น-สิ้นสุด คาบเกี่ยวกันข้อมูลในระบบ");
						return mav;
					}
			} else {
				if(refCodeList.size() > 0) {
					OsMessageUtils.setFail(httpServletRequest, "วันเวลา เริ่มต้น-สิ้นสุด คาบเกี่ยวกันข้อมูลในระบบ");
					return mav;
				}
			}
			
			CustomUserPrincipal user = OsUtils.getCurrentUser();
			
			if(promptPayService.checkRefCodeActive() > 0 && refCodeVo.getStatus().equals(PromptPayConstant.STATUS.ACTIVE)) {
				OsMessageUtils.setFail(httpServletRequest, "ไม่สามารถใช้สถานะ \"เปิด\" ได้  เนื่องจากมีรายการที่เปิดใช้งานอยู่");
				return mav;
			}
			
			promptPayService.save(refCodeVo, user);
			
			OsMessageUtils.setSuccess(httpServletRequest);
			mav.setViewName("redirect:/promptpay/refcode/init");

		} catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("promptpayRefCodeSave", e);
		}
		return mav;
	}
	
	@PostMapping("refcode/delete")
	public ModelAndView refCodeDelete(@ModelAttribute PromptPayReferenceCodeVo refCodeVo, Principal principal, HttpServletRequest httpServletRequest) {
		ModelAndView mav = new ModelAndView("views/ussd/PP1003.html");
		mav.addObject("refCodeVo", refCodeVo);
		
		try {
			
			CustomUserPrincipal user = OsUtils.getCurrentUser();
			promptPayService.delete(refCodeVo.getId(), user);
			
			OsMessageUtils.setSuccess(httpServletRequest);
			mav.setViewName("redirect:/promptpay/refcode/init");
		} catch (Exception e) {
			OsMessageUtils.setFail(httpServletRequest);
			log.error("PromptpayRefCode Delete", e);
		}
		return mav;
	}
}
