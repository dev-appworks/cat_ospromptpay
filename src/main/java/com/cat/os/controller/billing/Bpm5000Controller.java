package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5000Vo;
import com.cat.os.service.billing.Bpm5000Service;

@Controller
@RequestMapping("bpm/bpm5000")
@Profile(Profiles.BILLING)
public class Bpm5000Controller {
	
	@Autowired
	private Bpm5000Service bpm5000Service;

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm5000.html");
		return mav;
	}
	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm5000Vo> list(@RequestBody Bpm5000Vo bpm5000Vo){
		DataTableAjax<Bpm5000Vo> result = bpm5000Service.findAll(bpm5000Vo);
		return result;
	}
				
	
}









