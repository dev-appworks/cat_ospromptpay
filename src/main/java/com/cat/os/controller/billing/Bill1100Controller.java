package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill1100Vo;
import com.cat.os.service.billing.Bill1100Service;

@Controller
@RequestMapping("bill/bill1100")
@Profile(Profiles.BILLING)
public class Bill1100Controller {
	
	@Autowired
	private Bill1100Service bill1100Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/bill1100.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bill1100Vo> list(@RequestBody Bill1100Vo bill1100Vo){
		
		DataTableAjax<Bill1100Vo> result = bill1100Service.findALL(bill1100Vo);
		
		return result;
	}
}
