package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill2100Vo;
import com.cat.os.service.billing.Bill2100Service;

@Controller
@RequestMapping("bill/bill2100")
@Profile(Profiles.BILLING)
public class Bill2100Controller {
	
	@Autowired
	private Bill2100Service bill2100Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/bill2100.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bill2100Vo> list(@RequestBody Bill2100Vo bill2100Vo){
		DataTableAjax<Bill2100Vo> result = bill2100Service.findAll(bill2100Vo);
		 return result ;
		
	}
}
