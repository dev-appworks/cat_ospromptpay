package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm4200MarkVo;
import com.cat.os.dao.vo.Bpm4200Vo;
import com.cat.os.service.billing.Bpm4200Service;

@Controller
@RequestMapping("bpm/bpm4200")
@Profile(Profiles.BILLING)
public class Bpm4200Controller {

	@Autowired
	private Bpm4200Service bpm4200Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm4200.html");
		return mav;
	}	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm4200Vo> list(@RequestBody Bpm4200Vo bpm4200Vo){
		DataTableAjax<Bpm4200Vo> result = bpm4200Service.findAll(bpm4200Vo);
		return result;
	}
	@PostMapping("/listMark")
	@ResponseBody
	public DataTableAjax<Bpm4200MarkVo> list(@RequestBody Bpm4200MarkVo bpm4200MarkVo){
		DataTableAjax<Bpm4200MarkVo> result = bpm4200Service.findAll(bpm4200MarkVo);
		return result;
	}
}













