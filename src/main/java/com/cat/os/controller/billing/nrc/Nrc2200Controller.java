package com.cat.os.controller.billing.nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc2200Vo;
import com.cat.os.service.billing.nrc.Nrc2200Service;

@Controller
@RequestMapping("bill/nrc/nrc2200")
@Profile(Profiles.BILLING)
public class Nrc2200Controller {
	
	@Autowired
	private Nrc2200Service nrc2200Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/nrc/nrc2200.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Nrc2200Vo> list(@RequestBody Nrc2200Vo nrc2200Vo){
		DataTableAjax<Nrc2200Vo> result = nrc2200Service.findAll(nrc2200Vo);
		 return result;
		}
	
}
