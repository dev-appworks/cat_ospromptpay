package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3200Vo;
import com.cat.os.service.billing.Bpm3200Service;

@Controller
@RequestMapping("bpm/bpm3200")
@Profile(Profiles.BILLING)
public class Bpm3200Controller {

	@Autowired
	private Bpm3200Service bpm3200Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm3200.html");
		return mav;
	}
				
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm3200Vo> list(@RequestBody Bpm3200Vo bpm3200Vo){
		
		DataTableAjax<Bpm3200Vo> result = bpm3200Service.findAll(bpm3200Vo);
		
		return result;
	}
}













