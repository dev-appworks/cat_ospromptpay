package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;

import com.cat.os.dao.vo.Bpm4001Vo;
import com.cat.os.service.billing.Bpm4000Service;



@Controller
@RequestMapping("bpm/bpm4001")
@Profile(Profiles.BILLING)
public class Bpm4001Controller {
	
	@Autowired
	private Bpm4000Service bpm4001Service;
	
	
	

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm4001.html");
		return mav;
	}
	
	@PostMapping("/ServiceType")
	@ResponseBody
	public DataTableAjax<Bpm4001Vo> list(@RequestBody Bpm4001Vo bpm4001Vo){
		DataTableAjax<Bpm4001Vo> resule = bpm4001Service.findAll(bpm4001Vo);
		return resule;
	}
	
				
}
