package com.cat.os.controller.billing.nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc1200Vo;
import com.cat.os.service.billing.nrc.Nrc1200Service;
@Controller
@RequestMapping("bill/nrc/nrc1200")
@Profile(Profiles.BILLING)
public class Nrc1200Controller {
	
	@Autowired
	private Nrc1200Service nrc1200Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/nrc/nrc1200.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Nrc1200Vo> list(@RequestBody Nrc1200Vo nrc1200Vo){
		DataTableAjax<Nrc1200Vo> result = nrc1200Service.findAll(nrc1200Vo);
		 return result ;
		}
	
}
