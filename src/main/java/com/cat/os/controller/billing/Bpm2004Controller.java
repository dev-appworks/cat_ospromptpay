package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2004Vo;
import com.cat.os.service.billing.Bpm2004Service;
@Controller
@RequestMapping("bpm/bpm2004")
@Profile(Profiles.BILLING)
public class Bpm2004Controller {

	@Autowired
	private Bpm2004Service bpm2004Service; 
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2004.html");
		return mav;
	}	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm2004Vo> list(@RequestBody Bpm2004Vo bpm2004Vo){
		DataTableAjax<Bpm2004Vo> result = bpm2004Service.findAll(bpm2004Vo);
		 return result ;
	}
	
}















