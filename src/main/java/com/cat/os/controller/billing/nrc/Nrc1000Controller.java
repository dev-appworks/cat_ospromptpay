package com.cat.os.controller.billing.nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc1000Vo;
import com.cat.os.service.billing.nrc.Nrc1000Service;

@Controller
@RequestMapping("bill/nrc/nrc1000")
@Profile(Profiles.BILLING)
public class Nrc1000Controller {
	
	@Autowired
	
	private Nrc1000Service nrc1000Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/nrc/nrc1000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody	
	public DataTableAjax<Nrc1000Vo> list(@RequestBody Nrc1000Vo nrc1000Vo){
		DataTableAjax<Nrc1000Vo> resule = nrc1000Service.findAll(nrc1000Vo);
		return resule;
	}
}
