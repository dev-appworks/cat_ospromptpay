package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm4100Vo;
import com.cat.os.service.billing.Bpm4100Service;

@Controller
@RequestMapping("bpm/bpm4100")
@Profile(Profiles.BILLING)
public class Bpm4100Controller {

	@Autowired
	private Bpm4100Service bpm4100Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm4100.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm4100Vo> list(@RequestBody Bpm4100Vo bpm4100Vo){
		DataTableAjax<Bpm4100Vo> result = bpm4100Service.findAll(bpm4100Vo);
		return result;
	}
				
	
}









