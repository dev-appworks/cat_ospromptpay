package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5300MarkCDRsVo;
import com.cat.os.dao.vo.Bpm5300Vo;
import com.cat.os.service.billing.Bpm5300Service;

@Controller
@RequestMapping("bpm/bpm5300")
@Profile(Profiles.BILLING)
public class Bpm5300Controller {

	@Autowired
	private Bpm5300Service bpm5300Service;

	@Autowired
	private Bpm5300Service bpm5300MarkCDRsService;

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm5300.html");
		return mav;
	}

	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm5300Vo> list(@RequestBody Bpm5300Vo bpm5300Vo) {
		DataTableAjax<Bpm5300Vo> resule = bpm5300Service.findAll(bpm5300Vo);
		return resule;
	}

	@PostMapping("/MarkCDRs")
	@ResponseBody
	public DataTableAjax<Bpm5300MarkCDRsVo> list(@RequestBody Bpm5300MarkCDRsVo bpm5300MarkCDRsVo) {
		DataTableAjax<Bpm5300MarkCDRsVo> resule = bpm5300MarkCDRsService.findAll(bpm5300MarkCDRsVo);
		return resule;
	}

}
