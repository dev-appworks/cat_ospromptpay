package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3300RapVo;
import com.cat.os.dao.vo.Bpm3300Vo;
import com.cat.os.service.billing.Bpm3300Service;

@Controller
@RequestMapping("bpm/bpm3300")
@Profile(Profiles.BILLING)
public class Bpm3300Controller {
	
	@Autowired
	private Bpm3300Service bpm3300Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm3300.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm3300Vo> list(@RequestBody Bpm3300Vo bpm3300Vo){
		
		DataTableAjax<Bpm3300Vo> result = bpm3300Service.findALL(bpm3300Vo);
		
		return result;
	}
	
	
	@PostMapping("/listRap")
	@ResponseBody
	public DataTableAjax<Bpm3300RapVo> list(@RequestBody Bpm3300RapVo bpm3300RapVo){
		
		DataTableAjax<Bpm3300RapVo> result = bpm3300Service.findALL(bpm3300RapVo);
		
		return result;
	}
}