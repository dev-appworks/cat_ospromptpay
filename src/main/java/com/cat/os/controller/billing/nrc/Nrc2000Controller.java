package com.cat.os.controller.billing.nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc2000Vo;
import com.cat.os.service.billing.nrc.Nrc2000Service;
@Controller
@RequestMapping("bill/nrc/nrc2000")
@Profile(Profiles.BILLING)
public class Nrc2000Controller {
	
	@Autowired
	private Nrc2000Service nrc2000Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/nrc/nrc2000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Nrc2000Vo> list(@RequestBody Nrc2000Vo nrc2000Vo){
		DataTableAjax<Nrc2000Vo> result = nrc2000Service.findAll(nrc2000Vo);
		 return result ;
		}
	
}
