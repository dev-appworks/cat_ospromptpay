package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5002MarkVo;
import com.cat.os.dao.vo.Bpm5002Vo;
import com.cat.os.service.billing.Bpm5002Service;

@Controller
@RequestMapping("bpm/bpm5002")
@Profile(Profiles.BILLING)
public class Bpm5002Controller {

	@Autowired
	private Bpm5002Service bpm5002Service;
	
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm5002.html");
		return mav;
	}	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm5002Vo> list(@RequestBody Bpm5002Vo bpm5002Vo){
		DataTableAjax<Bpm5002Vo> result = bpm5002Service.findAll(bpm5002Vo);
		return result;
	}
	@PostMapping("/listMark")
	@ResponseBody
	public DataTableAjax<Bpm5002MarkVo> list(@RequestBody Bpm5002MarkVo bpm5002MarkVo){
		DataTableAjax<Bpm5002MarkVo> result = bpm5002Service.findAll(bpm5002MarkVo);
		return result;
	}
}




















