package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill2200Vo;
import com.cat.os.service.billing.Bill2200Service;

@Controller
@RequestMapping("bill/bill2200")
@Profile(Profiles.BILLING)
public class Bill2200Controller {
	
	@Autowired
	private Bill2200Service bill2200Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/bill2200.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bill2200Vo> list(@RequestBody Bill2200Vo bill2200Vo){
		
		DataTableAjax<Bill2200Vo> result = bill2200Service.findALL(bill2200Vo);
		
		return result;
	}
}
