package com.cat.os.controller.billing.nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc2100Vo;
import com.cat.os.service.billing.nrc.Nrc2100Service;
@Controller
@RequestMapping("bill/nrc/nrc2100")
@Profile(Profiles.BILLING)
public class Nrc2100Controller {
	
	@Autowired
	private Nrc2100Service nrc2100Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/nrc/nrc2100.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Nrc2100Vo> list(@RequestBody Nrc2100Vo nrc2100Vo){
		DataTableAjax<Nrc2100Vo> result = nrc2100Service.findAll(nrc2100Vo);
		 return result;
		}
	
}
