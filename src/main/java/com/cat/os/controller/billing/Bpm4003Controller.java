package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm4003FixCaseVo;
import com.cat.os.dao.vo.Bpm4003Vo;
import com.cat.os.service.billing.Bpm4000Service;




@Controller
@RequestMapping("bpm/bpm4003")
@Profile(Profiles.BILLING)
public class Bpm4003Controller {
	
	@Autowired
	private Bpm4000Service bpm4003Service;
	
	@Autowired
	private Bpm4000Service bpm4003FixCaseService;
	
	

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm4003.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm4003Vo> list(@RequestBody Bpm4003Vo bpm4003Vo){
		DataTableAjax<Bpm4003Vo> resule = bpm4003Service.findAll(bpm4003Vo);
		return resule;
	}
	@PostMapping("/FixCase")
	@ResponseBody
	public DataTableAjax<Bpm4003FixCaseVo> list(@RequestBody Bpm4003FixCaseVo bpm4003FixCaseVo){
		DataTableAjax<Bpm4003FixCaseVo> resule = bpm4003FixCaseService.findAll(bpm4003FixCaseVo);
		return resule;
	}
	
	
				
}
