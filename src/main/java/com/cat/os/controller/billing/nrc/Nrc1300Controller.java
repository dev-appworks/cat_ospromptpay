package com.cat.os.controller.billing.nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc1300Vo;
import com.cat.os.service.billing.nrc.Nrc1300Service;
@Controller
@RequestMapping("bill/nrc/nrc1300")
@Profile(Profiles.BILLING)
public class Nrc1300Controller {
	
	@Autowired
	private Nrc1300Service nrc1300Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/nrc/nrc1300.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Nrc1300Vo> list(@RequestBody Nrc1300Vo nrc1300Vo){
		DataTableAjax<Nrc1300Vo> result = nrc1300Service.findAll(nrc1300Vo);
		 return result ;
		}
	
}
