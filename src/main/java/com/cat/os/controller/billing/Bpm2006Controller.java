package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2006Vo;
import com.cat.os.service.billing.Bpm2006Service;

@Controller
@RequestMapping("bpm/bpm2006")
@Profile(Profiles.BILLING)
public class Bpm2006Controller {
	
	@Autowired
	private Bpm2006Service bpm2006Service; 
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2006.html");
		return mav;
	}	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm2006Vo> list(@RequestBody Bpm2006Vo bpm2006Vo){
		DataTableAjax<Bpm2006Vo> result = bpm2006Service.findALL(bpm2006Vo);
		 return result ;
	}
	
}
















