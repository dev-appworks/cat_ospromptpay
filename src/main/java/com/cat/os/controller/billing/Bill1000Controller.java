package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill1000Vo;
import com.cat.os.service.billing.Bill1000Service;

@Controller
@RequestMapping("bill/bill1000")
@Profile(Profiles.BILLING)
public class Bill1000Controller {
	
	@Autowired	
	private Bill1000Service bill1000Service;

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/bill1000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bill1000Vo> list(@RequestBody Bill1000Vo bill1000Vo){
		DataTableAjax<Bill1000Vo> result = bill1000Service.findAll(bill1000Vo);
		 return result ;
		}

}
