package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;

import com.cat.os.dao.vo.Bpm4000Vo;
import com.cat.os.service.billing.Bpm4000Service;


@Controller
@RequestMapping("bpm/bpm4000")
@Profile(Profiles.BILLING)
public class Bpm4000Controller {
	
	@Autowired
	private Bpm4000Service bpm4000Service;
	
	
	

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm4000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm4000Vo> list(@RequestBody Bpm4000Vo bpm4000Vo){
		DataTableAjax<Bpm4000Vo> resule = bpm4000Service.findAll(bpm4000Vo);
		return resule;
	}
	
				
}
