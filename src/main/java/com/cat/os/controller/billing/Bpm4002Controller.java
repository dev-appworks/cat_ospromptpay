package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;

import com.cat.os.dao.vo.Bpm4002Vo;
import com.cat.os.service.billing.Bpm4000Service;



@Controller
@RequestMapping("bpm/bpm4002")
@Profile(Profiles.BILLING)
public class Bpm4002Controller {
	
	@Autowired
	private Bpm4000Service bpm4002Service;
	
	
	

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm4002.html");
		return mav;
	}
	
	@PostMapping("/ServiceSession")
	@ResponseBody
	public DataTableAjax<Bpm4002Vo> list(@RequestBody Bpm4002Vo bpm4002Vo){
		DataTableAjax<Bpm4002Vo> resule = bpm4002Service.findAll(bpm4002Vo);
		return resule;
	}
	
				
}
