package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2001Vo;
import com.cat.os.service.billing.Bpm2001Service;
@Controller
@RequestMapping("bpm/bpm2001")
@Profile(Profiles.BILLING)
public class Bpm2001Controller {

	@Autowired
	private Bpm2001Service bpm2001Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2001.html");
		return mav;
	}	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm2001Vo> list(@RequestBody Bpm2001Vo bpm2001Vo){
		DataTableAjax<Bpm2001Vo> result = bpm2001Service.findAll(bpm2001Vo);
		 return result ;
	}

}















