package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2100T123Vo;
import com.cat.os.dao.vo.Bpm2100Vo;
import com.cat.os.service.billing.Bpm2100Service;




@Controller
@RequestMapping("bpm/bpm2100")
@Profile(Profiles.BILLING)
public class Bpm2100Controller {
	
	@Autowired
	private Bpm2100Service bpm2100Service;
	
	@Autowired
	private Bpm2100Service bpm2100T123Service;
	
	

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2100.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm2100Vo> list(@RequestBody Bpm2100Vo bpm2100Vo){
		DataTableAjax<Bpm2100Vo> resule = bpm2100Service.findAll(bpm2100Vo);
		return resule;
	}
	@PostMapping("/T123")
	@ResponseBody
	public DataTableAjax<Bpm2100T123Vo> list(@RequestBody Bpm2100T123Vo bpm2100T123Vo){
		DataTableAjax<Bpm2100T123Vo> resule = bpm2100T123Service.findAll(bpm2100T123Vo);
		return resule;
	}
	
	
				
}

