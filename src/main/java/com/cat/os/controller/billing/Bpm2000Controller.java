package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2000Vo;
import com.cat.os.service.billing.Bpm2000Service;

@Controller
@RequestMapping("bpm/bpm2000")
@Profile(Profiles.BILLING)
public class Bpm2000Controller {
	
	@Autowired
	private Bpm2000Service bpm2000Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm2000Vo> list(@RequestBody Bpm2000Vo bpm2000Vo){
		
		DataTableAjax<Bpm2000Vo> result = bpm2000Service.findALL(bpm2000Vo);
		
		return result;
	}
}
