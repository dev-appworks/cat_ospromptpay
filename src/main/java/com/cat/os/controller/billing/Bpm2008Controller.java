package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2008T2Vo;
import com.cat.os.dao.vo.Bpm2008T3Vo;
import com.cat.os.dao.vo.Bpm2008Vo;
import com.cat.os.service.billing.Bpm2008Service;

@Controller
@RequestMapping("bpm/bpm2008")
@Profile(Profiles.BILLING)
public class Bpm2008Controller {

	@Autowired
	private Bpm2008Service bpm2008Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2008.html");
		return mav;
	}	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm2008Vo> list(@RequestBody Bpm2008Vo bpm2008Vo){
		DataTableAjax<Bpm2008Vo> result = bpm2008Service.findAll(bpm2008Vo);
		 return result ;
	}
	
	@PostMapping("/listT2")
	@ResponseBody
	public DataTableAjax<Bpm2008T2Vo> list(@RequestBody Bpm2008T2Vo bpm2008T2Vo){
		DataTableAjax<Bpm2008T2Vo> result = bpm2008Service.findAll(bpm2008T2Vo);
		 return result ;
	}
	
	@PostMapping("/listT3")
	@ResponseBody
	public DataTableAjax<Bpm2008T3Vo> list(@RequestBody Bpm2008T3Vo bpm2008T3Vo){
		DataTableAjax<Bpm2008T3Vo> result = bpm2008Service.findAll(bpm2008T3Vo);
		 return result ;
	}
}
















