package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2007Vo;
import com.cat.os.service.billing.Bpm2007Service;

@Controller
@RequestMapping("bpm/bpm2007")
@Profile(Profiles.BILLING)
public class Bpm2007Controller {

	@Autowired
	private Bpm2007Service bpm2007Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2007.html");
		return mav;
	}	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm2007Vo> list(@RequestBody Bpm2007Vo bpm2007Vo){
		DataTableAjax<Bpm2007Vo> result = bpm2007Service.findALL(bpm2007Vo);
		 return result ;
	}
}
















