package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill3000Vo;
import com.cat.os.service.billing.Bill3000Service;

@Controller
@RequestMapping("bill/bill3000")
@Profile(Profiles.BILLING)
public class Bill3000Controller {

		@Autowired
		private Bill3000Service bill3000Service;
		
		@GetMapping("/")
		public ModelAndView home() {
			ModelAndView mav = new ModelAndView("views/billing/bill3000.html");
			return mav;
		}
		@GetMapping("/fixed")
		public ModelAndView fixed() {
			ModelAndView mav = new ModelAndView("views/billing/bill3000.html");
			return mav;
		}
		
		@PostMapping("/list")
		@ResponseBody
		public DataTableAjax<Bill3000Vo> list(@RequestBody Bill3000Vo bill3000Vo){
			
			DataTableAjax<Bill3000Vo> result = bill3000Service.findALL(bill3000Vo);
			
			return result;
		}
		
		
}
