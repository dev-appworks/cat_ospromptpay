package com.cat.os.controller.billing;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.constant.ProjectConstant.Profiles;
@Controller
@RequestMapping("bpm/bpm2002")
@Profile(Profiles.BILLING)
public class Bpm2002Controller {

	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2002.html");
		return mav;
	}	
	
}















