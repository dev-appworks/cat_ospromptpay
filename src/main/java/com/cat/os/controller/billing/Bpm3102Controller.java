package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3102Vo;
import com.cat.os.service.billing.Bpm3102Service;

@Controller
@RequestMapping("bpm/bpm3102")
@Profile(Profiles.BILLING)
public class Bpm3102Controller {

	@Autowired
	private Bpm3102Service bpm3102Service;
	
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm3102.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm3102Vo> list(@RequestBody Bpm3102Vo bpm3102Vo){
		
		DataTableAjax<Bpm3102Vo> result = bpm3102Service.findALL(bpm3102Vo);
		
		return result;
	}
	

}
