package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3101RapVo;
import com.cat.os.dao.vo.Bpm3101Vo;
import com.cat.os.service.billing.Bpm3101Service;

@Controller
@RequestMapping("bpm/bpm3101")
@Profile(Profiles.BILLING)
public class Bpm3101Controller {
	
	@Autowired
	private Bpm3101Service bpm3101Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm3101.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm3101Vo> list(@RequestBody Bpm3101Vo bpm3101Vo){
		
		DataTableAjax<Bpm3101Vo> result = bpm3101Service.findALL(bpm3101Vo);
		
		return result;
	}
	
	@PostMapping("/listRap")
	@ResponseBody
	public DataTableAjax<Bpm3101RapVo> list(@RequestBody Bpm3101RapVo bpm3101RapVo){
		
		DataTableAjax<Bpm3101RapVo> result = bpm3101Service.findALL(bpm3101RapVo);
		return result;
	}

}
