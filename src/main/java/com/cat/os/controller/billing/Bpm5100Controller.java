package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;

import com.cat.os.dao.vo.Bpm5100Vo;
import com.cat.os.service.billing.Bpm5100Service;

@Controller
@RequestMapping("bpm/bpm5100")
@Profile(Profiles.BILLING)
public class Bpm5100Controller {
	
	@Autowired
	private Bpm5100Service bpm5100Service;

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm5100.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm5100Vo> list(@RequestBody Bpm5100Vo bpm5100Vo){
		DataTableAjax<Bpm5100Vo> result = bpm5100Service.findAll(bpm5100Vo);
		 return result ;
		
	}
	
}
