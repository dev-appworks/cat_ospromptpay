package com.cat.os.controller.billing.nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc2300Vo;
import com.cat.os.service.billing.nrc.Nrc2300Service;
@Controller
@RequestMapping("bill/nrc/nrc2300")
@Profile(Profiles.BILLING)
public class Nrc2300Controller {
	
	@Autowired
	private  Nrc2300Service nrc2300Service;
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/nrc/nrc2300.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Nrc2300Vo> list(@RequestBody Nrc2300Vo nrc2300Vo){
		DataTableAjax<Nrc2300Vo> result = nrc2300Service.findAll(nrc2300Vo);
		 return result;
		}
	
}
