package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm3100Vo;
import com.cat.os.service.billing.Bpm3100Service;

@Controller
@RequestMapping("bpm/bpm3100")
@Profile(Profiles.BILLING)
public class Bpm3100Controller {

	@Autowired
	private Bpm3100Service bpm3100Service;
	
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm3100.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm3100Vo> list(@RequestBody Bpm3100Vo bpm3100Vo){
		
		DataTableAjax<Bpm3100Vo> result = bpm3100Service.findALL(bpm3100Vo);
		
		return result;
	}
	

}
