package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm2005TVo;
import com.cat.os.dao.vo.Bpm2005Vo;
import com.cat.os.service.billing.Bpm2005Service;

@Controller
@RequestMapping("bpm/bpm2005")
@Profile(Profiles.BILLING)
public class Bpm2005Controller {

	@Autowired
	private Bpm2005Service bpm2005Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm2005.html");
		return mav;
	}	
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm2005Vo> list(@RequestBody Bpm2005Vo bpm2005Vo){
		DataTableAjax<Bpm2005Vo> result = bpm2005Service.findALL(bpm2005Vo);
		 return result ;
	}
	
	@PostMapping("/listT")
	@ResponseBody
	public DataTableAjax<Bpm2005TVo> list(@RequestBody Bpm2005TVo bpm2005tVo){
		DataTableAjax<Bpm2005TVo> result = bpm2005Service.findALL(bpm2005tVo);
		 return result ;
	}

}
















