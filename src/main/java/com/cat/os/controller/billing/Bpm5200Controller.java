package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm5200Vo;
import com.cat.os.service.billing.Bpm5200Service;

@Controller
@RequestMapping("bpm/bpm5200")
@Profile(Profiles.BILLING)
public class Bpm5200Controller {
	
	@Autowired
	private Bpm5200Service bpm5200Service;

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm5200.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm5200Vo> list(@RequestBody Bpm5200Vo bpm5200Vo){
		DataTableAjax<Bpm5200Vo> result = bpm5200Service.findAll(bpm5200Vo);
		return result ; 
	}
	
}









