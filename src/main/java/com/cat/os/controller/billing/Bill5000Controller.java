package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill5000Vo;
import com.cat.os.service.billing.Bill5000Service;

@Controller
@RequestMapping("bill/bill5000")
@Profile(Profiles.BILLING)
public class Bill5000Controller {

	@Autowired
	private Bill5000Service bill5000Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/bill5000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bill5000Vo> list(@RequestBody Bill5000Vo bill5000Vo){
		
		DataTableAjax<Bill5000Vo> result = bill5000Service.findALL(bill5000Vo);
		
		return result;
	}
}
