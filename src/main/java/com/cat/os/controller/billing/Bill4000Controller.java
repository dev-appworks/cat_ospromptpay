package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill4000Vo;
import com.cat.os.service.billing.Bill4000Service;

@Controller
@RequestMapping("bill/bill4000")
@Profile(Profiles.BILLING)
public class Bill4000Controller {
	
	@Autowired
	private Bill4000Service bill4000Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/bill4000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bill4000Vo> list(@RequestBody Bill4000Vo bill4000Vo){
		DataTableAjax<Bill4000Vo> result = bill4000Service.findAll(bill4000Vo);
		 return result ;
		
	}
}
