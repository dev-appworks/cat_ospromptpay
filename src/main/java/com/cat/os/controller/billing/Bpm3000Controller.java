package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;

import com.cat.os.dao.vo.Bpm3000Vo;
import com.cat.os.service.billing.Bpm3000Service;

@Controller
@RequestMapping("bpm/bpm3000")
@Profile(Profiles.BILLING)
public class Bpm3000Controller {
	
	@Autowired
	private Bpm3000Service bpm3000Service;

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm3000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm3000Vo> list(@RequestBody Bpm3000Vo bpm3000Vo){
		DataTableAjax<Bpm3000Vo> result = bpm3000Service.findAll(bpm3000Vo);
		 return result ;
		
	}
	
}
