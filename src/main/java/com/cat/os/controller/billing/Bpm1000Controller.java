package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bpm1000Vo;
import com.cat.os.service.billing.Bpm1000Service;

@Controller
@RequestMapping("bpm/bpm1000")
@Profile(Profiles.BILLING)
public class Bpm1000Controller {

	@Autowired
	private Bpm1000Service bpm1000Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/bpm/bpm1000.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bpm1000Vo> list(@RequestBody Bpm1000Vo bpm1000Vo){
		
		DataTableAjax<Bpm1000Vo> result = bpm1000Service.findALL(bpm1000Vo);
		
		return result;
	}
}
