package com.cat.os.controller.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Bill3100Vo;
import com.cat.os.service.billing.Bill3100Service;

@Controller
@RequestMapping("bill/bill3100")
@Profile(Profiles.BILLING)
public class Bill3100Controller {

	@Autowired
	private Bill3100Service bill3100Service;

	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/bill3100.html");
		return mav;
	}

	@PostMapping("/list")
	@ResponseBody
	public DataTableAjax<Bill3100Vo> list(@RequestBody Bill3100Vo bill3100Vo) {

		DataTableAjax<Bill3100Vo> result = bill3100Service.findALL(bill3100Vo);

		return result;
	}
}
