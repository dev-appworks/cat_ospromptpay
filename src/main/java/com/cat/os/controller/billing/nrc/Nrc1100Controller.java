package com.cat.os.controller.billing.nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cat.os.common.bean.DataTableAjax;
import com.cat.os.common.constant.ProjectConstant.Profiles;
import com.cat.os.dao.vo.Nrc1100Vo;
import com.cat.os.service.billing.nrc.Nrc1100Service;

@Controller
@RequestMapping("bill/nrc/nrc1100")
@Profile(Profiles.BILLING)
public class Nrc1100Controller {
	
	@Autowired
	
	private Nrc1100Service nrc1100Service;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView("views/billing/nrc/nrc1100.html");
		return mav;
	}
	
	@PostMapping("/list")
	@ResponseBody	
	public DataTableAjax<Nrc1100Vo> list(@RequestBody Nrc1100Vo nrc1100Vo){
		DataTableAjax<Nrc1100Vo> resule = nrc1100Service.findAll(nrc1100Vo);
		return resule;
	}
}
