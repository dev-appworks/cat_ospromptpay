package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ibacss_payment_data_tax_inv_summary_wt")
public class IbacssPaymentDataTaxInvSummaryWt {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tax_inv_summary_wt_id")
	private Long taxInvSummaryWtId;

	@Column(name = "tx_date")
	private Date txDate;

	@Column(name = "bill_order_number")
	private String billOrderNumber;

	@Column(name = "rental")
	private String rental;

	@Column(name = "usage")
	private String usage;

	@Column(name = "total_amount_ex_vat")
	private String totalAmountExVat;

	@Column(name = "vat_amount")
	private String vatAmount;

	@Column(name = "rental_wt")
	private String rentalWt;

	@Column(name = "usage_wt")
	private String usageWt;

	@Column(name = "num_of_records")
	private String numOfRecords;

	@Column(name = "is_deleted")
	private String isDeleted;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	public Long getTaxInvSummaryWtId() {
		return taxInvSummaryWtId;
	}

	public void setTaxInvSummaryWtId(Long taxInvSummaryWtId) {
		this.taxInvSummaryWtId = taxInvSummaryWtId;
	}

	public Date getTxDate() {
		return txDate;
	}

	public void setTxDate(Date txDate) {
		this.txDate = txDate;
	}

	public String getBillOrderNumber() {
		return billOrderNumber;
	}

	public void setBillOrderNumber(String billOrderNumber) {
		this.billOrderNumber = billOrderNumber;
	}

	public String getRental() {
		return rental;
	}

	public void setRental(String rental) {
		this.rental = rental;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getTotalAmountExVat() {
		return totalAmountExVat;
	}

	public void setTotalAmountExVat(String totalAmountExVat) {
		this.totalAmountExVat = totalAmountExVat;
	}

	public String getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(String vatAmount) {
		this.vatAmount = vatAmount;
	}

	public String getRentalWt() {
		return rentalWt;
	}

	public void setRentalWt(String rentalWt) {
		this.rentalWt = rentalWt;
	}

	public String getUsageWt() {
		return usageWt;
	}

	public void setUsageWt(String usageWt) {
		this.usageWt = usageWt;
	}

	public String getNumOfRecords() {
		return numOfRecords;
	}

	public void setNumOfRecords(String numOfRecords) {
		this.numOfRecords = numOfRecords;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
