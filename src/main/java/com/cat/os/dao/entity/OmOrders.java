package com.cat.os.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ORDERS", schema = "OM")
public class OmOrders {

	@Id
	@Column(name = "ID")
	private String orderId;

	@Column(name = "ORDER_TYPE_LKP")
	private Integer orderTypeLkp;
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getOrderTypeLkp() {
		return orderTypeLkp;
	}

	public void setOrderTypeLkp(Integer orderTypeLkp) {
		this.orderTypeLkp = orderTypeLkp;
	}

}
