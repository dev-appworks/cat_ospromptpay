package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PROMPTPAY_REFERENCE_CODE")
@NamedQuery(name="PromptpayReferenceCode.findAll", query="SELECT p FROM PromptpayReferenceCode p")
public class PromptpayReferenceCode{

	@Id
	@Column(name="PROMPTPAY_REFERENCE_CODE_ID")
	private Integer id;
	
	@Column(name="SHARED_KEY")
	private String sharedKey;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATE")
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATE")
	private Date endDate;

	@Column(name="STATUS")
	private String status;

	@Column(name="IS_DELETED")
	private Integer isDeleted;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Column(name="CREATED_BYNAME")
	private String createdByName;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	@Column(name="UPDATED_BYNAME")
	private String updatedByName;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSharedKey() {
		return sharedKey;
	}

	public void setSharedKey(String sharedKey) {
		this.sharedKey = sharedKey;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedByName() {
		return updatedByName;
	}

	public void setUpdatedByName(String updatedByName) {
		this.updatedByName = updatedByName;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "PromptpayReferenceCode [id=" + id + ", sharedKey=" + sharedKey + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", status=" + status + ", isDeleted=" + isDeleted + ", createdBy="
				+ createdBy + ", createdByName=" + createdByName + ", createdDate=" + createdDate + ", updatedBy="
				+ updatedBy + ", updatedByName=" + updatedByName + ", updatedDate=" + updatedDate + "]";
	}
}