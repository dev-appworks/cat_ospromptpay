package com.cat.os.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "U_E_TERRITORY_AGENT", schema = "OM")
public class UETerritoryAgent {

	@Id
	@Column(name="E_TERRITORY_AGENT_ID")
	private String eTerritoryAgentId;
	
	@Column(name="E_TERRITORY_ID")
	private String eTerritoryId;
	
	@Column(name="AGENT_ID")
	private String agentId;
	
	@Column(name="AGENT_ROLE_LKP")
	private String agentRoleLkp;
	
	@Column(name="REVISION_NUMBER")
	private Integer revisionNumber;
	
	@Column(name="OBSOLETE_FLAG")
	private Integer obsoleteFlag;

	public String geteTerritoryAgentId() {
		return eTerritoryAgentId;
	}

	public void seteTerritoryAgentId(String eTerritoryAgentId) {
		this.eTerritoryAgentId = eTerritoryAgentId;
	}

	public String geteTerritoryId() {
		return eTerritoryId;
	}

	public void seteTerritoryId(String eTerritoryId) {
		this.eTerritoryId = eTerritoryId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentRoleLkp() {
		return agentRoleLkp;
	}

	public void setAgentRoleLkp(String agentRoleLkp) {
		this.agentRoleLkp = agentRoleLkp;
	}

	public Integer getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public Integer getObsoleteFlag() {
		return obsoleteFlag;
	}

	public void setObsoleteFlag(Integer obsoleteFlag) {
		this.obsoleteFlag = obsoleteFlag;
	}
	
}
