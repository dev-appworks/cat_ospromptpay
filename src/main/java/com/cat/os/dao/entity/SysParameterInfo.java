package com.cat.os.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_parameter_info")
public class SysParameterInfo {
	
	@Id
	@Column(name = "param_info_id")
	private Integer paramInfoId;
	
	@Column(name = "param_Group_Id")
	private Integer paramGroupId;
	
	@Column(name = "param_code")
	private String paramCode;
	
	@Column(name = "value_1")
	private String value1;
	
	@Column(name = "value_2")
	private String value2;
	
	@Column(name = "value_3")
	private String value3;
	
	@Column(name = "value_4")
	private String value4;
	
	@Column(name = "value_5")
	private String value5;
	
	@Column(name = "value_6")
	private String value6;
	
	@Column(name = "sorting_order")
	private int sortingOrder;
	
	@Column(name = "is_default")
	private String isDefault;
	
	@Column(name = "is_deleted")
	private String isDeleted;
	
	@Column(name = "version")
	private int version;

	public Integer getParamInfoId() {
		return paramInfoId;
	}

	public void setParamInfoId(Integer paramInfoId) {
		this.paramInfoId = paramInfoId;
	}

	public Integer getParamGroupId() {
		return paramGroupId;
	}

	public void setParamGroupId(Integer paramGroupId) {
		this.paramGroupId = paramGroupId;
	}

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getValue3() {
		return value3;
	}

	public void setValue3(String value3) {
		this.value3 = value3;
	}

	public String getValue4() {
		return value4;
	}

	public void setValue4(String value4) {
		this.value4 = value4;
	}

	public String getValue5() {
		return value5;
	}

	public void setValue5(String value5) {
		this.value5 = value5;
	}

	public String getValue6() {
		return value6;
	}

	public void setValue6(String value6) {
		this.value6 = value6;
	}

	public int getSortingOrder() {
		return sortingOrder;
	}

	public void setSortingOrder(int sortingOrder) {
		this.sortingOrder = sortingOrder;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
}
