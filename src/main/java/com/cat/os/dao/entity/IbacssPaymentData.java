package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ibacss_payment_data")
public class IbacssPaymentData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "payment_data_id")
	private Long paymentDataId;

	@Column(name = "src_filename")
	private String srcFilename;

	@Column(name = "tx_date")
	private Date txDate;

	@Column(name = "filename")
	private String filename;

	@Column(name = "num_of_records")
	private String numOfRecords;

	@Column(name = "e_auction_1000")
	private String eauction1000;

	@Column(name = "e_auction_1200")
	private String eauction1200;

	@Column(name = "e_auction_1400")
	private String eauction1400;

	@Column(name = "e_auction_1600")
	private String eauction1600;

	@Column(name = "e_auction_1800")
	private String eauction1800;

	@Column(name = "is_deleted")
	private String isDeleted;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	public Long getPaymentDataId() {
		return paymentDataId;
	}

	public void setPaymentDataId(Long paymentDataId) {
		this.paymentDataId = paymentDataId;
	}

	public String getSrcFilename() {
		return srcFilename;
	}

	public void setSrcFilename(String srcFilename) {
		this.srcFilename = srcFilename;
	}

	public Date getTxDate() {
		return txDate;
	}

	public void setTxDate(Date txDate) {
		this.txDate = txDate;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getNumOfRecords() {
		return numOfRecords;
	}

	public void setNumOfRecords(String numOfRecords) {
		this.numOfRecords = numOfRecords;
	}

	public String getEauction1000() {
		return eauction1000;
	}

	public void setEauction1000(String eauction1000) {
		this.eauction1000 = eauction1000;
	}

	public String getEauction1200() {
		return eauction1200;
	}

	public void setEauction1200(String eauction1200) {
		this.eauction1200 = eauction1200;
	}

	public String getEauction1400() {
		return eauction1400;
	}

	public void setEauction1400(String eauction1400) {
		this.eauction1400 = eauction1400;
	}

	public String getEauction1600() {
		return eauction1600;
	}

	public void setEauction1600(String eauction1600) {
		this.eauction1600 = eauction1600;
	}

	public String getEauction1800() {
		return eauction1800;
	}

	public void setEauction1800(String eauction1800) {
		this.eauction1800 = eauction1800;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
