package com.cat.os.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ORD_SVC_ITEMS" , schema="OM")
public class OrdsvcItems {
	
	@Id
	@Column(name = "ID")
	private String orderId;
	
	@Column(name = "SVC_STATUS_ID")
	private String svcStatusId;
	
}
