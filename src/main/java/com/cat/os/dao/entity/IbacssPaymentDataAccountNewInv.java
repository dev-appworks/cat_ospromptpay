package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ibacss_payment_data_account_pay")
public class IbacssPaymentDataAccountNewInv {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account_new_inv_id")
	private Long accountNewInvId;

	@Column(name = "tx_date")
	private Date txDate;

	@Column(name = "bill_order_no")
	private String billOrderNo;

	@Column(name = "amount")
	private String amount;

	@Column(name = "tax")
	private String tax;

	@Column(name = "num_of_records")
	private String numOfRecords;

	@Column(name = "is_deleted")
	private String isDeleted;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	public Long getAccountNewInvId() {
		return accountNewInvId;
	}

	public void setAccountNewInvId(Long accountNewInvId) {
		this.accountNewInvId = accountNewInvId;
	}

	public Date getTxDate() {
		return txDate;
	}

	public void setTxDate(Date txDate) {
		this.txDate = txDate;
	}

	public String getBillOrderNo() {
		return billOrderNo;
	}

	public void setBillOrderNo(String billOrderNo) {
		this.billOrderNo = billOrderNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getNumOfRecords() {
		return numOfRecords;
	}

	public void setNumOfRecords(String numOfRecords) {
		this.numOfRecords = numOfRecords;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
