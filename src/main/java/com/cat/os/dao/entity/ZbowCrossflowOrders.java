package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "Z_BOW_CROSSFLOW_ORDERS" , schema="OM")
@IdClass(ZbowCrossflowOrdersId.class)
public class ZbowCrossflowOrders {
	
	@Id
	@Column(name = "ORDER_ID")
	private String orderId;
	
	@Id
	@Column(name = "ORDER_SVC_ITEM")
	private String orderSvcItem;

	@Column(name = "REMARK")
	private String remark;
	
	@Id
	@Column(name = "SVC_STATUS_ID")
	private String svcStatusId;
	
	@Id
	@Column(name = "UPDATE_DATE")
	private Date updateDate;
	
	@Id
	@Column(name = "USERNAME")
	private String username;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderSvcItem() {
		return orderSvcItem;
	}

	public void setOrderSvcItem(String orderSvcItem) {
		this.orderSvcItem = orderSvcItem;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSvcStatusId() {
		return svcStatusId;
	}

	public void setSvcStatusId(String svcStatusId) {
		this.svcStatusId = svcStatusId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
