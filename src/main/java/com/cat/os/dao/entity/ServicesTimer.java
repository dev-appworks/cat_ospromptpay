package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "services_timer")
public class ServicesTimer {
//	CREATE TABLE `services_timer` (
//			  `id` int(11) NOT NULL AUTO_INCREMENT,
//			  `service_status_id` int(11) NOT NULL,
//			  `status` int(11) NOT NULL,
//			  `timer` datetime DEFAULT NULL,
//			  `email` varchar(500) DEFAULT NULL,
//			  `update_by` varchar(255) DEFAULT NULL,
//			  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//			  PRIMARY KEY (`id`)
//			) ENGINE=InnoDB DEFAULT CHARSET=utf8
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer  id;
	
	@Column(name = "service_status_id")
	private Integer  serviceStatusId;
	
	@Column(name = "status")
	private Integer  status;

	@Column(name = "timer")
	@Temporal(TemporalType.TIMESTAMP)
	private Date  timer;
	
	@Column(name = "email")
	private String  email;
	
	@Column(name = "update_by")
	private String  updateBy;
	
	@Column(name = "update_datetime")
	private Date  updateDatetime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getServiceStatusId() {
		return serviceStatusId;
	}

	public void setServiceStatusId(Integer serviceStatusId) {
		this.serviceStatusId = serviceStatusId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getTimer() {
		return timer;
	}

	public void setTimer(Date timer) {
		this.timer = timer;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(Date updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

}
