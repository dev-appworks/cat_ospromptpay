package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "U_AGENT", schema = "OM")
public class UAgent {

	@Id
	@Column(name="AGENT_ID")
	private String agentId;
	
	@Column(name="AGENT_NAME")
	private String agentName;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="EMP_ID")
	private String empId;
	
	@Column(name="ALIAS_AGENT_ID")
	private String aliasAgentId;
	
	@Column(name="AGENT_REGION")
	private String agentRegion;
	
	@Column(name="AVAILABLE_FLAG")
	private String availableFlag;
	
	@Column(name="CTI_USERNAME")
	private String ctiUserName;
	
	@Column(name="CTI_PASSWORD")
	private String ctiPassword;
	
	@Column(name="DATE_START")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateStart;
	
	@Column(name="DATE_END")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateEnd;
	
	@Column(name="DOWNLINE_SIZE")
	private Integer downlineSize;
	
	@Column(name="EMAIL_ADDRESS")
	private String emailAddress;
	
	@Column(name="EMAIL_ADDRESS_UP")
	private String emailAddressUp;
	
	@Column(name="ERP_SALES_PERSON_CODE")
	private String erpSalesPersonCode;
	
	@Column(name="FORWARD_TO_AGENT_ID")
	private String forwardToAgentId;
	
	@Column(name="JOB_TITLE")
	private String jobTitle;
	
	@Column(name="JOB_TITLE_UP")
	private String jobTitleUp;
	
	@Column(name="MANAGER_AGENT_ID")
	private String managerAgentId;
	
	@Column(name="OBSOLETE_FLAG")
	private Integer obsoleteFlag;
	
	@Column(name="REVISION_NUMBER")
	private Integer revisionNumber;
	
	@Column(name="SALES_RANK")
	private Integer salesRank;
	
	@Column(name="SELF_REGISTERED_FLAG")
	private Integer selfRegisteredFlag;
	
	@Column(name="TEAM_ID")
	private String teamId;
	
	@Column(name="TELEPHONE")
	private String telephone;
	
	@Column(name="USER_DATA_ID")
	private String userDataId;
	
	@Column(name="YEARS_OF_EXPERIENCE")
	private Integer yearsOfExperience;
	
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getAliasAgentId() {
		return aliasAgentId;
	}

	public void setAliasAgentId(String aliasAgentId) {
		this.aliasAgentId = aliasAgentId;
	}

	public String getAgentRegion() {
		return agentRegion;
	}

	public void setAgentRegion(String agentRegion) {
		this.agentRegion = agentRegion;
	}

	public String getAvailableFlag() {
		return availableFlag;
	}

	public void setAvailableFlag(String availableFlag) {
		this.availableFlag = availableFlag;
	}

	public String getCtiUserName() {
		return ctiUserName;
	}

	public void setCtiUserName(String ctiUserName) {
		this.ctiUserName = ctiUserName;
	}

	public String getCtiPassword() {
		return ctiPassword;
	}

	public void setCtiPassword(String ctiPassword) {
		this.ctiPassword = ctiPassword;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Integer getDownlineSize() {
		return downlineSize;
	}

	public void setDownlineSize(Integer downlineSize) {
		this.downlineSize = downlineSize;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailAddressUp() {
		return emailAddressUp;
	}

	public void setEmailAddressUp(String emailAddressUp) {
		this.emailAddressUp = emailAddressUp;
	}

	public String getErpSalesPersonCode() {
		return erpSalesPersonCode;
	}

	public void setErpSalesPersonCode(String erpSalesPersonCode) {
		this.erpSalesPersonCode = erpSalesPersonCode;
	}

	public String getForwardToAgentId() {
		return forwardToAgentId;
	}

	public void setForwardToAgentId(String forwardToAgentId) {
		this.forwardToAgentId = forwardToAgentId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobTitleUp() {
		return jobTitleUp;
	}

	public void setJobTitleUp(String jobTitleUp) {
		this.jobTitleUp = jobTitleUp;
	}

	public String getManagerAgentId() {
		return managerAgentId;
	}

	public void setManagerAgentId(String managerAgentId) {
		this.managerAgentId = managerAgentId;
	}

	public Integer getObsoleteFlag() {
		return obsoleteFlag;
	}

	public void setObsoleteFlag(Integer obsoleteFlag) {
		this.obsoleteFlag = obsoleteFlag;
	}

	public Integer getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public Integer getSalesRank() {
		return salesRank;
	}

	public void setSalesRank(Integer salesRank) {
		this.salesRank = salesRank;
	}

	public Integer getSelfRegisteredFlag() {
		return selfRegisteredFlag;
	}

	public void setSelfRegisteredFlag(Integer selfRegisteredFlag) {
		this.selfRegisteredFlag = selfRegisteredFlag;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getUserDataId() {
		return userDataId;
	}

	public void setUserDataId(String userDataId) {
		this.userDataId = userDataId;
	}

	public Integer getYearsOfExperience() {
		return yearsOfExperience;
	}

	public void setYearsOfExperience(Integer yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}

}
