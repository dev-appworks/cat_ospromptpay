package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PROMPTPAY_CONSENT")
@NamedQuery(name="PromptpayConsent.findAll", query="SELECT p FROM PromptpayConsent p")
public class PromptpayConsent {

	@Id
	@Column(name="PROMPTPAY_CONSENT_ID")
	private Integer id;
	
	@Column(name="SYSTEM_NAME")
	private String systemName;
	
	@Column(name="MSISDN")
	private String msisdn;

	@Column(name="CID")
	private String cid;
	
	@Column(name="INPF01")
	private String inpf01;
	
	@Column(name="REFERENCE_CODE")
	private String referenceCode;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="UPDATED_BY")
	private String updatedBy;

	@Column(name="UPDATED_BYNAME")
	private String updatedByName;
	
	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getInpf01() {
		return inpf01;
	}

	public void setInpf01(String inpf01) {
		this.inpf01 = inpf01;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedByName() {
		return updatedByName;
	}

	public void setUpdatedByName(String updatedByName) {
		this.updatedByName = updatedByName;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "PromptpayConsent [id=" + id + ", systemName=" + systemName + ", msisdn=" + msisdn + ", cid=" + cid
				+ ", inpf01=" + inpf01 + ", referenceCode=" + referenceCode + ", status=" + status + ", description="
				+ description + ", updatedBy=" + updatedBy + ", updatedByName=" + updatedByName + ", updatedDate="
				+ updatedDate + "]";
	}
	
}