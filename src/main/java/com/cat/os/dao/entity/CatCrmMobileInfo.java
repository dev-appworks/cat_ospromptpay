package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the CATCRM_MOBILE_INFO database table.
 * 
 */
@Entity
@Table(schema="CRMDATA", name="CATCRM_MOBILE_INFO")
public class CatCrmMobileInfo {

	@Column(name="MSISDN")
	private String msisdn;
	
	@Id
	@Column(name="ID_NUMBER")
	private String idNumber;
	
	@Column(name="PAYMENT_TYPE")
	private String paymentType;
	
	@Temporal(TemporalType.DATE)
	@Column(name="START_DATE")
	private Date startDate;
	
	@Column(name="CUSTOMER_NAME")
	private String customerName;
	
	@Temporal(TemporalType.DATE)
	@Column(name="BIRTHDATE")
	private Date birthdate;
	
	@Column(name="GENDER")
	private String gender;
	
	@Column(name="ALL_PACKAGE")
	private String allPackage;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="ACCOUNT_TYPE")
	private String accountType;

	@Column(name="CUSTOMER_NUMBER")
	private String customerNumber;

	@Column(name="BILLING_NUMBER")
	private String billingNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	public CatCrmMobileInfo() {
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAllPackage() {
		return allPackage;
	}

	public void setAllPackage(String allPackage) {
		this.allPackage = allPackage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getBillingNumber() {
		return billingNumber;
	}

	public void setBillingNumber(String billingNumber) {
		this.billingNumber = billingNumber;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "CatCrmMobileInfo [msisdn=" + msisdn + ", idNumber=" + idNumber + ", paymentType=" + paymentType
				+ ", startDate=" + startDate + ", customerName=" + customerName + ", birthdate=" + birthdate
				+ ", gender=" + gender + ", allPackage=" + allPackage + ", status=" + status + ", accountType="
				+ accountType + ", customerNumber=" + customerNumber + ", billingNumber=" + billingNumber
				+ ", updateDate=" + updateDate + "]";
	}

}