package com.cat.os.dao.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "service_status")
public class ServiceStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "service_id")
	private String serviceId;
	
	@Column(name = "service_name")
	private String serviceName;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "timer")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timer;
	
	@Column(name = "updated_by")
	private String updatedBy;
	
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@Column(name = "is_deleted")
	private Integer isDeleted;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="serviceStatusId" , fetch=FetchType.EAGER)
	List<ServicesTimer> servicesTimers = new ArrayList<ServicesTimer>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Date getTimer() {
		return timer;
	}

	public void setTimer(Date timer) {
		this.timer = timer;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<ServicesTimer> getServicesTimers() {
		return servicesTimers;
	}

	public void setServicesTimers(List<ServicesTimer> servicesTimers) {
		this.servicesTimers = servicesTimers;
	}

}
