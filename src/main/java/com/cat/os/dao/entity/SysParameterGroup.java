package com.cat.os.dao.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sys_parameter_group")
public class SysParameterGroup {

	@Id
	@Column(name = "param_group_id")
	private Integer paramGroupId;
	
	@Column(name = "param_group_code")
	private String paramGroupCode;
	
	@Column(name = "param_group_desc")
	private String paramGroupDesc;
	
	@Column(name = "is_deleted")
	private String isDeleted;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="paramGroupId" , fetch=FetchType.EAGER)
	List<SysParameterInfo> parameterInfos = new ArrayList<SysParameterInfo>();

	public Integer getParamGroupId() {
		return paramGroupId;
	}

	public void setParamGroupId(Integer paramGroupId) {
		this.paramGroupId = paramGroupId;
	}

	public String getParamGroupCode() {
		return paramGroupCode;
	}

	public void setParamGroupCode(String paramGroupCode) {
		this.paramGroupCode = paramGroupCode;
	}

	public String getParamGroupDesc() {
		return paramGroupDesc;
	}

	public void setParamGroupDesc(String paramGroupDesc) {
		this.paramGroupDesc = paramGroupDesc;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<SysParameterInfo> getParameterInfos() {
		return parameterInfos;
	}

	public void setParameterInfos(List<SysParameterInfo> parameterInfos) {
		this.parameterInfos = parameterInfos;
	}

}
