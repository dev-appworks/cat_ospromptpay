package com.cat.os.dao.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class ZbowCrossflowOrdersId implements Serializable {

	private static final long serialVersionUID = -2418531471373510828L;

	private String orderId;

	private String orderSvcItem;

	private String svcStatusId;

	private Date updateDate;

	private String username;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderSvcItem() {
		return orderSvcItem;
	}

	public void setOrderSvcItem(String orderSvcItem) {
		this.orderSvcItem = orderSvcItem;
	}

	public String getSvcStatusId() {
		return svcStatusId;
	}

	public void setSvcStatusId(String svcStatusId) {
		this.svcStatusId = svcStatusId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


}
