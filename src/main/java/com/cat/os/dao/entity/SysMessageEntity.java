package com.cat.os.dao.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.cat.os.dao.vo.ArmVo;

@Entity
@Table(name = "sys_message")
@NamedNativeQuery(name = "SysMessageEntity.findArmVoAll", query = "SELECT param_group_id messageId ,param_group_code messageCode FROM sys_parameter_group", resultSetMapping = "JediResult")
@SqlResultSetMapping(name = "JediResult",  classes = @ConstructorResult(
        targetClass = ArmVo.class,
        columns = {
            @ColumnResult(name = "messageId", type = Integer.class),
            @ColumnResult(name = "messageCode")}))
public class SysMessageEntity {

	@Id
	@Column(name = "message_id")
	private Integer messageId;

	@Column(name = "message_code")
	private String messageCode;

	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

}
