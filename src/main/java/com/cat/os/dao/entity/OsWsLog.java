package com.cat.os.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oswslog")
public class OsWsLog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "logdatetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date logDateTime;
	
	@Column(name = "function_name")
	private String functionName;
		
	@Column(name = "request_id")
	private String requestId;
	
	@Column(name = "response_id")
	private String responseId;
	
	@Column(name = "request_datetime")
	private Date requestDatetime;
	
	@Column(name = "response_datetime")
	private Date responseDatetime;
	
	@Column(name = "request_appid")
	private String requestAppid;
	
	@Column(name = "response_appid")
	private String responseAppid;
	
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "ipaddress")
	private String ipaddress;
	
	@Column(name = "auth_user_id")
	private String authUserId;
	
	@Column(name = "status_code")
	private String statusCode;
	
	@Column(name = "error_id")
	private int errorId;
	 
	@Column(name = "service_id")
	private String serviceId;
	
	@Column(name = "status")
	private int status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getLogDateTime() {
		return logDateTime;
	}

	public void setLogDateTime(Date logDateTime) {
		this.logDateTime = logDateTime;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public Date getRequestDatetime() {
		return requestDatetime;
	}

	public void setRequestDatetime(Date requestDatetime) {
		this.requestDatetime = requestDatetime;
	}

	public Date getResponseDatetime() {
		return responseDatetime;
	}

	public void setResponseDatetime(Date responseDatetime) {
		this.responseDatetime = responseDatetime;
	}

	public String getRequestAppid() {
		return requestAppid;
	}

	public void setRequestAppid(String requestAppid) {
		this.requestAppid = requestAppid;
	}

	public String getResponseAppid() {
		return responseAppid;
	}

	public void setResponseAppid(String responseAppid) {
		this.responseAppid = responseAppid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getAuthUserId() {
		return authUserId;
	}

	public void setAuthUserId(String authUserId) {
		this.authUserId = authUserId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public int getErrorId() {
		return errorId;
	}

	public void setErrorId(int errorId) {
		this.errorId = errorId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
