package com.cat.os.dao.mysql;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.AuditLog;


public interface AuditLogRepository extends CrudRepository<AuditLog, Integer> {

}
