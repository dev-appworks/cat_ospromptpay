package com.cat.os.dao.mysql;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.cat.os.common.persistence.dao.CommonBatchPreparedStatementSetter;
import com.cat.os.common.persistence.dao.CommonJdbcDao;
import com.cat.os.common.persistence.util.SqlGeneratorUtils;
import com.cat.os.dao.entity.IbacssPaymentDataAccountNewInv;

@Repository
public class IbacssAccountNewInvJdbcRepository {
	
	private static final Logger log = LoggerFactory.getLogger(IbacssAccountNewInvJdbcRepository.class);
	
	@Autowired
	@Qualifier("systemCommonJdbcDao")
	private CommonJdbcDao commonJdbcDao;
	
	public int[][] batchInsert(List<IbacssPaymentDataAccountNewInv> accountNewInvList) throws SQLException {
		log.info("batchInsert");
		
		String sql = SqlGeneratorUtils.genSqlInsert("`ibacss_payment_data_account_new_inv`", Arrays.asList(
			"`tx_date`",
			"`bill_order_no`",
			"`amount`",
			"`tax`",
			"`num_of_records`"
		));
		
		CommonBatchPreparedStatementSetter<IbacssPaymentDataAccountNewInv> bs = new CommonBatchPreparedStatementSetter<IbacssPaymentDataAccountNewInv>() {
			@Override
			public List<IbacssPaymentDataAccountNewInv> getBatchObjectList() {
				return accountNewInvList;
			}
			
			@Override
			public Object[] toObjects(IbacssPaymentDataAccountNewInv obj) {
				return new Object[] {
					obj.getTxDate(),
					obj.getBillOrderNo(),
					obj.getAmount(),
					obj.getTax(),
					obj.getNumOfRecords()
				};
			}
		};
		
		int result[][] = commonJdbcDao.executeBatch(sql, bs);
		
		return result;
	}
	
}
