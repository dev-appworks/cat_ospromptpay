package com.cat.os.dao.mysql;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.SysMessageEntity;
import com.cat.os.dao.vo.ArmVo;

public interface MessageRepository  extends CrudRepository<SysMessageEntity, Integer>  {
	  List<SysMessageEntity> findByMessageId( Integer messageId);
	  
	  @Query("SELECT m FROM SysMessageEntity m WHERE m.messageCode=?1")
	  List<SysMessageEntity> findByMessageCode( String messageCode);
	  
	  
	  @Query(value = "SELECT * FROM sys_message m WHERE m.message_code=?1", nativeQuery = true)
	  List<SysMessageEntity> findMessageByNative(String emailAddress);

	  @Query(value = "SELECT param_group_id message_id ,param_group_code message_code FROM sys_parameter_group", nativeQuery = true)
	  List<SysMessageEntity> findsysParameterGroupByNative();

	  //Native SQL , Custom Mapping Entity
	  List<ArmVo> findArmVoAll();
	  
	  
	  Page<SysMessageEntity> findAll(Pageable pageable);
	  
	  Page<SysMessageEntity> findByMessageIdIn(Collection<Integer> messageIds, Pageable pageable);
	  

}
