package com.cat.os.dao.mysql;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.EmailTemplate;

public interface EmailTemplateRepository extends CrudRepository<EmailTemplate, String>{

}
