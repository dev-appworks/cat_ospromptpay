package com.cat.os.dao.mysql;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.AdmRoleEntity;

public interface AdmRoleEntityRepository extends CrudRepository<AdmRoleEntity, Integer> {
	
	  Page<AdmRoleEntity> findAll(Pageable pageable);

}
