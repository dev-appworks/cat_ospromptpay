package com.cat.os.dao.mysql;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.cat.os.common.persistence.dao.CommonBatchPreparedStatementSetter;
import com.cat.os.common.persistence.dao.CommonJdbcDao;
import com.cat.os.common.persistence.util.SqlGeneratorUtils;
import com.cat.os.dao.entity.IbacssPaymentDataAccountPay;

@Repository
public class IbacssAccountPayJdbcRepository {
	
	private static final Logger log = LoggerFactory.getLogger(IbacssAccountPayJdbcRepository.class);
	
	@Autowired
	@Qualifier("systemCommonJdbcDao")
	private CommonJdbcDao commonJdbcDao;
	
	public int[][] batchInsert(List<IbacssPaymentDataAccountPay> accountPayList) throws SQLException {
		log.info("batchInsert");
		
		String sql = SqlGeneratorUtils.genSqlInsert("`ibacss_payment_data_account_pay`", Arrays.asList(
			"`tx_date`",
			"`pay_mode`",
			"`desc`",
			"`num_of_records`"
		));
		
		CommonBatchPreparedStatementSetter<IbacssPaymentDataAccountPay> bs = new CommonBatchPreparedStatementSetter<IbacssPaymentDataAccountPay>() {
			@Override
			public List<IbacssPaymentDataAccountPay> getBatchObjectList() {
				return accountPayList;
			}
			
			@Override
			public Object[] toObjects(IbacssPaymentDataAccountPay obj) {
				return new Object[] {
					obj.getTxDate(),
					obj.getPayMode(),
					obj.getDesc(),
					obj.getNumOfRecords()
				};
			}
		};
		
		int result[][] = commonJdbcDao.executeBatch(sql, bs);
		
		return result;
	}
	
}
