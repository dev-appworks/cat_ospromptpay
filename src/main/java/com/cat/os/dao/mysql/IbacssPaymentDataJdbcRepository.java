package com.cat.os.dao.mysql;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.cat.os.common.persistence.dao.CommonBatchPreparedStatementSetter;
import com.cat.os.common.persistence.dao.CommonJdbcDao;
import com.cat.os.common.persistence.util.SqlGeneratorUtils;
import com.cat.os.dao.entity.IbacssPaymentData;

@Repository
public class IbacssPaymentDataJdbcRepository {
	
	private static final Logger log = LoggerFactory.getLogger(IbacssPaymentDataJdbcRepository.class);
	
	@Autowired
	@Qualifier("systemCommonJdbcDao")
	private CommonJdbcDao commonJdbcDao;
	
	public int[][] batchInsert(List<IbacssPaymentData> paymentDataList) throws SQLException {
		log.info("batchInsert");
		
		String sql = SqlGeneratorUtils.genSqlInsert("`ibacss_payment_data`", Arrays.asList(
			"`src_filename`",
			"`tx_date`",
			"`filename`",
			"`num_of_records`",
			"`e_auction_1000`",
			"`e_auction_1200`",
			"`e_auction_1400`",
			"`e_auction_1600`",
			"`e_auction_1800`"
		));
		
		CommonBatchPreparedStatementSetter<IbacssPaymentData> bs = new CommonBatchPreparedStatementSetter<IbacssPaymentData>() {
			@Override
			public List<IbacssPaymentData> getBatchObjectList() {
				return paymentDataList;
			}
			
			@Override
			public Object[] toObjects(IbacssPaymentData obj) {
				return new Object[] {
					obj.getSrcFilename(),
					obj.getTxDate(),
					obj.getFilename(),
					obj.getNumOfRecords(),
					obj.getEauction1000(),
					obj.getEauction1200(),
					obj.getEauction1400(),
					obj.getEauction1600(),
					obj.getEauction1800()
				};
			}
		};
		
		int result[][] = commonJdbcDao.executeBatch(sql, bs);
		
		return result;
	}
	
}
