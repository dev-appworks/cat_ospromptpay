package com.cat.os.dao.mysql;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.SysMessageArmDBEntity;

public interface MessageArmDbRepository extends CrudRepository<SysMessageArmDBEntity, Integer> {
	
	  Page<SysMessageArmDBEntity> findAll(Pageable pageable);

}
