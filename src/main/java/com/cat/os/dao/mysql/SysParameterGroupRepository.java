package com.cat.os.dao.mysql;


import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.SysParameterGroup;

public interface SysParameterGroupRepository extends CrudRepository<SysParameterGroup, Integer>  {

	SysParameterGroup findSysParameterGroupByParamGroupCode(String paramGroupCode);
}
