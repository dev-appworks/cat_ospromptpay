package com.cat.os.dao.mysql;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.OsWsLog;

public interface OsWsLogRepository extends CrudRepository<OsWsLog, Integer> {

}
