package com.cat.os.dao.mysql;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.OsWsErrorLog;

public interface OsWsErrorLogRepository extends CrudRepository<OsWsErrorLog, Integer> {

}
