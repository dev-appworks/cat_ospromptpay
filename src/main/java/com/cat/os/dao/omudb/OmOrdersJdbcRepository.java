package com.cat.os.dao.omudb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.cat.os.common.utils.DataTableUtils;
import com.cat.os.dao.entity.ZbowCrossflowOrders;
import com.cat.os.dao.vo.OmFormVo;
import com.cat.os.dao.vo.OmOrderVo;
import com.cat.os.dao.vo.OmudbHistoryFormVo;
import com.cat.os.dao.vo.OmudbHistoryVo;
import com.cat.os.dao.vo.OrderStatusVo;

@Repository
public class OmOrdersJdbcRepository {

	private static final Logger log = LoggerFactory.getLogger(OmOrdersJdbcRepository.class);

	@Autowired(required=false)
	@Qualifier("omudbJdbcTemplate")
	private JdbcTemplate omudbJdbcTemplate;
	
	private final String orderbyTask =  "    SELECT" + 
			"      ROW_NUMBER() OVER(ORDER BY %s %s) AS rn," + 
			"      o.ID ORDER_ID," + 
			"      ovi.ID SERVICE_ITEM_ID," + 
			"      ovi.SVC_STATUS_ID SVC_STATUS_ID," + 
			"      orst.status STATUS" + 
			"    FROM" + 
			"      om.orders o" + 
			"    LEFT JOIN om.ORD_SVC_ITEMS ovi" + 
			"    ON" + 
			"      o.ID = ovi.ORDER_ID" + 
			"    JOIN om.ORDER_STATUS orst" + 
			"    ON" + 
			"      svc_status_id = orst.id" + 
			"    WHERE" + 
			"      o.ORDER_TYPE_LKP    IN (1, 5)" + 
			"      AND ovi.SVC_STATUS_ID is not null " + 
//			"    AND ovi.SVC_STATUS_ID IN (1001, 1005, 1006, 1008)" + 
			"  ";

	private final String orderbyServiceItemId = "    SELECT" + 
			"      ROW_NUMBER() OVER(ORDER BY %s %s) AS rn," + 
			"      o.ID ORDER_ID," + 
			"      ovi.ID SERVICE_ITEM_ID," + 
			"      ovi.SVC_STATUS_ID SVC_STATUS_ID," + 
			"      orst.status status" + 
			"    FROM" + 
			"      om.orders o" + 
			"    LEFT JOIN om.ORD_SVC_ITEMS ovi" + 
			"    ON" + 
			"      o.ID = ovi.ORDER_ID" + 
			"    LEFT JOIN om.ORDER_STATUS orst" + 
			"    ON" + 
			"      svc_status_id = orst.id" + 
			"    WHERE" + 
			"      o.ORDER_TYPE_LKP    IN (1, 5)" + 
			"      AND ovi.SVC_STATUS_ID is not null " + 
//			"    AND ovi.SVC_STATUS_ID IN (1001, 1005, 1006, 1008)" + 
			"  ";
	

	private final String getOmudbHistoryQuery = "SELECT ROW_NUMBER() OVER(ORDER BY z.update_date desc) AS rn, z.* FROM om.Z_BOW_CROSSFLOW_ORDERS z WHERE 1=1 ";
	
	public List<OmOrderVo> getOrderByTask(OmFormVo omFormVo) {
		List<Object> param = new ArrayList<Object>();
		int start = omFormVo.getStart();
		int length = omFormVo.getLength();
		String orderId = omFormVo.getOrderId();
		String field = DataTableUtils.getFieldFromDataTableOmOrder(omFormVo.getOrder(), omFormVo.getColumns());
		String orderBy = DataTableUtils.getOrderByFromDataTable(omFormVo.getOrder());
		
		StringBuilder sql = new StringBuilder("SELECT * FROM ( " + orderbyTask);
		
		if(StringUtils.isNotBlank(orderId)) {
			sql = sql.append("  AND o.ID like ? ");
			param.add(orderId + "%");
		}
		
		sql = sql.append(" order by %s %s ");
		sql = DataTableUtils.setParamToSqlSortDB2(sql, field, orderBy);
		
		sql = sql.append(" ) WHERE rn BETWEEN ? AND ? ");
		param.add(start);
		int limitLength = start + length - (start > 0 ? 1 : 0 );
		param.add(limitLength);
		
		log.debug("SQL : " + sql);
		List<OmOrderVo> list = omudbJdbcTemplate.query(sql.toString(), param.toArray() , orderbyTaskRowMapper);
		return list;
	}
	
	public Long getCountOrderByTask(OmFormVo omFormVo) {
		List<String> param = new ArrayList<String>();
		String orderId = omFormVo.getOrderId();
		String field = DataTableUtils.getFieldFromDataTableOmOrder(omFormVo.getOrder(), omFormVo.getColumns());
		String orderBy = DataTableUtils.getOrderByFromDataTable(omFormVo.getOrder());
		
		StringBuilder sql = new StringBuilder("SELECT  count(1) FROM ( " + orderbyTask);
		sql = DataTableUtils.setParamToSqlSortDB2(sql, field, orderBy);
		
		if(StringUtils.isNoneBlank(orderId)) {
			sql = sql.append("  AND o.ID like ? ");
			param.add(orderId + "%");
		}
		
		sql = sql.append(" ) ");
		log.debug("SQL : " + sql);

		return omudbJdbcTemplate.queryForObject(sql.toString(), param.toArray() , Long.class);
	}
	
	
	public List<OmOrderVo> getOrderByServiceItemId(OmFormVo omFormVo) {
		List<Object> param = new ArrayList<Object>();
		int start = omFormVo.getStart();
		int length = omFormVo.getLength();
		String serviceItemId = omFormVo.getServiceItemId();
		String field = DataTableUtils.getFieldFromDataTableOmOrder(omFormVo.getOrder(), omFormVo.getColumns());
		String orderBy = DataTableUtils.getOrderByFromDataTable(omFormVo.getOrder());
				
		StringBuilder sql = new StringBuilder("SELECT * FROM ( " + orderbyTask);
		
		if(StringUtils.isNotBlank(serviceItemId)) {
			sql = sql.append("  AND ovi.ID like ? ");
			param.add(serviceItemId + "%");
		}
		
		sql = sql.append(" order by %s %s ");
		sql = DataTableUtils.setParamToSqlSortDB2(sql, field, orderBy);
		
		sql = sql.append(" ) WHERE rn BETWEEN ? AND ? ");
		param.add(start);
		int limitLength = start + length - (start > 0 ? 1 : 0 );
		param.add(limitLength);
		
		log.debug("SQL : " + sql);
		List<OmOrderVo> list = omudbJdbcTemplate.query(sql.toString(), param.toArray() , orderbyTaskRowMapper);
		return list;
	
	}

	public Long getCountOrderByServiceItemId(OmFormVo omFormVo) {
		List<String> param = new ArrayList<String>();
		String serviceItemId = omFormVo.getServiceItemId();
		String field = DataTableUtils.getFieldFromDataTableOmOrder(omFormVo.getOrder(), omFormVo.getColumns());
		String orderBy = DataTableUtils.getOrderByFromDataTable(omFormVo.getOrder());
		
		StringBuilder sql = new StringBuilder("SELECT  count(1) FROM ( " + orderbyServiceItemId);
		sql = DataTableUtils.setParamToSqlSortDB2(sql, field, orderBy);
		
		if(StringUtils.isNoneBlank(serviceItemId)) {
			sql = sql.append("  AND  ovi.ID like ? ");
			param.add(serviceItemId + "%");
		}
		
		sql = sql.append(" ) ");
		log.debug("SQL : " + sql);

		return omudbJdbcTemplate.queryForObject(sql.toString(), param.toArray() , Long.class);
	}
	
	
	private RowMapper<OmOrderVo> orderbyTaskRowMapper = new RowMapper<OmOrderVo>() {

		@Override
		public OmOrderVo mapRow(ResultSet rs, int arg1) throws SQLException {
			OmOrderVo vo = new OmOrderVo();
			
			vo.setOrderId(rs.getString("ORDER_ID"));
			vo.setServiceItemId(rs.getString("SERVICE_ITEM_ID"));
			vo.setStatusCode(rs.getString("SVC_STATUS_ID"));
			vo.setStatusDisplay(rs.getString("status"));
			
			return vo;
		}
	};



	public List<OrderStatusVo> getFilterDropdownByStatusCode(String[] listCode) {
		
		String sql = "select id,category,status from om.ORDER_STATUS where id in (";
		for (int i= 0 ; i < listCode.length ; i++ ) {
			sql = sql.concat( "'" + listCode[i] + "'" );
			if(i != listCode.length-1) {
				sql = sql.concat(", ");
			}
		}
		sql = sql.concat(" ) ");
		
		log.info("sql : " + sql);
		List<OrderStatusVo> list = omudbJdbcTemplate.query(sql, OrderStatusVoRowMapper);
		
		return list;
	}
	
	private RowMapper<OrderStatusVo> OrderStatusVoRowMapper = new RowMapper<OrderStatusVo>() {

		@Override
		public OrderStatusVo mapRow(ResultSet rs, int arg1) throws SQLException {
			OrderStatusVo vo = new OrderStatusVo();
			
			vo.setId(rs.getString("id"));
			vo.setCategory(rs.getString("category"));
			vo.setStatus(rs.getString("status"));
			
			return vo;
		}
	};

	public void insertZbowCrossflowOrders(ZbowCrossflowOrders enity) {

		String sql = "insert into OM.Z_BOW_CROSSFLOW_ORDERS values(?,?, ?, ?, ?,?)";

		omudbJdbcTemplate.update(sql, new Object[] {
				enity.getOrderId(),
				enity.getOrderSvcItem(),
				enity.getSvcStatusId(),
				enity.getRemark(),
				enity.getUpdateDate(),
				enity.getUsername()
		});
		
	}

	public void updateSvcStatusId(String orderId, String status) {
		String sql = "update om.ORD_SVC_ITEMS i set i.SVC_STATUS_ID=? where  i.ORDER_ID = ? ";
		omudbJdbcTemplate.update(sql, new Object[] {
				status, orderId
		});
	}

	public void updateSvcStatusIdCaseServiceItem(String serviceItemId, String changeStatusCode) {
		String sql = "update om.ORD_SVC_ITEMS i set i.SVC_STATUS_ID=? where  i.id = ? ";
		omudbJdbcTemplate.update(sql, new Object[] {
				changeStatusCode, serviceItemId
		});
		
	}
	
	public List<OmOrderVo>  getBpmTaskIdById(String id) {
		String sql = "select BPM_TASK_ID from OM.ORDERS Where ID = ? ";
		
		log.info("sql : " + sql);
		log.info("Id : " + id);
		List<OmOrderVo> bpmTaskId = omudbJdbcTemplate.query(sql, new Object[] { id }, getBpmTaskIdByIdRowMapper);
		
		return bpmTaskId;
	}
	
	private RowMapper<OmOrderVo> getBpmTaskIdByIdRowMapper = new RowMapper<OmOrderVo>() {

		@Override
		public OmOrderVo mapRow(ResultSet rs, int arg1) throws SQLException {
			OmOrderVo vo = new OmOrderVo();
			
			vo.setBpmTaskId(rs.getString("BPM_TASK_ID"));
			
			return vo;
		}
	};

	public List<OmudbHistoryVo> getOmudbHistoryCriteria(OmudbHistoryFormVo omudbHistoryFormVo) {
		List<Object> param = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("SELECT * FROM ( " + getOmudbHistoryQuery);
		int start = omudbHistoryFormVo.getStart();
		int length = omudbHistoryFormVo.getLength();
		
		String orderId = omudbHistoryFormVo.getOrderId();
		String orderSvcItem = omudbHistoryFormVo.getOrderSvcItem();
		String dateFrom = omudbHistoryFormVo.getDateFrom();
		String dateTo = omudbHistoryFormVo.getDateTo();
		
		if(StringUtils.isNotBlank(orderId)) {
			sql = sql.append("  AND z.order_Id like ? ");
			param.add("%" + orderId + "%");
		}
		
		if(StringUtils.isNotBlank(orderSvcItem)) {
			sql = sql.append("  AND z.order_Svc_Item like ? ");
			param.add("%" + orderSvcItem + "%");
		}
		
		if(StringUtils.isNotBlank(dateFrom)) {
			sql = sql.append("  AND z.update_Date >= TIMESTAMP_FORMAT(?, 'DD/MM/YYYY') ");
			param.add(dateFrom);
		}
		
		if(StringUtils.isNotBlank(dateTo)) {
			sql = sql.append("  AND z.update_Date <= TIMESTAMP_FORMAT(?, 'DD/MM/YYYY') ");
			param.add(dateTo);
		}
		
		sql = sql.append(" ) WHERE rn BETWEEN ? AND ? ");
		param.add(start);
		int limitLength = start + length - (start > 0 ? 1 : 0 );
		param.add(limitLength);
		
		log.debug("SQL : " + sql);
		List<OmudbHistoryVo> list = omudbJdbcTemplate.query(sql.toString(), param.toArray() , omudbHistoryRowMapper);
		return list;
	}
	
	public Long getCountOmudbHistoryCriteria(OmudbHistoryFormVo omudbHistoryFormVo) {
		List<String> param = new ArrayList<String>();
		
		StringBuilder sql = new StringBuilder("SELECT count(1) FROM ( " + getOmudbHistoryQuery);
		
		String orderId = omudbHistoryFormVo.getOrderId();
		String orderSvcItem = omudbHistoryFormVo.getOrderSvcItem();
		String dateFrom = omudbHistoryFormVo.getDateFrom();
		String dateTo = omudbHistoryFormVo.getDateTo();
		
		if(StringUtils.isNotBlank(orderId)) {
			sql = sql.append("  AND z.order_Id like ? ");
			param.add("%" + orderId + "%");
		}
		
		if(StringUtils.isNotBlank(orderSvcItem)) {
			sql = sql.append("  AND z.order_Svc_Item like ? ");
			param.add("%" + orderSvcItem + "%");
		}
		
		if(StringUtils.isNotBlank(dateFrom)) {
			sql = sql.append("  AND z.update_Date >= TIMESTAMP_FORMAT(?, 'DD/MM/YYYY') ");
			param.add(dateFrom);
		}
		
		if(StringUtils.isNotBlank(dateTo)) {
			sql = sql.append("  AND z.update_Date <= TIMESTAMP_FORMAT(?, 'DD/MM/YYYY') ");
			param.add(dateTo);
		}
		
		sql = sql.append(" ) ");
		log.debug("SQL : " + sql);

		return omudbJdbcTemplate.queryForObject(sql.toString(), param.toArray() , Long.class);
	}
	
	private RowMapper<OmudbHistoryVo> omudbHistoryRowMapper = new RowMapper<OmudbHistoryVo>() {

		@Override
		public OmudbHistoryVo mapRow(ResultSet rs, int arg1) throws SQLException {
			OmudbHistoryVo vo = new OmudbHistoryVo();
			
			vo.setOrderId(rs.getString("ORDER_ID"));
			vo.setOrderSvcItem(rs.getString("ORDER_SVC_ITEM"));
			vo.setRemark(rs.getString("REMARK"));
			vo.setSvcStatusId(rs.getString("SVC_STATUS_ID"));
			vo.setUpdateDate(rs.getTimestamp("UPDATE_DATE"));
			vo.setUsername(rs.getString("USERNAME"));
			
			return vo;
		}
	};

}
