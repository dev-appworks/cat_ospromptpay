package com.cat.os.dao.omudb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.cat.os.common.utils.DataTableUtils;
import com.cat.os.dao.entity.UAgent;
import com.cat.os.dao.vo.OmUserFormVo;
import com.cat.os.dao.vo.UeTerritoryAgentVo;
import com.cat.os.dao.vo.UeTerritoryDropdown;

@Repository
public class UAgentJdbcRepository {
	private static final Logger log = LoggerFactory.getLogger(UAgentJdbcRepository.class);

	@Autowired(required=false)
	@Qualifier("omudbJdbcTemplate")
	private JdbcTemplate omudbJdbcTemplate;

	private final String GET_USER = " select ROW_NUMBER() OVER(ORDER BY %s %s) AS rn  , a.* from om.U_AGENT a WHERE a.OBSOLETE_FLAG =0  ";
	
	private final String FIND_TERRITORY_AGENT = " select u.* from  om.U_E_TERRITORY_AGENT u where u.OBSOLETE_FLAG = '0' and u.AGENT_ID=? " ;
	
	private final String DROPDOWN_TERRITORY = " select u.* from om.U_E_TERRITORY u where u.OBSOLETE_FLAG = '0' ";

	private final String CHECK_AGEN_NAME = " select count(1)from     om.U_AGENT a where    a.AGENT_NAME = ?  and a.OBSOLETE_FLAG in ('0') ";
	
	
	public List<UAgent> getUserByCriteria(OmUserFormVo omUserFormVo) {

		List<Object> params = new ArrayList<>(); 
		String field = DataTableUtils.getFieldFromDataTableUserAgent(omUserFormVo.getOrder(), omUserFormVo.getColumns());
		String orderBy = DataTableUtils.getOrderByFromDataTable(omUserFormVo.getOrder());
		StringBuilder sql = new StringBuilder(" SELECT * FROM ( ").append(GET_USER);
		
		if(StringUtils.isNotBlank(omUserFormVo.getUserName())) {
			sql.append("AND a.AGENT_NAME = ?");
			params.add(omUserFormVo.getUserName());
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getFirstName())) {
			sql.append("AND a.FIRST_NAME LIKE ?");
			params.add("%".concat(omUserFormVo.getFirstName()).concat("%"));
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getLastName())) {
			sql.append("AND a.LAST_NAME LIKE ?");
			params.add("%".concat(omUserFormVo.getLastName()).concat("%"));
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getEmpId())) {
			sql.append("AND a.EMP_ID = ?");
			params.add(omUserFormVo.getEmpId());
		}

		sql = sql.append(" order by %s %s ");
		sql = DataTableUtils.setParamToSqlSortDB2(sql, field, orderBy);
		
		sql = sql.append(" ) WHERE rn BETWEEN ? AND ? ");
		int start = omUserFormVo.getStart();
		int length = omUserFormVo.getLength();
		params.add(start );
		int limitLength = start + length - (start > 0 ? 1 : 0 );
		params.add(limitLength);
		log.debug("SQL : "  + sql.toString());
		return omudbJdbcTemplate.query(sql.toString(), params.toArray(), uAgentRowMapper);
	}
	
	public List<UAgent> getUserByCriteriaExistLimit(OmUserFormVo omUserFormVo) {

		List<Object> params = new ArrayList<>();
		StringBuilder sql = new StringBuilder(" select a.* from om.U_AGENT a WHERE a.OBSOLETE_FLAG = 0 ");
		
		if(StringUtils.isNotBlank(omUserFormVo.getUserName())) {
			sql.append(" AND a.AGENT_NAME = ? ");
			params.add(omUserFormVo.getUserName());
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getFirstName())) {
			sql.append(" AND a.FIRST_NAME LIKE ? ");
			params.add("%".concat(omUserFormVo.getFirstName()).concat("%"));
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getLastName())) {
			sql.append(" AND a.LAST_NAME LIKE ? ");
			params.add("%".concat(omUserFormVo.getLastName()).concat("%"));
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getEmpId())) {
			sql.append(" AND a.EMP_ID = ? ");
			params.add(omUserFormVo.getEmpId());
		}
		
		log.debug("SQL : "  + sql.toString());
		return omudbJdbcTemplate.query(sql.toString(), params.toArray(), uAgentRowMapper);
	}

	public Long geCounttUserByCriteria(OmUserFormVo omUserFormVo) {
		List<Object> params = new ArrayList<>(); 
		String field = DataTableUtils.getFieldFromDataTableUserAgent(omUserFormVo.getOrder(), omUserFormVo.getColumns());
		String orderBy = DataTableUtils.getOrderByFromDataTable(omUserFormVo.getOrder());
		StringBuilder sql = new StringBuilder(" select count(1) from ( ").append(GET_USER);
		sql = DataTableUtils.setParamToSqlSortDB2(sql, field, orderBy);

		if(StringUtils.isNotBlank(omUserFormVo.getUserName())) {
			sql.append("AND a.AGENT_NAME = ?");
			params.add(omUserFormVo.getUserName());
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getFirstName())) {
			sql.append("AND a.FIRST_NAME LIKE ?");
			params.add("%".concat(omUserFormVo.getFirstName()).concat("%"));
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getLastName())) {
			sql.append("AND a.LAST_NAME LIKE ?");
			params.add("%".concat(omUserFormVo.getLastName()).concat("%"));
		}
		
		if(StringUtils.isNotBlank(omUserFormVo.getEmpId())) {
			sql.append("AND a.EMP_ID = ?");
			params.add(omUserFormVo.getEmpId());
		}
		
		sql.append(" ) ");

		log.debug("SQL : "  + sql.toString());
		return omudbJdbcTemplate.queryForObject(sql.toString(), params.toArray(),Long.class);
	}
	
	
	private RowMapper<UAgent> uAgentRowMapper = new RowMapper<UAgent>() {

		@Override
		public UAgent mapRow(ResultSet rs, int arg1) throws SQLException {
			UAgent vo = new UAgent();
			
			vo.setAgentId(rs.getString("AGENT_ID"));
			vo.setFirstName(rs.getString("FIRST_NAME"));
			vo.setLastName(rs.getString("LAST_NAME"));
			vo.setEmpId(rs.getString("EMP_ID"));
			vo.setAgentName(rs.getString("AGENT_NAME"));
			vo.setObsoleteFlag(rs.getInt("OBSOLETE_FLAG"));
			
			return vo;
		}
	};
	
	
	public List<UeTerritoryAgentVo> findTerritoryAgent(String agentId) {
		return omudbJdbcTemplate.query(FIND_TERRITORY_AGENT, new Object[] { agentId } , ueTerritoryAgentVoRowMapper);
	}
	
	private RowMapper<UeTerritoryAgentVo> ueTerritoryAgentVoRowMapper = new RowMapper<UeTerritoryAgentVo>() {

		@Override
		public UeTerritoryAgentVo mapRow(ResultSet rs, int arg1) throws SQLException {
			UeTerritoryAgentVo vo = new UeTerritoryAgentVo();
			vo.seteTerritoryAgentId(rs.getString("E_TERRITORY_AGENT_ID"));
			vo.seteTerritoryId(rs.getString("E_TERRITORY_ID"));
			vo.setAgentId(rs.getString("AGENT_ID"));
			
			return vo;
		}
	};

	
	public List<UeTerritoryDropdown> getTerritoryDropdown() {
		return omudbJdbcTemplate.query(DROPDOWN_TERRITORY , ueTerritoryDropdownRowMapper);
	}
	
	
	private RowMapper<UeTerritoryDropdown> ueTerritoryDropdownRowMapper = new RowMapper<UeTerritoryDropdown>() {
		
		@Override
		public UeTerritoryDropdown mapRow(ResultSet rs, int arg1) throws SQLException {
			UeTerritoryDropdown vo = new UeTerritoryDropdown();
			vo.seteTerritoryId(rs.getString("E_TERRITORY_ID"));
			vo.setTerritoryName(rs.getString("TERRITORY_NAME"));
			vo.setTerritoryNameUp(rs.getString("TERRITORY_NAME_UP"));
			vo.setParentTerritoryId(rs.getString("PARENT_TERRITORY_ID"));
			vo.setDescription(rs.getString("DESCRIPTION"));
			return vo;
		}
	};
	
	public boolean checkDupicateUser(String userName) {
		Long count = omudbJdbcTemplate.queryForObject(CHECK_AGEN_NAME , Long.class , userName);
		return  count > 0;
	}
	
}
