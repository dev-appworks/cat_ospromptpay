package com.cat.os.dao.omudb;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.OmOrders;


public interface OmOrdersRepository  extends CrudRepository<OmOrders, String>  {

	OmOrders findByOrderId(String orderId);
	 
}
