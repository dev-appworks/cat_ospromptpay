package com.cat.os.dao.omudb;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.UAgent;

public interface UAgentRepository extends CrudRepository<UAgent, String> {

	public Page<UAgent> findAll(Pageable pageable);
	
	public UAgent findByAgentIdAndObsoleteFlag(String agentId, int obsoleteFlag);
	
	public  long countByAgentIdAndObsoleteFlag(String agentId, int obsoleteFlag);

}
