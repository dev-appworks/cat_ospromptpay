package com.cat.os.dao.omudb;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.UETerritoryAgent;

public interface UETerritoryAgentRepository extends CrudRepository<UETerritoryAgent, String> {

	
	UETerritoryAgent findByAgentIdAndObsoleteFlag(String agentId , Integer obsoleteFlag);
}
