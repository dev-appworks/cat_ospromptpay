package com.cat.os.dao.omudb;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.ZbowCrossflowOrders;
import com.cat.os.dao.entity.ZbowCrossflowOrdersId;

public interface ZbowCrossflowOrdersRepository extends CrudRepository<ZbowCrossflowOrders, ZbowCrossflowOrdersId>  {


}
