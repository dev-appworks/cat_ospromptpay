package com.cat.os.dao.promptpay;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.dao.vo.PromptPayConsentVo;

@Repository
public class PromptPayConsentJdbcRepository {
	private static final Logger log = LoggerFactory.getLogger(PromptPayConsentJdbcRepository.class);
	
	@Autowired
	@Qualifier("crmOracleJdbcTemplate")
	private JdbcTemplate crmOracleJdbcTemplate;
	
	private final String queryPage =  "SELECT * FROM ( SELECT ROWNUM R, PP.SYSTEM_NAME, PP.MSISDN, PP.CID, "
										   + "PP.INPF01, PP.REFERENCE_CODE, PP.STATUS, PP.DESCRIPTION, "
										   + "PP.UPDATED_BY, PP.UPDATED_BYNAME, PP.UPDATED_DATE "
								           + "FROM PROMPTPAY_CONSENT PP WHERE 1=1 ";
	
	private final String queryCountPage =  "SELECT COUNT(1) FROM PROMPTPAY_CONSENT PP WHERE 1=1 ";
	
	public List<PromptPayConsentVo> getPromptpayConsent(PromptPayConsentVo consentVo) {
		List<Object> param = new ArrayList<Object>();
		int start = consentVo.getStart();
		int length = consentVo.getLength();
		
		StringBuilder sql = null;
		
		List<PromptPayConsentVo> list = new ArrayList<PromptPayConsentVo>();
		
		try {
			sql = whereCondition(queryPage, param, consentVo);
			
			sql = sql.append(") WHERE R >= ? AND R <= ? ORDER BY UPDATED_DATE DESC ");
			param.add(start + 1);
			int limitLength = start + length;
			param.add(limitLength);
			
			log.info("Page SQL : " + sql);
			
			list = crmOracleJdbcTemplate.query(sql.toString(), param.toArray() , promptpayConsentRowMapper);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Long getPromptpayConsentCount(PromptPayConsentVo consentVo) {
		List<Object> param = new ArrayList<Object>();
		
		StringBuilder sql = whereCondition(queryCountPage, param, consentVo);
		
		log.debug("SQL : " + sql);

		return crmOracleJdbcTemplate.queryForObject(sql.toString(), param.toArray() , Long.class);
	}
	
	protected StringBuilder whereCondition(String query, List<Object> param, PromptPayConsentVo consentVo) {
		StringBuilder sql = new StringBuilder(query);
		
		if(StringUtils.isAllBlank(consentVo.getDateFrom(), consentVo.getDateTo(), consentVo.getStatus(), consentVo.getMsisdn())) {
			String newDate = OsDateUtils.getCalendarDateEnDDMMYYYYFormat();
			
			sql = sql.append("AND PP.UPDATED_DATE >= TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') ");
			param.add(newDate.concat(" 00:00:00").trim());
			
			sql = sql.append("AND PP.UPDATED_DATE <= TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') ");
			param.add(newDate.concat(" 23:59:59").trim());
		}
		
		if (StringUtils.isNotBlank(consentVo.getDateFrom())) {
			sql = sql.append("AND PP.UPDATED_DATE >= TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') ");
			param.add(consentVo.getDateFrom().concat(" 00:00:00").trim());
		}
		
		if (StringUtils.isNotBlank(consentVo.getDateTo())) {

			sql = sql.append("AND PP.UPDATED_DATE <= TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') ");
			param.add(consentVo.getDateTo().concat(" 23:59:59").trim());
		}
		
		if(StringUtils.isNotBlank(consentVo.getMsisdn())) {
			sql = sql.append("AND PP.MSISDN = ? ");
			param.add(consentVo.getMsisdn().trim());
		}
		
		if(StringUtils.isNotBlank(consentVo.getStatus())) {
			sql = sql.append("AND PP.STATUS = ? ");
			param.add(consentVo.getStatus());
		}
		
		return sql;
	}
	
	private RowMapper<PromptPayConsentVo> promptpayConsentRowMapper = new RowMapper<PromptPayConsentVo>() {

		@Override
		public PromptPayConsentVo mapRow(ResultSet rs, int arg1) throws SQLException {
			
			PromptPayConsentVo vo = new PromptPayConsentVo();
//			vo.setId(rs.getInt("PROMPTPAY_CONSENT_ID"));
			vo.setSystemName(rs.getString("SYSTEM_NAME"));
			vo.setMsisdn(rs.getString("MSISDN"));
			vo.setCid(rs.getString("CID"));
			vo.setInpf01(rs.getString("INPF01"));
			vo.setReferenceCode(rs.getString("REFERENCE_CODE"));
			vo.setStatus(rs.getString("STATUS"));
			vo.setDescription(rs.getString("DESCRIPTION"));
			vo.setUpdatedBy(rs.getString("UPDATED_BY"));
			vo.setUpdatedByName(rs.getString("UPDATED_BYNAME"));
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(rs.getDate("UPDATED_DATE"));
			vo.setUpdatedDate(cal.getTime());

			return vo;
		}
	};
}
