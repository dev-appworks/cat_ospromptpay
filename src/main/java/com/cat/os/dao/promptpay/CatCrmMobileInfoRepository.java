package com.cat.os.dao.promptpay;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.CatCrmMobileInfo;

public interface CatCrmMobileInfoRepository extends CrudRepository<CatCrmMobileInfo, Integer> {

	List<CatCrmMobileInfo> findByMsisdnAndIdNumberAndStatus(String msisdn, String idNumber, String status);
}