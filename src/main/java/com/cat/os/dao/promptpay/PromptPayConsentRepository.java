package com.cat.os.dao.promptpay;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.PromptpayConsent;

public interface PromptPayConsentRepository extends CrudRepository<PromptpayConsent, Integer> {
	
//	Page<PromptpayConsent> findAllByIsDeleted(Pageable pageable, int isDeleted);
//	List<PromptpayConsent> findAllByIsDeleted(int isDeleted);
	
	@Query(value = "SELECT PROMPTPAY.PROMPTPAY_CONSENT_SEQ.NEXTVAL FROM DUAL", nativeQuery = true)
	Integer getNextId();
}