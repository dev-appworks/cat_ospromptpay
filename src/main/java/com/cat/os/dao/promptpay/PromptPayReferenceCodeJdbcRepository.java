package com.cat.os.dao.promptpay;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.cat.os.common.constant.PromptPayConstant;
import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.dao.vo.PromptPayReferenceCodeVo;

@Repository
public class PromptPayReferenceCodeJdbcRepository {
	private static final Logger log = LoggerFactory.getLogger(PromptPayReferenceCodeJdbcRepository.class);
	
	@Autowired
	@Qualifier("crmOracleJdbcTemplate")
	private JdbcTemplate crmOracleJdbcTemplate;
	
	private final String queryPage = "SELECT * FROM ( " ;
	private final String query = "SELECT ROWNUM R, PP.PROMPTPAY_REFERENCE_CODE_ID, PP.SHARED_KEY, "
                                                + "PP.START_DATE, PP.END_DATE, PP.STATUS, PP.IS_DELETED, "
                                                + "PP.CREATED_BY, PP.CREATED_BYNAME, PP.CREATED_DATE, "
                                                + "PP.UPDATED_BY, PP.UPDATED_BYNAME, PP.UPDATED_DATE "
                                                + "FROM PROMPTPAY_REFERENCE_CODE PP "
											    + "WHERE PP.IS_DELETED = 0 ";
	
	private final String queryCount = "SELECT COUNT(1) FROM PROMPTPAY_REFERENCE_CODE PP WHERE PP.IS_DELETED = 0 ";
	private final String queryLastEnd = "SELECT * FROM (SELECT * FROM PROMPTPAY_REFERENCE_CODE WHERE IS_DELETED = 0 "
													 + "ORDER BY END_DATE DESC) WHERE ROWNUM = 1";
	
	public List<PromptPayReferenceCodeVo> getPromptpayRefCodePage(PromptPayReferenceCodeVo refCodeVo) {
		List<Object> param = new ArrayList<Object>();
		int start = refCodeVo.getStart();
		int length = refCodeVo.getLength();

		StringBuilder sql = null;
		List<PromptPayReferenceCodeVo> list = new ArrayList<PromptPayReferenceCodeVo>();
		
		try {
			
			sql = whereCondition(queryPage + query, param, refCodeVo);

			sql = sql.append(") WHERE R >= ? AND R <= ? ORDER BY START_DATE DESC ");
			param.add(start + 1);
			int limitLength = start + length;
			param.add(limitLength);
			
			log.info("Page SQL : " + sql);
			
			list = crmOracleJdbcTemplate.query(sql.toString(), param.toArray() , promptpayReferenceCodeRowMapper);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List<PromptPayReferenceCodeVo> getPromptpayRefCode(PromptPayReferenceCodeVo refCodeVo) {
		List<Object> param = new ArrayList<Object>();

		StringBuilder sql = null;
		List<PromptPayReferenceCodeVo> list = new ArrayList<PromptPayReferenceCodeVo>();
		
		try {
			
			sql = whereCondition(query, param, refCodeVo);
			
			log.debug("SQL getPromptpayRefCode : " + sql);
			
			list = crmOracleJdbcTemplate.query(sql.toString(), param.toArray() , promptpayReferenceCodeRowMapper);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List<PromptPayReferenceCodeVo> getLastEnd() {
		List<Object> param = new ArrayList<Object>();

		StringBuilder sql = new StringBuilder(queryLastEnd);
		List<PromptPayReferenceCodeVo> list = new ArrayList<PromptPayReferenceCodeVo>();
		
		try {
			list = crmOracleJdbcTemplate.query(sql.toString(), param.toArray() , promptpayReferenceCodeRowMapper);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Long getPromptpayRefCodeCount(PromptPayReferenceCodeVo refCodeVo) {
		List<Object> param = new ArrayList<Object>();
		
		StringBuilder sql = whereCondition(queryCount, param, refCodeVo);

		log.debug("SQL : " + sql);

		return crmOracleJdbcTemplate.queryForObject(sql.toString(), param.toArray() , Long.class);
	}
	
	protected StringBuilder whereCondition(String query, List<Object> param, PromptPayReferenceCodeVo refCodeVo) {
		StringBuilder sql = new StringBuilder(query);
		
//		if(StringUtils.isAllBlank(refCodeVo.getDateFrom(), refCodeVo.getDateTo(), refCodeVo.getStatus())) {
//			String newDate = OsDateUtils.getCalendarDateEnDDMMYYYYFormat();
//			
//			sql = sql.append("AND TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') BETWEEN PP.START_DATE AND PP.END_DATE ");
//			param.add(newDate.concat(" 00:00:00").trim());
//			
//			sql = sql.append("AND TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') BETWEEN PP.START_DATE AND PP.END_DATE ");
//			param.add(newDate.concat(" 23:59:59").trim());
//		}
//		
//		if (StringUtils.isNotBlank(refCodeVo.getDateFrom()) && StringUtils.isNotBlank(refCodeVo.getDateTo())) {
//			sql = sql.append("AND (TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') BETWEEN PP.START_DATE AND PP.END_DATE ");
//			param.add(refCodeVo.getDateFrom().trim());
//			
//			sql = sql.append("OR TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') BETWEEN PP.START_DATE AND PP.END_DATE) ");
//			param.add(refCodeVo.getDateTo().trim());
//		} else {
//			if (StringUtils.isNotBlank(refCodeVo.getDateFrom())) {
//				sql = sql.append("AND TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') BETWEEN PP.START_DATE AND PP.END_DATE ");
//				param.add(refCodeVo.getDateFrom().trim());
//			}
//			
//			if (StringUtils.isNotBlank(refCodeVo.getDateTo())) {
//				sql = sql.append("AND TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss') BETWEEN PP.START_DATE AND PP.END_DATE ");
//				param.add(refCodeVo.getDateTo().trim());
//			}
//		}
		
		if(refCodeVo.getDateFrom().equals(OsDateUtils.getCalendarDateEnDDMMYYYYFormat()) && 
		   refCodeVo.getDateTo().equals(OsDateUtils.getCalendarDateEnDDMMYYYYFormat()) && 
		   StringUtils.isBlank(refCodeVo.getStatus())) {
			String newDate = OsDateUtils.getCalendarDateEnDDMMYYYYFormat();
			
			sql = sql.append("AND PP.STATUS = ? ");
			param.add(PromptPayConstant.STATUS.ACTIVE);
			
//			sql = sql.append("AND PP.START_DATE BETWEEN " + toDate() + "  AND " + toDate() + " ");
//			param.add(newDate.concat(" 00:00:00").trim());
//			param.add(newDate.concat(" 23:59:59").trim());
//			
//			sql = sql.append("AND PP.END_DATE BETWEEN " + toDate() + " AND " + toDate() + " ");
//			param.add(newDate.concat(" 00:00:00").trim());
//			param.add(newDate.concat(" 23:59:59").trim());
		} else if (StringUtils.isNotBlank(refCodeVo.getDateFrom()) && StringUtils.isNotBlank(refCodeVo.getDateTo())) {
			sql = sql.append("AND (PP.START_DATE BETWEEN " + toDate() + "  AND " + toDate() + " ");
			param.add(refCodeVo.getDateFrom().trim());
			param.add(refCodeVo.getDateTo().trim());
			
			sql = sql.append("OR PP.END_DATE BETWEEN " + toDate() + " AND " + toDate() + ") ");
			param.add(refCodeVo.getDateFrom().trim());
			param.add(refCodeVo.getDateTo().trim());
		} else {
			String newDate = OsDateUtils.getCalendarDateEnDDMMYYYYFormat();
			if (StringUtils.isNotBlank(refCodeVo.getDateFrom())) {
				sql = sql.append("AND PP.START_DATE BETWEEN " + toDate() + "  AND " + toDate() + " ");
				param.add(refCodeVo.getDateFrom().trim());
				param.add(newDate.concat(" 23:59:59").trim());
			}
			
			if (StringUtils.isNotBlank(refCodeVo.getDateTo())) {
				sql = sql.append("AND PP.END_DATE BETWEEN " + toDate() + " AND " + toDate() + " ");
				param.add(newDate.concat(" 00:00:00").trim());
				param.add(refCodeVo.getDateTo().trim());
			}
		}
		
		if(refCodeVo.getId() != null) {
			sql = sql.append("AND PP.PROMPTPAY_REFERENCE_CODE_ID = ? ");
			param.add(refCodeVo.getId());
		}
		
		if(StringUtils.isNotBlank(refCodeVo.getStatus())) {
			sql = sql.append("AND PP.STATUS = ? ");
			param.add(refCodeVo.getStatus());
		}
		
		return sql;
	}
	
	private RowMapper<PromptPayReferenceCodeVo> promptpayReferenceCodeRowMapper = new RowMapper<PromptPayReferenceCodeVo>() {

		@Override
		public PromptPayReferenceCodeVo mapRow(ResultSet rs, int arg1) throws SQLException {
			
			Calendar cal = Calendar.getInstance();
			
			PromptPayReferenceCodeVo vo = new PromptPayReferenceCodeVo();
			vo.setId(rs.getInt("PROMPTPAY_REFERENCE_CODE_ID"));
			vo.setSharedKey(rs.getString("SHARED_KEY"));
			
			cal.setTime(rs.getDate("START_DATE"));
			vo.setStartDate(cal.getTime());
			
			cal.setTime(rs.getDate("END_DATE"));
			vo.setEndDate(cal.getTime());
			
			vo.setStatus(rs.getString("STATUS"));
			vo.setIsDeleted(rs.getInt("IS_DELETED"));
			vo.setCreatedBy(rs.getString("CREATED_BY"));
			vo.setCreatedByName(rs.getString("CREATED_BYNAME"));
			
			cal.setTime(rs.getDate("CREATED_DATE"));
			vo.setCreatedDate(cal.getTime());
			
			vo.setUpdatedBy(rs.getString("UPDATED_BY"));
			vo.setUpdatedByName(rs.getString("UPDATED_BYNAME"));
			
			if(rs.getDate("UPDATED_DATE") != null) {
				cal.setTime(rs.getDate("UPDATED_DATE"));
				vo.setUpdatedDate(cal.getTime());
			}
			
			return vo;
		}
	};
	
	private String toDate() {
		return "TO_DATE ( ? , 'dd/mm/yyyy hh24:mi:ss')";
	}
}
