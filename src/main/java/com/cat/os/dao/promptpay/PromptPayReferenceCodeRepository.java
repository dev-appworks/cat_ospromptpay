package com.cat.os.dao.promptpay;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.PromptpayReferenceCode;

public interface PromptPayReferenceCodeRepository extends CrudRepository<PromptpayReferenceCode, Integer> {

	@Query(value = "SELECT PROMPTPAY.PROMPTPAY_REFERENCE_CODE_SEQ.NEXTVAL FROM DUAL", nativeQuery = true)
	Integer getNextId();
	
	@Query(value = "SELECT u FROM PromptpayReferenceCode u WHERE u.status IN ?1 AND u.isDeleted = ?2 AND "
				 + "SYSDATE BETWEEN u.startDate AND u.endDate")
	List<PromptpayReferenceCode> findByStatusAndIsDeleted(List<String> status, Integer isDeleted);
	
	@Query(value = "SELECT u FROM PromptpayReferenceCode u WHERE u.status IN ?1 AND u.isDeleted = ?2 "
				 + "AND SYSDATE BETWEEN u.startDate AND u.endDate")
	List<PromptpayReferenceCode> findRefCode(List<String> status, Integer isDeleted);
	
	@Query(value = "SELECT u FROM PromptpayReferenceCode u WHERE u.status IN ?1 AND u.isDeleted = ?2")
	List<PromptpayReferenceCode> findByStatus(List<String> status, Integer isDeleted);
	
	@Query(value = "SELECT u FROM PromptpayReferenceCode u WHERE (TO_DATE (?1, 'dd/mm/yyyy hh24:mi:ss') BETWEEN u.startDate AND u.endDate OR "
															  + "TO_DATE (?2, 'dd/mm/yyyy hh24:mi:ss') BETWEEN u.startDate AND u.endDate) AND "
															  + "u.isDeleted = ?3 ")
	List<PromptpayReferenceCode> findRefCode(String startDate, String endDate, Integer isDeleted);
	
	@Query(value = "SELECT u FROM PromptpayReferenceCode u WHERE u.endDate >= TO_DATE (?1, 'dd/mm/yyyy hh24:mi:ss') AND "
			  + "u.isDeleted = ?2 ")
	List<PromptpayReferenceCode> findDateOverlap(String startDate, Integer isDeleted);
	
	List<PromptpayReferenceCode> findByStatusAndIsDeleted(String status, Integer isDeleted);
}