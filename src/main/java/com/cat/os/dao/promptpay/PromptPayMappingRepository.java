package com.cat.os.dao.promptpay;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.PromptpayMapping;

public interface PromptPayMappingRepository extends CrudRepository<PromptpayMapping, Integer> {
	
	@Query(value = "SELECT u.value FROM PromptpayMapping u WHERE u.name = ?1 AND u.status = ?2 AND u.isDeleted = ?3")
	String findValue(String name, String status, Integer isDeleted);
	
	@Query(value = "SELECT u FROM PromptpayMapping u WHERE u.status = ?1 AND u.isDeleted = ?2")
	List<PromptpayMapping> findValue(String status, Integer isDeleted);
	
}