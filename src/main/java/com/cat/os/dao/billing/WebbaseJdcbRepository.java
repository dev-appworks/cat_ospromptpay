package com.cat.os.dao.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cat.os.common.constant.ProjectConstant.Profiles;

@Repository
@Profile(Profiles.BILLING)
public class WebbaseJdcbRepository {

	@Autowired
	@Qualifier("billingOracleJdbcTemplate")
	private JdbcTemplate billingOracleJdbcTemplate;
	
	public void testDb() {
		billingOracleJdbcTemplate.queryForObject("select count(*) from dual", Long.class);
	}
	
}
