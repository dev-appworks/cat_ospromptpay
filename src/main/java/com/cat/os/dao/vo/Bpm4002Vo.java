package com.cat.os.dao.vo;
 
import com.cat.os.common.bean.DataTableRequest;

public class Bpm4002Vo extends DataTableRequest{
	
	private String externalId;
	private String miuError;
	private String nRow;
	private String sMin;
	private String sMax;
	private String displayValue;
	private String activeDate;
	private String inactiveDate;
	private String changeDate;
	private String usageStatus;
		
	
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getMiuError() {
		return miuError;
	}
	public void setMiuError(String miuError) {
		this.miuError = miuError;
	}
	public String getnRow() {
		return nRow;
	}
	public void setnRow(String nRow) {
		this.nRow = nRow;
	}
	public String getsMin() {
		return sMin;
	}
	public void setsMin(String sMin) {
		this.sMin = sMin;
	}
	public String getsMax() {
		return sMax;
	}
	public void setsMax(String sMax) {
		this.sMax = sMax;
	}
	public String getDisplayValue() {
		return displayValue;
	}
	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}
	public String getActiveDate() {
		return activeDate;
	}
	public void setActiveDate(String activeDate) {
		this.activeDate = activeDate;
	}
	public String getInactiveDate() {
		return inactiveDate;
	}
	public void setInactiveDate(String inactiveDate) {
		this.inactiveDate = inactiveDate;
	}
	public String getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}
	public String getUsageStatus() {
		return usageStatus;
	}
	public void setUsageStatus(String usageStatus) {
		this.usageStatus = usageStatus;
	}

}
