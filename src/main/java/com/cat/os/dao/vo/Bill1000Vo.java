package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bill1000Vo extends DataTableRequest{

	private String accountNo;
	private String externalId;
	private String lastBalance;
	private String thisBalance;
	private String billAmt;
	private String migAmt;
	private String payBill;
	private String adjAmt;
	private String revAdjAmt;
	private String revAmt;
	private String wrtAmt;
	private String calc;
	private String diff;
	private String postError;
	private String lastRegion;
	private String thisRegion;
	private String hold2unhold;
	private String reason;
	private String action;
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getLastBalance() {
		return lastBalance;
	}
	public void setLastBalance(String lastBalance) {
		this.lastBalance = lastBalance;
	}
	public String getThisBalance() {
		return thisBalance;
	}
	public void setThisBalance(String thisBalance) {
		this.thisBalance = thisBalance;
	}
	public String getBillAmt() {
		return billAmt;
	}
	public void setBillAmt(String billAmt) {
		this.billAmt = billAmt;
	}
	public String getMigAmt() {
		return migAmt;
	}
	public void setMigAmt(String migAmt) {
		this.migAmt = migAmt;
	}
	public String getPayBill() {
		return payBill;
	}
	public void setPayBill(String payBill) {
		this.payBill = payBill;
	}
	public String getAdjAmt() {
		return adjAmt;
	}
	public void setAdjAmt(String adjAmt) {
		this.adjAmt = adjAmt;
	}
	public String getRevAdjAmt() {
		return revAdjAmt;
	}
	public void setRevAdjAmt(String revAdjAmt) {
		this.revAdjAmt = revAdjAmt;
	}
	public String getRevAmt() {
		return revAmt;
	}
	public void setRevAmt(String revAmt) {
		this.revAmt = revAmt;
	}
	public String getWrtAmt() {
		return wrtAmt;
	}
	public void setWrtAmt(String wrtAmt) {
		this.wrtAmt = wrtAmt;
	}
	public String getCalc() {
		return calc;
	}
	public void setCalc(String calc) {
		this.calc = calc;
	}
	public String getDiff() {
		return diff;
	}
	public void setDiff(String diff) {
		this.diff = diff;
	}
	public String getPostError() {
		return postError;
	}
	public void setPostError(String postError) {
		this.postError = postError;
	}
	public String getLastRegion() {
		return lastRegion;
	}
	public void setLastRegion(String lastRegion) {
		this.lastRegion = lastRegion;
	}
	public String getThisRegion() {
		return thisRegion;
	}
	public void setThisRegion(String thisRegion) {
		this.thisRegion = thisRegion;
	}
	public String getHold2unhold() {
		return hold2unhold;
	}
	public void setHold2unhold(String hold2unhold) {
		this.hold2unhold = hold2unhold;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
}
