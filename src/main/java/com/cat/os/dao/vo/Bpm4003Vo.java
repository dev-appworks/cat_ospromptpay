package com.cat.os.dao.vo;
 
import com.cat.os.common.bean.DataTableRequest;

public class Bpm4003Vo extends DataTableRequest{
	
	private String transactionId;
	private String processId;
	private String processType;
	private String processName;
	private String createdBy;
	private String status;
	private String createdDate;
	private String logReport;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLogReport() {
		return logReport;
	}
	public void setLogReport(String logReport) {
		this.logReport = logReport;
	}
	
	
	
}
