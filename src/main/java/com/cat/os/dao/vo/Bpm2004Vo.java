package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm2004Vo extends DataTableRequest{
	
	private String billRefNo;
	private String accountNo;
	private String ba;
	private String noBill;
	private String pointOrigin;
	private String monthly;
	private String record;
	private String charge;
	private String secs;
	private String minTransDate;
	private String maxTransDate;
	
	public String getBillRefNo() {
		return billRefNo;
	}
	public void setBillRefNo(String billRefNo) {
		this.billRefNo = billRefNo;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getNoBill() {
		return noBill;
	}
	public void setNoBill(String noBill) {
		this.noBill = noBill;
	}
	public String getPointOrigin() {
		return pointOrigin;
	}
	public void setPointOrigin(String pointOrigin) {
		this.pointOrigin = pointOrigin;
	}
	public String getMonthly() {
		return monthly;
	}
	public void setMonthly(String monthly) {
		this.monthly = monthly;
	}
	public String getRecord() {
		return record;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public String getSecs() {
		return secs;
	}
	public void setSecs(String secs) {
		this.secs = secs;
	}
	public String getMinTransDate() {
		return minTransDate;
	}
	public void setMinTransDate(String minTransDate) {
		this.minTransDate = minTransDate;
	}
	public String getMaxTransDate() {
		return maxTransDate;
	}
	public void setMaxTransDate(String maxTransDate) {
		this.maxTransDate = maxTransDate;
	}
	
	
}
