package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class USSDHistoryLogVo extends DataTableRequest{
	
	private String serviceId;
	private String dateFrom;
	private String dateTo;
	private String logBy;
	private String status;
	
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getLogBy() {
		return logBy;
	}
	public void setLogBy(String logBy) {
		this.logBy = logBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
