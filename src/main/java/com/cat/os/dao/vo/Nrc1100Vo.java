package com.cat.os.dao.vo;
 
import com.cat.os.common.bean.DataTableRequest;

public class Nrc1100Vo extends DataTableRequest{
	
	private String no;
	private String ba;
	private String externalId;
	private String domponentId;
	private String domponentName;
	private String domponentActiveDt;
	private String domponentInactiveDt;
	private String packagetId;
	private String packagetName;
	private String packagetActiveDt;
	private String packagetInactiveDt;
	private String packagetPkgInstDt;
	private String trueActiveDt;
	private String trueInactiveDt;
	private String del;
	private String comment;
	
	
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getDomponentId() {
		return domponentId;
	}
	public void setDomponentId(String domponentId) {
		this.domponentId = domponentId;
	}
	public String getDomponentName() {
		return domponentName;
	}
	public void setDomponentName(String domponentName) {
		this.domponentName = domponentName;
	}
	public String getDomponentActiveDt() {
		return domponentActiveDt;
	}
	public void setDomponentActiveDt(String domponentActiveDt) {
		this.domponentActiveDt = domponentActiveDt;
	}
	public String getDomponentInactiveDt() {
		return domponentInactiveDt;
	}
	public void setDomponentInactiveDt(String domponentInactiveDt) {
		this.domponentInactiveDt = domponentInactiveDt;
	}
	public String getPackagetId() {
		return packagetId;
	}
	public void setPackagetId(String packagetId) {
		this.packagetId = packagetId;
	}
	public String getPackagetName() {
		return packagetName;
	}
	public void setPackagetName(String packagetName) {
		this.packagetName = packagetName;
	}
	public String getPackagetActiveDt() {
		return packagetActiveDt;
	}
	public void setPackagetActiveDt(String packagetActiveDt) {
		this.packagetActiveDt = packagetActiveDt;
	}
	public String getPackagetInactiveDt() {
		return packagetInactiveDt;
	}
	public void setPackagetInactiveDt(String packagetInactiveDt) {
		this.packagetInactiveDt = packagetInactiveDt;
	}
	public String getPackagetPkgInstDt() {
		return packagetPkgInstDt;
	}
	public void setPackagetPkgInstDt(String packagetPkgInstDt) {
		this.packagetPkgInstDt = packagetPkgInstDt;
	}
	public String getTrueActiveDt() {
		return trueActiveDt;
	}
	public void setTrueActiveDt(String trueActiveDt) {
		this.trueActiveDt = trueActiveDt;
	}
	public String getTrueInactiveDt() {
		return trueInactiveDt;
	}
	public void setTrueInactiveDt(String trueInactiveDt) {
		this.trueInactiveDt = trueInactiveDt;
	}
	public String getDel() {
		return del;
	}
	public void setDel(String del) {
		this.del = del;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	
	
}
