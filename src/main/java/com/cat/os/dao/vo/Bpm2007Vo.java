package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm2007Vo extends DataTableRequest{
	
	private String ba;
	private String idType;
	private String accountNo;
	private String noBill;
	private String pointOrigin;
	private String record;
	private String charge;
	private String secs;
	private String minTransDate;
	private String maxTransDate;
	private String bperiod;
	private String remark;
	private String billType;
	
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getNoBill() {
		return noBill;
	}
	public void setNoBill(String noBill) {
		this.noBill = noBill;
	}
	public String getPointOrigin() {
		return pointOrigin;
	}
	public void setPointOrigin(String pointOrigin) {
		this.pointOrigin = pointOrigin;
	}
	public String getRecord() {
		return record;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public String getSecs() {
		return secs;
	}
	public void setSecs(String secs) {
		this.secs = secs;
	}
	public String getMinTransDate() {
		return minTransDate;
	}
	public void setMinTransDate(String minTransDate) {
		this.minTransDate = minTransDate;
	}
	public String getMaxTransDate() {
		return maxTransDate;
	}
	public void setMaxTransDate(String maxTransDate) {
		this.maxTransDate = maxTransDate;
	}
	public String getBperiod() {
		return bperiod;
	}
	public void setBperiod(String bperiod) {
		this.bperiod = bperiod;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getBillType() {
		return billType;
	}
	public void setBillType(String billType) {
		this.billType = billType;
	}
	
	
	

}
