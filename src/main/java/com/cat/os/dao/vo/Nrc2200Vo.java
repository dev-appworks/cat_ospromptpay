package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Nrc2200Vo extends DataTableRequest{

	
	private String no;
	private String ba;
	private String externalId;
	
	private String componentId;
	private String componentName;

	private String componentOverrideName;
	private String componentOverrideRate;
	private String componentOverrideUnit;
	
	private String packageId;
	private String packageName;
	private String packagePkgInstDt;
	
	private String effectiveDateActiceDt;
	private String effectiveDateInactiveDt;
	
	private String runDate;
	private String status;
	private String rerunNo;
    private String compinstid;
    
    
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getComponentOverrideName() {
		return componentOverrideName;
	}
	public void setComponentOverrideName(String componentOverrideName) {
		this.componentOverrideName = componentOverrideName;
	}
	public String getComponentOverrideRate() {
		return componentOverrideRate;
	}
	public void setComponentOverrideRate(String componentOverrideRate) {
		this.componentOverrideRate = componentOverrideRate;
	}
	public String getComponentOverrideUnit() {
		return componentOverrideUnit;
	}
	public void setComponentOverrideUnit(String componentOverrideUnit) {
		this.componentOverrideUnit = componentOverrideUnit;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackagePkgInstDt() {
		return packagePkgInstDt;
	}
	public void setPackagePkgInstDt(String packagePkgInstDt) {
		this.packagePkgInstDt = packagePkgInstDt;
	}
	public String getEffectiveDateActiceDt() {
		return effectiveDateActiceDt;
	}
	public void setEffectiveDateActiceDt(String effectiveDateActiceDt) {
		this.effectiveDateActiceDt = effectiveDateActiceDt;
	}
	public String getEffectiveDateInactiveDt() {
		return effectiveDateInactiveDt;
	}
	public void setEffectiveDateInactiveDt(String effectiveDateInactiveDt) {
		this.effectiveDateInactiveDt = effectiveDateInactiveDt;
	}
	public String getRunDate() {
		return runDate;
	}
	public void setRunDate(String runDate) {
		this.runDate = runDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRerunNo() {
		return rerunNo;
	}
	public void setRerunNo(String rerunNo) {
		this.rerunNo = rerunNo;
	}
	public String getCompinstid() {
		return compinstid;
	}
	public void setCompinstid(String compinstid) {
		this.compinstid = compinstid;
	}
    
    
	
}
