package com.cat.os.dao.vo;

import java.util.Date;

import com.cat.os.common.bean.DataTableRequest;

public class Bill3000Vo extends DataTableRequest{

	private String createdDate;
	private String createdBy;
	private String result;
	private String action;
	
	
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
}
