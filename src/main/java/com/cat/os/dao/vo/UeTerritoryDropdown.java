package com.cat.os.dao.vo;

public class UeTerritoryDropdown {
	
	private String eTerritoryId;
	private String territoryName;
	private String territoryNameUp;
	private String parentTerritoryId;
	private String description;

	public String geteTerritoryId() {
		return eTerritoryId;
	}

	public void seteTerritoryId(String eTerritoryId) {
		this.eTerritoryId = eTerritoryId;
	}

	public String getTerritoryName() {
		return territoryName;
	}

	public void setTerritoryName(String territoryName) {
		this.territoryName = territoryName;
	}

	public String getTerritoryNameUp() {
		return territoryNameUp;
	}

	public void setTerritoryNameUp(String territoryNameUp) {
		this.territoryNameUp = territoryNameUp;
	}

	public String getParentTerritoryId() {
		return parentTerritoryId;
	}

	public void setParentTerritoryId(String parentTerritoryId) {
		this.parentTerritoryId = parentTerritoryId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
