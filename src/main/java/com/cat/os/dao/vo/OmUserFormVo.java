package com.cat.os.dao.vo;

import java.util.ArrayList;
import java.util.List;

import com.cat.os.common.bean.DataTableSortRequest;

public class OmUserFormVo extends DataTableSortRequest {
	
	private String firstName;
	private String lastName;
	private String empId;
	private String userName;
	private String agentId;
	private Boolean isNew;
	private String password;
	private String territoryAgent;
	private String actionType;

	private List<String> groupTempInLdap = new ArrayList<>();
	private List<String> groupAdd = new ArrayList<>();
	private List<String> groupDel = new ArrayList<>();
	private String currentAddGroup;
	private String currentDelGroup;
	private String uidNumber;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTerritoryAgent() {
		return territoryAgent;
	}

	public void setTerritoryAgent(String territoryAgent) {
		this.territoryAgent = territoryAgent;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public List<String> getGroupAdd() {
		return groupAdd;
	}

	public void setGroupAdd(List<String> groupAdd) {
		this.groupAdd = groupAdd;
	}

	public List<String> getGroupDel() {
		return groupDel;
	}

	public void setGroupDel(List<String> groupDel) {
		this.groupDel = groupDel;
	}

	public String getCurrentAddGroup() {
		return currentAddGroup;
	}

	public void setCurrentAddGroup(String currentAddGroup) {
		this.currentAddGroup = currentAddGroup;
	}

	public String getCurrentDelGroup() {
		return currentDelGroup;
	}

	public void setCurrentDelGroup(String currentDelGroup) {
		this.currentDelGroup = currentDelGroup;
	}

	public List<String> getGroupTempInLdap() {
		return groupTempInLdap;
	}

	public void setGroupTempInLdap(List<String> groupTempInLdap) {
		this.groupTempInLdap = groupTempInLdap;
	}

	public String getUidNumber() {
		return uidNumber;
	}

	public void setUidNumber(String uidNumber) {
		this.uidNumber = uidNumber;
	}

}
