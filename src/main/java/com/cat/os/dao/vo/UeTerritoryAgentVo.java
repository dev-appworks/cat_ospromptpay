package com.cat.os.dao.vo;

public class UeTerritoryAgentVo {
	
	private String agentId;
	private String agentRoleLkp;
	private String eTerritoryAgentId;
	private String eTerritoryId;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentRoleLkp() {
		return agentRoleLkp;
	}

	public void setAgentRoleLkp(String agentRoleLkp) {
		this.agentRoleLkp = agentRoleLkp;
	}

	public String geteTerritoryAgentId() {
		return eTerritoryAgentId;
	}

	public void seteTerritoryAgentId(String eTerritoryAgentId) {
		this.eTerritoryAgentId = eTerritoryAgentId;
	}

	public String geteTerritoryId() {
		return eTerritoryId;
	}

	public void seteTerritoryId(String eTerritoryId) {
		this.eTerritoryId = eTerritoryId;
	}

}
