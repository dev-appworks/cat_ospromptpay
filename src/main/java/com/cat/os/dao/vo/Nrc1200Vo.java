package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Nrc1200Vo extends DataTableRequest{
	
	private String no;
	private String ba;
	private String externalId;
	private String componentId;
	private String componentName;
	private String packageId;
	private String packageName;
	private String packagePkgInstId;
	private String trueEffectiveDateActiceDt;
	private String trueEffectiveDateInactiveDt;
	private String runDate;
	private String status;
	private String rerunNo;
	private String compInstId;
	
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackagePkgInstId() {
		return packagePkgInstId;
	}
	public void setPackagePkgInstId(String packagePkgInstId) {
		this.packagePkgInstId = packagePkgInstId;
	}
	public String getTrueEffectiveDateActiceDt() {
		return trueEffectiveDateActiceDt;
	}
	public void setTrueEffectiveDateActiceDt(String trueEffectiveDateActiceDt) {
		this.trueEffectiveDateActiceDt = trueEffectiveDateActiceDt;
	}
	public String getTrueEffectiveDateInactiveDt() {
		return trueEffectiveDateInactiveDt;
	}
	public void setTrueEffectiveDateInactiveDt(String trueEffectiveDateInactiveDt) {
		this.trueEffectiveDateInactiveDt = trueEffectiveDateInactiveDt;
	}
	public String getRunDate() {
		return runDate;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRunDate(String runDate) {
		this.runDate = runDate;
	}
	public String getRerunNo() {
		return rerunNo;
	}
	public void setRerunNo(String rerunNo) {
		this.rerunNo = rerunNo;
	}
	public String getCompInstId() {
		return compInstId;
	}
	public void setCompInstId(String compInstId) {
		this.compInstId = compInstId;
	}
	
	
	
}
