package com.cat.os.dao.vo;

public class ResponseDataTaskDetail {

	private String piid;
	private String tkiid;
	public String getPiid() {
		return piid;
	}
	public void setPiid(String piid) {
		this.piid = piid;
	}
	public String getTkiid() {
		return tkiid;
	}
	public void setTkiid(String tkiid) {
		this.tkiid = tkiid;
	}
	
}
