package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm2000Vo extends DataTableRequest{
	
	private String description;
	private String createdBy;
	private String createDate;
	private String state;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	

}
