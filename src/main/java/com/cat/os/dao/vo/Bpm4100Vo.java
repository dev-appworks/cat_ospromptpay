package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm4100Vo extends DataTableRequest{
	
	private String parentAcountNo;
	private String chargeDate;
	private String subscriberNo;
	private String serviceActiveDate;
	private String serviceInactiveDate;
	private String chargeStatus;
	
	
	public String getParentAcountNo() {
		return parentAcountNo;
	}
	public void setParentAcountNo(String parentAcountNo) {
		this.parentAcountNo = parentAcountNo;
	}
	public String getChargeDate() {
		return chargeDate;
	}
	public void setChargeDate(String chargeDate) {
		this.chargeDate = chargeDate;
	}
	public String getSubscriberNo() {
		return subscriberNo;
	}
	public void setSubscriberNo(String subscriberNo) {
		this.subscriberNo = subscriberNo;
	}
	public String getServiceActiveDate() {
		return serviceActiveDate;
	}
	public void setServiceActiveDate(String serviceActiveDate) {
		this.serviceActiveDate = serviceActiveDate;
	}
	public String getServiceInactiveDate() {
		return serviceInactiveDate;
	}
	public void setServiceInactiveDate(String serviceInactiveDate) {
		this.serviceInactiveDate = serviceInactiveDate;
	}
	public String getChargeStatus() {
		return chargeStatus;
	}
	public void setChargeStatus(String chargeStatus) {
		this.chargeStatus = chargeStatus;
	}
	
	

}
