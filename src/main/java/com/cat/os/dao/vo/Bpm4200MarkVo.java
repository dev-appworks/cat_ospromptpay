package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm4200MarkVo extends DataTableRequest{
	
	private String externalId;
	private String type;
	private String miuErrorCode;
	private String count;
	private String sum;
	private String minTransDate;
	private String maxTransDate;
	
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMiuErrorCode() {
		return miuErrorCode;
	}
	public void setMiuErrorCode(String miuErrorCode) {
		this.miuErrorCode = miuErrorCode;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}
	public String getMinTransDate() {
		return minTransDate;
	}
	public void setMinTransDate(String minTransDate) {
		this.minTransDate = minTransDate;
	}
	public String getMaxTransDate() {
		return maxTransDate;
	}
	public void setMaxTransDate(String maxTransDate) {
		this.maxTransDate = maxTransDate;
	}
	
	
}
