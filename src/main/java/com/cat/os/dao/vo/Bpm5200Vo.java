package com.cat.os.dao.vo;
 
import com.cat.os.common.bean.DataTableRequest;

public class Bpm5200Vo extends DataTableRequest{
	
	private String createDate;
	private String extTrackingId;
	private String sourceTb;
	private String fileId;
	private String numRecords;
	private String remark;
	private String attachedFile;
	
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getExtTrackingId() {
		return extTrackingId;
	}
	public void setExtTrackingId(String extTrackingId) {
		this.extTrackingId = extTrackingId;
	}
	public String getSourceTb() {
		return sourceTb;
	}
	public void setSourceTb(String sourceTb) {
		this.sourceTb = sourceTb;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getNumRecords() {
		return numRecords;
	}
	public void setNumRecords(String numRecords) {
		this.numRecords = numRecords;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getAttachedFile() {
		return attachedFile;
	}
	public void setAttachedFile(String attachedFile) {
		this.attachedFile = attachedFile;
	}
	

	
}
