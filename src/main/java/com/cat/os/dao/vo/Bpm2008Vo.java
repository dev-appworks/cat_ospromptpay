package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm2008Vo extends DataTableRequest{
	
	private String cutOffDate;
	private String ppddDate;
	private String nextPpddDate;
	
	public String getCutOffDate() {
		return cutOffDate;
	}
	public void setCutOffDate(String cutOffDate) {
		this.cutOffDate = cutOffDate;
	}
	public String getPpddDate() {
		return ppddDate;
	}
	public void setPpddDate(String ppddDate) {
		this.ppddDate = ppddDate;
	}
	public String getNextPpddDate() {
		return nextPpddDate;
	}
	public void setNextPpddDate(String nextPpddDate) {
		this.nextPpddDate = nextPpddDate;
	}

	
}
