package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bill4000Vo extends DataTableRequest{
	
	private String ba;
	private String invoiceNo;
	private String amtOriginal;
	private String amtProduct;
	private String diff;
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getAmtOriginal() {
		return amtOriginal;
	}
	public void setAmtOriginal(String amtOriginal) {
		this.amtOriginal = amtOriginal;
	}
	public String getAmtProduct() {
		return amtProduct;
	}
	public void setAmtProduct(String amtProduct) {
		this.amtProduct = amtProduct;
	}
	public String getDiff() {
		return diff;
	}
	public void setDiff(String diff) {
		this.diff = diff;
	}
	
	
	
		
	

}
