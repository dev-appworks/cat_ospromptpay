package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Nrc2000Vo extends DataTableRequest{
	
	private String ba;
	private String externalId;
	private String externalIdType;
	private String componentId;
	private String componentActiveDt;
	private String componentInactiveDt;
	private String packageId;
	private String packageInstId;
	private String overrideName;
	private String overrideRate;
	private String overrideUnit;
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getExternalIdType() {
		return externalIdType;
	}
	public void setExternalIdType(String externalIdType) {
		this.externalIdType = externalIdType;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public String getComponentActiveDt() {
		return componentActiveDt;
	}
	public void setComponentActiveDt(String componentActiveDt) {
		this.componentActiveDt = componentActiveDt;
	}
	public String getComponentInactiveDt() {
		return componentInactiveDt;
	}
	public void setComponentInactiveDt(String componentInactiveDt) {
		this.componentInactiveDt = componentInactiveDt;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getPackageInstId() {
		return packageInstId;
	}
	public void setPackageInstId(String packageInstId) {
		this.packageInstId = packageInstId;
	}
	public String getOverrideName() {
		return overrideName;
	}
	public void setOverrideName(String overrideName) {
		this.overrideName = overrideName;
	}
	public String getOverrideRate() {
		return overrideRate;
	}
	public void setOverrideRate(String overrideRate) {
		this.overrideRate = overrideRate;
	}
	public String getOverrideUnit() {
		return overrideUnit;
	}
	public void setOverrideUnit(String overrideUnit) {
		this.overrideUnit = overrideUnit;
	}
	
	

}
