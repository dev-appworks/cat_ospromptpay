package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm1000Vo extends DataTableRequest{
	
	private String transectoinId;
	private String procressId;
	private String processType;
	private String cretedBy;
	private String state;
	private String createdDate;
	private String logReport;
	
	
	public String getTransectoinId() {
		return transectoinId;
	}
	public void setTransectoinId(String transectoinId) {
		this.transectoinId = transectoinId;
	}
	public String getProcressId() {
		return procressId;
	}
	public void setProcressId(String procressId) {
		this.procressId = procressId;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public String getCretedBy() {
		return cretedBy;
	}
	public void setCretedBy(String cretedBy) {
		this.cretedBy = cretedBy;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLogReport() {
		return logReport;
	}
	public void setLogReport(String logReport) {
		this.logReport = logReport;
	}
	

}
