package com.cat.os.dao.vo;

import java.io.Serializable;

public class ArmVo implements Serializable {

	private static final long serialVersionUID = -7576357182619814963L;

	private Integer messageId;

	private String messageCode;

	public ArmVo(Integer messageId, String messageCode) {
		this.messageId = messageId;
		this.messageCode = messageCode;
	}

	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

}
