package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Nrc1300Vo extends DataTableRequest{
	
	private String ba;
	private String mdn;
	private String packageId;
	private String packageName;
	private String packageActiveDt;
	private String packageInactiveDt;
	private String packagePkgInstId;
	private String componentId;
	private String componentName;
	private String componentActiceDt;
	private String componentInactiveDt;
	private String componentCompInstId;
	
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getMdn() {
		return mdn;
	}
	public void setMdn(String mdn) {
		this.mdn = mdn;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageActiveDt() {
		return packageActiveDt;
	}
	public void setPackageActiveDt(String packageActiveDt) {
		this.packageActiveDt = packageActiveDt;
	}
	public String getPackageInactiveDt() {
		return packageInactiveDt;
	}
	public void setPackageInactiveDt(String packageInactiveDt) {
		this.packageInactiveDt = packageInactiveDt;
	}
	public String getPackagePkgInstId() {
		return packagePkgInstId;
	}
	public void setPackagePkgInstId(String packagePkgInstId) {
		this.packagePkgInstId = packagePkgInstId;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getComponentActiceDt() {
		return componentActiceDt;
	}
	public void setComponentActiceDt(String componentActiceDt) {
		this.componentActiceDt = componentActiceDt;
	}
	public String getComponentInactiveDt() {
		return componentInactiveDt;
	}
	public void setComponentInactiveDt(String componentInactiveDt) {
		this.componentInactiveDt = componentInactiveDt;
	}
	public String getComponentCompInstId() {
		return componentCompInstId;
	}
	public void setComponentCompInstId(String componentCompInstId) {
		this.componentCompInstId = componentCompInstId;
	}

	
}
