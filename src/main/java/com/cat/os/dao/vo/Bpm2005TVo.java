package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm2005TVo extends DataTableRequest{
	
	private String phoneNo;
	private String defaultBa;
	private String group;
	private String bill;
	private String invoiceNo;
	private String number;
	private String priceVat;
	private String newBa;
	private String newgroup;
	private String createdDate;
	
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getDefaultBa() {
		return defaultBa;
	}
	public void setDefaultBa(String defaultBa) {
		this.defaultBa = defaultBa;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getBill() {
		return bill;
	}
	public void setBill(String bill) {
		this.bill = bill;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPriceVat() {
		return priceVat;
	}
	public void setPriceVat(String priceVat) {
		this.priceVat = priceVat;
	}
	public String getNewBa() {
		return newBa;
	}
	public void setNewBa(String newBa) {
		this.newBa = newBa;
	}
	public String getNewgroup() {
		return newgroup;
	}
	public void setNewgroup(String newgroup) {
		this.newgroup = newgroup;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	

}
