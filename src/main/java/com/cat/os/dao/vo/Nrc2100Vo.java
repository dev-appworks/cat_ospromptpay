package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Nrc2100Vo extends DataTableRequest{
	
	private String no;
	private String ba;
	private String externalId;
	private String componentId;
	private String componentName;
	private String componentActiveDt;
	private String componentInactiveDt;
	private String componentOverrideName;
	private String componentOverrideRate;
	private String componentOverrideUnit;
	private String packageId;
	private String packageName;
	private String packageActiveDt;
	private String packageInactiveDt;
	private String packagePkgInstDt;
	private String trueEffectDateActiceDt;
	private String trueEffectDateInactiveDt;
	private String del;
	private String comment;
	
	
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getComponentActiveDt() {
		return componentActiveDt;
	}
	public void setComponentActiveDt(String componentActiveDt) {
		this.componentActiveDt = componentActiveDt;
	}
	public String getComponentInactiveDt() {
		return componentInactiveDt;
	}
	public void setComponentInactiveDt(String componentInactiveDt) {
		this.componentInactiveDt = componentInactiveDt;
	}
	public String getComponentOverrideName() {
		return componentOverrideName;
	}
	public void setComponentOverrideName(String componentOverrideName) {
		this.componentOverrideName = componentOverrideName;
	}
	public String getComponentOverrideRate() {
		return componentOverrideRate;
	}
	public void setComponentOverrideRate(String componentOverrideRate) {
		this.componentOverrideRate = componentOverrideRate;
	}
	public String getComponentOverrideUnit() {
		return componentOverrideUnit;
	}
	public void setComponentOverrideUnit(String componentOverrideUnit) {
		this.componentOverrideUnit = componentOverrideUnit;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageActiveDt() {
		return packageActiveDt;
	}
	public void setPackageActiveDt(String packageActiveDt) {
		this.packageActiveDt = packageActiveDt;
	}
	public String getPackageInactiveDt() {
		return packageInactiveDt;
	}
	public void setPackageInactiveDt(String packageInactiveDt) {
		this.packageInactiveDt = packageInactiveDt;
	}
	public String getPackagePkgInstDt() {
		return packagePkgInstDt;
	}
	public void setPackagePkgInstDt(String packagePkgInstDt) {
		this.packagePkgInstDt = packagePkgInstDt;
	}
	public String getTrueEffectDateActiceDt() {
		return trueEffectDateActiceDt;
	}
	public void setTrueEffectDateActiceDt(String trueEffectDateActiceDt) {
		this.trueEffectDateActiceDt = trueEffectDateActiceDt;
	}
	public String getTrueEffectDateInactiveDt() {
		return trueEffectDateInactiveDt;
	}
	public void setTrueEffectDateInactiveDt(String trueEffectDateInactiveDt) {
		this.trueEffectDateInactiveDt = trueEffectDateInactiveDt;
	}
	public String getDel() {
		return del;
	}
	public void setDel(String del) {
		this.del = del;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	
	

}
