package com.cat.os.dao.vo;

public class ResponseDataInstance {

	private String executionState;
	private String state;
	public String getExecutionState() {
		return executionState;
	}
	public void setExecutionState(String executionState) {
		this.executionState = executionState;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}
