package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm3300RapVo extends DataTableRequest{
	
	private String billingAccount;
	private String guilding;
	private String type;
	private String accountNo;
	private String numberOfBill;
	private String pointOrigin;
	private String monthly;
	private String records;
	private String charge;
	private String secs;
	private String minTransDate;
	private String maxTransDate;
	public String getBillingAccount() {
		return billingAccount;
	}
	public void setBillingAccount(String billingAccount) {
		this.billingAccount = billingAccount;
	}
	public String getGuilding() {
		return guilding;
	}
	public void setGuilding(String guilding) {
		this.guilding = guilding;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getNumberOfBill() {
		return numberOfBill;
	}
	public void setNumberOfBill(String numberOfBill) {
		this.numberOfBill = numberOfBill;
	}
	public String getPointOrigin() {
		return pointOrigin;
	}
	public void setPointOrigin(String pointOrigin) {
		this.pointOrigin = pointOrigin;
	}
	public String getMonthly() {
		return monthly;
	}
	public void setMonthly(String monthly) {
		this.monthly = monthly;
	}
	public String getRecords() {
		return records;
	}
	public void setRecords(String records) {
		this.records = records;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public String getSecs() {
		return secs;
	}
	public void setSecs(String secs) {
		this.secs = secs;
	}
	public String getMinTransDate() {
		return minTransDate;
	}
	public void setMinTransDate(String minTransDate) {
		this.minTransDate = minTransDate;
	}
	public String getMaxTransDate() {
		return maxTransDate;
	}
	public void setMaxTransDate(String maxTransDate) {
		this.maxTransDate = maxTransDate;
	}
	
	

}
