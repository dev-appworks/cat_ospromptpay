package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm2006Vo extends DataTableRequest{
	
	private String externalId;
	private String externalIdType;
	private String accountNo;
	private String noBill;
	private String pointOrigin;
	private String monthly;
	private String record;
	private String charge;
	private String secs;
	private String minTransDate;
	private String maxTransDate;
	
	
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getExternalIdType() {
		return externalIdType;
	}
	public void setExternalIdType(String externalIdType) {
		this.externalIdType = externalIdType;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getNoBill() {
		return noBill;
	}
	public void setNoBill(String noBill) {
		this.noBill = noBill;
	}
	public String getPointOrigin() {
		return pointOrigin;
	}
	public void setPointOrigin(String pointOrigin) {
		this.pointOrigin = pointOrigin;
	}
	public String getMonthly() {
		return monthly;
	}
	public void setMonthly(String monthly) {
		this.monthly = monthly;
	}
	public String getRecord() {
		return record;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public String getSecs() {
		return secs;
	}
	public void setSecs(String secs) {
		this.secs = secs;
	}
	public String getMinTransDate() {
		return minTransDate;
	}
	public void setMinTransDate(String minTransDate) {
		this.minTransDate = minTransDate;
	}
	public String getMaxTransDate() {
		return maxTransDate;
	}
	public void setMaxTransDate(String maxTransDate) {
		this.maxTransDate = maxTransDate;
	}
	
	

}
