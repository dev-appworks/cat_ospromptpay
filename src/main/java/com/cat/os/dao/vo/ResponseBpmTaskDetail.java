package com.cat.os.dao.vo;

public class ResponseBpmTaskDetail {

	private ResponseDataTaskDetail data;

	public ResponseDataTaskDetail getData() {
		return data;
	}

	public void setData(ResponseDataTaskDetail data) {
		this.data = data;
	}
	
}
