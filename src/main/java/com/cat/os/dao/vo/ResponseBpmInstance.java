package com.cat.os.dao.vo;

public class ResponseBpmInstance {

	private ResponseDataInstance data;

	public ResponseDataInstance getData() {
		return data;
	}

	public void setData(ResponseDataInstance data) {
		this.data = data;
	}

}
