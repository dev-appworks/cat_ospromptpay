package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class OmudbHistoryFormVo extends DataTableRequest{
	
	private String orderId;
	private String orderSvcItem;
	private String dateFrom;
	private String dateTo;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderSvcItem() {
		return orderSvcItem;
	}

	public void setOrderSvcItem(String orderSvcItem) {
		this.orderSvcItem = orderSvcItem;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
}
