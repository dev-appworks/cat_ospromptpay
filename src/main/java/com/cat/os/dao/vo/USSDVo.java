package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableSortRequest;

public class USSDVo extends DataTableSortRequest{

	private Integer currentStatus;
	private Integer id;
	private String modBatchConfig;
	private String displayDate;
	private String displayHH;
	private String displayMM;
	private String serviceId;
//	private String email;
	private String serviceName;

	public Integer getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(Integer currentStatus) {
		this.currentStatus = currentStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModBatchConfig() {
		return modBatchConfig;
	}

	public void setModBatchConfig(String modBatchConfig) {
		this.modBatchConfig = modBatchConfig;
	}

	public String getDisplayDate() {
		return displayDate;
	}

	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}

	public String getDisplayHH() {
		return displayHH;
	}

	public void setDisplayHH(String displayHH) {
		this.displayHH = displayHH;
	}

	public String getDisplayMM() {
		return displayMM;
	}

	public void setDisplayMM(String displayMM) {
		this.displayMM = displayMM;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
