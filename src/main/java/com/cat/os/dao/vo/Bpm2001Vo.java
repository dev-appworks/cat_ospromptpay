package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm2001Vo extends DataTableRequest{
	private String phoneNumber;
	private String defaultBa;
	private String group;
	private String bill;
	private String invoiceNo;
	private String number;
	private String priceVat;
	private String newBa;
	private String newGroup;
	private String createdDate;
	private String totalBill;
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getDefaultBa() {
		return defaultBa;
	}
	public void setDefaultBa(String defaultBa) {
		this.defaultBa = defaultBa;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getBill() {
		return bill;
	}
	public void setBill(String bill) {
		this.bill = bill;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPriceVat() {
		return priceVat;
	}
	public void setPriceVat(String priceVat) {
		this.priceVat = priceVat;
	}
	public String getNewBa() {
		return newBa;
	}
	public void setNewBa(String newBa) {
		this.newBa = newBa;
	}
	public String getNewGroup() {
		return newGroup;
	}
	public void setNewGroup(String newGroup) {
		this.newGroup = newGroup;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getTotalBill() {
		return totalBill;
	}
	public void setTotalBill(String totalBill) {
		this.totalBill = totalBill;
	}
	
	
	
}
