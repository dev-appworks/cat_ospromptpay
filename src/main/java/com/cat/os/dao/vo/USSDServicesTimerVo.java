package com.cat.os.dao.vo;

import java.util.Date;

import com.cat.os.common.bean.DataTableRequest;

public class USSDServicesTimerVo extends DataTableRequest{

	private Integer id;
	private Integer servicesTimerId;
	private Integer serviceStatusId;
	private String serviceStatusCode;
	private String serviceName;
	private String email;
	private Date timer;
	private Integer status;
	private String updateBy;
	private String modBatchConfig;
	private String displayDate;
	private String displayHH;
	private String displayMM;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getServicesTimerId() {
		return servicesTimerId;
	}
	public void setServicesTimerId(Integer servicesTimerId) {
		this.servicesTimerId = servicesTimerId;
	}
	public Integer getServiceStatusId() {
		return serviceStatusId;
	}
	public void setServiceStatusId(Integer serviceStatusId) {
		this.serviceStatusId = serviceStatusId;
	}
	public String getServiceStatusCode() {
		return serviceStatusCode;
	}
	public void setServiceStatusCode(String serviceStatusCode) {
		this.serviceStatusCode = serviceStatusCode;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getTimer() {
		return timer;
	}
	public void setTimer(Date timer) {
		this.timer = timer;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getModBatchConfig() {
		return modBatchConfig;
	}
	public void setModBatchConfig(String modBatchConfig) {
		this.modBatchConfig = modBatchConfig;
	}
	public String getDisplayDate() {
		return displayDate;
	}
	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}
	public String getDisplayHH() {
		return displayHH;
	}
	public void setDisplayHH(String displayHH) {
		this.displayHH = displayHH;
	}
	public String getDisplayMM() {
		return displayMM;
	}
	public void setDisplayMM(String displayMM) {
		this.displayMM = displayMM;
	}
	
}
