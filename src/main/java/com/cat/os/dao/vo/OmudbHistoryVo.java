package com.cat.os.dao.vo;

import java.util.Date;

public class OmudbHistoryVo {
	
	private String orderId;
	private String orderSvcItem;
	private String remark;
	private String svcStatusId;
	private Date updateDate;
	private String username;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderSvcItem() {
		return orderSvcItem;
	}

	public void setOrderSvcItem(String orderSvcItem) {
		this.orderSvcItem = orderSvcItem;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSvcStatusId() {
		return svcStatusId;
	}

	public void setSvcStatusId(String svcStatusId) {
		this.svcStatusId = svcStatusId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
