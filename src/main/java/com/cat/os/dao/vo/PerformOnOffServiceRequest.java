package com.cat.os.dao.vo;

import java.util.Date;

public class PerformOnOffServiceRequest {
	
    private String functionName;
    private String requestId;
    private Date requestDatetime;
    private String requestAppId;
    private String userId;
    private String terminalId;
    private String authUserId;
    private String authPassword;
    private String serviceId;
    private int status;
    private Date Timer;
    
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public Date getRequestDatetime() {
		return requestDatetime;
	}
	public void setRequestDatetime(Date requestDatetime) {
		this.requestDatetime = requestDatetime;
	}
	public String getRequestAppId() {
		return requestAppId;
	}
	public void setRequestAppId(String requestAppId) {
		this.requestAppId = requestAppId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public String getAuthUserId() {
		return authUserId;
	}
	public void setAuthUserId(String authUserId) {
		this.authUserId = authUserId;
	}
	public String getAuthPassword() {
		return authPassword;
	}
	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getTimer() {
		return Timer;
	}
	public void setTimer(Date timer) {
		Timer = timer;
	}

}
