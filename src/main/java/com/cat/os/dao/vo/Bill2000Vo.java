package com.cat.os.dao.vo;

import java.util.Date;

import com.cat.os.common.bean.DataTableRequest;

public class Bill2000Vo extends DataTableRequest{
	
	private int ba;
	private int invoiceNo;
	private Date postDate;
	private Date payDate; 
	private int amtOriginal;
	private int amtProduct;
	private int diff;
	public int getBa() {
		return ba;
	}
	public void setBa(int ba) {
		this.ba = ba;
	}
	public int getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(int invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public Date getPostDate() {
		return postDate;
	}
	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
	public Date getPayDate() {
		return payDate;
	}
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	public int getAmtOriginal() {
		return amtOriginal;
	}
	public void setAmtOriginal(int amtOriginal) {
		this.amtOriginal = amtOriginal;
	}
	public int getAmtProduct() {
		return amtProduct;
	}
	public void setAmtProduct(int amtProduct) {
		this.amtProduct = amtProduct;
	}
	public int getDiff() {
		return diff;
	}
	public void setDiff(int diff) {
		this.diff = diff;
	}
	
	
	
}
