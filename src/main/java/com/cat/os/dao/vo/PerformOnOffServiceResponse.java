package com.cat.os.dao.vo;

import java.util.Date;

public class PerformOnOffServiceResponse {
	
	private String functionName;
	private String requestId;
    private String responseAppId;
    private String responseId;
    private Date responseDatetime;
    private String statusCode;
    private String errorCode;
    private String errorDesc;
    private String serviceId;
    private int status;
    
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getResponseAppId() {
		return responseAppId;
	}
	public void setResponseAppId(String responseAppId) {
		this.responseAppId = responseAppId;
	}
	public String getResponseId() {
		return responseId;
	}
	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}
	public Date getResponseDatetime() {
		return responseDatetime;
	}
	public void setResponseDatetime(Date responseDatetime) {
		this.responseDatetime = responseDatetime;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDesc() {
		return errorDesc;
	}
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
    
}
