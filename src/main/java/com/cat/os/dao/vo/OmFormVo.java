package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableSortRequest;

public class OmFormVo extends DataTableSortRequest {

	private String orderId;
	private String serviceItemId;
	private String statusCode;
	private String remark;
	private String changeStatusCode;
	private String statusMappingKeys;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getServiceItemId() {
		return serviceItemId;
	}

	public void setServiceItemId(String serviceItemId) {
		this.serviceItemId = serviceItemId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getChangeStatusCode() {
		return changeStatusCode;
	}

	public void setChangeStatusCode(String changeStatusCode) {
		this.changeStatusCode = changeStatusCode;
	}

	public String getStatusMappingKeys() {
		return statusMappingKeys;
	}

	public void setStatusMappingKeys(String statusMappingKeys) {
		this.statusMappingKeys = statusMappingKeys;
	}


}
