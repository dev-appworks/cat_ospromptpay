package com.cat.os.dao.vo;

import java.util.Date;

import com.cat.os.common.bean.DataTableRequest;

public class PromptPayConsentVo extends DataTableRequest{

	private Integer id;
	private String systemName;
	private String msisdn;
	private String cid;
	private String inpf01;
	private String referenceCode;
	private String status;
	private String description;
	private String updatedBy;
	private String updatedByName;
	private Date updatedDate;
	
	private String dateFrom;
	private String dateTo;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getInpf01() {
		return inpf01;
	}
	public void setInpf01(String inpf01) {
		this.inpf01 = inpf01;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedByName() {
		return updatedByName;
	}
	public void setUpdatedByName(String updatedByName) {
		this.updatedByName = updatedByName;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	@Override
	public String toString() {
		return "PromptpayConsentVo [id=" + id + ", systemName=" + systemName + ", msisdn=" + msisdn + ", cid=" + cid
				+ ", inpf01=" + inpf01 + ", referenceCode=" + referenceCode + ", status=" + status + ", description="
				+ description + ", updatedBy=" + updatedBy + ", updatedByName=" + updatedByName + ", updatedDate="
				+ updatedDate + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}
}