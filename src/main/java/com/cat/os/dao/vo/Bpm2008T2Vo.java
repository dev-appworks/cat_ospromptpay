package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bpm2008T2Vo extends DataTableRequest{

	private String accountNo;
	private String prevCutOffDate;
	private String prevBillDate;
	private String nextBillDate;
	
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getPrevCutOffDate() {
		return prevCutOffDate;
	}
	public void setPrevCutOffDate(String prevCutOffDate) {
		this.prevCutOffDate = prevCutOffDate;
	}
	public String getPrevBillDate() {
		return prevBillDate;
	}
	public void setPrevBillDate(String prevBillDate) {
		this.prevBillDate = prevBillDate;
	}
	public String getNextBillDate() {
		return nextBillDate;
	}
	public void setNextBillDate(String nextBillDate) {
		this.nextBillDate = nextBillDate;
	}
	
	
}
