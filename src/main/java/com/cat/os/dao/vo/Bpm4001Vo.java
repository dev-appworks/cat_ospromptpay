package com.cat.os.dao.vo;
 
import com.cat.os.common.bean.DataTableRequest;

public class Bpm4001Vo extends DataTableRequest{
	
	private String ba;
	private String externalIdType;
	private String subscriberNo;
	private String serviceDate;
	private String serviceEndDate;
	private String checkServiceType;
	private String checkServiceDate;
	private String checkNocus;
	private String checkSession;
	
	public String getBa() {
		return ba;
	}
	public void setBa(String ba) {
		this.ba = ba;
	}
	public String getExternalIdType() {
		return externalIdType;
	}
	public void setExternalIdType(String externalIdType) {
		this.externalIdType = externalIdType;
	}
	public String getSubscriberNo() {
		return subscriberNo;
	}
	public void setSubscriberNo(String subscriberNo) {
		this.subscriberNo = subscriberNo;
	}
	public String getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}
	public String getServiceEndDate() {
		return serviceEndDate;
	}
	public void setServiceEndDate(String serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}
	public String getCheckServiceType() {
		return checkServiceType;
	}
	public void setCheckServiceType(String checkServiceType) {
		this.checkServiceType = checkServiceType;
	}
	public String getCheckServiceDate() {
		return checkServiceDate;
	}
	public void setCheckServiceDate(String checkServiceDate) {
		this.checkServiceDate = checkServiceDate;
	}
	public String getCheckNocus() {
		return checkNocus;
	}
	public void setCheckNocus(String checkNocus) {
		this.checkNocus = checkNocus;
	}
	public String getCheckSession() {
		return checkSession;
	}
	public void setCheckSession(String checkSession) {
		this.checkSession = checkSession;
	}
	
}
