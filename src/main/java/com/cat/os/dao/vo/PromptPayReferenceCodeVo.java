package com.cat.os.dao.vo;

import java.util.Date;
import java.util.List;

import com.cat.os.common.bean.DataTableRequest;

public class PromptPayReferenceCodeVo extends DataTableRequest{

	private Integer id;
	private String sharedKey;
	private Date startDate;
	private Date endDate;
	private String status;
	private List<String> statusList;
	private Integer isDeleted;
	private String createdBy;
	private String createdByName;
	private Date createdDate;
	private String updatedBy;
	private String updatedByName;
	private Date updatedDate;

	private String dateFrom;
	private String dateTo;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSharedKey() {
		return sharedKey;
	}
	public void setSharedKey(String sharedKey) {
		this.sharedKey = sharedKey;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getStatusList() {
		return statusList;
	}
	public void setStatusList(List<String> statusList) {
		this.statusList = statusList;
	}
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedByName() {
		return updatedByName;
	}
	public void setUpdatedByName(String updatedByName) {
		this.updatedByName = updatedByName;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	@Override
	public String toString() {
		return "PromptpayReferenceCodeVo [id=" + id + ", sharedKey=" + sharedKey + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", status=" + status + ", statusList=" + statusList + ", isDeleted="
				+ isDeleted + ", createdBy=" + createdBy + ", createdByName=" + createdByName + ", createdDate="
				+ createdDate + ", updatedBy=" + updatedBy + ", updatedByName=" + updatedByName + ", updatedDate="
				+ updatedDate + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}
}