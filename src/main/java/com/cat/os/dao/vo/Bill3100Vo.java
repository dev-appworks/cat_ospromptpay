package com.cat.os.dao.vo;

import com.cat.os.common.bean.DataTableRequest;

public class Bill3100Vo extends DataTableRequest{
	
	private String contractNo;
	private String arRef;
	private String invoiceAmt;
	private String balanceAmt;
	private String productCode;
	private String subProductCode;
	private String revenueTypeCode;
	private String advanceAmt;
	private String revenueAmt;
	private String lgAmount;
	private String netAdvAmt;
	
	
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public String getArRef() {
		return arRef;
	}
	public void setArRef(String arRef) {
		this.arRef = arRef;
	}
	public String getInvoiceAmt() {
		return invoiceAmt;
	}
	public void setInvoiceAmt(String invoiceAmt) {
		this.invoiceAmt = invoiceAmt;
	}
	public String getBalanceAmt() {
		return balanceAmt;
	}
	public void setBalanceAmt(String balanceAmt) {
		this.balanceAmt = balanceAmt;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getSubProductCode() {
		return subProductCode;
	}
	public void setSubProductCode(String subProductCode) {
		this.subProductCode = subProductCode;
	}
	public String getRevenueTypeCode() {
		return revenueTypeCode;
	}
	public void setRevenueTypeCode(String revenueTypeCode) {
		this.revenueTypeCode = revenueTypeCode;
	}
	public String getAdvanceAmt() {
		return advanceAmt;
	}
	public void setAdvanceAmt(String advanceAmt) {
		this.advanceAmt = advanceAmt;
	}
	public String getRevenueAmt() {
		return revenueAmt;
	}
	public void setRevenueAmt(String revenueAmt) {
		this.revenueAmt = revenueAmt;
	}
	public String getLgAmount() {
		return lgAmount;
	}
	public void setLgAmount(String lgAmount) {
		this.lgAmount = lgAmount;
	}
	public String getNetAdvAmt() {
		return netAdvAmt;
	}
	public void setNetAdvAmt(String netAdvAmt) {
		this.netAdvAmt = netAdvAmt;
	}
	

}
