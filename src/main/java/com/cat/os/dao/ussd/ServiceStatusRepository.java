package com.cat.os.dao.ussd;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.ServiceStatus;

public interface ServiceStatusRepository extends CrudRepository<ServiceStatus, Integer>  {
	
	Page<ServiceStatus> findAllByIsDeleted(Pageable pageable, int isDeleted);
	
	List<ServiceStatus> findByTimerGreaterThanEqualAndTimerLessThanEqual(Date timer,Date timer2);
	
	List<ServiceStatus> findAllByIsDeleted(int isDeleted);
	
	ServiceStatus findOneByServiceIdAndIsDeleted(String serviceId,int isDeleted);
}
