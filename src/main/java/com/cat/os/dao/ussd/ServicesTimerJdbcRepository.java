package com.cat.os.dao.ussd;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.cat.os.dao.vo.USSDServicesTimerVo;
import com.cat.os.dao.vo.USSDVo;

@Repository
public class ServicesTimerJdbcRepository {
	private static final Logger log = LoggerFactory.getLogger(ServicesTimerJdbcRepository.class);
	
	@Autowired
	private JdbcTemplate ussdOracleJdbcTemplate;
	
	private final String servicesTimeBySerivceStatusId =  " SELECT st.id SERVICES_TIMER_ID, ss.id SERVICE_STATUS_ID, ss.service_name SERVICE_NAME, "
//			+ " ss.service_id SERVICE_STATUS_CODE, st.email EMAIL, st.timer TIMER, st.status STATUS, st.update_by UPDATE_BY "
			+ " ss.service_id SERVICE_STATUS_CODE, st.timer TIMER, st.status STATUS, st.update_by UPDATE_BY "
			+ " FROM services_timer st "
			+ " JOIN service_status ss "
			+ "	ON ss.id = st.service_Status_id ";
	
	private final String countServicesTimeBySerivceStatusId =  " SELECT count(1) "
			+ " FROM services_timer st "
			+ " JOIN service_status ss "
			+ "	ON ss.id = st.service_Status_id ";
	
	private final String deleteServiceTimerById =  " DELETE  FROM services_timer WHERE id = ? ";

	public List<USSDServicesTimerVo> getservicesTimeByServiceStatusId(USSDVo ussdVo) {
		List<Object> param = new ArrayList<Object>();
		int start = ussdVo.getStart();
		int length = ussdVo.getLength();
		
		StringBuilder sql = new StringBuilder(servicesTimeBySerivceStatusId);
		
		if(ussdVo.getId() != null) {
			sql = sql.append(" WHERE st.service_Status_id = ? ");
			param.add(ussdVo.getId());
		}

		sql = sql.append(" limit ?, ? ");
		param.add(start);
		int limitLength = start + length - (start > 0 ? 1 : 0 );
		param.add(limitLength);
		
		log.debug("SQL : " + sql);
		List<USSDServicesTimerVo> list = ussdOracleJdbcTemplate.query(sql.toString(), param.toArray() , servicesTimerTaskRowMapper);
		
		return list;
	}
	
	public Long getCountServicesTimeByTask(USSDVo ussdVo) {
		List<Object> param = new ArrayList<Object>();
		
		StringBuilder sql = new StringBuilder(countServicesTimeBySerivceStatusId);
		
		if(ussdVo.getId() != null) {
			sql = sql.append("  WHERE st.service_Status_id = ? ");
			param.add(ussdVo.getId());
		}
		
		log.debug("SQL : " + sql);

		return ussdOracleJdbcTemplate.queryForObject(sql.toString(), param.toArray() , Long.class);
	}
	
	public void deleteServicesTimerById(Integer id) {
		
		log.debug("SQL : " + deleteServiceTimerById);

		ussdOracleJdbcTemplate.update(deleteServiceTimerById, id);
	}
	
	private RowMapper<USSDServicesTimerVo> servicesTimerTaskRowMapper = new RowMapper<USSDServicesTimerVo>() {

		@Override
		public USSDServicesTimerVo mapRow(ResultSet rs, int arg1) throws SQLException {
			USSDServicesTimerVo vo = new USSDServicesTimerVo();
			
			vo.setServicesTimerId(rs.getInt("SERVICES_TIMER_ID"));
			vo.setServiceStatusId(rs.getInt("SERVICE_STATUS_ID"));
			vo.setServiceStatusCode(rs.getString("SERVICE_STATUS_CODE"));
			vo.setServiceName(rs.getString("SERVICE_NAME"));
//			vo.setEmail(rs.getString("EMAIL"));
			vo.setUpdateBy(rs.getString("UPDATE_BY"));
			vo.setStatus(rs.getInt("STATUS"));
			vo.setTimer(rs.getTimestamp("TIMER"));
			
			return vo;
		}
	};
	
}
