package com.cat.os.dao.ussd;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cat.os.dao.entity.ServicesTimer;

public interface ServicesTimerRepository  extends CrudRepository<ServicesTimer, Integer> {

	void deleteById(Integer id);
	
	List<ServicesTimer> findByTimerGreaterThanEqualAndTimerLessThanEqual(Date time1, Date time2);
	
}
