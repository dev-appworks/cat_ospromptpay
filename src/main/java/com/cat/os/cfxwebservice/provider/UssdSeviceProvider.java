package com.cat.os.cfxwebservice.provider;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cat.os.common.bean.AuthenticationException;
import com.cat.os.common.bean.BusinessException;
import com.cat.os.common.constant.UssdConstant;
import com.cat.os.common.utils.OsDateUtils;
import com.cat.os.common.utils.OsUtils;
import com.cat.os.dao.vo.PerformOnOffServiceRequest;
import com.cat.os.service.ApplicationCacheService;
import com.cat.os.service.AuditLogService;
import com.cat.os.service.ussd.UssdService;
import com.cat.ussd.performonoffservice.Error;
import com.cat.ussd.performonoffservice.PerformOnOffServicePortImpl;
import com.cat.ussd.performonoffservice.Rq;
import com.cat.ussd.performonoffservice.RqBody;
import com.cat.ussd.performonoffservice.RqHeader;
import com.cat.ussd.performonoffservice.Rs;
import com.cat.ussd.performonoffservice.RsBody;
import com.cat.ussd.performonoffservice.RsHeader;

public class UssdSeviceProvider extends PerformOnOffServicePortImpl {

	private static final Logger log = LoggerFactory.getLogger(UssdSeviceProvider.class);

	@Autowired
	private UssdService ussdService;
	
	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	private ApplicationCacheService applicationCacheService;

	@Override
	public Rs process(Rq rq) {
		Rs rs = new Rs();
		try {
			auditLogService.saveOsWsLog(rq, rs, null, null, UssdConstant.REQUEST);
			PerformOnOffServiceRequest request = mappingBean(rq, rs);
			
			ussdService.process(request);
			
			rs.getRsHeader().setStatusCode(UssdConstant.STATUS_CODE_00);
			rs.getRsHeader().getError().clear();
			
			auditLogService.saveOsWsLog(rq, rs, null, null, UssdConstant.RESPONSE);
		} catch (AuthenticationException e) {
			rs.getRsHeader().setStatusCode(UssdConstant.STATUS_CODE_30);
			setErrorMessage(rq, rs, e.getErrorCode(), applicationCacheService.getUssdErrorCode(e.getErrorCode()));
		} catch (BusinessException e) {
			rs.getRsHeader().setStatusCode(UssdConstant.STATUS_CODE_10);
			setErrorMessage(rq, rs, e.getErrorCode(), applicationCacheService.getUssdErrorCode(e.getErrorCode()));
		} catch (Exception e) {
			log.error("Error Process UssdSeviceProvider : ", e);
			rs.getRsHeader().setStatusCode(UssdConstant.STATUS_CODE_20);
			setErrorMessage(rq, rs, UssdConstant.E999_CODE, applicationCacheService.getUssdErrorCode(UssdConstant.E999_CODE));
		}

		return rs;
	}

	private PerformOnOffServiceRequest mappingBean(Rq rq, Rs rs) {
		PerformOnOffServiceRequest request = new PerformOnOffServiceRequest();
		RqHeader rqHeader = rq.getRqHeader();
		RqBody rqBody = rq.getRqBody();
		RsHeader rsHeader = new RsHeader();
		RsBody rsBody = new RsBody();
		
		request.setFunctionName(rqHeader.getFunctionName());
		request.setRequestId(rqHeader.getRequestId());
		request.setRequestDatetime(OsDateUtils.convertXMLGregorianCalendarToDate(rqHeader.getRequestDatetime()));
		request.setRequestAppId(rqHeader.getRequestAppId());
		request.setUserId(rqHeader.getUserId());
		request.setTerminalId(OsUtils.getValue(rqHeader.getTerminalId()));
		request.setAuthUserId(OsUtils.getValue(rqHeader.getAuthUserId()));
		request.setAuthPassword(OsUtils.getValue(rqHeader.getAuthPassword()));

		request.setServiceId(String.valueOf(rqBody.getServiceId()));
		request.setStatus(rqBody.getStatus());
		

		rsHeader.setFunctionName(rqHeader.getFunctionName());
		rsHeader.setRequestId(rqHeader.getRequestId());
		rsHeader.setResponseAppId(rqHeader.getRequestAppId());
		rsHeader.setResponseDatetime(OsDateUtils.convertDateToXMLGregorianCalendar(new Date()));
		rsHeader.setResponseId(rqHeader.getRequestId());
		rs.setRsHeader(rsHeader);
		
		rsBody.setServiceId(rqBody.getServiceId());
		rsBody.setStatus(rqBody.getStatus());
		rs.setRsBody(rsBody);

		return request;
	}
	
	private void setErrorMessage(Rq rq,Rs rs, String errorCode, String errorDesc) {
		Error error = new Error();
		error.setErrorCode(errorCode);
		error.setErrorDesc(errorDesc);
		rs.getRsHeader().getError().add(error);
		rs.setRsBody(null);
		
		auditLogService.saveOsWsLog(rq, rs, errorCode, errorDesc, UssdConstant.RESPONSE);
	}
}
