package com.cat.os.cfxwebservice.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cat.os.common.constant.PromptPayConstant;
import com.cat.os.dao.promptpay.PromptPayMappingRepository;
import com.cat.os.service.ussd.PromptPayService;
import com.cat.ussd.promptpay.ProcessUMBReqInput;
import com.cat.ussd.promptpay.ProcessUMBReqOutput;
import com.cat.ussd.promptpay.Soap_GatewayPortImpl;

public class PromptPayServiceProvider extends Soap_GatewayPortImpl {
	private static final Logger log = LoggerFactory.getLogger(PromptPayServiceProvider.class);
	
	@Autowired
	private PromptPayService promptPayService;
	
	@Autowired
	private PromptPayMappingRepository promptPayMappingRepository;
	
	@Override
	public ProcessUMBReqOutput processUMBReqArr(ProcessUMBReqInput processUMBReqInput) { 
		
		ProcessUMBReqOutput processUMBReqOutput = new ProcessUMBReqOutput();
		
		processUMBReqOutput = promptPayService.provider(processUMBReqInput);
		
//		String smsResponse = promptPayMappingRepository.findValue(PromptPayConstant.MAPPING.SMS_RESPONSE,PromptPayConstant.STATUS.ACTIVE, PromptPayConstant.IS_DELETED.ACTIVE);
//		
//		processUMBReqOutput.setMsgCode(PromptPayConstant.WS.WS_SUCCESS);
//		processUMBReqOutput.setMsgDesc(smsResponse);
		
        return processUMBReqOutput;
    }
	
//	@Override
//	public ProcessUMBReqOutput processUMBReqArr(ProcessUMBReqInput processUMBReqInput) { 
//		
//		ProcessUMBReqOutput processUMBReqOutput = new ProcessUMBReqOutput();
//		
//		Map<String, String> mapping = new HashMap<String, String>();
//		List<PromptpayMapping> mappingList = promptPayMappingRepository.findValue(PromptPayConstant.STATUS.ACTIVE, PromptPayConstant.IS_DELETED.ACTIVE);
//		for (PromptpayMapping promptpayMapping : mappingList) {
//			mapping.put(promptpayMapping.getName(), promptpayMapping.getValue());
//		}
//		
//		PromptpayConsent consent = null;
//		String refCode = "";
//		String smsMessage = "";
//		String sourceAddr = mapping.get(PromptPayConstant.MAPPING.SOURCE_ADDR);
//		String destAddr = "";
//	    String date = DateFormatUtils.format(Calendar.getInstance().getTime(), "yyyyMMdd");
//	    String msisdn = "";
//	    String idCard = "";
//	    String idCardMask = "";
//	    String smsSuccessMatch = mapping.get(PromptPayConstant.MAPPING.SMS_SUCCESS_MATCH);
//	    String smsSuccessNotMatch = mapping.get(PromptPayConstant.MAPPING.SMS_SUCCESS_NOT_MATCH);
//	    String smsFail = mapping.get(PromptPayConstant.MAPPING.SMS_FAIL);
//	    String markMsisdnFormat = mapping.get(PromptPayConstant.MAPPING.MASK_MOBILE_NO);
//	    String markIdnoFormat = mapping.get(PromptPayConstant.MAPPING.MASK_ID_CARD);
//	    
//	    processUMBReqOutput.setMsgCode(PromptPayConstant.WS.SUCCESS);
//	    
//		try {
//			String[] splitUSSD = processUMBReqInput.getInpF01().split("\\*");
//			idCard = splitUSSD[splitUSSD.length - 1].replace("#", "");
//			destAddr = processUMBReqInput.getMsisdn();
//			try {
//				
//				if(!CommonService.isNumbericDigit(destAddr, 11) || !CommonService.isNumbericDigit(idCard, 13)) {
//					processUMBReqOutput.setMsgDesc("หมายเลยโทรศัพท์ หรือ เลขบัตรประจำตัวประชาชน ไม่มีข้อมูลอยู่ในระบบ กรุณาติดต่อเจ้าหน้าที่");
//				} else {
//					
//					msisdn = CommonService.maskString(destAddr.replaceFirst("66", "0"), markMsisdnFormat);
//					idCardMask = CommonService.maskString(idCard, markIdnoFormat);
//					
//					smsSuccessNotMatch = smsSuccessNotMatch.replace("{date}", date);
//					smsSuccessNotMatch = smsSuccessNotMatch.replace("{id_card}", idCardMask);
//					smsSuccessNotMatch = smsSuccessNotMatch.replace("{mobile_no}", msisdn);
//					
//					if(!promptPayService.checkMobileInfo(destAddr.replaceFirst("66", ""), idCard)) {
//						
//						smsMessage = smsSuccessNotMatch;
//						processUMBReqOutput.setMsgDesc(smsSuccessNotMatch);
//					
//					} else {
//						refCode = promptPayService.generateRefernceCode(msisdn, idCard, date);
//						
//						if(StringUtils.isBlank(refCode)) {
//					
//							smsMessage = smsSuccessNotMatch;
//							processUMBReqOutput.setMsgDesc(smsMessage);
//							
//						} else {
//							
//							smsMessage = smsSuccessMatch;
//							smsMessage = smsMessage.replace("{reference_code}", refCode);
//							smsMessage = smsMessage.replace("{date}", date);
//							smsMessage = smsMessage.replace("{id_card}", idCardMask);
//							smsMessage = smsMessage.replace("{mobile_no}", msisdn);
//							
//							processUMBReqOutput.setMsgDesc(smsMessage);
//						}	
//					}
//				}
//				
//			} catch (Exception e) {
//				smsMessage = smsFail;
////				processUMBReqOutput.setMsgDesc(smsMessage + " : " + e.getMessage());
//				processUMBReqOutput.setMsgDesc(smsMessage);
//				processUMBReqOutput.setOutF01(e.getMessage());
//				e.printStackTrace();
//				log.error("PromptPayServiceProvider >> " + e.getMessage());
//			}
//			
//			try {
//				smsService.sendSMS(sourceAddr, destAddr, smsMessage);
//			} catch (Exception e) {
//				processUMBReqOutput.setMsgCode(PromptPayConstant.WS.ERROR);
//				processUMBReqOutput.setMsgDesc(e.getMessage());
//				e.printStackTrace();
//				log.error("PromptPayServiceProvider SMS >> " + e.getMessage());
//			}
//
//			consent = new PromptpayConsent();
//			consent.setSystemName(processUMBReqInput.getCpurl());
//			consent.setMsisdn(processUMBReqInput.getMsisdn());
//			consent.setCid(idCard);
//			consent.setInpf01(processUMBReqInput.getInpF01());
//			consent.setReferenceCode(refCode);
//			consent.setStatus(processUMBReqOutput.getMsgCode());
//			
//			
//			if(StringUtils.isBlank(processUMBReqOutput.getOutF01()))
//				consent.setDescription(processUMBReqOutput.getMsgDesc());
//			else
//				consent.setDescription(processUMBReqOutput.getMsgDesc() + " >>> " + processUMBReqOutput.getOutF01());
//			
//			consent.setUpdatedBy("SYSTEM");
//			consent.setUpdatedByName("SYSTEM");
//			consent.setUpdatedDate(new Date());
//			consent = promptPayService.save(consent);
//			
//		} catch(Exception e) {
//			processUMBReqOutput.setMsgCode(PromptPayConstant.WS.ERROR);
//			processUMBReqOutput.setMsgDesc(e.getMessage());
//			e.printStackTrace();
//			log.error("PromptPayServiceProvider >> " + e.getMessage());
//		}
//		
//        return processUMBReqOutput;
//    }
	
//	@Override
//	public ProcessUMBReqOutput processUMBReqArr(ProcessUMBReqInput processUMBReqInput) { 
//		
//		ProcessUMBReqOutput processUMBReqOutput = new ProcessUMBReqOutput();
//		
//		Map<String, String> mapping = new HashMap<String, String>();
//		List<PromptpayMapping> mappingList = promptPayMappingRepository.findValue(PromptPayConstant.STATUS.ACTIVE, PromptPayConstant.IS_DELETED.ACTIVE);
//		for (PromptpayMapping promptpayMapping : mappingList) {
//			mapping.put(promptpayMapping.getName(), promptpayMapping.getValue());
//		}
//		
//		PromptpayConsent consent = null;
//		String refCode = "";
//		String smsMessage = "";
//		String sourceAddr = mapping.get(PromptPayConstant.MAPPING.SOURCE_ADDR);
//		String destAddr = "";
//	    String date = DateFormatUtils.format(Calendar.getInstance().getTime(), "yyyyMMdd");
//	    String msisdn = "";
//	    String idCard = "";
//	    String smsSuccessMatch = mapping.get(PromptPayConstant.MAPPING.SMS_SUCCESS_MATCH);
//	    String smsSuccessNotMatch = mapping.get(PromptPayConstant.MAPPING.SMS_SUCCESS_NOT_MATCH);
//	    String smsFail = mapping.get(PromptPayConstant.MAPPING.SMS_FAIL);
//	    String markMsisdnFormat = mapping.get(PromptPayConstant.MAPPING.MASK_MOBILE_NO);
//	    String markIdnoFormat = mapping.get(PromptPayConstant.MAPPING.MASK_ID_CARD);
//	    
//	    processUMBReqOutput.setMsgCode(PromptPayConstant.WS.SUCCESS);
//	    
//		try {
//
//			try {
//				
//				if(!CommonService.isNumbericDigit(processUMBReqInput.getMsisdn(), 11) || 
//				   !CommonService.isNumbericDigit(processUMBReqInput.getTranxId(), 13)) {
//					
//					processUMBReqOutput.setMsgDesc("หมายเลยโทรศัพท์ หรือ เลขบัตรประจำตัวประชาชน ไม่มีข้อมูลอยู่ในระบบ กรุณาติดต่อเจ้าหน้าที่");
//					
//				} else {
//					
//					destAddr = processUMBReqInput.getMsisdn();
//					msisdn = CommonService.maskString(destAddr.replaceFirst("66", "0"), markMsisdnFormat);
//					idCard = CommonService.maskString(processUMBReqInput.getTranxId(), markIdnoFormat);
//					
//					smsSuccessNotMatch = smsSuccessNotMatch.replace("{date}", date);
//					smsSuccessNotMatch = smsSuccessNotMatch.replace("{id_card}", idCard);
//					smsSuccessNotMatch = smsSuccessNotMatch.replace("{mobile_no}", msisdn);
//					
//					if(!promptPayService.checkMobileInfo(processUMBReqInput.getMsisdn().replaceFirst("66", ""), processUMBReqInput.getTranxId())) {
//						
//						smsMessage = smsSuccessNotMatch;
//						processUMBReqOutput.setMsgDesc(smsSuccessNotMatch);
//					
//					} else {
//						refCode = promptPayService.generateRefernceCode(processUMBReqInput.getMsisdn(), processUMBReqInput.getTranxId(), date);
//						
//						if(StringUtils.isBlank(refCode)) {
//					
//							smsMessage = smsSuccessNotMatch;
//							processUMBReqOutput.setMsgDesc(smsMessage);
//							
//						} else {
//							
//							smsMessage = smsSuccessMatch;
//							smsMessage = smsMessage.replace("{reference_code}", refCode);
//							smsMessage = smsMessage.replace("{date}", date);
//							smsMessage = smsMessage.replace("{id_card}", idCard);
//							smsMessage = smsMessage.replace("{mobile_no}", msisdn);
//							
//							processUMBReqOutput.setMsgDesc(smsMessage);
//						}	
//					}
//				}
//				
//			} catch (Exception e) {
//				smsMessage = smsFail;
//				processUMBReqOutput.setMsgDesc(smsMessage + " : " + e.getMessage());
//			}
//			
////			smsService.sendSMS(sourceAddr, destAddr, smsMessage);
//
//			consent = new PromptpayConsent();
//			consent.setSystemName(processUMBReqInput.getCpurl());
//			consent.setMsisdn(processUMBReqInput.getMsisdn());
//			consent.setCid(processUMBReqInput.getTranxId());
//			consent.setInpf01(processUMBReqInput.getInpF01());
//			consent.setReferenceCode(refCode);
//			consent.setStatus(processUMBReqOutput.getMsgCode());
//			consent.setDescription(processUMBReqOutput.getMsgDesc());
//			consent.setUpdatedBy("SYSTEM");
//			consent.setUpdatedByName("SYSTEM");
//			consent.setUpdatedDate(new Date());
//			consent = promptPayService.save(consent);
//			
//		} catch(Exception e) {
//			processUMBReqOutput.setMsgCode(PromptPayConstant.WS.ERROR);
//			processUMBReqOutput.setMsgDesc(e.getMessage());
//			e.printStackTrace();
//			log.error("PromptPayServiceProvider >> " + e.getMessage());
//		}
//		
//        return processUMBReqOutput;
//    }
}