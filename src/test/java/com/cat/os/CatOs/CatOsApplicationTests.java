package com.cat.os.CatOs;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.cat.os.cfxwebservice.consumer.GobalWeatherConsumer;
import com.cat.os.service.AuditLogService;
import com.cat.os.service.BatchService;
import com.cat.os.service.ussd.UssdService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CatOsApplicationTests {

	@Autowired
	private GobalWeatherConsumer gobalWeatherConsumer;
	
	@Autowired
	private BatchService batchService;
	
	@Autowired
	private UssdService ussdService;
	
	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private SpringTemplateEngine emailTemplateEngine;
//	
//	@Test
//	public void contextLoads() {
////		messageService.getAll();
////		messageService.getArm();
////		batchService.run();
//		gobalWeatherConsumer.test();
//	}

	@Test
	public void testSaveUssdService() {
		StringBuilder email = new StringBuilder();
		email.append(" <!DOCTYPE html>                                                                 ");
		email.append(" <html xmlns:th=\"http://www.thymeleaf.org\">                                    ");
		email.append("   <head>                                                                        ");
		email.append("     <title th:remove=\"all\">Template for HTML email with inline image</title>  ");
		email.append("     <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />   ");
		email.append("   </head>                                                                       ");
		email.append("   <body>                                                                        ");
		email.append("     <p th:text=\"${name}\">                                                     ");
		email.append("       Hello, Peter Static!                                                      ");
		email.append("     </p>                                                                        ");
		email.append("     <p>                                                                         ");
		email.append("       Regards, <br />                                                           ");
		email.append("       <em>The Thymeleaf Team</em>                                               ");
		email.append("     </p>                                                                        ");
		email.append("   </body>                                                                       ");
		email.append(" </html>                                                                         ");
		
		
		final Context ctx = new Context(Locale.US);
	    ctx.setVariable("name", "Testxxxxxxxxxxxx");
		String ss = emailTemplateEngine.process(email.toString(), ctx);
		System.out.println(ss);
	}
//	
//	@Test
//	public void testSaveAuditLog() {
//		AuditLog auditLog = new AuditLog();
//		auditLog.setLogBy("test");
//		auditLog.setLogDatetime(new Date());
//		auditLog.setServiceId("1111");
//		auditLog.setServiceName("SName");
//		auditLog.setStatus("0");
//		auditLog.setTimer(new Date());
//		auditLog.setType("Test T");
//		auditLog.setUpdatedBy("Test USer");
//		auditLog.setUpdatedDatetime(new Date());
//		auditLogService.save(auditLog);
//	}
}
