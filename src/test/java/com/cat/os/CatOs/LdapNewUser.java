package com.cat.os.CatOs;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.DigestUtils;

public class LdapNewUser {
	
	 Hashtable<String, String> env = new Hashtable<String, String>();

	 LdapNewUser (){
	      env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	      env.put(Context.PROVIDER_URL, "ldap://localhost:10389/dc=example,dc=com");
	      env.put(Context.SECURITY_PRINCIPAL, "uid=admin,ou=system");
	      env.put(Context.SECURITY_CREDENTIALS, "secret");
	}

	public void create() throws Exception {
		   	 
		
		      DirContext ctx;
		      
		      try {
		         ctx = new InitialDirContext(env);
		      } catch (NamingException e) {
		         throw new RuntimeException(e);
		      }
		      

/*		      inetOrgPerson (structural)
		      organizationalPerson (structural)
		      person (structural)
		      posixAccount (auxiliary)
		      top (abstract)
		      
		      */
		   // Create attributes to be associated with the object
		   Attributes attrs = new BasicAttributes(true); // case-ignore
		   Attribute objclass = new BasicAttribute("objectclass");
		   objclass.add("inetOrgPerson");
		   objclass.add("organizationalPerson");
		   objclass.add("person");
		   objclass.add("posixAccount");
		   objclass.add("top");
		   attrs.put(objclass);
		   
		   Attribute attr = null;
		   
		   attr = new BasicAttribute("cn");
		   attr.add("arm arm");
		   attrs.put(attr);
		   
		   attr = new BasicAttribute("gidNumber");
		   attr.add("501");
		   attrs.put(attr);

		   attr = new BasicAttribute("homeDirectory");
		   attr.add("/home/users/arm2018");
		   attrs.put(attr);
		   
		   attr = new BasicAttribute("sn");
		   attr.add("arm");
		   attrs.put(attr);

		   attr = new BasicAttribute("uid");
		   attr.add("arm2019");
		   attrs.put(attr);

		   attr = new BasicAttribute("uidNumber");
		   attr.add("8888");
		   attrs.put(attr);

		   attr = new BasicAttribute("userPassword");
		   String pwdStr = LdapNewUser.encryptMD5Password("1234");
		   System.out.println("PWD : "  + pwdStr );
		   attr.add(pwdStr);
		   attrs.put(attr);
		   
		   LdapContext users = (LdapContext) ctx.lookup("ou=users,ou=om_udb");

		   
		   // Perform bind
		   users.bind("cn=arm arm", null, attrs);
		   
		   System.out.println("success...");
	}
	
	public static String encryptMD5Password(String pwdstr) {
		  
		  String pwdStr  = null;
		try {
			byte[] pwd = DigestUtils.md5Digest(pwdstr.getBytes("UTF8"));
			pwdStr = "{MD5}"+ Base64.encodeBase64String(pwd);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		  return pwdStr;
	}
	
	public void addGroup() throws Exception {
		   DirContext ctx;
		      
		      try {
		         ctx = new InitialDirContext(env);
		      } catch (NamingException e) {
		         throw new RuntimeException(e);
		      }
		      
		      Attributes matchAttrs = new BasicAttributes(true); 
		      matchAttrs.put(new BasicAttribute("objectClass", "groupOfNames"));
		      
		      NamingEnumeration<SearchResult> answer = ctx.search("ou=groups,ou=om_udb", matchAttrs);

		      while (answer.hasMore()) {
		    	    SearchResult sr = answer.next();
		    	    System.out.println(">>>" + sr.getName());
		    	    System.out.println(">>>" + sr.getNameInNamespace());
		    	    
//		    	    member: cn=อาม อาม,ou=users,ou=om_udb,dc=example,dc=com
		    	    ModificationItem[] mods = new ModificationItem[1];
//		    	    mods[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("member", "cn=arm arm,ou=users,ou=om_udb,dc=example,dc=com"));
//		    	    mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("member", "cn=arm arm,ou=users,ou=om_udb,dc=example,dc=com"));
		    	    String nameOfGroup = sr.getName()+",ou=groups,ou=om_udb";
		    	    
//		    	    ctx.modifyAttributes(nameOfGroup, mods);
		    	   
		      }
		      
		      System.out.println(answer);
		
		
	}
	
	public void changePassword()  throws Exception {
		 DirContext ctx;
	      
	      try {
	         ctx = new InitialDirContext(env);
	      } catch (NamingException e) {
	         throw new RuntimeException(e);
	      }
	      
	      Attributes matchAttrs = new BasicAttributes(true); 

	      matchAttrs.put(new BasicAttribute("cn", "arm arm"));
	      
	      NamingEnumeration<SearchResult> answer = ctx.search("ou=users,ou=om_udb", matchAttrs);
	      while (answer.hasMore()) {
	    	    SearchResult sr = answer.next();
	    	    System.out.println(">>>" + sr.getName());
	    	    System.out.println(">>>" + sr.getNameInNamespace());
	    	    Attributes arrt = sr.getAttributes();
	    	    Attribute upwd = arrt.get("userpassword");
	    	    
	    	    
	    	    ModificationItem[] mods = new ModificationItem[1];
	    	    mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userpassword", LdapNewUser.encryptMD5Password("123456")));
	    	    String nameOfGroup = sr.getName()+",ou=users,ou=om_udb";
	    	    ctx.modifyAttributes(nameOfGroup , mods);
	    	    System.out.println("change pwd success...");
	      }
	}
	
	
	public void findGroup() throws NamingException {
		 DirContext ctx;
	      
	      try {
	         ctx = new InitialDirContext(env);
	      } catch (NamingException e) {
	         throw new RuntimeException(e);
	      }
	      
	      Attributes matchAttrs = new BasicAttributes(true); 
	      matchAttrs.put(new BasicAttribute("objectClass", "groupOfNames"));
	      
	      NamingEnumeration<SearchResult> answer = ctx.search("ou=groups,ou=om_udb", matchAttrs);
	      while (answer.hasMore()) {
	    	    SearchResult sr = answer.next();
	    	    Attributes arrt = sr.getAttributes();
	    	    Attribute cn = arrt.get("cn");
	    	    System.out.println("cn >>> " + cn.get());
	    	    System.out.println("getName >>> " + sr.getName());
	    	    System.out.println("getNameInNamespace >>> " + sr.getNameInNamespace()); 
	    	    System.out.println("--------------------------------"); 

	      }
	      
//	} catch (NameNotFoundException e) {
//		// The base context was not found.
//		// Just clean up and exit.
//		throw new Exception(e.getMessage());
//	} catch (NamingException e) {
//		throw new Exception(e.getMessage());
//	} finally {
//		if (answer != null) {
//			try {
//				answer.close();
//			} catch (Exception e) {
//				// Never mind this.
//			}
//		}
//		if (ctx != null) {
//			try {
//				ctx.close();
//			} catch (Exception e) {
//				// Never mind this.
//			}
//		}
//	}
		
	}
	
	public static void main(String[] args) throws Exception {
		new LdapNewUser().findGroup();
	}
}
