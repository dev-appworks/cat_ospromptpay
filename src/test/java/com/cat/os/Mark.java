package com.cat.os;

import java.text.ParseException;
import java.util.Calendar;

import javax.swing.text.MaskFormatter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import com.cat.os.common.constant.PromptPayConstant;

public class Mark {

	public static void main(String[] args) throws ParseException {

		Calendar calendar = Calendar.getInstance();
		
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		System.out.println(DateFormatUtils.format(calendar.getTime(), "dd/MM/yyyy HH:mm:ss"));
		
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		System.out.println(DateFormatUtils.format(calendar.getTime(), "dd/MM/yyyy HH:mm:ss"));
		
//		if(!refCodeVoBefore.getStartDate().equals(dateFrom) && !refCodeVoBefore.getEndDate().equals(dateTo))
//			System.out.println(!refCodeVoBefore.getStartDate().equals(dateFrom) && !refCodeVoBefore.getEndDate().equals(dateTo));
//		else 
//			System.out.println();
//		String value = "0850192297";
//		String format = "XX-XX##-####";
//		System.out.println("MSISDN > " + maskString(value, format));
//
//		System.out.println();
//		
//		value = "1101401947376";
//	    format = "#-####-XXXXX-##-#";
//		System.out.println("ID Card > " + maskString(value, format));
	}
	
	public static String maskString(String text, String maskFormat) throws ParseException {
		
		Integer start = 0;
		Integer end = 0;
		maskFormat = maskFormat.replace("X", "?");
		
		MaskFormatter mf = new MaskFormatter(maskFormat);	    
	    mf.setValueContainsLiteralCharacters(false);
	    
	    start += maskFormat.replace("-", "").indexOf("?");
	    end += maskFormat.replace("-", "").lastIndexOf("?") + 1;
//	    System.out.println("Start > " + start);
//	    System.out.println("End > " + end);
		if(StringUtils.isBlank(text))
			return null;
		
		text = StringUtils.overlay(text, StringUtils.repeat("X", StringUtils.countMatches(maskFormat, "?")), start, end);
//		System.out.println("TEXT > " + text);
		text = mf.valueToString(text);
		
		return text;
	}	
}