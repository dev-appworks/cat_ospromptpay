package com.cat.os;

import java.util.Date;

public class Test {

	public static void main(String[] args) {
		Date dateFrom = new Date("20/02/2018 15:02");
		Date dateTo = new Date("20/04/2018 15:02");
		
		Date dateStart = null;
		Date dateEnd = null;
		
		dateStart = new Date("03/01/2018 12:00:01 AM");
		dateEnd = new Date("03/31/2018 11:59:59 PM");
		System.out.println("dateEnd >> " + dateStart);
		System.out.println("dateEnd >> " + dateEnd);
		
		
		dateStart = new Date("04/05/2018 12:00:01 AM");
		dateEnd = new Date("04/30/2018 11:59:59 PM");
		System.out.println("dateEnd >> " + dateStart);
		System.out.println("dateEnd >> " + dateEnd);
		
		
		dateStart = new Date("05/01/2018 3:59:00 PM");
		dateEnd = new Date("05/31/2018 3:59:00 PM");
		System.out.println("dateEnd >> " + dateStart);
		System.out.println("dateEnd >> " + dateEnd);
	}
}

//SELECT 
//ROWNUM R, PP.PROMPTPAY_REFERENCE_CODE_ID, PP.SHARED_KEY, 
//          PP.START_DATE, PP.END_DATE, PP.STATUS, PP.IS_DELETED, 
//          PP.CREATED_BY, PP.CREATED_BYNAME, PP.CREATED_DATE, 
//          PP.UPDATED_BY, PP.UPDATED_BYNAME, PP.UPDATED_DATE 
//FROM 
//PROMPTPAY_REFERENCE_CODE PP 
//WHERE 
//PP.IS_DELETED = 0 AND 
//(PP.START_DATE BETWEEN TO_DATE ( '20/01/2018 15:02:00' , 'dd/mm/yyyy hh24:mi:ss') AND TO_DATE ( '30/04/2018 15:02' , 'dd/mm/yyyy hh24:mi:ss') OR
//PP.END_DATE BETWEEN TO_DATE ( '20/01/2018 15:02:00' , 'dd/mm/yyyy hh24:mi:ss') AND TO_DATE ( '30/04/2018 15:02' , 'dd/mm/yyyy hh24:mi:ss'))
//
//
//SELECT 
//ROWNUM R, PP.PROMPTPAY_REFERENCE_CODE_ID, PP.SHARED_KEY, 
//          PP.START_DATE, PP.END_DATE, PP.STATUS, PP.IS_DELETED, 
//          PP.CREATED_BY, PP.CREATED_BYNAME, PP.CREATED_DATE, 
//          PP.UPDATED_BY, PP.UPDATED_BYNAME, PP.UPDATED_DATE 
//FROM 
//PROMPTPAY_REFERENCE_CODE PP --01/01/2017 00:00:01
//WHERE 
//PP.IS_DELETED = 0 AND 
//(TO_DATE ( '20/03/2018 15:02' , 'dd/mm/yyyy hh24:mi:ss') BETWEEN PP.START_DATE AND PP.END_DATE OR
//TO_DATE ( '20/04/2018 15:02' , 'dd/mm/yyyy hh24:mi:ss') BETWEEN PP.START_DATE AND PP.END_DATE)
//
//SELECT 
//ROWNUM R, PP.PROMPTPAY_REFERENCE_CODE_ID, PP.SHARED_KEY, 
//          PP.START_DATE, PP.END_DATE, PP.STATUS, PP.IS_DELETED, 
//          PP.CREATED_BY, PP.CREATED_BYNAME, PP.CREATED_DATE, 
//          PP.UPDATED_BY, PP.UPDATED_BYNAME, PP.UPDATED_DATE 
//FROM 
//PROMPTPAY_REFERENCE_CODE PP --01/01/2017 00:00:01
//WHERE 
//PP.IS_DELETED = 0 AND 
//PP.START_DATE >= TO_DATE ( '20/01/2018 15:02:00' , 'dd/mm/yyyy hh24:mi:ss') AND 
//PP.END_DATE <= TO_DATE ( '30/04/2018 15:02:00' , 'dd/mm/yyyy hh24:mi:ss')