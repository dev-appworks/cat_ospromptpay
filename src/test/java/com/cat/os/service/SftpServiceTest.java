package com.cat.os.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SftpServiceTest {
	
	@Autowired
	private SftpService sftpService;
	
	@Test
	public void test_downloadFile() {
		String path = "/GenFile_20171101";
		String filename = "Kenan_Payment_Summary_20171101.csv";
		
		byte[] downloadFile = sftpService.downloadFile(path, filename);
		System.out.println(downloadFile);
	}
	
}
