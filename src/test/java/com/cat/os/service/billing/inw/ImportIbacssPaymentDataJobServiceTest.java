package com.cat.os.service.billing.inw;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cat.os.dao.entity.IbacssPaymentData;
import com.cat.os.dao.entity.IbacssPaymentDataAccountNewInv;
import com.cat.os.dao.entity.IbacssPaymentDataAccountPay;
import com.cat.os.dao.entity.IbacssPaymentDataTaxInvSummaryWt;
import com.cat.os.service.billing.inw.ImportIbacssPaymentDataJobService.DataType;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImportIbacssPaymentDataJobServiceTest {
	
	@Autowired
	@Qualifier("importIbacssPaymentDataJobService")
	private ImportIbacssPaymentDataJobService service;
	
	//@Test
	public void test_runJob_Manual() {
		List<String> lineList = null;
		File file = null;
		InputStream is = null;
		try {
			file = new File("src/test/resources/ibacss/payment/data/GenFile_WEBBASE/GenFile_20171101/Kenan_Payment_Summary_20171101.csv");
			is = new FileInputStream(file);
			lineList = IOUtils.readLines(is, "TIS-620");
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(is);
		}
		
		Map<DataType, Object> dataMap = service.processCsvData(file.getName(), lineList);
		List<IbacssPaymentData> paymentDataList = (List<IbacssPaymentData>) dataMap.get(DataType.PAYMENT_DATA);
		List<IbacssPaymentDataAccountPay> accountPayList = (List<IbacssPaymentDataAccountPay>) dataMap.get(DataType.ACCOUNT_PAY);
		List<IbacssPaymentDataAccountNewInv> accountNewInvList = (List<IbacssPaymentDataAccountNewInv>) dataMap.get(DataType.ACCOUNT_NEW_INV);
		List<IbacssPaymentDataTaxInvSummaryWt> taxInvSummaryWtList = (List<IbacssPaymentDataTaxInvSummaryWt>) dataMap.get(DataType.TAX_INV_SUMMARY_WT);
		
		// Debugging Data
		for (IbacssPaymentData paymentData : paymentDataList) {
			System.out.println(ToStringBuilder.reflectionToString(paymentData, ToStringStyle.JSON_STYLE));
		}
		System.out.println("- - - - - - - - - -");
		for (IbacssPaymentDataAccountPay accountPay : accountPayList) {
			System.out.println(ToStringBuilder.reflectionToString(accountPay, ToStringStyle.JSON_STYLE));
		}
		System.out.println("- - - - - - - - - -");
		for (IbacssPaymentDataAccountNewInv accountNewInv : accountNewInvList) {
			System.out.println(ToStringBuilder.reflectionToString(accountNewInv, ToStringStyle.JSON_STYLE));
		}
		System.out.println("- - - - - - - - - -");
		for (IbacssPaymentDataTaxInvSummaryWt taxInvSummaryWt : taxInvSummaryWtList) {
			System.out.println(ToStringBuilder.reflectionToString(taxInvSummaryWt, ToStringStyle.JSON_STYLE));
		}
		
		try {
			service.insertData(dataMap);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_runJob() {
		String txDate = "20171102";
		service.runJob(txDate);
	}
	
}
